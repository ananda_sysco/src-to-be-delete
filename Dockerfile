FROM 230930891673.dkr.ecr.us-east-1.amazonaws.com/hardened-node:8.9.4
MAINTAINER tharukaj

# Creating app directory
WORKDIR /usr/src/app

# Copying application server source and build files
COPY ./node_modules ./node_modules
COPY ./build ./build
COPY ./src/server ./src/server

# Adding Supervisor config
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Creating supervisord.pid and env-config.js files and granting appuser the write permissions
USER root
RUN touch supervisord.pid ./build/env-config.js
RUN chmod 646 supervisord.pid ./build/env-config.js ./build/index.html
USER appuser

ENV APP_ID 'olo-v3'

EXPOSE 3003
EXPOSE 12201
CMD ["/usr/bin/supervisord"]

#CMD node ./dist/App.js --env=$APP_ENVIRONMENT --conf-api=$CONFIG_API_HOST --conf-api-key=$CONFIG_API_BASIC_AUTH_KEY --app-id=$APP_ID
