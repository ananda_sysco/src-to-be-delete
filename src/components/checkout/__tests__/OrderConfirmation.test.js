import React from 'react';
import {shallow} from 'enzyme';
import {ThemeProvider} from 'styled-components';
import theme from 'lib/ui/themes/cake';
import { OrderConfirmation } from './../OrderConfirmation';
import configureStore from 'redux-mock-store';
import * as restaurantTestData from 'fixtures/restaurant';

const initialState = restaurantTestData;
const mockStore = configureStore();

describe('src/components/checkout/OrderConfirmation',()=>{
  it('should render correctly',()=>{
    const store = mockStore(initialState);
    const setRouteLeaveHook = jest.fn();
    const snapshot = shallow(<ThemeProvider theme={theme}>
      <OrderConfirmation.wrappedComponent params={{router:setRouteLeaveHook}} store={store}/>
    </ThemeProvider>);
    expect(snapshot).toMatchSnapshot();
  });

  it('should render correctly when sign up is requested',()=>{

    const store = mockStore(Object.assign({},initialState, {user: {isSignUpCompleted: false,
      isSignUpRequested: true}}));
    const setRouteLeaveHook = jest.fn();
    const snapshot = shallow(<ThemeProvider theme={theme}>
      <OrderConfirmation.wrappedComponent params={{router:setRouteLeaveHook}} store={store}/>
    </ThemeProvider>);
    expect(snapshot).toMatchSnapshot();
  })

});
