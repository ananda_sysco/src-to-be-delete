/**
 * Component developed by Achintha, integrated by Salinda
 * 2018 01 29
 *
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as AppPropTypes from 'utils/proptypes';
import {Label, Select, themed} from 'lib/ui';
import * as constants from '../../lib/constants';
import {setSelectedState} from 'store/user';
import {checkNullOrUndefinedOrEmptyString} from '../../utils/commonUtil'

const StyledSelect = styled(Select)`
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const ErrorLabel = styled(Label)`
  color: ${themed('colors.error')};
  margin-top: ${themed('layout.gutter.xxsmall')}rem;
`;

const getClassName = (error, focused) => {
  if (error && focused) {
    return "selectFocusedError";
  } else if (focused) {
    return "selectFocused";
  } else if (error) {
    return "selectError";
  } else {
    return "";
  }
};

export class AreaSelector extends Component {

  static propTypes = {
    cart: AppPropTypes.cart.isRequired,
    setSelectedState: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      error: false,
      focused: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedState && checkNullOrUndefinedOrEmptyString(nextProps.value)) {
      this.setState({error: true});
    } else {
      this.setState({error: false});
    }
  }


  handleOnChange = ({value}) => {
    this.props.onChange(value);
    this.props.setSelectedState(value);
    if (checkNullOrUndefinedOrEmptyString(value)) {
      this.setState({error: true});
    } else {
      this.setState({error: false});
    }
  };

  handleBlur = () => {
    if (checkNullOrUndefinedOrEmptyString(this.props.selectedState)) {
      this.setState({error: true, focused: false});
    } else {
      this.setState({error: false, focused: false});
    }
  };
  onFocusHandler = () => {
    this.setState({focused: true});
  };

  render() {
    const {disabled, placeHolder, selectedState, error} = this.props;
    const message = Array.isArray(error) ? error.length && error[0] : error;

    return (
      <div>
        <StyledSelect
          placeholder={placeHolder}
          clearable={false}
          searchable={false}
          options={constants.US_STATES.map(state => {
            return {value: state, label: state}
          })}
          value={selectedState}
          disabled={disabled}
          onChange={this.handleOnChange}
          onBlur={this.handleBlur}
          onFocus={this.onFocusHandler}
          className={getClassName(this.state.error || error, this.state.focused)}
        />
        {message && <ErrorLabel xsmall>{message}</ErrorLabel>}
        {(!message && this.state.error) && <ErrorLabel xsmall>{'State is required'}</ErrorLabel>}
      </div>
    );
  }
}

function mapState(state) {
  let selectedState = state.user.selectedState;
  const currentUser = state.user.currentUser;
  if (!selectedState && currentUser && currentUser.contacts && currentUser.contacts.length > 0 && currentUser.contacts[0]['state']) {
    selectedState = currentUser.contacts[0]['state'];
  }
  return {
    selectedState
  };
}

const actions = {setSelectedState};

export default connect(mapState, actions)(AreaSelector);
