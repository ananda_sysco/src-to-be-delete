import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import * as AppPropTypes from 'utils/proptypes';
import {BackIcon, Button, Heading, Label, Paragraph, themed} from 'lib/ui';
import {invalidateOrder} from 'store/cart';
import Link from 'components/app/router/Link';
import MdChevronLeft from 'react-icons/lib/md/chevron-left';
import {CONSTANT, trackPageView} from '../analytics/analyticsUtils';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";
import {scrollToElement} from "../../utils/commonUtil";


const Wrapper = styled.div`
  padding: ${themed('layout.gutter.small')}rem;
  padding-top: 30px;
  @media (max-width: 767px) {
    padding-top: 10px;
}
`;

const LeftArrow = styled(MdChevronLeft)`
  font-size: ${themed('text.size.large')}rem;
`;

const ConfirmationHeading = styled(Heading)`
  font-family: BrixSansRegular;
  font-size: 3rem;
  word-break: normal;
  @media (max-width: 767px) {
    padding-top: 10px;
    font-size: 1.5rem;    
}
  word-wrap: break-word;
`;

const BackToMenu = styled(Label)`
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.small')}rem;
`;
const BackLink = styled(Link)`
  margin-left: -9px;
  display: block;
  width: 160px;
`;

export class OrderConfirmation extends Component {

  static propTypes = {
    restaurant: AppPropTypes.restaurant.isRequired,
    invalidateOrder: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.invalidateOrder();
    trackPageView(CONSTANT.CONFIRM);
  }

  renderBackToMenu(isFlowWaiting) {
    if (isFlowWaiting) {
      return null;
    } else {
      return 'Back to menu';
    }
  }

  renderLeftArrow(isFlowWaiting) {
    if (isFlowWaiting) {
      return null;
    } else {
      return <LeftArrow/>;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isSignUpCompleted && nextProps.isSignUpCompleted) {
      scrollToElement('.restaurantDetailContainer');
    }
  }

  render() {

    const {restaurant, isFlowWaiting, isSignUpCompleted, signUpErrorCode} = this.props;
    return (
        <Wrapper>
          <BackLink onClick={() => {
            trackEvent(SCENARIOS.ORDER_CONFIRMATION, EVENTS[SCENARIOS.ORDER_CONFIRMATION].CLICK_ON_GO_BACK, this.props.restaurant, 1);
          }} to={`/${restaurant.accountId}`}>
            <Button className="hideFromMobile" trim link>
              {this.renderLeftArrow(isFlowWaiting)} <BackToMenu>{this.renderBackToMenu(isFlowWaiting)}</BackToMenu>
            </Button>
            <Button trim link className="showOnMobile backIconButton">
                    <BackIcon />
            </Button>
          </BackLink>
          {(isSignUpCompleted && !signUpErrorCode) ?
            <ConfirmationHeading large>
              Thanks for signing up. You’re on your way to speedier orders!
            </ConfirmationHeading> :
            <div>
              <ConfirmationHeading large>
                Your order has been sent
                to {this.props.restaurant.name}
              </ConfirmationHeading>
              <Paragraph>
                When the restaurant approves your order, you will receive a confirmation email with the expected ETA.
                Please call the restaurant if you have questions about the status of your order.
              </Paragraph>
            </div>
          }
        </Wrapper>
    );
  }
}

function mapState(state) {
  return {
    restaurant: state.app.restaurantInfo,
    isFlowWaiting: state.user.isFlowWaiting,
    isSignUpCompleted: state.user.isSignUpCompleted,
    signUpErrorCode: state.user.signUpErrorCode
  };
}


const actions = {invalidateOrder};

export default connect(mapState, actions)(OrderConfirmation);
