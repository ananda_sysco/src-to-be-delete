import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { get, pick } from 'lodash';
import styled, {withTheme} from 'styled-components';
import createDOMForm from 'rc-form/lib/createDOMForm';
import {connect} from 'react-redux';
import MdChevronLeft from 'react-icons/lib/md/chevron-left';
import {BackIcon, Button, Col, Heading, Label, Paragraph, Row, themed} from 'lib/ui';
import {addContact, addPaymentInfo, setSelectedStateToStore} from 'store/user';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";
import {
  addTip,
  createErrorNotification,
  createOrder,
  orderTypes,
  setCartDisplayCondition,
  showInvalidItemsPopup,
  updateCartViewMode,
  updateInvalidItems
} from 'store/cart';
import * as AppPropTypes from 'utils/proptypes';
import * as validators from 'utils/validators';
import Link from 'components/app/router/Link';
import ProductModal from 'components/product/ProductModal';
import AddressAutocomplete from 'components/global/AddressAutocomplete';
import TipSelect from './TipSelect';
import OrderProcessing from './OrderProcessing';
import {ORDER_ERRORS} from '../../store/utils/errors';
import LoginSection from '../user/LoginSection';
import CardSection from '../checkout/CardSection';
import AreaSelector from './AreaSelector';
import {validatePrepDeliverTime} from 'store/utils/selectors';
import {withAvailability} from 'utils/sessionUtil';
import {FormInput, Section, StyledCheckbox, StyledAnchor as TermsLink} from 'lib/StyledUI';
import config from 'config';
import {formatPrice} from 'utils/toolbox';

const BackToMenu = styled(Label)`
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.small')}rem;
`;

const HeaderTotalText = styled(Heading)`
  font-family: "DuplicateSlabThin";
  font-size: 1rem;
  margin-top: -36px;
  margin-bottom: 20px;
`;

const Header = styled.div`
  margin-bottom: ${themed('layout.gutter.large')}rem;
  margin-top: 30px;
  width:200px;
  @media (max-width: 767px) {
    margin-top: 10px;
    margin-bottom: ${themed('layout.gutter.small')}rem;
}
`;

const Divider = styled.hr`
  margin-top: ${themed('layout.gutter.medium')}rem;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
  background-color: #F7F6F4;
  border-color: #F7F6F4;
  border-style: solid;
  color: #F7F6F4;
`;

const BackLink = styled(Link)`
    margin-top: 20px;
`;

const OptionalLabel = styled(Label)`
    margin-bottom: 5px;
`;

const FormLabel = ({children}) => (
  <Col xs={12} sm={3} md={4} lg={4}>{children}</Col>
);

const FormField = ({children}) => (
  <Col xs={12} sm={9} md={8} lg={6}>{children}</Col>
);

const LeftArrow = styled(MdChevronLeft)`
  font-size: ${themed('text.size.large')}rem;
`;

const PlaceOrderButtonWrapper = styled.div`
  margin-top: 1.875rem;
  margin-bottom: 1.875rem;
  display: flex;
  @media (max-width: 767px) {
    justify-content: center;
  }
`;

const TermsWrapper = ({ children, additionalTerms }) =>(
  <div>
    <span>By placing this order, you accept the </span>
    {children}
    <span> of CAKE. {additionalTerms}</span>
  </div>
);

const termsOfUseLink = <TermsLink href="https://www.trycake.com/legal/consumer-terms/" target="_blank" rel="noopener noreferrer">
  Terms of Use
</TermsLink>;

const privacyPolicyLink = <TermsLink href="https://www.trycake.com/legal/privacy-policy/" target="_blank" rel="noopener noreferrer">
  Privacy Policy
</TermsLink>;

const giftTermsLink = <TermsLink href="https://www.trycake.com/legal/restaurant-gift-terms/" target="_blank" rel="noopener noreferrer">
  Restaurant Gift Terms
</TermsLink>;

const additionalGiftingTerms = 'Gifts once made are nonrefundable. In the event your restaurant order is canceled (for any reason), ' +
  'the gift amount you selected will continue to be processed.';

export class PaymentLayout extends Component {

  static propTypes = {
    user: PropTypes.object,
    restaurant: AppPropTypes.restaurant.isRequired,
    form: PropTypes.object.isRequired,
    addContact: PropTypes.func,
    addPaymentInfo: PropTypes.func,
    addTip: PropTypes.func,
    createOrder: PropTypes.func,
    showCardRemoveConfirmationModal: PropTypes.func
  };

  static getInitialState() {
    return Object.assign({}, {
      tcChecked: false,
      txtMsgChecked: false,
      emailError: false,
      nameOnCardError: false,
      cardNumberError: false,
      cardExpirationError: false,
      CVVError: false,
      zipCodeError: false
    });
  }

  constructor(props) {
    super(props);
    this.state = PaymentLayout.getInitialState();
  }

  componentWillUnmount() {
    // save fields before we disappear
    const {form} = this.props;
    const values = form.getFieldsValue();

    this.updateStore(values);
  }

  componentWillReceiveProps(nextProps) {
    if ((!this.props.loggedInUser && nextProps.loggedInUser) || (this.props.loggedInUser && !nextProps.loggedInUser)) {
      this.props.form.resetFields();
      this.setState(PaymentLayout.getInitialState());
    }

    if (!this.props.loggedInUser && this.props.cart.orderType === orderTypes.delivery && nextProps.cart.orderType === orderTypes.pickup) {
      this.props.setSelectedStateToStore(null);
    }
  }

  updateStore(values) {
    let contact = pick(values, [
      'firstName',
      'lastName',
      'phone',
      'address1',
      'address2',
      'city',
      'state',
      'zipCode',
    ]);
    contact['textMessageConfirmation'] = this.state.txtMsgChecked;
    this.props.addContact(contact);
  }

  /**
   * Track event place order of an user who has account not but not logged in
   */
  trackOrderPlaceEvent = () => {
    const user = this.props.user;
    if (user.hasAccount && !this.props.loggedInUser) {
      trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].EXISTING_CLICK_ON_PLACE_ORDER_NOT_LOGIN_IN, this.props.restaurant, this.props.cart.grandTotal);
    }
  };

  validatePaymentInfo = () => {
    const {creditCards} = this.props.currentUser;
    let nameOnCardError = false;
    let cardNumberError = false;
    let cardExpirationError = false;
    let CVVError = false;
    let zipCodeError = false;

    if (this.props.paymentFormShown || !this.props.loggedInUser || Object.getOwnPropertyNames(this.props.loggedInUser.paymentTypes).length === 0) {

      if (creditCards && creditCards.length > 0) {
        const card = creditCards[0];
        if (!card['nameOnCard']) {
          nameOnCardError = true;
          this.setState({nameOnCardError});
        }
        if (!card['cardNumber']) {
          cardNumberError = true;
          this.setState({cardNumberError});
        }
        if (!card['expiration']) {
          cardExpirationError = true;
          this.setState({cardExpirationError});
        }
        if (!card['securityCode']) {
          CVVError = true;
          this.setState({CVVError});
        }
        if (!card['zipCode']) {
          zipCodeError = true;
          this.setState({zipCodeError});
        }
        return !(nameOnCardError || cardNumberError || cardExpirationError || CVVError || zipCodeError);
      }

      nameOnCardError = true;
      cardNumberError = true;
      cardExpirationError = true;
      CVVError = true;
      zipCodeError = true;
      this.setState({nameOnCardError, cardNumberError, cardExpirationError, CVVError, zipCodeError});
      return false;
    }
    return true;
  };

  createTermsErrorNotification = () => {
    const { cart : { donationAmount }, createErrorNotification } = this.props;
    const termsError = donationAmount > 0 ?
      ORDER_ERRORS.TERMS_OF_USE_WITH_GIFT_TERMS_ERROR : ORDER_ERRORS.TERMS_OF_USE_ERROR;
    createErrorNotification(termsError);
  };

  handlePlaceOrderPressed = () => {

    let emailError;
    if (!this.props.currentUser || !this.props.currentUser.email) {
      emailError = true;
      this.setState({emailError});
    }
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (this.state.tcChecked) {
        if (!err && !emailError && this.validatePaymentInfo()) {
          // trackEvent(CONSTANT.CHECKOUT, 'Order Placed'); TODO this should be order requested
          this.updateStore(values);
          if (this.revalidateSessions()) {
            this.props.showInvalidItemsPopup();
            return;
          } else {
            this.trackOrderPlaceEvent();
            this.props.createOrder();
          }
        }
      } else {
        this.createTermsErrorNotification();
      }
    });
  };

  handleTipFieldBlur = (event) => {
    let tip = Number(event.target.value);
    if (!tip || tip < 0) {
      tip = 0;
    }
    this.props.addTip(tip);
  };

  toggleChange = () => {
    this.setState({
      tcChecked: !this.state.tcChecked,
    });
  };

  toggleTxtMsgChange = () => {
    if (!this.state.txtMsgChecked) {
      trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].CLICK_ON_SEND_TEXT, this.props.restaurant, 1);
    }
    this.setState({
      txtMsgChecked: !this.state.txtMsgChecked,
    });
  };

  handleTipSelectionChanged = () => {
    this.props.form.setFieldsValue({tip: ''});
  };

  handleStateSelectionChanged = (value) => {
    this.props.form.setFieldsValue({state: value});
  };

  handleDeliveryAddressChanged = (value) => {
    const {currentUser} = this.props;
    const contacts = currentUser ? currentUser.contacts : null;
    const zipCode = value.zipCode ? value.zipCode : (contacts && contacts[0] && contacts[0].zipCode) ? contacts[0].zipCode : '';
    this.props.form.setFieldsValue({
      address1: value.address,
      city: value.city,
      state: value.state,
      zipCode,
    });
    if (value.state) {
      this.props.setSelectedStateToStore(value.state);
    }

  };


  /**
   * This method is duplicated in Cart Layout. Need to make this a pure fucntion. Until that. Both places need to be changed.
   * @returns {boolean}
   */
  revalidateSessions() {
    let invalidItems = [];
    let tempInvalidCart = false;
    if (this.props.sessions && this.props.restaurant) {
      let sessions = withAvailability(this.props.sessions, this.props.restaurant.timeZone, false);

      const etaTimeValidations = {};

      if (sessions) {
        const {items = []} = this.props.cart;
        items.forEach(item => {
          let tempSession = sessions[item.sessionId];
          // get session's preparation and deliver time validation
          etaTimeValidations[item.sessionId] = etaTimeValidations[item.sessionId] || validatePrepDeliverTime(this.props.restaurant, item.sessionId, this.props.sessions, this.props.cart.orderType, false);

          if (!tempSession || !(tempSession.isAvailable || tempSession.isPending) || etaTimeValidations[item.sessionId].isTimeOver) {
            invalidItems.push(item);
          }
        });
        if (invalidItems.length !== 0) {
          this.props.updateInvalidItems(invalidItems);
        }
      } else {
        tempInvalidCart = true;
        this.props.updateInvalidItems(null, true);
      }
    }
    return tempInvalidCart || invalidItems.length > 0;
  }

  renderDeliveryAddress() {
    const {currentUser, isAuthenticationPending} = this.props;
    const {getFieldDecorator, getFieldError, getFieldProps} = this.props.form;

    return (
      <Section>
        <FormLabel>
          <Heading xxsmall uppercase>Delivery Info</Heading>
        </FormLabel>
        <FormField>
          <Row>
            <Col xs={12} md={12}>
              <AddressAutocomplete
                loggedInUser={this.props.loggedInUser}
                onSelect={this.handleDeliveryAddressChanged}
                disabled={isAuthenticationPending}
                placeholder={'Address'}
                countryList={config.countryList}
                error={getFieldError('address1')}
                {...getFieldProps('address1', {
                  validateTrigger: 'onBlur',
                  initialValue: get(currentUser, 'contacts[0].address1', ''),
                  rules: [{
                    required: true,
                    whitespace: true,
                    message: 'Delivery address is required',
                  }],
                })}
              />
            </Col>
          </Row>
          <Row>
          <Col xs={12} md={12}>
              {
                getFieldDecorator('address2', {
                  validateTrigger: 'onBlur',
                  initialValue: get(currentUser, 'contacts[0].address2', ''),
                })(
                  <FormInput placeholder={'Apt, suite, company etc. (optional)'} disabled={isAuthenticationPending}/>
                )
              }
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={5}>
              {
                getFieldDecorator('city', {
                  validateTrigger: 'onBlur',
                  initialValue: get(currentUser, 'contacts[0].city', ''),
                  rules: [{
                    required: true,
                    whitespace: true,
                    message: 'City is required',
                  }],
                })(
                  <FormInput placeholder={'City'} error={getFieldError('city')} disabled={isAuthenticationPending}/>
                )
              }
            </Col>
            <Col xs={12} md={4}>
              {
                getFieldDecorator('state', {
                  validateTrigger: 'onBlur',
                  initialValue: get(currentUser, 'contacts[0].state', ''),
                  rules: [{
                    required: true,
                    message: 'State is required',
                  },
                    {validator: validators.addressStateValidation},
                  ],
                })(
                  <AreaSelector placeHolder={'State'} error={getFieldError('state')}
                                disabled={isAuthenticationPending} onChange={this.handleStateSelectionChanged}/>
                )
              }
            </Col>
            <Col xs={12} md={3}>
              {
                getFieldDecorator('zipCode', {
                  validateTrigger: 'onBlur',
                  initialValue: get(currentUser, 'contacts[0].zipCode', ''),
                  rules: [{
                    required: true,
                    message: 'Zip code is required',
                  },
                    {validator: validators.addressZipCodeValidation},
                  ],
                })(
                  <FormInput
                    maxLength={5}
                    placeholder={'ZIP'}
                    error={getFieldError('zipCode')}
                    disabled={isAuthenticationPending}
                  />
                )
              }
            </Col>
          </Row>
        </FormField>
      </Section>
    );
  }

  generateTermsNConditionsText () {
    const { cart : { donationAmount } } = this.props;
    if (donationAmount) {
      return (
        <TermsWrapper additionalTerms={additionalGiftingTerms}>
          {termsOfUseLink}
          <span>, </span>
          {privacyPolicyLink}
          <span>, and </span>
          {giftTermsLink}
        </TermsWrapper>
      );
    }
    return(
      <TermsWrapper>
        {termsOfUseLink}
        <span> &amp; </span>
        {privacyPolicyLink}
      </TermsWrapper>
    );
  };

  render() {
    const {restaurant, currentUser, cart, isAuthenticationPending} = this.props;
    const {getFieldDecorator, getFieldError} = this.props.form;
    const isCakeOLOTipsEnabled = get(restaurant, 'isCakeOLOTipsEnabled', true);
    const termsNConditionsText = this.generateTermsNConditionsText();

    return (
      <Col>

        <Row center="xs">
          <Col xs={12}>
            <HeaderTotalText className="showOnMobile">Total:
              ${formatPrice(this.props.cart.grandTotal)}</HeaderTotalText>
          </Col>
        </Row>
        <Header>
          <BackLink to={`/${restaurant.accountId}`} onClick={() => {
            this.props.setCartDisplayCondition(true);
          }}>
            <Button className="hideFromMobile" trim link>
              <LeftArrow/> <BackToMenu>Back to menu</BackToMenu>
            </Button>
            <Button trim link className="showOnMobile backIconButton">
              <BackIcon/>
            </Button>
          </BackLink>
        </Header>
        <Section>
          <FormLabel>
            <Heading xxsmall uppercase>Contact</Heading>
          </FormLabel>
          <FormField>
            <LoginSection emailError={this.state.emailError}/>
            {
              getFieldDecorator('firstName', {
                validateTrigger: 'onBlur',
                initialValue: get(currentUser, 'firstName', ''),
                rules: [{
                  required: true,
                  whitespace: true,
                  message: 'First name is required',
                }],
              })(
                <FormInput placeholder={'First Name'} error={getFieldError('firstName')} maxlength={'40'}
                           disabled={isAuthenticationPending}/>
              )
            }
            {
              getFieldDecorator('lastName', {
                validateTrigger: 'onBlur',
                initialValue: get(currentUser, 'lastName', ''),
                rules: [{
                  required: true,
                  whitespace: true,
                  message: 'Last name is required',
                }],
              })(
                <FormInput placeholder={'Last Name'} error={getFieldError('lastName')} maxlength={'40'}
                           disabled={isAuthenticationPending}/>
              )
            }
            {
              getFieldDecorator('phone', {
                validateTrigger: 'onBlur',
                initialValue: get(currentUser, 'contacts[0].phone', ''),
                rules: [{
                  required: true,
                  message: 'Phone number is required',
                },
                  {validator: validators.phoneNumberValidation}
                ],
                normalize: value => value && value.replace(/[^0-9]+/g, ''),
              })(
                <FormInput placeholder="Mobile phone" error={getFieldError('phone')}
                           mask="(999) 999-9999" disabled={isAuthenticationPending}/>
              )
            }
            {
              <Paragraph xsmall>
                <StyledCheckbox
                  id={'checkBoxTextMsg'}
                  checked={this.state.txtMsgChecked}
                  onChange={this.toggleTxtMsgChange}
                  label={"Text me when the order is received."}
                  disabled={isAuthenticationPending}
                />
              </Paragraph>
            }
          </FormField>
        </Section>

        {(cart.orderType === orderTypes.delivery || (restaurant.deliver && !restaurant.pickup)) && <Divider/>}
        {(cart.orderType === orderTypes.delivery || (restaurant.deliver && !restaurant.pickup)) && this.renderDeliveryAddress()}

        {isCakeOLOTipsEnabled && <Divider/>}
        {isCakeOLOTipsEnabled && this.renderTips()}

        <Divider/>
        <CardSection nameOnCardError={this.state.nameOnCardError}
                     cardNumberError={this.state.cardNumberError}
                     cardExpirationError={this.state.cardExpirationError}
                     CVVError={this.state.CVVError}
                     zipCodeError={this.state.zipCodeError}/>
        <Section>
          <FormLabel>
          </FormLabel>
          <FormField>
            <Paragraph xsmall>
              <StyledCheckbox
                id={'tccheckbox'}
                checked={this.state.tcChecked}
                onChange={this.toggleChange}
                disabled={isAuthenticationPending}
                label={termsNConditionsText}
              />
            </Paragraph>
            <Row>
              <Col xs={12}>
                <Row center="xs">
                  <Col xs={12}>
                    <PlaceOrderButtonWrapper>
                      <Button threeQtr primary onPress={this.handlePlaceOrderPressed}
                              disabled={isAuthenticationPending || cart.orderIsSending}>
                        Place Order
                      </Button>
                    </PlaceOrderButtonWrapper>
                  </Col>
                </Row>
              </Col>
            </Row>
          </FormField>
        </Section>
        <ProductModal/>
        <OrderProcessing isOpen={cart.orderIsSending}/>
      </Col>
    );
  }

  renderTips() {
    const { cart, isAuthenticationPending } = this.props;
    const { getFieldDecorator, getFieldError } = this.props.form;
    return <Section>
      <FormLabel>
        <Heading xxsmall uppercase>Tip</Heading>
        <OptionalLabel small neutral italic>Optional</OptionalLabel>
      </FormLabel>
      <FormField>
        <TipSelect onChange={this.handleTipSelectionChanged} disabled={isAuthenticationPending}/>
        {
          getFieldDecorator('tip', {
            validateTrigger: 'onBlur',
            initialValue: cart.tipPercentage || cart.tip === null ? '' : cart.tip,
            rules: [
              {
                pattern: new RegExp(/^\d*\.?\d+$/),
                message: 'Invalid tip amount',
              },
            ]
          })(
            <FormInput placeholder={'Custom tip amount'} maxlength={7} onBlur={this.handleTipFieldBlur}
                       error={getFieldError('tip')} disabled={isAuthenticationPending}/>
          )
        }
      </FormField>
    </Section>;
  }
}

function mapState(state) {
  return {
    user: state.user,
    currentUser: state.user.currentUser,
    loggedInUser: state.user.loggedInUser,
    cart: state.cart,
    sessions: state.app.sessions,
    restaurant: state.app.restaurantInfo,
    isAuthenticationPending: state.user.isFlowWaiting,
    isGuest: state.user.isGuest,
    selectedState: state.cart.selectedState,
    cardRemoveConfirmationModalShown: state.user.cardRemoveConfirmationModalShown,
    paymentFormShown: state.user.paymentInfoEnteringFormShown,
  }
}

const actions = {
  addContact,
  addPaymentInfo,
  createOrder,
  addTip,
  createErrorNotification,
  showInvalidItemsPopup,
  updateInvalidItems,
  setSelectedStateToStore,
  updateCartViewMode,
  setCartDisplayCondition
};

export default createDOMForm()(connect(mapState, actions)(withTheme(PaymentLayout)));
