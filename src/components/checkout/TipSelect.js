import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MdCheck from 'react-icons/lib/md/check';
import * as AppPropTypes from 'utils/proptypes';
import {Col, Label, Row, Select, themed} from 'lib/ui';
import {addTip, tipTypes} from 'store/cart';
import {round} from 'lodash';

const CheckIcon = styled(MdCheck)`
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.large')}rem;
`;

const StyledSelect = styled(Select)`
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const options = [
  { value: 0, label: '0%' },
  { value: 0.1, label: '10%' },
  { value: 0.15, label: '15%' },
  { value: 0.18, label: '18%' },
  { value: 0.2, label: '20%' },
];

export class TipSelect extends Component {

  static propTypes = {
    cart: AppPropTypes.cart.isRequired,
    addTip: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  handleChange = ({value}) => {
    this.props.addTip(value, tipTypes.percentage);
    this.props.onChange(value);
  };

  renderValue = ({value, label}) => {
    const {totalCost} = this.props.cart;
    const tipValue = round((totalCost * value), 2).toFixed(2);
    return (
      <Col>
        <Label small>{`${label} - $${tipValue}`}</Label>
      </Col>
    )
  };

  renderOption = ({value, label}) => {
    const {totalCost, tipPercentage} = this.props.cart;
    const tipValue = round((totalCost * value), 2).toFixed(2);

    const isChecked = tipPercentage === value;

    return (
      <Row>
        <Col xs={10}>
          <Label>{`${label} - $${tipValue}`}</Label>
        </Col>
        <Col xs={2}>
          <Row center="xs" middle="xs">
            {isChecked && <CheckIcon/>}
          </Row>
        </Col>
      </Row>
    )
  }

  render() {
    const {tipPercentage} = this.props.cart;
    const {disabled} = this.props;

    return (
      <StyledSelect
        placeholder="Select Tip"
        clearable={false}
        searchable={false}
        value={tipPercentage}
        options={options}
        disabled={disabled}
        onChange={this.handleChange}
        optionRenderer={this.renderOption}
        valueRenderer={this.renderValue}
      />
    );
  }
}

function mapState(state) {
  return {
    cart: state.cart,
  };
}

const actions = { addTip };

export default connect(mapState, actions)(TipSelect);
