import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from 'react-modal';
import Loading from 'components/global/Loading';
import {themed, Col, Paragraph, Heading} from 'lib/ui';
import {Section, modalStyle} from 'lib/StyledUI';


const StyledModal = styled(Modal)`
  margin: 5rem auto 0 auto;
  max-width: 30rem;
  padding: ${themed('layout.gutter.xlarge')}rem;
  background: ${themed('colors.foregroundLight')};
  overflow: auto;
  -webkit-overflow-scrolling: touch;
  outline: none;
  text-align: center;
`

export default class OrderProcessing extends Component {

  static propTypes = {
    isOpen: PropTypes.bool,
  };

  static defaultProps = {
    isOpen: false,
  };

  render() {
    return (
      <StyledModal
        contentLabel="Order Processing"
        isOpen={this.props.isOpen}
        style={modalStyle}
      >
        <Col xs={12}>
          <Section center="xs">
            <Loading />
          </Section>
          <Section center="xs">
            <Heading xsmall>Thanks for your patience while we process your order.</Heading>
            <Paragraph>
              When the restaurant approves your order, you will receive a confirmation email with the expected ETA.
              Please call the restaurant if you have questions about the status of your order.
            </Paragraph>
          </Section>
        </Col>
      </StyledModal>
    );
  }
}
