import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {SAVE_THIS_CARD_LABEL} from '../../lib/constants';
import {
  checkNullOrUndefinedOrEmptyString,
  isExpired,
  isValidExpiryMonth,
  isValidExpiryYear
} from '../../utils/commonUtil';
import {Col, Heading, Paragraph, Row, themed} from 'lib/ui';
import {FormInput, Section, StyledCheckbox} from 'lib/StyledUI';

import {Mask} from 'react-input-enhancements';
import {
  changeSaveCardStatus,
  changeSelectedCardIdentifier,
  clearGuestPaymentInfo,
  setGuestPaymentInfo,
  showCardRemoveConfirmationModal,
  switchPaymentInfoEnteringForm
} from '../../store/user';
import PaymentCards from '../card/PaymentCards';
import validator from 'validator';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";

const FormLabel = ({children}) => (
  <Col xs={12} sm={3} md={4} lg={4}>{children}</Col>
);

const FormField = ({children}) => (
  <Col xs={12} sm={9} md={8} lg={6}>{children}</Col>
);

const SpacedParagraph = styled(Paragraph)`
   padding: ${themed('layout.gutter.small')}rem;
   margin-bottom: -13px;
   padding-left: 0px;
`;

const CardWrapper = styled.div`
   margin-bottom: 1rem;
`;

const CARD_HOLDER_NAME_REQUIRED = "Card holder name is required";
const INVALID_CARD_NUMBER = "Invalid card number";
const CARD_NUMBER_REQUIRED = "Card number is required";
const EXPIRATION_DATE_REQUIRED = "Expiration date is required";
const CVV_CODE_REQUIRED = "CVV code is required";
const INVALID_CVV = "Invalid CVV code";
const INVALID_EXPIRATION_DATE = "Invalid expiration date";
const CARD_IS_EXPIRED = "Credit card is expired";
const ZIP_CODE_REQUIRED = "Zip code is required";
const ZIP_CODE_INVALID = "Invalid zip code";

export class CardSection extends Component {

  constructor(props) {
    super(props);
    this.toggleChange.bind(this);
    this.state = {
      nameOnCard: null,
      cardNumber: null,
      expiration: null,
      securityCode: null,
      zipCode: null,
      addCardChecked: false
    };
  }

  componentWillUnmount() {
    this.props.clearGuestPaymentInfo();
  }

  componentWillReceiveProps(nextProps) {

    if (!this.props.nameOnCardError && nextProps.nameOnCardError) {
      this.setState({nameOnCardError: 'Card holder name is required'});
    }

    if (!this.props.cardNumberError && nextProps.cardNumberError) {
      if (!this.state.cardNumber) {
        this.setState({cardNumberError: 'Card number is required'});
      }
      else {
        this.setState({cardNumberError: 'Invalid card number'});
      }
    }

    if (!this.props.cardExpirationError && nextProps.cardExpirationError) {
      if (!this.state.expiration) {
        this.setState({cardExpirationError: 'Card expiration date is required'});
      } else {
        this.setState({cardExpirationError: 'Invalid card expiration date'});
      }

    }

    if (!this.props.CVVError && nextProps.CVVError) {
      if (!this.state.securityCode) {
        this.setState({CVVError: CVV_CODE_REQUIRED});
      } else {
        this.setState({CVVError: INVALID_CVV});
      }
    }

    if (!this.props.zipCodeError && nextProps.zipCodeError) {
      if (!this.state.zipCode) {
        this.setState({zipCodeError: ZIP_CODE_REQUIRED});
      } else {
        this.setState({zipCodeError: ZIP_CODE_INVALID});
      }
    }
  }

  static propTypes = {
    handleClose: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    isShown: PropTypes.any.bool,
    isPending: PropTypes.any.bool,
    errorCode: PropTypes.any.int,
    showCardRemoveConfirmationModal: PropTypes.func,
    paymentInfoEnteringFormShown: PropTypes.bool
  };

  handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };
    this.setState({...tempState});
  };

  handleCloseModal = () => {
    this.setState({email: undefined, password: undefined, emailError: null, pwdError: null});
    this.props.hideLoginModal();
  };

  handleForgetPassword = () => {
    this.handleCloseModal();
    this.props.showForgotPasswordModal();
  };

  handleGuestFlow = () => {
    this.handleCloseModal();
    this.props.continueAsGuest();
  };

  handleBlur = (component) => {
    let target = component.target;
    let value = target.value;
    let field = target.name;
    switch (field) {

      case 'nameOnCard':
        if (!value || !value.trim()) {
          this.setState({nameOnCardError: CARD_HOLDER_NAME_REQUIRED});
          this.props.setGuestPaymentInfo('nameOnCard', null);
        } else {
          this.setState({nameOnCardError: null});
          this.props.setGuestPaymentInfo('nameOnCard', value);
        }

        break;
      case 'creditCardNumber':
        if (!value) {
          this.setState({cardNumberError: CARD_NUMBER_REQUIRED});
          this.props.setGuestPaymentInfo('cardNumber', null);
        } else {
          if (!validator.isCreditCard(value)) {
            this.setState({cardNumberError: INVALID_CARD_NUMBER});
            this.props.setGuestPaymentInfo('cardNumber', null);
          } else {
            this.setState({cardNumberError: null});
            this.props.setGuestPaymentInfo('cardNumber', formatCardNumber(value));
          }
        }
        break;

      case 'cardExpiration':
        if (!value) {
          this.setState({cardExpirationError: EXPIRATION_DATE_REQUIRED});
          this.props.setGuestPaymentInfo('expiration', null);
        } else {
          const expiration = value.split('/');
          const expirationYear = expiration[1].trim();

          if (isValidExpiryMonth(expiration[0].trim()) && isValidExpiryYear(expirationYear)) {
            if (isExpired(expiration[0].trim(), '20' + expirationYear, this.props.timeZone)) {
              this.setState({cardExpirationError: CARD_IS_EXPIRED});
              this.props.setGuestPaymentInfo('expiration', null);
            } else {
              this.setState({cardExpirationError: null});
              this.props.setGuestPaymentInfo('expiration', value);
            }
          } else {
            this.setState({cardExpirationError: INVALID_EXPIRATION_DATE});
            this.props.setGuestPaymentInfo('expiration', null);
          }
        }
        break;

      case 'CVV':
        if (!value) {
          this.setState({CVVError: CVV_CODE_REQUIRED});
          this.props.setGuestPaymentInfo('securityCode', null);
          this.setState({securityCode: null});
        } else if (value.length < 3) {
          this.setState({CVVError: INVALID_CVV});
          this.props.setGuestPaymentInfo('securityCode', null);
          this.setState({securityCode: value});
        } else {
          this.setState({CVVError: null});
          this.props.setGuestPaymentInfo('securityCode', value);
          this.setState({securityCode: value});
        }
        break;

      case 'zipCode':
        if (!value) {
          this.setState({ zipCodeError: ZIP_CODE_REQUIRED });
          this.props.setGuestPaymentInfo('zipCode', null);
          this.setState({ zipCode: null });
        } else if (!validator.isPostalCode(value, 'US')) {
          this.setState({ zipCodeError: ZIP_CODE_INVALID });
          this.props.setGuestPaymentInfo('zipCode', null);
          this.setState({ zipCode: value });
        } else {
          this.setState({ zipCodeError: null });
          this.props.setGuestPaymentInfo('zipCode', value);
          this.setState({ zipCode: value });
        }
        break;

      default:
        return;
    }
  };


  toggleChange = () => {
    if (!this.state.addCardChecked) {
      trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].REGISTERED_CLICK_ON_SAVE_CARD, this.props.restaurant, 1);
    }
    this.props.changeSaveCardStatus(!this.state.addCardChecked);
    this.setState({
      addCardChecked: !this.state.addCardChecked,
    });
  };

  constructCardList = (loggedInUser) => {
    if (loggedInUser && loggedInUser.paymentTypes && loggedInUser.paymentTypes.credit_card) {
      return loggedInUser.paymentTypes.credit_card;
    }
    return null;
  };

  renderCardDropDown(isPending, loggedInUser) {
    return (<CardWrapper>
                <PaymentCards cardSet={this.constructCardList(loggedInUser)}
                          showCardRemoveConfirmationModal={this.props.showCardRemoveConfirmationModal}
                          switchPaymentInfoEnteringForm={this.props.switchPaymentInfoEnteringForm}
                          changeSelectedCardIdentifier={this.props.changeSelectedCardIdentifier}
                          isPending={this.props.isPending}
                          restaurant={this.props.restaurant}
                          saveCurrentCard={this.props.saveCurrentCard}
                          changeSaveCardStatus={this.props.changeSaveCardStatus}
                          cardDropDownOption={this.props.cardDropDownOption}
                          cardSelection={this.props.cardSelection}
            /></CardWrapper>);
  }

  renderSectionName() {
    return (<FormLabel>
      <Heading xxsmall uppercase>Payment Info</Heading>
    </FormLabel>);
  }

  renderForm(loggedInUser, paymentInfoEnteringModalShown, hasCards, isPending) {
    return ((!loggedInUser || paymentInfoEnteringModalShown || !hasCards) && <div>

      <FormInput name={'nameOnCard'}
                 placeholder={'Name On Card'}
                 disabled={isPending}
                 error={this.state.nameOnCardError}
                 maxlength={'50'}
                 onBlur={this.handleBlur}/>

      <Row>
      <Col xs={8} sm={8}>
      <Mask pattern={'0000 0000 0000 0000' || '0000 0000 0000 000'} emptyChar={''} placeholder={'Credit Card Number'}>
        <FormInput name={'creditCardNumber'}
                   disabled={isPending}
                   error={this.state.cardNumberError}
                   onBlur={this.handleBlur}/>
      </Mask>
      </Col>
      <Col xs={4} sm={4}>
          <FormInput
            name={'CVV'}
            placeholder={'Security Code'}
            error={this.state.CVVError}
            mask="9999"
            maskChar={null}
            disabled={isPending}
            onBlur={this.handleBlur}/>
        </Col>
      </Row>
      <Row>
        <Col xs={6} sm={6}>
          <Mask pattern='00/00' placeholder={'Expiration (MM/YY)'}>
            <FormInput name={'cardExpiration'}
                       error={this.state.cardExpirationError}
                       disabled={isPending}
                       onBlur={this.handleBlur}/>
          </Mask>
        </Col>
        <Col xs={6} sm={6}>
          <FormInput
            name={'zipCode'}
            maxLength={5}
            placeholder={'Billing ZIP Code'}
            error={this.state.zipCodeError}
            disabled={isPending}
            onBlur={this.handleBlur}/>
        </Col>
      </Row>
      {loggedInUser && <SpacedParagraph xsmall>
        <StyledCheckbox
          id={'saveCardCheckBox'}
          checked={this.props.saveCurrentCard}
          onChange={this.toggleChange}
          label={SAVE_THIS_CARD_LABEL}
          disabled={isPending}
        />
      </SpacedParagraph>
      }
    </div>)
  }

  userHasCards(user) {
    return (user && user.paymentTypes && user.paymentTypes.credit_card && user.paymentTypes.credit_card.length > 0);
  };

  render() {
    const {loggedInUser, paymentInfoEnteringFormShown, isPending} = this.props;
    const hasCard = this.userHasCards(loggedInUser);
    return (
      <Section>
        {this.renderSectionName()}
        <FormField>
            {hasCard && this.renderCardDropDown(isPending, loggedInUser)}
          {this.renderForm(loggedInUser, paymentInfoEnteringFormShown, hasCard, isPending)}
        </FormField>
      </Section>
    );
  };
}


function mapState(state) {
  return {
    isPending: state.user.isFlowWaiting,
    loggedInUser: state.user.loggedInUser,
    cardSelection: (state.user.currentUser && state.user.currentUser.cardDropDownOption) ? state.user.currentUser.cardDropDownOption : null,
    paymentInfoEnteringFormShown: state.user.paymentInfoEnteringFormShown,
    cardDropDownOption: state.user.currentUser ? state.user.currentUser.cardDropDownOption : null,
    saveCurrentCard: state.user.currentUser ? state.user.currentUser.saveCurrentCard : false,
    restaurant: state.app.restaurantInfo,
    timeZone: state.app.restaurantInfo.timeZone ? state.app.restaurantInfo.timeZone : null
  };
}

function formatCardNumber(cardNumber) {
  if (!checkNullOrUndefinedOrEmptyString(cardNumber)) {
    let card = cardNumber.trim();
    let vals = card.split(' ');
    let number = '';
    vals.forEach((val) => {
      number += val;
    });
    return number;
  }
}

const actions = {
  showCardRemoveConfirmationModal,
  switchPaymentInfoEnteringForm,
  setGuestPaymentInfo,
  changeSelectedCardIdentifier,
  changeSaveCardStatus,
  clearGuestPaymentInfo
};

export default connect(mapState, actions)(CardSection);
