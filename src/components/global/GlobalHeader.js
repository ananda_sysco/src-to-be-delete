import React, {Component} from 'react';
import styled from 'styled-components';
import {BackIcon, CakeLogo, Col, Heading, Label, Row, Select, themed, UserDarkIcon, UserIcon} from 'lib/ui';
import 'lib/ui/global/search.css';
import Link from 'components/app/router/Link';
import {connect} from "react-redux";
import {logoutUser, showLoginModal, updateUserDetailsPanelStatus} from 'store/user';

const GlobalHeaderRow = styled(Row)`
height: 54px;
align-items: center;
border-bottom: 0.72px solid rgb(216, 215, 211, 0.5);
@media (max-width: 767px)
{
    border-bottom: 1px solid transparent;
}
`;
const HeaderLinkContainer = styled(Row)`
height: 100%;
`;
const HeaderLinks = styled(Col)`
height: 100%;
`;

const HeaderItem = styled.div`
display: flex;
float:right;
flex-direction: row;
cursor: pointer;
height: 100%;
width: 100%;
svg{
    min-width: 24px;
}
&:hover{
    background-color: #A8A7A4;
    svg path{
        opacity: 1;        
    }
}
@media (max-width: 767px)
{
    &:hover{
        background-color: transparent;
        svg path{
            opacity: 0.7;        
            fill: #A8A7A4 !important;
        }
    }
}
`;
const UserName = styled.span`
padding-top: 5px;
padding-left: 5px;
font-family: BrixSansLight;
font-size: 0.9rem;
text-align: left;
`;
const LoginLink = styled(Link)`
font-family: BrixSansLight;
font-size: 0.9rem;
text-align: center;
height: 100%;
width: 100%;
display: block;
&:focus {
    outline: none;
}
@media (max-width: 767px)
{
    &:hover{
        opacity: 1;
    }
}
`;
const LogoutLink = styled(Link)`
font-family: BrixSansLight;
font-size: 0.9rem;
text-align: center;
height: 100%;
width: 100%;
color: #5f5f5f;
display: block;
padding-top: 7px;
padding-left: 5px;
padding-right: 5px;
box-sizing: border-box;
display: inline-block;
&:focus {
    outline: none;
}
&:hover {
    opacity: 1;
    color: #fff;
}
`;
const HeaderLinkWrapper = styled(Col)`
height: 100%;
display: flex;
align-items: center;
justify-content: center;
padding-right: 0px;
svg path{
    fill: #A8A7A4;
}
&:hover{
    .logoutLink {
        svg path{
            fill: #fff;
        }
    }
}
@media (max-width: 767px)
{
    &:hover{
        svg path{
            opacity: 0.7;
        }
    }
}
`;
const Subject = styled(Label)`
color: ${themed('colors.primary')};
text-transform: uppercase;
margin-top: ${themed('layout.gutter.xsmall')}rem;
min-height: ${themed('text.size.medium')}rem;
padding-left: 10px;
margin-bottom: -5px;
@media (max-width: 767px) {
  display: block;
  padding-left: 36px;
}
`;
const Title = styled(Heading)`
margin: 0;
font-family: "DuplicateSlabLight";
font-size: 32px;
color: #363532;
text-overflow: ellipsis;
white-space: nowrap;
overflow: hidden;
padding-left: 10px;
@media (max-width: 992px) {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}
@media (max-width: 767px) {
  font-size: 1.3rem;
  position: absolute;
  width: 100%;
  text-align: center;
  top: -4px;
  padding-left: 0px;
}
`;

export class GlobalHeader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mobUserDetail: false
    };
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.isDrawerOpen !== this.props.isDrawerOpen) {
      this.setSidePanelVisibility();
    }
  }

  handleDropDownClick = (item) => {
    if (item.value === 'logout') {
      this.props.logoutUser();
    }
  };

  setSidePanelVisibility = () => {
    if (!this.props.isDrawerOpen) {
      document.body.classList.add('userDetailsPanelOn');
    } else {
      document.body.classList.remove('userDetailsPanelOn');
    }
  };

  renderUserValue = () => {
    return (
        <LogoutLink to="#" onClick={() => {
          this.props.updateUserDetailsPanelStatus(!this.props.isDrawerOpen)
        }}>
          <span className="mobileUserIcon"><UserDarkIcon/></span>
          <UserName className="loggedInUserName hideFromMobile">{this.props.loggedInUser.firstName}</UserName>
        </LogoutLink>
    )
  };

  render() {
    const profileOptions = [
      {
        value: 'logout',
        label: 'Logout'
      }
    ];

    return (
      <GlobalHeaderRow className="globalHeader">
        {!this.props.customTitle &&
        <Col xs={2}>
          <Link to="#" onClick={this.props.onBackClicked}><CakeLogo/></Link>
          {!this.props.isReceiptView && <Link to="#" onClick={() => this.props.onBackClicked(true)}><BackIcon/></Link>}
        </Col>
        }
        <Col xs={!this.props.customTitle ? 6 : 8}>
          <Subject className="hideFromMobile" xsmall>{this.props.subHead}</Subject>
          <Title className="headerTitle">{this.props.customTitle}</Title>
        </Col>
        <HeaderLinks xs={4}>
          <HeaderLinkContainer end="xs" middle="xs">
            <HeaderLinkWrapper xs={6}>
              {!this.props.loggedInUser &&
              <LoginLink to="#" onClick={this.props.showLoginModal}><span
                className="hideFromMobile signInText">Sign In</span><span
                className="mobileUserIcon showOnMobile"><UserIcon/></span></LoginLink>}
              {this.props.loggedInUser && <div className="showOnMobile mobileLogout">{this.renderUserValue()}</div>}
              {this.props.loggedInUser &&
              <HeaderItem className="logoutLink">
                <Select
                  className="profileMenu hideFromMobile"
                  clearable={false}
                  searchable={false}
                  options={profileOptions}
                  value={"logout"}
                  onChange={this.handleDropDownClick}
                  valueRenderer={this.renderUserValue}
                />
              </HeaderItem>
              }
            </HeaderLinkWrapper>
          </HeaderLinkContainer>
        </HeaderLinks>
      </GlobalHeaderRow>
    );
  }
}

function mapState(state) {
  return {
    isLoginModalOpen: state.user.isLoginModalShown,
    isDrawerOpen: state.user.isDrawerOpen,
    loggedInUser: state.user.loggedInUser,
  };
}

export default connect(mapState, {showLoginModal, logoutUser, updateUserDetailsPanelStatus})(GlobalHeader);
