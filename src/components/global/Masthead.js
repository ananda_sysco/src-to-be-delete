import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {Col, Heading, Label, Row, themed} from 'lib/ui';
import SessionBar from "components/menu/SessionBar";
import {connect} from "react-redux";
import {logoutUser, showLoginModal, updateUserDetailsPanelStatus} from 'store/user';
import {checkNullOrUndefinedOrEmptyString} from '../../utils/commonUtil';

const Wrapper = styled(Row)`
  border-bottom: 1px solid ${themed('colors.border')};
  height: 80px;
`;

const Subject = styled(Label)`
color: ${themed('colors.primary')};
text-transform: uppercase;
margin-top: ${themed('layout.gutter.xsmall')}rem;
min-height: ${themed('text.size.medium')}rem;
@media (max-width: 767px) {
  display: block;
  padding-left: 36px;
}
`;

const Title = styled(Heading)`
margin: 0;
font-family: "DuplicateSlabLight";
font-size: 28px;
color: #363532;
text-overflow: ellipsis;
white-space: nowrap;
overflow: hidden;
@media (max-width: 992px) {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
}
@media (max-width: 767px) {
  font-size: 1.3rem;
  margin-top: 4px;
}
`;
const AddressLine = styled(Heading)`
font-family: BrixSansLight;
font-size: 12px;
margin-bottom: 0px;
margin-top: 6px;
`;

const SessionContainer = styled.div`
  flex-grow: 4;
  @media (max-width: 767px) {
    flex-direction: column;
    width:100%;
    }
    @media (max-width: 1200px) {
    width:100%;
    }
`;

const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;

  @media (max-width: 767px) {
    text-align: center;
    }
  height: 80px;
`;
const Logo = styled.img`
  display: flex;
  flex-direction: row;
  padding: 5px;
  background-color: #fff;
  height: 76px;
  width: 122px;
`;
const RestaurantDetailContainer = styled.div`
    display: flex;
    flex-direction: column;
    @media (min-width: 1600px) {
        max-width:calc(100vw - 60vw)
    }
    @media (min-width: 1201px) {
        max-width:calc(100vw - 46vw)
    }
    @media (max-width: 1200px) {
        max-width:calc(100vw - 46vw)
    }
    @media (max-width: 992px) {
        max-width:calc(100vw - 43vw);
        padding-left: 12px;
    }
    @media (max-width: 767px) {
        text-align: center;
        width:100%;
        max-width:calc(100vw);
        padding-left: 44px;
        padding-right: 44px;
        box-sizing: border-box;
    }
`;

const HeaderLogoContainer = styled.div`
  height:100px;
  margin-right: 12px;
  margin-left: 12px;
  position: relative;
  margin-top: -8px;
  @media (max-width: 1024px) {
    display: none;
    }

`;

export class Masthead extends Component {

  constructor(props) {
    super(props);
    let imgUrl = '';
    const address = props.address || '';
    let imageAvailable = false;
    if (!(checkNullOrUndefinedOrEmptyString(props.baseImgUrl)) && !(checkNullOrUndefinedOrEmptyString(props.bgImageUrl))) {
      const urlLength = props.baseImgUrl.length;
      if (props.baseImgUrl.substr(urlLength - 1, urlLength) === '/') {
        imgUrl = props.baseImgUrl + props.bgImageUrl;
      } else {
        imgUrl = props.baseImgUrl + '/' + props.bgImageUrl;
      }
      imageAvailable = true;
    }
    this.state = {
      imageAvailable: imageAvailable,
      imgUrl: imgUrl,
      address: address,
      mobUserDetail: false
    };
    this.handleUserIconClick = this.handleUserIconClick.bind(this)
  }

  static propTypes = {
    user: PropTypes.object.isRequired,
    heading: PropTypes.string.isRequired,
    subject: PropTypes.string,
    showLoginModal: PropTypes.func.isRequired,
    logoutUser: PropTypes.func.isRequired,
    showAddress: PropTypes.bool,
    showLogo: PropTypes.bool,
    isReceiptView: PropTypes.bool
  };

  handleUserIconClick() {
    if (!this.state.mobUserDetail) {
      document.body.classList.add('userDetailsPanelOn');
    } else {
      document.body.classList.remove('userDetailsPanelOn');
    }
    this.setState({
      mobUserDetail: !this.state.mobUserDetail
    })
  }

  renderLogo(imageAvailable, imgUrl) {
    const param = encodeURIComponent(imgUrl);
    if (imageAvailable) {
      return (
        <HeaderLogoContainer>
          <Logo src={`/s3/getLogo?img-url=${param}`}/>
        </HeaderLogoContainer>
      );
    }
  }

  renderAddress(address) {
    if (!checkNullOrUndefinedOrEmptyString(address)) {
      const address1 = address.address1 || '';
      const address2 = address.address2 || '';
      const city = address.city || '';
      const state = address.state || '';

      if (checkNullOrUndefinedOrEmptyString(city) && checkNullOrUndefinedOrEmptyString(state)) {
        return (<AddressLine>{address1} {address2}</AddressLine>);
      } else if (!checkNullOrUndefinedOrEmptyString(city) && checkNullOrUndefinedOrEmptyString(state)) {
        return (<AddressLine>{address1} {address2} | {city}</AddressLine>)
      } else if (checkNullOrUndefinedOrEmptyString(city) && !checkNullOrUndefinedOrEmptyString(state)) {
        return (<AddressLine>{address1} {address2} | {state}</AddressLine>);
      } else if (checkNullOrUndefinedOrEmptyString(address1) && checkNullOrUndefinedOrEmptyString(address2)) {
        return (<AddressLine>{city}, {state}</AddressLine>)
      }
      return (
        <AddressLine>{address1} {address2} | {city}, {state}</AddressLine>
      );
    }
  }

  render() {
    const {heading, subject, currentDay, sessions, selectedSession, currentSession, sessionChange, isSessionValidForOrdering, isReceiptView} = this.props;
    const {imageAvailable, imgUrl} = this.state;

    return (
      <Wrapper className={'headerRow'}>
        <Col xs={12}>
          <HeaderWrapper className="headerWrapper">
            {this.props.showLogo && this.renderLogo(imageAvailable, imgUrl)}
            <RestaurantDetailContainer
              className={'restaurantDetailContainer ' + (!imageAvailable ? `headerWithLogo` : '')}>
              {!this.props.showAddress &&
                <Subject className="hideFromMobile" xsmall>{subject}</Subject>
              }
                <Title
                  className="headerTitle"
                  onClick={this.props.onTitleClick ? this.props.onTitleClick : null}
                >{heading}</Title>
              {this.props.showAddress && this.renderAddress(this.state.address)}
            </RestaurantDetailContainer>
            {/* <HeaderUserActions>
              {userArea}
            </HeaderUserActions> */}
            <SessionContainer className="sessionContainer">
              <SessionBar
                currentDay={currentDay}
                sessions={sessions}
                selectedSession={selectedSession}
                activeSession={currentSession}
                orderEnabled={true}
                sessionChange={sessionChange}
                isSessionValidForOrdering={isSessionValidForOrdering}
                isReceiptView={isReceiptView}
              />
            </SessionContainer>
          </HeaderWrapper>
        </Col>
      </Wrapper>
    );
  }
}

function mapState(state) {
  return {
    isLoginModalOpen: state.user.isLoginModalShown,
    isDrawerOpen: state.user.isDrawerOpen,
    baseImgUrl: state.app.restaurantInfo.baseImgUrl,
    bgImageUrl: state.app.restaurantInfo.bgImageUrl,
    address: state.app.restaurantInfo.address,
    isSessionValidForOrdering: state.app.isSessionValidForOrdering
  };
}

export default connect(mapState, {showLoginModal, logoutUser, updateUserDetailsPanelStatus})(Masthead);
