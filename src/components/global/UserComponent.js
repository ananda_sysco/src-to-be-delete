import {Component} from 'react';
import {connect} from 'react-redux';
import {loadLoggedInUser} from 'store/app';

/**
 * This component loads the logged in user from session.
 * Doesn't do any DOM manipulations
 */
export class UserComponent extends Component {

  componentDidMount() {
    this.props.loadLoggedInUser();
  }

  render() {
    return null;
  }

}

function mapState(state) {
  return {};
}

export default connect(mapState, {loadLoggedInUser})(UserComponent);
