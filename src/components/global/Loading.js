import React from 'react';
import PropTypes from 'prop-types';
import styled, {withTheme} from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const Loading = ({theme, size}) => {
  if (window.navigator && window.navigator.userAgent && window.navigator.userAgent.indexOf("Edge") > -1) {
    return <Wrapper>
        <div className="loader">Loading...</div>;
    </Wrapper>
  } else {
    return <Wrapper>
      <svg xmlns="http://www.w3.org/2000/svg" width={`${size}px`} height={`${size}px`} viewBox="0 0 100 100"
           preserveAspectRatio="xMidYMid">
        <circle cx="50" cy="50" r="40" stroke={theme.colors.primary} fill="none" strokeWidth="6" strokeLinecap="round">
          <animate attributeName="stroke-dashoffset" dur="2s" repeatCount="indefinite" from="0" to="502"/>
          <animate attributeName="stroke-dasharray" dur="2s" repeatCount="indefinite"
                   values="150.6 100.4;1 250;150.6 100.4"/>
        </circle>
      </svg>
    </Wrapper>
  }
};

Loading.propTypes = {
  theme: PropTypes.object.isRequired,
  size: PropTypes.number,
};

Loading.defaultProps = {
  size: 100,
};

export default withTheme(Loading);
