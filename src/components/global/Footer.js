import React, { Component } from 'react';
import styled from 'styled-components';
import { themed, Col, Row, Button, Label, PoweredByLogo } from 'lib/ui';

const Wrapper = styled(Col)`
  border-top: 1px solid ${themed('colors.border')};
  padding: ${themed('layout.gutter.large')}rem;
  position: relative;
  @media (max-width: 767px) {
    background-color: #F0EFEB;
    position: static;
  }
`;
const FooterItem = styled.span`
  display: inline-flex;
`;
const Divider = styled(Label)`
  margin: 0 ${themed('layout.gutter.xsmall')}rem;
`;
const MobibleDivider = styled(Label)`
  margin: 0;
  @media (max-width: 767px) {
    margin: 0 ${themed('layout.gutter.xsmall')}rem;
    font-size: 0.9rem;
    margin-bottom: 60px;
    padding: 0 31px;
  }
`;
const FooterLabel = styled(Label)`
@media (max-width: 767px) {
  text-transform: uppercase;
  color: #A8A7A4;
  font-size: 0.9rem;
}
`;
const CopyrightLabel = styled(Label)`
@media (max-width: 767px) {
  text-transform: uppercase;
  color: #A8A7A4;
  font-size: 0.65rem;
  margin-top: 14px;
}
`;
const FooterContentWrapper = styled(Col)`
  text-align: left;
`;

export default class Footer extends Component {

  render() {
    return (
      <Wrapper xs={12}>
        <Row middle="sm" center="xs" start="sm">
            <FooterContentWrapper sm={12}>
                <FooterItem className="hideFromMobile"><Label neutral>&copy; {(new Date()).getFullYear()} Cake Corporation</Label></FooterItem>
                <FooterItem className="hideFromMobile"><Divider neutral>|</Divider></FooterItem>
                <FooterItem><a href="https://www.trycake.com/legal/consumer-terms/" target="_blank" rel="noopener noreferrer">
                    <Button trim link>
                        <FooterLabel primary>Terms of Use</FooterLabel>
                    </Button>
                </a></FooterItem>
                <FooterItem className="hideFromMobile"><Divider neutral>&bull;</Divider></FooterItem>
                <FooterItem><MobibleDivider neutral><span className="showOnMobile">|</span></MobibleDivider></FooterItem>
                <FooterItem><a href="https://www.trycake.com/legal/privacy-policy/" target="_blank" rel="noopener noreferrer">
                    <Button trim link>
                        <FooterLabel primary>Privacy Policy</FooterLabel>
                    </Button>
                </a></FooterItem>
            </FooterContentWrapper>
        </Row>
        <Row center="xs">
          <Col className="showOnMobile">
            <PoweredByLogo />
            <CopyrightLabel neutral>&copy; {(new Date()).getFullYear()} Cake Corporation</CopyrightLabel>
          </Col>
        </Row>
      </Wrapper>
    );
  }

}
