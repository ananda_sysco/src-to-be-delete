import React from 'react';
import styled from 'styled-components';
import { Col, themed } from 'lib/ui';

const Wrapper = styled(Col)`
  padding-bottom: ${themed('layout.gutter.xlarge')}rem;
  border-left: 1px solid #ececec;
  @media (max-width: 767px) {
    padding-bottom: 0px;
}
`;

export default function Sidebar({ children }) {
  return (
    <Wrapper xs={12} sm={4} md={4} lg={3}>
      {children}
    </Wrapper>
  );
}
