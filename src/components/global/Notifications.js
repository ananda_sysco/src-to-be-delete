import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Grid, NotificationBar } from 'lib/ui';
import { dismissNotification } from 'store/app';

const Wrapper = styled(Grid)`
  position: fixed;
  z-index: 9999;
  left: 0;
  width: 100%;
  max-height: 80%;
  overflow-y: auto;
`;

export class Notifications extends Component {

  static propTypes = {
    notifications: PropTypes.arrayOf(PropTypes.object).isRequired,
    dismissNotification: PropTypes.func.isRequired,
  };

  handleDismiss = () => {
    this.props.dismissNotification();
  };

  renderNotification(notification) {
    const {type, message, disableActions} = notification;

    const actions = [
      {
        primary: true,
        label: 'Got It',
        handler: this.handleDismiss,
      },
    ];

    return (
      <Wrapper>
        <NotificationBar
          xs={12}
          sm={8}
          smOffset={2}
          md={6}
          mdOffset={3}
          type={type}
          message={message}
          actions={disableActions === true ? [] : actions}
        />
      </Wrapper>
    );
  }

  render() {
    const { notifications } = this.props;
    const current = notifications[0];

    if (current) {
      return this.renderNotification(current);
    }

    return null;
  }

}

function mapState(state) {
  return {
    notifications: state.app.notifications,
  };
}

export default connect(mapState, { dismissNotification })(Notifications);
