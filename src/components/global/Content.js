import React from 'react';
import styled from 'styled-components';
import { Col, themed } from 'lib/ui';

const Wrapper = styled(Col)`
  padding: ${themed('layout.gutter.small')}rem;
`;

export default function Content({ children }) {
  return (
    <Wrapper xs={12} sm={8} md={8} lg={9}>
      {children}
    </Wrapper>
  );
}
