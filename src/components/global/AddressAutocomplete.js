import React, {Component} from 'react';
import {find, get} from 'lodash';
import PropTypes from 'prop-types';
import styled, {withTheme} from 'styled-components';
import PlacesAutocomplete, {geocodeByPlaceId} from 'react-places-autocomplete-extended';
import {Label, themed} from 'lib/ui';
import {checkNullOrUndefinedOrEmptyString} from "../../utils/commonUtil";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const ErrorLabel = styled(Label)`
  color: ${themed('colors.error')};
  margin-top: ${themed('layout.gutter.xxsmall')}rem;
`;

const getStyles = ({error, theme}, state) => {
  let borderColor = theme.colors.borderLight;
  let borderWidth = '1px';
  if (state.error) {
    borderColor = theme.colors.error;
  } else if (state.focused) {
    borderColor = theme.colors.primary;
  }
  if (state.focused) {
    borderWidth = '2px';
  }
  return {
    root: {
      width: '100%',
      display: 'flex',
      zIndex: '750',
    },
    input: {
      display: 'block',
      fontSize: `${theme.text.size.small}rem`,
      fontFamily: 'BrixSansLight',
      width: '100%',
      height: '24px',
      padding: `${theme.layout.gutter.xsmall}rem`,
      borderRadius: '5px',
      borderColor: borderColor,
      borderWidth: borderWidth,
      borderStyle: 'solid',
      outline: 'none'
    },
    autocompleteContainer: {
      backgroundColor: 'red',
      boxShadow: '0, 2px, 4px, rgba(0, 0, 0, 0.8)',
      borderRadius: '0, 0, 2px, 2px',
      borderStyle: 'solid',
      borderColor: theme.colors.borderLight,
      borderWidth: '1px',
    },
    autocompleteItem: {
      padding: '10px',
      cursor: 'pointer',
      fontFamily: 'BrixSansLight',
      fontSize: `${theme.text.size.small}rem`,
    },
  };
};

export class AddressAutocomplete extends Component {

  static propTypes = {
    theme: PropTypes.object,
    value: PropTypes.string,
    onChange: PropTypes.func,
    onSelect: PropTypes.func,
    error: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.string),
      PropTypes.string,
    ]),
  };

  constructor(props) {
    super(props);

    this.state = {
      addressValue: props.value,
      error: false,
      focused: false
    };
  }

  componentWillReceiveProps(newProps) {
    if (!this.props.error && newProps.error) {
      this.setState({error: true});
    } else if (checkNullOrUndefinedOrEmptyString(newProps.value)) {
      if (this.props.loggedInUser && !newProps.loggedInUser) {//handling user logout
        this.setState({error: false});
      } else if (!this.props.loggedInUser && newProps.loggedInUser) {//handling login
        if (newProps.loggedInUser.contacts[0]['address1'] === undefined && !this.state.addressValue) {
          this.setState({error: false});
        } else {
          this.setState({error: true});
        }
      }
    } else {
      this.setState({error: false});
    }
    this.setState({
      addressValue: newProps.value,
    });

  }

  handleBlur = () => {
    const value = this.state.addressValue;
    if (!value || !value.trim()) {
      this.setState({'error': true, focused: false});
    } else {
      this.setState({'error': false, focused: false})
    }
  };

  handleChange = (address) => {
    this.setState({
      addressValue: address,
      error: false
    });

    if (this.props.onChange) {
      this.props.onChange(address);
    }
  };

  onFocus = () => {
    this.setState({focused: true})
  };

  handleSelect = async (address, placeId) => {
    const place = await geocodeByPlaceId(placeId);

    if (place && place.length) {
      const components = place[0].address_components;
      let streetNumber = get(find(components, {types: ['street_number']}), 'short_name');
      const streetName = get(find(components, {types: ['route']}), 'short_name');
      const city = get(find(components, {types: ['locality']}), 'short_name');
      const state = get(find(components, {types: ['administrative_area_level_1']}), 'short_name');
      const zipCode = get(find(components, {types: ['postal_code']}), 'short_name');
      const addressValue = streetNumber ? `${streetNumber} ${streetName}` : `${streetName}`;

      const address = {
        address: addressValue,
        city,
        state,
        zipCode,
      };

      if (this.props.onSelect) {
        this.props.onSelect(address);
      }
    }
  };

  render() {
    const styles = getStyles(this.props, this.state);
    const {disabled} = this.props;

    const options = {
      types: ['address'],
      componentRestrictions: {country: this.props.countryList}
    };

    return (
      <Wrapper className="deliveryAddressInput">
        <PlacesAutocomplete
          onSelect={this.handleSelect}
          options={options}
          inputProps={{
            type: 'text',
            value: this.state.addressValue,
            placeholder: 'Delivery address',
            onChange: this.handleChange,
            onFocus: this.onFocus,
            onBlur: this.handleBlur,
            autoComplete: 'off',
            disabled: disabled
          }}
          styles={styles}
        />
        {this.state.error && <ErrorLabel xsmall>{'Delivery address is required'}</ErrorLabel>}

      </Wrapper>
    );
  }
}

export default withTheme(AddressAutocomplete);
