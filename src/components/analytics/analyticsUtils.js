/**
 * Created by Salinda on 11/15/17.
 */
import mixpanel from 'mixpanel-browser';

export function trackEvent(eventCategory, eventAction, data) {
  //window.ga('send', 'event', eventCategory, eventAction, data);
}

export function trackGaEvent(eventCategory, eventAction, eventLabel, eventValue) {
  //eventValueInt must be an integer otherwise GA will ignore the event itself.
  let eventValueInt = 1;
  try {
    eventValueInt = parseInt(eventValue, 10)
  } catch (e) {
    //do nothing pass the default value
  }
  window.ga('send', 'event', eventCategory, eventAction, eventLabel, eventValueInt);
}

export function trackPageView(page) {
  mixpanel.track("PAGE_LOADED", {
    "PAGE": page
  });
  window.ga('send', 'pageview', page);
}

export function constructRestaurantDetails(restaurant) {
  let result = '';
  if (restaurant) {
    if (typeof restaurant !== 'string') {
      result = restaurant.name + "(" + restaurant.id + ")";
    } else {
      result = restaurant;
    }
  }
  return result;
}

export function trackMixPanelEvent(eventCategory, eventProperty, restaurantIdentifier, eventData) {
  if (!eventData) {
    eventData = 1;
  }
  mixpanel.track(eventProperty, {
    "category": eventCategory,
    "restaurant": restaurantIdentifier,
    "count": eventData
  });
}

export const CONSTANT = {
  VIEW_RESTAURANT: 'View Restaurant View',
  ADD_ITEMS: 'Add Items View',
  CHECKOUT: 'Checkout View',
  CONFIRM: 'Order Confirmation View',
  SEARCH_HOME: 'Search Home View',
  SEARCH_RESULTS: 'Search Results View'
};
