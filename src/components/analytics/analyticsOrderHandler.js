import {EVENTS, SCENARIOS, trackEvent} from './analyticalHandler';


export function handleClickOrderPlaceEvent(cart, restaurant) {
  if (cart.donationAmount > 0) {
    trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].CLICK_PLACE_ORDER_WITH_DONATION, restaurant, cart.donationAmount);
  } else {
    trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].CLICK_PLACE_ORDER_WITHOUT_DONATION, restaurant, 1);
  }
}

export function handleOrderPlacementSuccessEvent(result, restaurant) {
  if (result.donation && result.donation.donationId) {
    trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].ORDER_PLACED_WITH_DONATION, restaurant, result.donation.amount);
  } else {
    trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].ORDER_PLACED_WITHOUT_DONATION, restaurant, 1);
  }
}
