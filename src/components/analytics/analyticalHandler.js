/**
 * Created by Salinda on 03/02/18.
 * This file handles analytical events. All the scenarios/categories are listed in
 * SCENARIOS and all the events are listed in EVENTS. We have to make sure
 * no repeating events are mentioned in EVENTS as that might break the
 * Google analytics reports.
 *
 */
const analyticsUtils = require('./analyticsUtils');

export const SCENARIOS = {
  CHECKOUT_SCREEN: 'Checkout Screen',
  MENU_SCREEN: 'Menu Screen',
  ORDER_CONFIRMATION: 'Order Confirmation Screen',
  SIGN_IN: 'User Sign In',
  FORGOT_PASSWORD_PROCESS: 'Forgot Password Process',
  SEARCH_SCREEN: 'Restaurant Searching Screen',
  ERROR: 'Error'
};

let EVENTS = {};

EVENTS[SCENARIOS.CHECKOUT_SCREEN] = {
  CLICK_ON_SIGN_IN: 'Click on Sign In on email validation',
  EXISTING_CLICK_ON_PLACE_ORDER_NOT_LOGIN_IN: 'Existing Users Click on Place Order without login in',
  REGISTERED_CLICK_ON_PLACE_ORDER: 'Registered User Click on "Add New Card"',
  REGISTERED_CLICK_ON_SAVE_CARD: 'Registered User Click on "Save Card Info" check box',
  REGISTERED_CLICK_ON_REMOVE_CARD: 'Registered User Click on "Remove Card"',
  CLICK_ON_SEND_TEXT: 'Click on Send Text message text box',
  CLICK_PLACE_ORDER_WITH_DONATION : 'Click place order with donation',
  CLICK_PLACE_ORDER_WITHOUT_DONATION : 'Click place order without donation',
  ORDER_PLACED_WITH_DONATION: 'Order placed with donation',
  ORDER_PLACED_WITHOUT_DONATION: 'Order placed without donation'
};

EVENTS[SCENARIOS.MENU_SCREEN] = {
  CLICK_ON_REMOVE_SHOPPING_CART: 'Click on "Remove" in shopping cart',
  CLICK_ON_EDIT_SHOPPING_CART: 'Click on "Edit" in shopping cart',
  CLICK_ON_MENU_DROP_DOWN: 'Click on Session Drop-down & swap between sessions',
  CLICK_ON_DELIVERY_RADIO: 'Click on Delivery radio button',
  CLICK_ON_PICK_UP_RADIO: 'Click on Pickup radio button',
  CLICK_ON_ETA_DROP_DOWN: 'Click on ETA Drop down',
};

EVENTS[SCENARIOS.ORDER_CONFIRMATION] = {
  NEW_USER_CLICK_ON_CREATE_ACCOUNT: 'New User click on Create Account on Order Completion screen',
  CLICK_ON_GO_BACK: 'Click on "Go back to menu"',
};

EVENTS[SCENARIOS.SIGN_IN] = {
  CLICK_ON_SIGN_IN: 'User click Sign In to login to the application',
};

EVENTS[SCENARIOS.FORGOT_PASSWORD_PROCESS] = {
  CLICK_ON_FORGOT_PASSWORD: 'User access Forgot Password button',
};

EVENTS[SCENARIOS.SEARCH_SCREEN] = {
  CLICK_ON_REDO_SEARCH: 'User clicks Redo Search button',
  NAVIGATE_TO_RESTAURANT: 'User navigates to restaurant using search results'
};

EVENTS[SCENARIOS.ERROR] = {};

export {EVENTS};

/**
 * Tracks and event based on the provided category and event. The event should be matched
 * with the particular scenario or returns a error. A numeric value can be provided for the
 * eventData and this will be aggregated in the report with total and average values etc.
 *
 *
 * @param eventCategory
 * @param eventProperty
 * @param restaurant
 * @param eventData
 */
export function trackEvent(eventCategory, eventProperty, restaurant, eventData) {
  const restaurantIdentifier = analyticsUtils.constructRestaurantDetails(restaurant);
  analyticsUtils.trackGaEvent(eventCategory, eventProperty, restaurantIdentifier, eventData);
  analyticsUtils.trackMixPanelEvent(eventCategory, eventProperty, restaurantIdentifier, eventData);
}

/**
 * Tracks an error, only the error event, restaurant and error message are need to fire this event
 * @param eventAction
 * @param restaurant
 * @param errorDetails
 */
export function trackError(eventAction, restaurant, errorDetails) {
  const restaurantIdentifier = analyticsUtils.constructRestaurantDetails();
  analyticsUtils.trackGaEvent(SCENARIOS.ERROR, eventAction, restaurantIdentifier + " " + errorDetails, 1);
}
