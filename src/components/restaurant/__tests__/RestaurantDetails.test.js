import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import RestaurantDetails from '../RestaurantDetails';
import {ThemeProvider} from 'styled-components';
import theme from 'lib/ui/themes/cake';
import * as restaurantData from 'fixtures/restaurantOLO';
import * as hours from 'fixtures/hours';

const testProps = {
  restaurant: restaurantData,
  hours: hours
};

describe('src/componenets/restauraant/RestaurantDetails', () => {
  it('should render correctly', () => {
    const snapshot = renderer.create()
      .toJSON(<ThemeProvider theme={theme}>
        <RestaurantDetails {...testProps}/>
      </ThemeProvider>);
    expect(snapshot).toMatchSnapshot();
  });
});
