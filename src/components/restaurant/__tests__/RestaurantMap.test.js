import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import RestaurantMap from '../RestaurantMap';
import {ThemeProvider} from 'styled-components';
import theme from 'lib/ui/themes/cake';
import * as restaurantData from 'fixtures/restaurantOLO';

const testProps = {restaurant: restaurantData.default}

describe('src/components/restaurant/RestaurantMap', () => {
  it('should render correctly', () => {
    const snapshot = renderer.create(<ThemeProvider theme={theme}>
      <RestaurantMap {...testProps}/>
    </ThemeProvider>)
      .toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});


