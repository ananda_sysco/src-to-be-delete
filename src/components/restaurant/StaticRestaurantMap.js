/**
 * Created by Tharuka Jayalath on 01/24/2019
 */
import React, { Component } from 'react';
import styled from 'styled-components';

const MapImage = styled.img`
  max-width:100%;
  max-height:100%;
  border-bottom: 1px solid #ececec;
`;

export default class StaticRestaurantMap extends Component {

  render() {
    const {
      headerMap, mobileHeader,
      mapInfo: { sideMapURI, headerMapURI, mobileHeaderMapURI }
    } = this.props;

    let mapURI;
    if (mobileHeader) {
      mapURI = mobileHeaderMapURI;
    } else if (headerMap){
      mapURI = headerMapURI;
    } else {
      mapURI = sideMapURI;
    }
    return (<MapImage src={mapURI} alt={'Restaurant location on Google map'}/>);
  }
}
