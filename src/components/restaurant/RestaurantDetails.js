import {groupBy, map} from 'lodash';
import moment from 'moment-timezone';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {themed, Row, Col, Label, Paragraph, ToolTip, PhoneIcon} from 'lib/ui';
import * as AppPropTypes from 'utils/proptypes';
import {contains24Hours} from 'utils/toolbox';
import {Section} from 'lib/StyledUI';


const WrappedLabel = styled(Label)`
  word-break: initial !important;
  font-size: 12px;
  padding-top: 2px;
`;

const LabelHighlight = styled(Label)`
  display: inline-block;
  color: ${themed('colors.primary')};
  cursor: pointer;
  font-size: 14px;
`;
const PhoneLabelHighlight = styled(Label)`
  display: inline-block;
  color: #A8A7A4;
  cursor: pointer;
  font-size: 14px;
`;
const WebLabelHighlight = styled(Label)`
  display: inline-block;
  color: #A8A7A4;
  cursor: pointer;
  width: 100%;
  font-size: 14px;
`;
const PhoneLink = styled.a`
  display: inline-block;
  position: relative;
  margin-top: 4px;
  top: -4px;
  margin-left: 9px;
  color: #A8A7A4;
}
`;

const HoursLabel = styled(Label)`
  white-space: nowrap;
`;
const CuisinesLabel = styled(Label)`
  font-size: 14px;
`;
const DescriptionParagraph = styled(Paragraph)`
  font-size: 1rem;
  color: #5f5f5f;
  padding-left: 8px;
  font-family: "BrixSansLight"
`;
const RestaurantDetailSection = styled(Section)`
  padding-bottom: 0.15rem;
`;
const DescriptionSection = styled(Section)`
  padding-bottom: 0.15rem;
  margin-top: 25px;
  padding-top: 26px;
  border-top: 1px solid #F0EFEB;
`;

export default class RestaurantDetails extends Component {

  static propTypes = {
    restaurant: AppPropTypes.restaurant.isRequired,
    hours: PropTypes.array,
  };

  renderPhone() {
    const {phone} = this.props.restaurant;

    if (!phone) {
      return null;
    }

    const phoneNumber = `(${phone.areaCode}) ${phone.exchangeCode}-${phone.subscriberNumber}`;

    return (
      <RestaurantDetailSection>
        <Col xs={12} sm={10}>
        <PhoneIcon /><PhoneLink href={`tel:${phoneNumber.replace(/[^0-9]+/g, '')}`}><PhoneLabelHighlight>{phoneNumber}</PhoneLabelHighlight></PhoneLink>
        </Col>
      </RestaurantDetailSection>
    );
  }

  renderWebSite() {
    const {webaddress} = this.props.restaurant;

    if (!webaddress) {
      return null;
    }

    return (
      <RestaurantDetailSection>
        <Col xs={12}>
          <a href={webaddress}><WebLabelHighlight>{webaddress}</WebLabelHighlight></a>
        </Col>
      </RestaurantDetailSection>
    );
  }

  renderHours() {
    const {hours, restaurant} = this.props;

    // group hours by day
    const grouped = groupBy(hours, 'day');

    // get today's hours
    const slots = grouped[moment().tz(restaurant.timeZone).day() + 1];

    return (
      <RestaurantDetailSection>
        <Col xs={3} sm={4} md={3}>
          <WrappedLabel bold>HOURS:&nbsp;</WrappedLabel>
        </Col>
        <Col xs={9} sm={8} md={9}>
          <LabelHighlight data-tip="">
            {contains24Hours(slots) ? 'Open 24 Hours' :
              slots ? map(slots, (slot) => {
                const start = moment(slot.start, 'HH:mm:ss');
                const end = moment(slot.end, 'HH:mm:ss');
                const label = `${start.format('h:mm A')} - ${end.format('h:mm A')}`;

                return (
                  <span key={label}>
                  {label}<br/>
                </span>
                );
              }) : 'Closed'}
          </LabelHighlight>
        </Col>
      </RestaurantDetailSection>
    );
  }

  renderSlots = (slots) => {
    if (contains24Hours(slots)) {
      return <Label>Open 24 Hours</Label>
    }

    return map(slots, (slot) => {
      const start = moment(slot.start, 'HH:mm:ss');
      const end = moment(slot.end, 'HH:mm:ss');
      const label = `${start.format('h:mm A')} - ${end.format('h:mm A')}`;

      return <HoursLabel key={label}>{label}</HoursLabel>;
    });
  }

  renderHoursContent = () => {
    const {hours} = this.props;

    // group hours by day
    const grouped = groupBy(hours, 'day');

    // render sections
    const sections = map(grouped, (slots, day) => {
      const time = moment().day(day - 1);

      return (
        <RestaurantDetailSection key={day}>
          <Col xs={3}>
            <WrappedLabel bold>{time.format('ddd')}:&nbsp;</WrappedLabel>
          </Col>
          <Col xs={9}>
            {this.renderSlots(slots)}
          </Col>
        </RestaurantDetailSection>
      );
    });

    return (
      <div>
        {sections}
      </div>
    );
  }

  renderCuisines() {
    const {restaurant} = this.props;

    if (!restaurant.cuisines) {
      return null;
    }

    return (
      <RestaurantDetailSection>
        <Col xs={3} sm={4} md={3}>
          <WrappedLabel bold>CUISINES:&nbsp;</WrappedLabel>
        </Col>
        <Col xs={9} sm={8} md={9}>
          <CuisinesLabel>
            {restaurant.cuisines.map(cuisine => cuisine.name).join(', ')}
          </CuisinesLabel>
        </Col>
      </RestaurantDetailSection>
    );
  }

  renderDescription() {
    const {description} = this.props.restaurant;

    if (!description) {
      return null;
    }

    return (
      <DescriptionSection>
        <DescriptionParagraph>{description}</DescriptionParagraph>
      </DescriptionSection>
    );
  }

  render() {
    return (
      <Row>
        <Col xs={12}>
          {this.renderPhone()}
          {this.renderHours()}
          {this.renderCuisines()}
          {this.renderDescription()}
          {this.renderWebSite()}
        </Col>
        <ToolTip
          place="bottom"
          effect="solid"
          event="click"
          globalEventOff="click"
          getContent={this.renderHoursContent}
        />
      </Row>
    );
  }
}
