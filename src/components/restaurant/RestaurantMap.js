import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled, {withTheme} from 'styled-components';
import {MdLocationOn} from 'react-icons/lib/md';
import {startCase, toLower, upperCase} from 'lodash';
import {Label, Row, themed} from 'lib/ui';
import * as AppPropTypes from 'utils/proptypes';
import StaticRestaurantMap from './StaticRestaurantMap';

const MapSection = styled.div`
  position: relative;
  width:100%;
  height: 100%;
  overflow-y: hidden;
  overflow-x: hidden;
`;

const AddressRow = styled.div`
  padding: ${themed('layout.gutter.small')}rem;
  padding-top: ${themed('layout.gutter.xlarge')}rem;
  background: linear-gradient(
    transparent,
    ${themed('colors.foreground')}
  );
  position: absolute;
  bottom: 0;
  left: 0;
  width: calc(100% - ${({theme}) => theme.layout.gutter.small * 2}rem);
`;

const AddressLabel = styled(Label)`
  display: inline-block;
  text-shadow: 2px 2px 2px ${themed('colors.foreground')};
`;

const LocationIcon = styled(MdLocationOn)`
  margin-right: ${themed('layout.gutter.xsmall')}rem;
  font-size: ${themed('text.size.large')}rem;
  color: ${themed('colors.background')};
`;

export class RestaurantMap extends Component {

  static propTypes = {
    restaurant: AppPropTypes.restaurant.isRequired,
    theme: PropTypes.object.isRequired,
    compact: PropTypes.bool,
  };

  static defaultProps = {
    compact: false
  };

  renderAddress = () => {
    const {address} = this.props.restaurant;

    if (!address) {
      return null;
    }

    const {
      address1,
      address2,
      city,
      state,
    } = address;

    return (
      <AddressRow className="addressRow">
        <LocationIcon/>
        <AddressLabel inverted small>
          {`${startCase(toLower(address1))} ${startCase(toLower(address2))}, ${startCase(toLower(city))}, ${upperCase(state)}`}
        </AddressLabel>
      </AddressRow>
    );
  };

  render() {
    const { headerMap, mobileHeader, restaurant: { mapInfo } } = this.props;

    return (
      <Row>
        <MapSection>
          <div style={{height: 'inherit', width: 'inherit'}}>
            <StaticRestaurantMap headerMap={headerMap}
                                 mobileHeader={mobileHeader}
                                 mapInfo={mapInfo}
            />
          </div>
          {this.renderAddress()}
        </MapSection>
      </Row>
    );
  }
}

export default withTheme(RestaurantMap);
