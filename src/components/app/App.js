import {each, merge, set} from 'lodash';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled, {ThemeProvider} from 'styled-components';
import {Container} from 'lib/ui';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import cakeTheme from 'lib/ui/themes/cake';
import Loading from 'components/global/Loading';
import Notifications from 'components/global/Notifications';
import UserComponent from 'components/global/UserComponent';
import Footer from 'components/global/Footer';
import Routes from './router/Routes';
import InitializeStore from './router/InitializeStore';
import NotFoundPage from './pages/NotFoundPage';
import mixpanel from 'mixpanel-browser';
import config from '../../config';
import UserDetails from './pages/UserDetailsPage.js';


const AppContainer = styled(Container)`
  position: relative;
  padding: 0 !important;
  @media (max-width: 767px) {
    overflow-x: hidden;
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;  
  }
`;

export default class App extends Component {

  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      rehydrated: false,
    }
  }

  handleInit = () => {
    this.setState({rehydrated: true});
  };

  getThemeFromQuery() {
    const query = this.getRequest().getQuery();
    const result = {};

    each(query, (value, key) => {
      // check query form params starting with "theme."
      if (/^theme\./.test(key)) {
        set(result, key, value);
      }
    });

    return result.theme;
  }

  componentWillMount() {
    //region mixpanel
    mixpanel.init(config.mixpanelToken, {'cross_subdomain_cookie': false, 'cookie_expiration': 120});

    const tempPath = window.location.pathname.split('/');
    mixpanel.track("OLO_HOME_PAGE_LOADED", {
      "ID": tempPath.length > 0 ? tempPath[tempPath.length - 1] : 'null'
    });
    //endregion
    //Google Analytics
    window.ga('create', config.gaToken, 'auto');
  }

  render() {

    const theme = merge({}, cakeTheme);

    if (!this.state.rehydrated) {
      return (
        <BrowserRouter>
          <ThemeProvider theme={theme}>
            <Switch>
              <Route>
                <InitializeStore store={this.props.store} onInit={this.handleInit}>
                  <Loading/>
                </InitializeStore>
              </Route>
              <Route component={NotFoundPage}/>
            </Switch>
          </ThemeProvider>
        </BrowserRouter>
      );
    }

    return (
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Provider store={this.props.store}>
            <AppContainer fluid className="appContainer">
              <Notifications/>
              <Routes/>
              <UserComponent/>
              <UserDetails/>
              <Footer/>
            </AppContainer>
          </Provider>
        </ThemeProvider>
      </BrowserRouter>
    );
  }
}
