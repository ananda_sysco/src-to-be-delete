import React from 'react';
import {Route, Switch} from 'react-router-dom';
import ScrollToTop from './ScrollToTop';
import RestaurantPage from '../pages/RestaurantPage';
import NotFoundPage from '../pages/NotFoundPage';
import MenuPage from '../pages/MenuPage';
import CheckoutPage from '../pages/CheckoutPage';
import WalletPage from '../pages/wallet/WalletPage';
import ConfirmationPage from '../pages/ConfirmationPage';
import SearchPage from '../pages/search/SearchPage';
import ReceiptPage from '../pages/ReceiptPage'

export default function Routes() {
  return (
    <ScrollToTop>
      <Switch>
        <Route exact path="/" component={SearchPage}/>
        <Route exact path="/balance" component={WalletPage}/>
        <Route exact path="/receipt/:id" component={ReceiptPage}/>
        <Route path="/:id">
          <RestaurantPage>
            <Switch>
              <Route exact path="/:id" component={MenuPage}/>
              <Route exact path="/:id/checkout" component={CheckoutPage}/>
              <Route exact path="/:id/confirmation" component={ConfirmationPage}/>
              <Route component={NotFoundPage}/>
            </Switch>
          </RestaurantPage>
        </Route>
        <Route component={NotFoundPage}/>
      </Switch>
    </ScrollToTop>
  );
}
