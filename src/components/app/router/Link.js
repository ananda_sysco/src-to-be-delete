import { omit } from 'lodash';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { themed, when } from 'lib/ui';

const full = css`
  display: flex;
  flex: 1;
`;

// strip props
const RLink = (props) => <RouterLink {...omit(props, ['full'])} />;

export default styled(RLink)`
  text-decoration: none;
  color: ${themed('colors.primary')};
  ${when('full', full)};

  &:hover {
    opacity: 0.7;
  }
`;
