import {Component} from 'react';
import PropTypes from 'prop-types';
import {definePrefix, persist, reset} from 'store/utils/persist';
import {withRouter} from 'react-router';

class InitializeStore extends Component {

  static propTypes = {
    onInit: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const {location} = this.props;
    const pathname = location && location.pathname ? location.pathname : null;
    let restaurantId;

    if (pathname) {
      const tempPath = pathname.split('/');
      restaurantId = tempPath.length >= 2 && !isNaN(parseInt(tempPath[1], 10)) ? tempPath[1] : null;
    }

    if (restaurantId) {
      definePrefix(restaurantId);
    } else {
      definePrefix('consumer');
    }

    persist(this.props.store, this.props.onInit);

    if (!(process.env.NODE_ENV === 'production')) {
      window.reset = () => {
        reset().then(() => window.location.reload());
      }
    }
  }

  render() {
    return this.props.children;
  }
}

export default withRouter(InitializeStore);
