import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import styled from 'styled-components';
import backgroundImage from 'assets/404-bg.png';
import { Heading, Paragraph } from 'lib/ui';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10rem;
  width: 100%;
  height: 100%;
  background: url(${backgroundImage}) no-repeat center center;
  background-size: cover;
`;

export default class MerchantNotActivePage extends Component {

  render() {
    return (
      <DocumentTitle title="Restaurant is closed">
        <Wrapper>

          <Heading xxlarge>Oops!</Heading>
          <Paragraph>Restaurant is closed for ordering.</Paragraph>
        </Wrapper>
      </DocumentTitle>
    );
  }
}
