/* eslint-disable import/no-named-as-default */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Grid, Row, themed} from 'lib/ui';
import styled from 'styled-components';
import OrderConfirmation from 'components/checkout/OrderConfirmation';
import RestaurantDetails from 'components/restaurant/RestaurantDetails';
import RestaurantMap from 'components/restaurant/RestaurantMap';
import Sidebar from 'components/global/Sidebar';
import Content from 'components/global/Content';
import * as AppPropTypes from 'utils/proptypes';
import {restaurantHoursSelector} from 'store/utils/selectors';
import SignupSection from 'components/user/SignupSection';
import {clearSignUpState} from 'store/user';
import {scrollToElement} from "../../../utils/commonUtil";
import GlobalHeader from "../../global/GlobalHeader";

const SignupWrapper = styled.div`
  padding: ${themed('layout.gutter.small')}rem;
  padding-top: 30px;
  @media (max-width: 767px) {
    padding-top: 10px;
  }
`;
const MobileRestaurantDetailContainer = styled.div`
    display: none;
    -webkit-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    -moz-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    padding-bottom: 20px;
    margin-top: -11px;
    @media (max-width: 767px) {
        height: calc(100vh - 80px);            
        display: block;
        position: absolute;
        background-color: #fff;
        z-index: 4;
    }
`;
const CloseLink = styled.a`
  font-family: BrixSansLight;
  font-size: 0.9rem;
  padding-bottom: 10px;
  text-align: center;
  width:106px;
  display: flex;
  margin-top: 30px;
  justify-content: center;
  margin-left: auto;
  margin-right: auto;
  &:focus {
    outline: none;
  }
`;

export class CheckoutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    hours: PropTypes.array.isRequired,
    cart: AppPropTypes.cart,
    user: PropTypes.any,
    currentUser: PropTypes.any,
    hasAccount: PropTypes.bool,
    isPhoneNumberValid: PropTypes.bool,
    clearSignUpState: PropTypes.func.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.user && !nextProps.user) {
      return this.props.history.push('/' + this.props.restaurant.accountId);
    }
  }

  handleClick() {
    this.setState({
      condition: !this.state.condition
    });
  }

  componentDidMount() {
    scrollToElement('.restaurantDetailContainer');
  }

  render() {
    const { restaurant, hours, user, cart, hasAccount, currentUser, isPhoneNumberValid, isMobile } = this.props;
    const { condition } = this.state;
    const showCart = restaurant.isCakeOloAvailable && !cart.empty;

    return (
      <Grid className="confirmationPage">
        <GlobalHeader customTitle={"Order Sent"} subHead={restaurant.name}/>
        <Row>
          <Content>
            {/* <a ref="btn" onClick={this.handleClick.bind(this)} className="showOnMobile infoIcon"
               role="button"><InfoIcon/></a> */}
            {
              condition &&
              <MobileRestaurantDetailContainer className={"showOnMobile"}>
                <RestaurantMap restaurant={restaurant} compact={showCart}/>
                <RestaurantDetails restaurant={restaurant} hours={hours}/>
                <CloseLink role="button" onClick={this.handleClick.bind(this)}>Close Details</CloseLink>
              </MobileRestaurantDetailContainer>
            }
            <div className="confirmation-content">
              <OrderConfirmation restaurant={restaurant}/>
              <SignupWrapper>{(!hasAccount && !user && currentUser && isPhoneNumberValid) &&
              <SignupSection/>}</SignupWrapper>
            </div>
          </Content>
          <Sidebar>
            { !isMobile &&
              <div>
                <RestaurantMap restaurant={restaurant}/>
                <RestaurantDetails restaurant={restaurant} hours={hours}/>
              </div>
            }
          </Sidebar>
        </Row>
      </Grid>
    );
  }

  componentWillUnmount() {
    this.props.clearSignUpState();
  }
}


function select(state) {
  return {
    restaurant: state.app.restaurantInfo,
    hours: restaurantHoursSelector(state),
    cart: state.cart,
    user: state.user.loggedInUser,
    currentUser: state.user.currentUser,
    hasAccount: state.user.hasAccount,
    isPhoneNumberValid: state.user.isPhoneNumberValid,
    isMobile: state.app.isMobile
  };
}

export default connect(select, {clearSignUpState})(CheckoutPage);
