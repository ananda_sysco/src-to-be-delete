import styled, { keyframes } from 'styled-components';
import { Button } from 'lib/ui';

export const HOME_ROUTE = '/';

export const maxHeightKeyframes = keyframes`
0% {
  max-height: 0px;
}
100% {
  max-height: calc(100vh - 80px);
}
`;
export const MobileRestaurantDetailContainer = styled.div`
    display: none;
    -webkit-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    -moz-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    padding-bottom: 20px;
    margin-top: -11px;
    @media (max-width: 767px) {
        display: block;
        position: absolute;
        background-color: #fff;
        z-index: 4;
        width: 100%;
        overflow: visible;
        animation: ${maxHeightKeyframes} 0.25s ease-out;
        height: 105%;
    }
`;
export const CloseButton = styled(Button)`
    margin: 40px auto 20px auto;
    display: block !important;
`;
