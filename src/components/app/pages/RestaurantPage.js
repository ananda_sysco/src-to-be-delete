/* eslint-disable import/no-named-as-default */
import {get} from "lodash";
import React, {Component, Fragment} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import DocumentTitle from "react-document-title";
import * as AppPropTypes from "utils/proptypes";
import {getRestaurant} from "store/app";
import {logoutUser} from "store/user";
import Loading from "components/global/Loading";
import NotFoundPage from "./NotFoundPage";
import MerchantNotActivePage from "./MerchantNotActivePage";
import InternalErrorPage from "./InternalErrorPage";
import MerchantWithIncompleteData from "./MerchantWithIncompleteData";
import {CONSTANT, constructRestaurantDetails, trackEvent, trackPageView} from '../../analytics/analyticsUtils';
import AuthenticationSection from "components/user/AuthenticationSection";


export class RestaurantPage extends Component {

  static propTypes = {
    match: PropTypes.object,
    restaurant: AppPropTypes.restaurant,
    restaurantError: PropTypes.any,
    session: PropTypes.string,
    getRestaurant: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const accountId = get(this.props.match, 'params.id');

    if (accountId) {
      this.props.getRestaurant(accountId);
    }
    trackPageView(CONSTANT.VIEW_RESTAURANT);
  }

  componentWillReceiveProps(nextProps) {
    const {restaurant} = this.props;
    if (restaurant) {
      trackPageView('Restaurant page of ' + constructRestaurantDetails(restaurant));
    }
  }


  render() {
    const {restaurant, restaurantError, session, children} = this.props;
    if (restaurant) {
      if (!restaurant.active) {
        trackEvent(CONSTANT.VIEW_RESTAURANT, 'Try to order from inactive merchant' + constructRestaurantDetails(restaurant));
        return <MerchantNotActivePage/>;
      }
      if (!restaurant.sessions || restaurant.sessions.length === 0) {
        return <MerchantWithIncompleteData/>;
      }
    }

    if (!restaurant && restaurantError) {
      if (restaurantError === 404) {
        trackEvent(CONSTANT.VIEW_RESTAURANT, 'Merchant Not Found');
        return <NotFoundPage/>;
      } else {
        trackEvent(CONSTANT.VIEW_RESTAURANT, 'Internal Error');
        try {
          this.props.logoutUser(true);
        } catch (e) {
          //DO nothing
        }
        return <InternalErrorPage/>;
      }
    }

    if (!restaurant || !session || (restaurant.accountId !== this.props.match.params.id)) {
      return <Loading/>;
    }

    const parts = [
      get(restaurant, 'name'),
      get(restaurant, 'address.city'),
      'Online Ordering',
    ];

    const title = parts.filter(val => !!val).join(' | ');
    return (
      <DocumentTitle title={title}>
        <Fragment>
          <AuthenticationSection/>
          {children}
        </Fragment>
      </DocumentTitle>
    );
  }
}

function select(state) {
  return {
    restaurant: state.app.restaurantInfo,
    restaurantError: state.app.restaurantError,
    session: state.app.selectedSession,
    isDrawerOpen: state.user.isDrawerOpen
  };
}

export default connect(select, {getRestaurant, logoutUser})(withRouter(RestaurantPage));
