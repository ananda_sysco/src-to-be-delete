import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import config from 'config';

const maxZoomLevel = 14;

class GoogleMap extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: maxZoomLevel
  };

  constructor(props) {
    super(props);
    this.map = null;
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.updateBounds && nextProps.updateBounds && nextProps.bounds) {
      if (this.map && this.map.map_) {
        this.map.map_.fitBounds(nextProps.bounds);
      }
    }
  }

  render() {
    return (
      <div style={{height: 'inherit', width: '100%'}}>
        <GoogleMapReact
          bootstrapURLKeys={{key: config.googleApiKey}}
          {...this.props}
          yesIWantToUseGoogleMapApiInternals={true}
          ref={(ref) => this.map = ref}
        >
          {this.props.children}
        </GoogleMapReact>
      </div>
    );
  }
}

export default GoogleMap;
