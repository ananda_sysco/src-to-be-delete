/**
 * Created by Tharuka Jayalath on 08/28/2018
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from 'react-modal';
import Loading from 'components/global/Loading';
import {Col, Heading, themed} from 'lib/ui';
import {modalStyle, Section} from 'lib/StyledUI';

const StyledModal = styled(Modal)`  
  margin: 5rem auto 0 auto;
  margin-top: 14%;
  max-width: 34rem;
  padding: ${themed('layout.gutter.xlarge')}rem;
  background: #F7F7F7;
  overflow: auto;
  -webkit-overflow-scrolling: touch;
  outline: none;
  text-align: center;
`;

export default class SearchResultsLoadingModal extends Component {

  static propTypes = {
    isOpen: PropTypes.bool,
  };

  static defaultProps = {
    isOpen: false,
  };

  render() {
    return (
      <StyledModal
        contentLabel="Results Loading"
        isOpen={this.props.isOpen}
        style={modalStyle}
      >
        <Col xs={12}>
          <Section center="xs">
            <Loading />
          </Section>
          <Section center="xs">
            <Heading xsmall>Thank you for your patience while we process the results.</Heading>
          </Section>
        </Col>
      </StyledModal>
    );
  }
}

