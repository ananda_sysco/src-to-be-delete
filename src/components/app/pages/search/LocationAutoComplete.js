import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import PlacesAutocomplete from 'react-places-autocomplete';
import {FormInput} from 'lib/StyledUI';
import {searchOptions} from './SearchCommon';

const SearchInputWrapper = styled.div`
display: inline-block;
width: 75%;
@media (max-width: 767px)
{
    width: 100%;
}
`;
const SearchInput = styled(FormInput)`
width: 100%;
height: 42px;
box-sizing: border-box;
margin-bottom: 0px;
display: inline;
padding-left: 65px;
`;
const PlaceholderStateInput = styled.div`
width: 100%;
box-sizing: border-box;
font-family: BrixSansMedium;
color: #363532;
display: inline-block;
padding: 0.5rem 0px 0.5rem 0.5rem;
font-size: 0.875rem;
outline: none;
line-height: 24px;
text-align: left;

top: 2px;
position: absolute;
width: 65px;
left: 0px;
span {
    color: #A8A7A4;
    margin-left: 10px;
}
`;
export default class LocationAutoComplete extends Component {
  static propTypes = {
    searchOptions: PropTypes.any,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    value: PropTypes.any,
    searchResultsPending: PropTypes.bool,
    shouldAutoFocus: PropTypes.bool
  };

  componentDidMount() {
    if (this.props.shouldAutoFocus) {
      const elements = document.getElementsByClassName("location-search-input");
      if (elements.length > 0) {
        elements[0].focus();
      }
    }
  }

  render() {
    return (
      <PlacesAutocomplete {...this.props} searchOptions={searchOptions}>
        {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
          <SearchInputWrapper className="searchInputWrapper">
            <SearchInput {...getInputProps({
              placeholder: 'Street Address, City or Zip Code',
              className: 'location-search-input',
              disabled: this.props.searchResultsPending,
              onFocus: this.props.onFocus,
              onBlur: this.props.onBlur
            })}/>
            <div className="autocomplete-dropdown-container">
              {loading && <div className="loadingIndicator">Loading...</div>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item';
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? {backgroundColor: '#fafafa', cursor: 'pointer'}
                  : {backgroundColor: '#ffffff', cursor: 'pointer'};
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
            <PlaceholderStateInput className="searchPlaceholder">I'm Near</PlaceholderStateInput>
          </SearchInputWrapper>
        )}

      </PlacesAutocomplete>
    );
  }

}
