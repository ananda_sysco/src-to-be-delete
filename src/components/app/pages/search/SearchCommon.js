export const searchOptions = {
  types: ['geocode'],
  componentRestrictions: {country: ['US']}
};
