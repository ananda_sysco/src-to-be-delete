import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Heading} from 'lib/ui';
import {connect} from "react-redux";

const BALLOON_HEIGHT = 75;
const BALLOON_WIDTH = 200;
const pointerStyle = {cursor: 'pointer'};

export class MarkerComponent extends Component {

  static propTypes = {
    $dimensionKey: PropTypes.any,
    $getDimensions: PropTypes.func,
    $geoService: PropTypes.any,
    id: PropTypes.any,
    lat: PropTypes.number,
    lng: PropTypes.number,
    name: PropTypes.string,
    address: PropTypes.string,
    key: PropTypes.any,
    restaurant: PropTypes.bool,
    number: PropTypes.number,
    activeRestaurant: PropTypes.any,
    onClick: PropTypes.func
  };


  static getMarkerStyle() {
    return {
      border: '2px solid #fff',
      'background-color': '#F2700F',
      width: 19,
      height: 19,
      'border-radius': '50%',
      'text-align': 'center',
      'box-sizing': 'border-box',
      'font-size': '9px',
      'font-family': "BrixSansLight",
      color: '#fff',
      'padding-top': '2px',
      '-webkit-box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)',
      '-moz-box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)',
      'box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)'
    };
  }

  static getBalloonStyle(markerDim, mapWidth) {
    const marginLeft = MarkerComponent.calculateMarginLeft(markerDim, mapWidth);
    if (markerDim.y > BALLOON_HEIGHT) {
      return MarkerComponent.getBalloonTopStyle(marginLeft);
    } else {
      return MarkerComponent.getBalloonBottomStyle(marginLeft);
    }
  }

  static calculateMarginLeft(markerDim, mapWidth) {
    let marginLeft = -160;
    const halfOfBalloonWidth = BALLOON_WIDTH / 2;
    if (markerDim.x < (halfOfBalloonWidth)) {
      marginLeft = -75 - markerDim.x;
    } else if ((mapWidth - halfOfBalloonWidth) < markerDim.x) {
      const remainingSpace = mapWidth - markerDim.x;
      marginLeft = -270 + remainingSpace;
    }
    return marginLeft;
  }

  static getBalloonTopStyle(marginLeft) {
    return {
      position: 'absolute',
      width: `${BALLOON_WIDTH}px`,
      height: `${BALLOON_HEIGHT}px`,
      'margin-top': '-101px',
      'margin-left': `${marginLeft}px`,
      padding: '15px',
      'box-sizing': 'border-box',
      border: '1px solid rgb(216, 215, 211, 0.5)',
      'background-color': '#fff',
      'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.2)',
      left: '70px',
      'z-index': 9999
    };
  }

  // balloon shown bottom
  static getBalloonBottomStyle(marginLeft) {
    return {
      position: 'absolute',
      width: `${BALLOON_WIDTH}px`,
      height: `${BALLOON_HEIGHT}px`,
      'margin-top': '15px',
      'margin-left': `${marginLeft}px`,
      padding: '15px',
      'box-sizing': 'border-box',
      border: '1px solid rgb(216, 215, 211, 0.5)',
      'background-color': '#fff',
      'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.2)',
      left: '70px',
      'z-index': 9999
    };
  }

  // pointer pointing top
  static getBalloonPointerTopStyle() {
    return {
      top: '20px',
      left: '-1px'
    };
  }

  // pointer pointing bottom
  static getBalloonPointerBottomStyle() {
    return {
      top: '-10px',
      left: '-1px',
    };
  }

  isActive() {
    return this.props.activeRestaurant === this.props.id;
  }

  onNotificationClicked() {
    this.props.onNotificationClicked(this.props.id);
  }

  static showBalloonAtTop(positionY) {
    return positionY > BALLOON_HEIGHT;
  }

  getActiveRestaurantStyle(markerDim, mapWidth) {
    return <div style={pointerStyle} onClick={this.onNotificationClicked.bind(this)}>
      {MarkerComponent.showBalloonAtTop(markerDim.y) ? '' :
        <div className={'mapPointerTop'} style={MarkerComponent.getBalloonPointerTopStyle()}/>}
      <div style={MarkerComponent.getBalloonStyle(markerDim, mapWidth)}>
        <Heading>{this.props.name}</Heading>
        <p>{this.props.address}</p>
      </div>
      {MarkerComponent.showBalloonAtTop(markerDim.y) ?
        <div className={'mapPointerBottom'} style={MarkerComponent.getBalloonPointerBottomStyle()}/> : ''}
    </div>
  }

  render() {
    const mapWidth = this.props.$geoService.getWidth();
    const markerDim = this.props.$getDimensions(this.props.$dimensionKey);

    return <div className={'MapOverlay'}
                style={MarkerComponent.getMarkerStyle()}>
      {this.props.number}{(this.isActive()) ? this.getActiveRestaurantStyle(markerDim, mapWidth) : ''}
    </div>
  }

}

function select(state) {
  return {
    activeRestaurant: state.search.activeRestaurant,
  };
}

const actions = {};

export default connect(select, actions)(MarkerComponent);
