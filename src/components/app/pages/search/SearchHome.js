import React, {Component} from 'react';
import styled from 'styled-components';
import {Button, CakeLogo, Col, Grid, Heading, Label, Row} from 'lib/ui';
import GlobalHeader from '../../../global/GlobalHeader';
import PropTypes from "prop-types";
import * as AppPropTypes from 'utils/proptypes';
import {geocodeByAddress, getLatLng} from 'react-places-autocomplete';
import {doSearch} from 'store/search';
import {createErrorNotification} from 'store/productModal';
import {clearNotifications} from "store/app";
import {getPasswordResetUser} from 'store/user';
import {connect} from "react-redux";
import queryString from 'query-string';
import LocationAutoComplete from "./LocationAutoComplete";
import SearchResultsLoadingModal from "./SearchResultsLoadingModal";
import {validateIsLatLng} from "../../../../utils/commonUtil";
import {CONSTANT, trackPageView} from '../../../../components/analytics/analyticsUtils';


const SearchRow = styled(Row)`
background-image: url(${require(`../../../../assets/hero.png`)});
background-size: cover;
background-repeat: no-repeat;
min-height: calc(100vh - 115px);
.cakeLogo{
    margin-top: 18px;
    margin-left: 0px;
    width:138px;
}
@media (max-width: 767px)
{
    background-image: none;
}
`;
const SearchContent = styled(Col)`
`;
const SearchHeader = styled(Heading)`
font-family: "DuplicateSlabThin";
color: #fff;
font-size: 60px;
text-align:center;
margin-top: 140px;
margin-bottom: 32px;
@media (max-width: 767px)
{
    font-size: 32px;
    margin-top: 42px;
    margin-bottom: 62px;
    color: #5F5F5F;
}
`;
const SearchButton = styled(Button)`
display: inline-block;
vertical-align: top;
margin-left: 5px;
padding-top: 12px;
padding-bottom: 13px;
width: 104px;
flex: 0 1 auto;
font-size: 14px;
font-family: "BrixSansLight";
font-weight: bold;
@media (max-width: 767px)
{
    display: block;
    margin-top: 20px;
    margin-bottom: 50px;
    margin-left: 0px;
    width: 100%;
}
`;

const SearchDescription = styled(Label)`
font-family: "BrixSansLight";
font-size: 18px;
display: block;
text-align: center;
color: #fff;
padding-top: 35px;
`;

const SearchFieldContainer = styled.div`
 margin: 0 auto;
 max-width: 665px;
`;
const localStorageKey = 'olo_address';

export class SearchHome extends Component {

  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    hours: PropTypes.array,
    cart: AppPropTypes.cart,
    user: PropTypes.any,
    searchBarVisible: PropTypes.boolean,

  };

  constructor(props) {
    super(props);
    this.state = {
      address: localStorage ? localStorage.getItem(localStorageKey) : null,
      shouldAutoFocus: false,
      tempAddress: ''
    };
  }

  componentDidMount() {
    const searchQuery = window.location.search || "";
    const parsed = queryString.parse(searchQuery);
    if (parsed.resetPassword && parsed.token) {
      this.props.getPasswordResetUser(parsed.token);
    }
    const {href} = window.location;
    if (href && href.includes('#search')) {
      window.history.replaceState(null, null, window.location.pathname);
    }
    trackPageView(CONSTANT.SEARCH_HOME);
  }

  onLogoClicked() {
    window.location.reload();
  }


  handleAddressChange = (address) => this.setState({address});

  handleOk = async () => {
    const address = this.state.address;
    const result = validateIsLatLng(address);
    let latLng;
    if (result.isLatLng) {
      latLng = result.latLng;
    } else {
      latLng = await geocodeByAddress(this.state.address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
          return latLng;
        })
        .catch(error => console.error('Error', error));
    }
    if (!latLng) {
      this.props.clearNotifications();
      this.props.createErrorNotification('Please enter a valid location.');
      return;
    }
    this.props.doSearch(latLng, address);
    if (localStorage) {
      localStorage.setItem(localStorageKey, this.state.address);
    }
    this.props.clearNotifications();
  };

  onClickSearchBar() {
    this.setState({searchBarVisible: true, shouldAutoFocus: true});
  }

  // NOTE: `placeId` is null when user hits Enter key with no suggestion item selected.
  handleSelect(address, placeId) {
    if (!placeId) {
      this.handleOk();
    } else {
      this.setState({address});
    }
  }

  handleFocus() {
    this.setState({tempAddress: this.state.address, address: ''});
  }

  handleBlur() {
    const {address, tempAddress} = this.state;
    if (address !== '' && address !== tempAddress) {
      this.setState({tempAddress: '', address: address});
    } else {
      this.setState({tempAddress: '', address: tempAddress});
    }
  }

  render() {
    return (
      <Grid fluid className="searchContainer searchHome">
        <GlobalHeader onBackClicked={this.onLogoClicked}/>
        <SearchRow center="xs">
          <SearchContent xs={10}>
            <div className="showOnMobile">
              <CakeLogo/>
            </div>
            <SearchHeader>Find the best food around you.</SearchHeader>
            <SearchFieldContainer>
              <LocationAutoComplete onChange={this.handleAddressChange.bind(this)}
                                    value={this.state.address}
                                    searchResultsPending={this.props.searchResultsPending}
                                    onSelect={this.handleSelect.bind(this)}
                                    onFocus={this.handleFocus.bind(this)}
                                    onBlur={this.handleBlur.bind(this)}
                                    shouldAutoFocus={this.state.shouldAutoFocus}/>
              <SearchButton primary full onPress={this.handleOk.bind(this)}
                            disabled={this.props.searchResultsPending}>Search</SearchButton>
            </SearchFieldContainer>
            <SearchDescription className="hideFromMobile">You're hungry. We make it easy to order food online for
              pick up or delivery.</SearchDescription>
          </SearchContent>
        </SearchRow>
        <SearchResultsLoadingModal isOpen={this.props.searchResultsPending}/>
      </Grid>
    );
  }

}

function select(state) {
  return {
    searchResults: state.search.results,
    location: state.search.location,
    address: state.search.address,
    searchResultsPending: state.search.searchResultsPending
  };
}

const actions = {doSearch, createErrorNotification, clearNotifications, getPasswordResetUser};
export default connect(select, actions)(SearchHome);
