import React, {Component} from 'react';

export class DefaultMarkerComponent extends Component {

  static getMarkerStyle() {
    return {
      border: '2px solid #fff',
      'background-color': '#F2700F',
      width: 19,
      height: 19,
      'border-radius': '50%',
      'text-align': 'center',
      'box-sizing': 'border-box',
      'font-size': '9px',
      'font-family': "BrixSansLight",
      color: '#fff',
      'padding-top': '2px',
      '-webkit-box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)',
      '-moz-box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)',
      'box-shadow': '0px 2px 35px -8px rgba(0, 0, 0, 0.46)'
    };
  }

  render() {
    return <div>
      {this.props.isHeaderMarker ?
        <div style={DefaultMarkerComponent.getMarkerStyle()}/> :
        <img width={45} height={40} src={require('../../../../assets/googleMarker.png')}
             alt={'Background of the Search Home Page'}/>
      }
    </div>;
  }
}

export default DefaultMarkerComponent;
