import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Loading from 'components/global/Loading';
import {join} from 'lodash';
import {Button, Grid, Heading, ListIcon, MapIcon, Row} from 'lib/ui';
import GlobalHeader from '../../../global/GlobalHeader';
import GoogleMap from './GoogleMap';
import RestaurantListItem from './SearchItem';
import {connect} from "react-redux";
import LocationAutoComplete from "./LocationAutoComplete";
import InfiniteScroll from 'react-infinite-scroller';
import {
  doSearch,
  loadNextPage,
  resetSearch,
  setActiveRestaurant,
  setPage,
  switchMapView,
  setDecodedAddress,
  setDisplayRedoSearchButton
} from '../../../../store/search';
import {clearCart} from 'store/cart';
import {createErrorNotification} from 'store/productModal';
import {resetStore, clearNotifications} from 'store/app';
import {geocodeByAddress, getLatLng} from 'react-places-autocomplete';
import MarkerComponent from "./MarkerComponent";
import DefaultMarkerComponent from "./DefaultMarkerComponent";
import {isMobile, validateIsLatLng} from "../../../../utils/commonUtil";
import SearchResultsLoadingModal from "./SearchResultsLoadingModal";
import {CONSTANT, trackPageView} from '../../../../components/analytics/analyticsUtils';
import {EVENTS, SCENARIOS, trackEvent} from "../../../../components/analytics/analyticalHandler";

/* global google*/
const SearchRow = styled(Row)`
min-height: calc(100vh - 115px);
`;
const RestaurantContainer = styled.div`
display: flex;
flex-direction: row;
padding-left: 0.5rem;
padding-right: 0.5rem;
`;

const RestaurantListWrapper = styled.div`
display: flex;
flex-direction: column;
width: 345px;
border-right:1px solid rgb(216, 215, 211, 0.5);
@media (max-width: 767px)
{
    width: 100vw;
    border-right:0px;
}
`;
const MapContainer = styled.div`
width: calc(100vw - 345px);
height: calc(100vh - 115px);
display: flex;
@media (max-width: 767px)
{
    width: 100%;
    height: ${props => props.isFullScreen ? 'calc(100vh - 109px)' : 'calc(100vh - 308px)'};
}
`;
const SearchButton = styled(Button)`
display: inline;
margin-left: 5px;
padding-top: 7px;
padding-bottom: 7px;
width: 78px;
flex: 0 1 auto;
font-size: 13px;
font-family: "BrixSansLight";
font-weight: bold; 
margin-left:20;
@media (max-width: 767px)
{
    font-size: 1rem;
    padding: 0.5rem;
}

`;
const RedoSearchButton = styled(Button)`
display: inline;
position: absolute;
bottom: 88px;
margin-left: 5px;
padding-top: 7px;
padding-bottom: 7px;
width: 278px;
flex: 0 1 auto;
font-size: 13px;
font-family: "BrixSansLight";
font-weight: bold;
@media (max-width: 767px)
{
    font-size: 0.85rem;
    padding: 0.5rem;
    z-index: 999;
    left: 4%;
    width: 90%;
    bottom: 30px;
}

`;

// Search Results
const RestaurantFilter = styled.div`
background-color: #F0EFEB;
padding: 12px 13px;
`;
const MobileMapContainer = styled.div`
width: 100%;
}
`;

const RestaurantList = styled.div`
height: calc(100vh - 169px);
overflow-y:auto;
@media (max-width: 767px)
{
    height: auto;
    overflow:visible;
}
`;
const NoResultsLabel = styled(Heading)`
font-size: 1.3rem;
padding: 40px 10px 10px 10px;
text-align: center;
`;


const LoadingWrapper = styled.div`
  padding:100px;
`;

const localStorageKey = 'olo_address';

const loader = <LoadingWrapper><Loading/></LoadingWrapper>;

export class SearchResults extends Component {
  static propTypes = {
    isMapView: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      address: this.props.address ? this.props.address :
        (localStorage ? localStorage.getItem(localStorageKey) : null),
      isSearchPressed: false,
      isRedoSearchPressed: false,
      isMobile: false,
      tempAddress: '',
      bounds: null,
      updateBounds: false
    };

    this.dragListner = null;
  }

  onAddressChange(address) {
    this.setState({address});
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.searchResultsPending && !nextProps.searchResultsPending) {
      if (this.state.isRedoSearchPressed) {
        this.setState({isRedoSearchPressed: false});
      } else if (this.state.isSearchPressed) {
        this.setState({isSearchPressed: false});
      }
    }

    const {address} = nextProps;
    if (address && address !== this.props.address) {
      this.setState({address: address});
      if (localStorage) {
        localStorage.setItem(localStorageKey, address);
      }
    }

    const {searchResults} = this.props;

    if (this.props.searchResultsPending && !nextProps.searchResultsPending && searchResults) {
      let bounds;
      if (nextProps.searchResults.length <= this.props.searchResults.length) { // performed a redo search or search button pressed
        bounds = new google.maps.LatLngBounds();
        for (let i = 0; i < nextProps.searchResults.length; i++) {
          const temp = nextProps.searchResults[i];
          bounds.extend(new google.maps.LatLng(temp.lat, temp.lon));
        }
        bounds.extend(new google.maps.LatLng(this.props.location.lat, this.props.location.lng));
      } else {// scrolled the restaurant list
        bounds = this.state.bounds;
        for (let i = (nextProps.searchResults.length - this.props.searchResults.length); i < nextProps.searchResults.length; i++) {
          const temp = nextProps.searchResults[i];
          bounds.extend(new google.maps.LatLng(temp.lat, temp.lon));
        }
      }
      this.setState({updateBounds: true, bounds: bounds});
    } else {
      this.setState({updateBounds: false});
    }
  }

  onHashChange(e) {
    if (e.oldURL.includes('#search') && !e.newURL.includes('#search')) {
      this.props.resetSearch();
    }
  }

  /**
   * Calculate & Update state of new dimensions, whether its mobile or not
   */
  updateMobileStatus() {
    this.setState({isMobile: isMobile()});
  }

  componentDidMount() {
    this.setState({isMobile: isMobile()});
    window.addEventListener("resize", this.updateMobileStatus.bind(this));
    window.addEventListener("hashchange", this.onHashChange.bind(this), false);
    window.location = '#search';
    trackPageView(CONSTANT.SEARCH_RESULTS);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateMobileStatus.bind(this));
    window.removeEventListener("hashchange", this.onHashChange.bind(this), false);
    if (this.dragListner) {
      google.maps.event.removeListener(this.dragListner);
    }
  }

  onMapLoaded({map, maps}) {
    if (this.props.searchResults && this.props.searchResults.length > 0) {
      //region to zoom according to the set of locations
      this.setState({isBoundsSet: false}, () => {
        const bounds = new google.maps.LatLngBounds();

        this.props.searchResults
          .filter(pin => pin.lat && pin.lon)
          .forEach(pin => {
            bounds.extend(new google.maps.LatLng(pin.lat, pin.lon));
          });

        bounds.extend(new google.maps.LatLng(this.props.location.lat, this.props.location.lng));
        this.setState({bounds: bounds});
        map.fitBounds(bounds);
      });
      //endregion
    }

    this.dragListner = map.addListener('dragend', () => {
      this.props.setDisplayRedoSearchButton(true);
    });

  }

  handleSwitchMapView() {
    this.props.switchMapView();
  }

  handleFocus() {
    this.setState({tempAddress: this.state.address, address: ''});
  }

  handleBlur() {
    const {address, tempAddress} = this.state;
    if (address !== '' && address !== tempAddress) {
      this.setState({tempAddress: '', address: address});
    } else {
      this.setState({tempAddress: '', address: tempAddress});
    }
  }

  renderFilter() {
    return <RestaurantFilter>

      <LocationAutoComplete onChange={this.onAddressChange.bind(this)}
                            onBlur={this.handleBlur.bind(this)}
                            value={this.state.address}
                            searchResultsPending={this.props.searchResultsPending}
                            onFocus={this.handleFocus.bind(this)}
                            onSelect={this.handleSelect}/>
      <div className="searchResultButton">
        <SearchButton primary full onPress={this.handleOk.bind(this)}
                      disabled={this.props.searchResultsPending}>
          Search
        </SearchButton>
      </div>
      <div className="switchViewIcon" onClick={this.handleSwitchMapView.bind(this)}>
        {!this.props.isMapView ? <MapIcon/> : <ListIcon/>}
      </div>
    </RestaurantFilter>
  }

  getMarker(restaurant, number) {
    return <MarkerComponent
      lat={restaurant.lat}
      lng={restaurant.lon}
      name={restaurant.name}
      address={restaurant.address}
      number={number}
      key={number}
      id={restaurant.id}
      onNotificationClicked={this.onResultClick.bind(this)}
    />
  }

  getDefaultMarker() {

    if (!this.props.location) {
      return null;
    }
    return <DefaultMarkerComponent
      lat={this.props.location.lat}
      lng={this.props.location.lng}
    />;
  }

  getPins(places, activeRestaurant) {
    let results = [];
    let active = null;
    let number = null;
    for (let i = 0; i < places.length; i++) {
      const restaurant = places[i];
      if (restaurant.id !== activeRestaurant) {
        results.push(this.getMarker(restaurant, i + 1));
      } else {
        active = restaurant;
        number = i;
      }
    }
    if (active) {
      results.push(this.getMarker(active, number + 1));
    }
    return results;
  }

  //region For results list
  onResultClick(id) {
    if (isMobile()) {
      if (!this.props.cartEmpty && this.props.cartAccount && this.props.cartAccount !== String(id)) {
        this.props.clearCart();
      }
      if (this.props.currentAccountId !== id) {
        this.props.resetStore();
      }
      this.props.history.push(`/${id}`);
    } else {
      const {origin} = window.location;
      if (origin) {
        window.open(`${origin}/${id}`);
      }
    }
    trackEvent(SCENARIOS.SEARCH_SCREEN, EVENTS[SCENARIOS.SEARCH_SCREEN].NAVIGATE_TO_RESTAURANT, id, 1);
  }

  // NOTE: `placeId` is null when user hits Enter key with no suggestion item selected.
  handleSelect = (address, placeId) => {
    this.setState({address}, () => {
      if (!placeId || isMobile()) {
        this.handleOk(); //No search button in mobile
      }
    });
  };

  onMouseOverResult(id) {
    this.props.setActiveRestaurant(id);
  }

  onMouseLeaveResult(id) {
    this.props.setActiveRestaurant(null);
  }

  //region For Map Pins
  onChildMouseEnter = (key, childProps) => {
    const markerId = childProps.id;
    this.props.setActiveRestaurant(markerId);
  };
  //endregion

  handleRedoSearch = () => {
    const newLocation = {lat: this.state.newLat, lng: this.state.newLon};
    this.props.setDisplayRedoSearchButton(false);
    this.setState({isRedoSearchPressed: true});
    this.props.doSearch(newLocation);
    if (newLocation.lat && newLocation.lng) {
      this.props.setDecodedAddress(newLocation);
    }
    this.props.setActiveRestaurant(null);
    this.props.setPage(0);
    trackEvent(SCENARIOS.SEARCH_SCREEN, EVENTS[SCENARIOS.SEARCH_SCREEN].CLICK_ON_REDO_SEARCH, null, 1);
  };


  static getRestaurantOpenStatus(restaurant) {
    if (!restaurant.isOnline) {
      return false;
    } else {
      let openingTime = restaurant.openingTime;
      if (!openingTime) {
        return false;
      } else {
        return openingTime.isAvailable && (restaurant.isDeliveryAvailable || restaurant.isPickupAvailable);
      }
    }
  }

  static getRestaurantOpenTime(restaurant) {
    if (restaurant.openingTime) {
      return restaurant.openingTime.nextAvailableTime;
    } else {
      return null;
    }
  }

  getRestaurantListItem(restaurant, id) {
    let iterationKey = id ? id : null;
    return <RestaurantListItem
      isDeliveryAvailable={restaurant.isDeliveryAvailable}
      restaurantName={restaurant.name}
      address={restaurant.address}
      cuisine={join(restaurant.cuisines, ", ")}
      key={iterationKey + restaurant.id}
      number={iterationKey}
      accountId={restaurant.id}
      onClick={this.onResultClick.bind(this)}
      onMouseOver={this.onMouseOverResult.bind(this)}
      onMouseLeave={this.onMouseLeaveResult.bind(this)}
      isOpen={SearchResults.getRestaurantOpenStatus(restaurant)}
      isSelected={this.props.activeRestaurant === restaurant.id}
      nextOpenTime={SearchResults.getRestaurantOpenTime(restaurant)}
    />
  }

  getRestaurantList(places, isMobile) {

    let results = [];
    let number = 1;
    const noResultsLabel = <NoResultsLabel>No results found for your search</NoResultsLabel>;
    if (isMobile) {
      if (!places || places.length === 0) {
        return noResultsLabel
      }
      let selectedRestaurant = [];
      if (this.props.activeRestaurant) {
        selectedRestaurant = places.filter(place => this.props.activeRestaurant === place.id);
      }
      if (!this.props.activeRestaurant || (this.props.activeRestaurant && selectedRestaurant.length === 0)) {
        return places && places.length > 0 ? this.getRestaurantListItem(places[0]) : null;
      }
      return selectedRestaurant && selectedRestaurant.length > 0 ? this.getRestaurantListItem(selectedRestaurant[0]) : null;
    }
    for (const restaurant of places) {
      results.push(this.getRestaurantListItem(restaurant, number));
      number++;
    }
    if (results.length > 0) {
      return results;
    } else {
      return noResultsLabel
    }
  }

  onBoundsChange = (data, zoom /* , bounds, marginBounds */) => {
    const {center} = data;
    if (this.state.isBoundsSet) {
      this.setState({newLat: center.lat, newLon: center.lng});
    } else {
      this.setState({isBoundsSet: true});
    }
  };

  loadFunc(page) {
    if (!this.props.searchResultsPending) {
      this.props.setDisplayRedoSearchButton(false);
      this.props.loadNextPage(page);
    }
  }

  handleOk = async () => {
    this.setState({isSearchPressed: true});
    const address = this.state.address;
    const result = validateIsLatLng(address);
    let latLng;
    if (result.isLatLng) {
      latLng = result.latLng;
    } else {
      latLng = await geocodeByAddress(this.state.address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
          return latLng;
        })
        .catch(error => console.error('Error', error));
    }
    if (!latLng) {
      this.props.clearNotifications();
      this.props.createErrorNotification('Please enter a valid location.');
      this.setState({isSearchPressed: false});
      return;
    }
    this.props.setDisplayRedoSearchButton(false);
    this.props.doSearch(latLng, address);
    this.props.setActiveRestaurant(null);
    if (localStorage) {
      localStorage.setItem(localStorageKey, this.state.address);
    }
    this.props.clearNotifications();
  };

  generateDynamicResultContent = (places, isMobile) => {
    const {searchResultsPending, newSearchRequested} = this.props;
    if (searchResultsPending && newSearchRequested) {
      if (isMobile) {
        return loader;
      }
      return null;
    }
    return this.getRestaurantList(places, isMobile);
  };

  goBackToHome() {
    this.props.resetSearch();
  }

  render() {
    if (!this.props.searchResults) {
      return (
        <Grid fluid className="searchContainer searchResults">
          <GlobalHeader/>
          <SearchResultsLoadingModal isOpen={true}/>
          <SearchRow/>
        </Grid>
      );
    } else {
      const results = this.props.searchResults;
      const activeRestaurant = this.props.activeRestaurant;
      const googleMap = <GoogleMap
        center={this.props.location}
        onChange={this.onBoundsChange}
        onGoogleApiLoaded={this.onMapLoaded.bind(this)}
        onChildMouseEnter={this.onChildMouseEnter}
        bounds={this.state.bounds}
        updateBounds={this.state.updateBounds}
        options={
          {
            zoomControl: this.state.isMobile ? !this.props.displayRedoSearchButton : true,
            scaleControl: true,
            gestureHandling: this.props.searchResultsPending ? 'none' : '',
            fullscreenControl: false
          }
        }
      >
        {this.getDefaultMarker()}
        {this.getPins(results, activeRestaurant)}
      </GoogleMap>;

      return (
        <Grid fluid className="searchContainer searchResults">
          <GlobalHeader onBackClicked={this.goBackToHome.bind(this)}/>

          <SearchRow>
            {!this.state.isMobile ?
              <RestaurantContainer>
                <RestaurantListWrapper>
                  {this.renderFilter()}
                  <RestaurantList>
                    {this.state.isSearchPressed || this.state.isRedoSearchPressed ? loader : null}
                    <InfiniteScroll
                      pageStart={1}
                      loadMore={this.loadFunc.bind(this)}
                      hasMore={results.length < this.props.maxOperatorCount && (!this.props.allResultsLoaded || this.props.resultsWaitingList.length > 0)}
                      loader={this.state.isSearchPressed || this.state.isRedoSearchPressed ? null : loader}
                      useWindow={false}
                      threshold={500}
                    >
                      {this.generateDynamicResultContent(results)}
                    </InfiniteScroll>
                  </RestaurantList>
                </RestaurantListWrapper>
                <MapContainer>
                  {googleMap}
                  {this.props.displayRedoSearchButton &&
                  <RedoSearchButton full primary
                                    onPress={this.handleRedoSearch}
                                    disabled={this.props.searchResultsPending}
                  >
                    Redo Search In This Area
                  </RedoSearchButton>}
                </MapContainer>
              </RestaurantContainer> :
              (this.props.isMapView ?
                <RestaurantContainer>
                  <MobileMapContainer>
                    {this.renderFilter()}
                    <MapContainer isFullScreen={this.props.displayRedoSearchButton}>
                      {googleMap}
                      {this.props.displayRedoSearchButton &&
                      <RedoSearchButton full primary
                                        onPress={this.handleRedoSearch}
                                        disabled={this.props.searchResultsPending}
                      >
                        Redo Search In This Area
                      </RedoSearchButton>}
                    </MapContainer>
                    {!this.props.displayRedoSearchButton && this.generateDynamicResultContent(results, isMobile)}
                  </MobileMapContainer>
                </RestaurantContainer> :
                <RestaurantContainer>
                  <RestaurantListWrapper>
                    {this.renderFilter()}
                    <RestaurantList>
                      {this.state.isSearchPressed ? loader : null}
                      <InfiniteScroll
                        pageStart={1}
                        loadMore={this.loadFunc.bind(this)}
                        hasMore={results.length < this.props.maxOperatorCount && (!this.props.allResultsLoaded || this.props.resultsWaitingList.length > 0)}
                        loader={this.state.isSearchPressed ? null : loader}
                        useWindow={false}
                        threshold={500}
                      >
                        {this.generateDynamicResultContent(results)}
                      </InfiniteScroll>
                    </RestaurantList>
                  </RestaurantListWrapper>
                </RestaurantContainer>)
            }
          </SearchRow>
        </Grid>
      );
    }
  }
}

function select(state) {

  return {
    searchResults: state.search.results,
    location: state.search.location,
    address: state.search.address,
    currentAccountId: state.app.restaurantInfo ? state.app.restaurantInfo.accountId : null,
    cartEmpty: state.cart.items.length === 0,
    cartAccount: state.cart.accountId,
    cartAccountName: state.cart.accountName,
    searchResultsPending: state.search.searchResultsPending,
    allResultsLoaded: state.search.allResultsLoaded,
    activeRestaurant: state.search.activeRestaurant,
    newSearchRequested: state.search.newSearchRequested,
    isActiveRestaurantLocked: state.search.isActiveRestaurantLocked,
    page: state.search.page,
    isMapView: state.search.isMapView,
    maxOperatorCount: state.search.maxOperatorCount,
    displayRedoSearchButton: state.search.displayRedoSearchButton,
    resultsWaitingList: state.search.resultsWaitingList
  };


}

const actions = {
  loadNextPage,
  setActiveRestaurant,
  doSearch,
  setPage,
  clearCart,
  resetSearch,
  resetStore,
  createErrorNotification,
  clearNotifications,
  switchMapView,
  setDecodedAddress,
  setDisplayRedoSearchButton
};

export default connect(select, actions)(SearchResults);
