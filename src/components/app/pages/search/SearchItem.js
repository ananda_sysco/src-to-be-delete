import React, {Component} from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {toUpper} from 'lodash';
import {Heading} from 'lib/ui';

const ListItem = styled.div`
display: flex;
flex-flow: row wrap;
padding-top:20px;
padding-bottom: 20px;
border-bottom: 0.72px solid rgba(209,204,199,0.5);
padding-right: 10px;
cursor: pointer;
span{
    font-family: "BrixSansMedium";
    font-size: 10px;
    color: #fff;
    background-color: ${props => !props.isListActive ? '#363532' : '#84BD00'};    
    border-radius: 4px;
    padding: 1px 10px 2px 10px;
}
  background-color: ${props => !props.isListActive ? '#F0EFEB' : 'transparent'};
  opacity: ${props => !props.isListActive ? '0.3' : '1'};  
`;

const CusineType = styled.div`
font-family: "BrixSansLightItalic";
font-size: 12px;
font-weight: bold;
color: #5f5f5f;
margin-top: 8px;
`;

const ItemNo = styled.div`
width: 27px;
text-align:right;
font-family: "BrixSansLight";
font-size: 14px;
color: #5f5f5f;
padding-right: 3px;
padding-top: 4px;
box-sizing: content-box;
`;

const ItemDescription = styled.div`
flex: 1;
`;

const TagContainer = styled.div`
width: 100%;
padding-left:30px;
margin-bottom:17px;
span:nth-child(even) {
    margin-left: 5px;
}
`;

const AddressLine = styled.div`
font-family: "BrixSansLight";
font-size: 12px;
color: #A8A7A4;
margin-top:3px;
`;

const RestaurantName = styled(Heading)`
font-family: "BrixSansLight";
font-size: 20px;
color: #5f5f5f;
margin: 0px;
`;


export default class RestaurantListItem extends Component {

  static propTypes = {
    isRestaurantClosed: PropTypes.bool,
    isDeliveryAvailable: PropTypes.bool,
    restaurantName: PropTypes.string,
    address: PropTypes.string,
    cuisine: PropTypes.string,
    number: PropTypes.number,
    accountId: PropTypes.string,
    onMouseOver: PropTypes.func,
    onMouseLeave: PropTypes.func,
    onClick: PropTypes.func
  };

  mouseEnter() {
    this.props.onMouseOver(this.props.accountId);
  }

  mouseLeave() {
    this.props.onMouseLeave(this.props.accountId);
  }

  handleClick() {
    this.props.onClick(this.props.accountId);
  }

  render() {
    return (
      <ListItem className={this.props.isSelected ? 'selectedOperator' : ''} onMouseEnter={this.mouseEnter.bind(this)}
                onMouseLeave={this.mouseLeave.bind(this)}
                onClick={this.handleClick.bind(this)}
                isListActive={this.props.isOpen}>
        <TagContainer>
          {this.props.isOpen && this.props.isDeliveryAvailable && <span>DELIVERY AVAILABLE</span>}
          {(!this.props.isOpen && this.props.nextOpenTime) && <span>{"OPENS " + this.props.nextOpenTime}</span>}
        </TagContainer>
        <ItemNo>{(this.props.number)}.</ItemNo>
        <ItemDescription>
          <RestaurantName large>{this.props.restaurantName}</RestaurantName>
          <AddressLine>{this.props.address}</AddressLine>
          <CusineType>{toUpper(this.props.cuisine)}</CusineType>
        </ItemDescription>
      </ListItem>
    );
  }

}
