import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import SearchHomePage from "./SearchHome";
import SearchResults from "./SearchResults";
import AuthenticationSection from "components/user/AuthenticationSection";
import {updateUserDetailsPanelStatus} from 'store/user';
import {clearNotifications} from 'store/app';
import {validateIsLatLng} from "../../../../utils/commonUtil";
import {geocodeByAddress, getLatLng} from "react-places-autocomplete";
import {doSearch} from 'store/search';

const localStorageKey = 'olo_address';

export class SearchPage extends Component {

  handleSideOverlayView() {
    this.props.updateUserDetailsPanelStatus(false)
  }

  componentDidUpdate(prevProps) {
    if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).addEventListener('click', this.handleSideOverlayView.bind(this));
    } else if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && !this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).removeEventListener('click', this.handleSideOverlayView.bind(this));
    }
  }

  loadSearchResults = async (address) => {
    const result = validateIsLatLng(address);
    let latLng;
    if (result.isLatLng) {
      latLng = result.latLng;
    } else {
      latLng = await geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {
          return latLng;
        })
        .catch(error => console.error('Error', error));
    }
    if (!latLng) {
      this.props.clearNotifications();
      this.props.createErrorNotification('We are experiencing some technical difficulties. Please try again later.');
      return;
    }
    this.props.doSearch(latLng, address);
    if (localStorage) {
      localStorage.setItem(localStorageKey, address);
    }
    this.props.clearNotifications();
  };

  componentDidMount() {
    if (this.props.notificationsAvailable) {
      this.props.clearNotifications();
    }
  }

  componentWillMount() {
    const address = localStorage.getItem(localStorageKey);
    if (!this.props.searchResults && window.location.href.includes('#search') && address) {
      return this.loadSearchResults(address);
    }
  }

  render() {
    let components = [<AuthenticationSection/>];
    const address = localStorage.getItem(localStorageKey);
    if (this.props.searchResults || (window.location.href.includes('#search') && address)) {
      components.push(<SearchResults history={this.props.history}/>);
    } else {
      components.push(<SearchHomePage/>);
    }
    return components;
  }
}

function select(state) {
  return {
    searchResults: state.search.results,
    isDrawerOpen: state.user.isDrawerOpen,
    notificationsAvailable: state.app.notifications && state.app.notifications.length > 0
  };
}

const actions = {updateUserDetailsPanelStatus, clearNotifications, doSearch};


export default connect(select, actions)(SearchPage);
