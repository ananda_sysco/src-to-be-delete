import React, {Component} from 'react';
import {connect} from 'react-redux';
import WalletSearchPage from "./WalletSearchPage";
import WalletResultsPage from "./WalletResultsPage";


export class WalletPage extends Component {

  componentDidUpdate(prevProps) {
    if (prevProps.isWalletRequestPending && !this.props.isWalletRequestPending && this.props.walletData) {
      if (document.getElementsByClassName && !this.props.walletError) {
        const elements = document.getElementsByClassName("headerClass");
        if (elements.length > 0 && elements[0].scrollIntoView) {
          elements[0].scrollIntoView();
        }
      }
    }
  }

  render() {
    if (this.props.walletData) {
      return <WalletResultsPage/>
    } else {
      return <WalletSearchPage/>
    }
  }
}

function select(state) {
  return {
    walletError: state.wallet.walletError,
    walletData: state.wallet.walletData,
    isWalletRequestPending: state.wallet.isWalletRequestPending,
  };
}

const actions = {};


export default connect(select, actions)(WalletPage);
