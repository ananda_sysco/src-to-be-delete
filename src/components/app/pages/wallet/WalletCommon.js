import styled from 'styled-components';
import {Button, Col, Grid, Heading, Label, Row, themed} from 'lib/ui';

export const NotificationWrapper = styled(Grid)`
  position: fixed;
  z-index: 999;
  left: 0;
  width: 100%;
`;

export const HeaderBar = styled(Col)`
  border-bottom: 1px solid #d1ccc7;
  padding: 27px 17px;
  margin-bottom: 72px;
`;

export const BalanceHeading = styled(Heading)`
  font-family: BrixSansLight;
  font-size: 2.05rem;
  margin-bottom: 4.1rem;
  margin-top: 0.5rem;
`;

export const BalanceButton = styled(Button)`
  margin-top: 70px;
  margin-bottom: 100px;
`;

export const FieldIconWrapper = styled.div `
  padding-top: 9px;
  margin-left: -43px;
`;

export const LoaderText = styled(Label)`
  color: ${themed('colors.primary')};
  font-family: BrixSansLight;
  font-size: 1rem;
  color: #A8A7A4;
`;

export const LoaderWrapper = styled(Row)`
  margin-top:70px;
  margin-bottom: 91px;
`;


export function validateField(field) {
  const value = this.state[field];
  let cardError, pinError;
  switch (field) {
    case "CardNumber":
      if (!value || value.trim().length === 0) {
        cardError = "Card Number cannot be empty";
      } else if (value.trim().length < 12) {
        cardError = "Invalid card number";
      }
      this.setState({isCardValid: !cardError, cardError: cardError});
      break;
    case "PinNumber":
      if (!value || value.trim().length === 0) {
        pinError = "Pin Cannot be empty";
      } else if (value.trim().length < 3) {
        pinError = "Invalid pin";
      }
      this.setState({isPinValid: !pinError, pinError: pinError});
      break;
    default:
      return;
  }
};
