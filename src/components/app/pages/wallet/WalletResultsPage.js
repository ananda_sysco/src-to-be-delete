import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Grid, Heading, InfoIcon, Label, NotificationBar, Row, themed} from 'lib/ui';
import styled, {ThemeProvider} from 'styled-components';
import {ErrorLabel, FormInput, Section} from 'lib/StyledUI';
import theme from 'lib/ui/themes/cake';
import {options} from 'lib/ui/utils/theme';
import ReactTooltip from 'react-tooltip';
import {clearWalletError, getWalletDetails} from "store/wallet";
import Loading from 'components/global/Loading';
import {Mask} from 'react-input-enhancements';
import config from 'config';
import moment from 'moment-timezone';
import ReCAPTCHA from 'react-google-recaptcha';
import {startCase} from 'lodash';
import {
  BalanceButton,
  BalanceHeading,
  FieldIconWrapper,
  HeaderBar,
  LoaderText,
  LoaderWrapper,
  NotificationWrapper,
  validateField
} from './WalletCommon';

const BalanceLabel = styled(Heading)`
  font-family: "DuplicateSlabThin";
  font-size: 1.5rem;
  margin-bottom: 35px;
  margin-top: 0.5rem;
`;

const ClusterName = styled(Heading)`
  font-family: "BrixSansMedium";
  font-size: 1.5rem;
  font-weight: bold;
  border-top: ${options({
  Secondary: '7px solid #5f5f5f',
  Primary: '0px solid'
}, 'Secondary')};
  display: ${options({
  Secondary: 'inline-block',
  Primary: 'static'
}, 'Secondary')};
  padding-top: ${options({
  Secondary: '10px',
  Primary: '0px'
}, 'Secondary')};
  margin-top: ${options({
  Secondary: '50px',
  Primary: '0px'
}, 'Secondary')};
`;

const MerchantName = styled(Heading)`
  font-family: "BrixSansMedium";
  font-size: 1.125rem;
  font-weight: bold;
`;

const ClusterValue = styled(Label)`
  font-family: "BrixSansMedium";
  font-size: 3.375rem;
  font-weight: bold;
  margin-top: 0.5rem;
  margin-bottom: 1rem;
`;

const LastActivity = styled(Label)`
  font-family: "BrixSansLight";
  font-size: 1rem;
  color: #A8A7A4;
  margin-top: 0.5rem;
  margin-bottom: 1rem;
`;

const LocationSummaryHeading = styled.a`
  color: ${themed('colors.primary')};
  font-family: BrixSansLightItalic;
  font-size: 1.125rem;
  margin-top: 14px;
  display: block;
`;

const MerchantAddress = styled(Label)`
  color: ${themed('colors.primary')};
  font-family: BrixSansLightItalic;
  font-size: 0.875rem;
  color: #5f5f5f;
  display: block;
`;

const HorizontalDevider = styled.span`
  width: 100%;
  height: 1px;
  border-bottom: 1px solid #D1CCC7;
  margin-top: 5rem;
  margin-bottom: 3rem;
`;

const LocationContainer = styled(Col)`
  margin-top:50px
`;

const cashRedeemRawFormat = "CASHREDEEM";
const cashRedeemDisplayName = "Cash Redeem";

export class WalletResultsPage extends Component {

  constructor() {
    super();
    this.state = {};
    this.validateField = validateField.bind(this);
  }

  getWalletData() {
    this.validateField('CardNumber');
    this.validateField('PinNumber');
    let isCardValid = this.state.isCardValid;
    let isPinValid = this.state.isPinValid;
    if (isCardValid && isPinValid && (this.state.captchaValue || !config.captchaEnabled)) {
      this.props.getWalletDetails(this.state.CardNumber, this.state.PinNumber, this.state.captchaValue);
    } else if (!this.state.captchaValue) {
      this.setState({captchaError: true});
    }
  }

  handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };
    this.setState(tempState);
  };

  handleBlur = (component) => {
    let target = component.target;
    const field = target.name;
    this.validateField(field);
  };

  static renderClusters(clusters) {
    if (!clusters || clusters.length === 0) {
      return <Col md={12}>
        <BalanceHeading>This card does not have any activity yet.</BalanceHeading>
      </Col>;
    }
    const tempClusters = [];
    for (let i = 0; i < clusters.length; i++) {
      let cluster = clusters[i];
      let tempRestaurants = [];
      let lastActivity = null;
      if (cluster.lastTransaction) {
        let lastTransactionDisplayName = cluster.lastTransaction === cashRedeemRawFormat
          ? cashRedeemDisplayName : startCase(cluster.lastTransaction.toLowerCase());

        lastActivity = <LastActivity>
          Last
          use: {moment.unix(cluster.lastTransactionTS).format("MMM DD, YYYY") + " (" + lastTransactionDisplayName + ")"}
        </LastActivity>
      }
      let numberOfLocations = <Col xs={12}>
        <LocationSummaryHeading
          to="#">{cluster.locations.length} Location{cluster.locations.length > 1 && 's'}</LocationSummaryHeading>
      </Col>;

      cluster.locations.forEach((location) => {
        tempRestaurants.push(<LocationContainer xs={12} sm={6} md={4}>
          <MerchantName large>{location.name}</MerchantName>
          <MerchantAddress>
            <div>{location.address1 + ',' + location.address2}</div>
            <div>{location.city}</div>
          </MerchantAddress>
        </LocationContainer>)
      });
      tempClusters.push([<Col md={12}>
        {i === 0 && <BalanceLabel xxlarge>Your Balance is:</BalanceLabel>}
        {i === 0 && <ClusterName Primary xlarge>{cluster.name}</ClusterName>}
        {i !== 0 && <ClusterName xlarge>{cluster.name}</ClusterName>}
        <ClusterValue>${cluster.value}</ClusterValue>
        {lastActivity}
      </Col>,
        numberOfLocations
        , tempRestaurants])
    }
    return <Row>
      {tempClusters}
    </Row>;
  }

  // specifying verify callback function
  verifyCallback = function (response) {
    this.setState({captchaValue: response});
    if (response) {
      this.setState({captchaError: false});
    }
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.isWalletRequestPending && !nextProps.isWalletRequestPending) {
      this.captcha.reset(); //To avoid duplicate captcha values
      this.setState({captchaValue: null});
    }
  }

  render() {

    let buttonComponent;

    if (this.props.isWalletRequestPending) {
      buttonComponent = <LoaderWrapper start="xs" middle="xs">
        <Col xs={2} sm={2} md={1}>
          <Loading size={45}/>
        </Col>
        <Col xs={10} sm={10} md={11}>
          <LoaderText>...Checking card balance</LoaderText>
        </Col>
      </LoaderWrapper>;
    } else {
      buttonComponent = <Row center="xs" start="sm">
        <Col xs={8} sm={6} md={4}>
          <BalanceButton primary full onPress={this.getWalletData.bind(this)}>Check Balance</BalanceButton>
        </Col>
      </Row>;
    }

    return <ThemeProvider theme={theme}>
      <Grid>
        <Row>
          {this.props.walletError && <NotificationWrapper>
            <NotificationBar
              type={'error'}
              message={this.props.walletError}
              actions={[{
                primary: true, label: 'Got It', handler: () => {
                  this.props.clearWalletError();
                }
              }]}/>
          </NotificationWrapper>}
          <HeaderBar className='headerClass' xs={12}>
            <svg width="110" viewBox="0 0 275 68">
              <g fill="#F2700F">
                <path
                  d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                <polygon
                  points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                <path
                  d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
              </g>
            </svg>
          </HeaderBar>
        </Row>
        <Row>
          <Col xsOffset={1} xs={10} sm={8} md={7}>
            {WalletResultsPage.renderClusters(this.props.walletData)}
            <Row>
              <HorizontalDevider/>
            </Row>
            <Row>
              <Col md={12}>
                <BalanceHeading>Check Another Card's Balance</BalanceHeading>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={7} md={6}>
                <Mask pattern={'0000 0000 0000 0000'} emptyChar={''} placeholder={'Card Number'}>
                  <FormInput
                    name={'CardNumber'}
                    placeholder={'Card Number'}
                    error={this.state.cardError}
                    disabled={this.disabled}
                    onChange={this.handleInputChange}
                    onBlur={this.handleBlur}/>
                </Mask>
              </Col>
              <Col>
                <FieldIconWrapper
                  data-tip={`<img src=${require(`../../../../assets/wallet-scratch-pin.png`)} />`}
                  data-html={true}><InfoIcon/></FieldIconWrapper>
              </Col>
            </Row>
            <Row>
              <Col xs={6} sm={3} md={3}>
                <FormInput
                  name={'PinNumber'}
                  placeholder={'Pin Number'}
                  error={this.state.pinError}
                  onChange={this.handleInputChange}
                  mask="9999"
                  maskChar={null}
                  onBlur={this.handleBlur}
                />
              </Col>
              <Col>
                <FieldIconWrapper
                  data-tip={`<img src=${require(`../../../../assets/wallet-scratch-pin.png`)} />`}
                  data-html={true}>
                  <InfoIcon/>
                </FieldIconWrapper>
                <ReactTooltip place='right' effect='solid' html={true}/></Col>
            </Row>
            <Section>
              <Col>
                <ReCAPTCHA
                  ref={e => (this.captcha = e)}
                  onChange={this.verifyCallback.bind(this)}
                  sitekey={config.captchaSiteKey}/>
                {this.state.captchaError && <ErrorLabel xsmall>Please complete captcha</ErrorLabel>}
              </Col>
            </Section>
            <Row>
              <Col xs={12}>
                {buttonComponent}
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </ThemeProvider>
  }
}

function select(state) {
  return {
    walletError: state.wallet.walletError,
    walletData: state.wallet.walletData,
    isWalletRequestPending: state.wallet.isWalletRequestPending
  };
}

const actions = {getWalletDetails, clearWalletError};


export default connect(select, actions)(WalletResultsPage);
