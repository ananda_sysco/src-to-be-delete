import React, {Component} from 'react';
import {logoutUser, showLoginModal, updateUserDetailsPanelStatus} from 'store/user';
import {connect} from "react-redux";
import {withRouter} from "react-router";
import Link from 'components/app/router/Link';
import styled from 'styled-components';
import {Label, themed} from 'lib/ui';

const WelcomeLabel = styled(Label)`
    font-family: DuplicateSlabThin;
    font-size: 1.15rem;
    font-style: normal;
    margin-bottom: 1rem;
    color: ${themed('colors.background')};
    margin-top: 1rem;
    margin-bottom: 1.5rem;
    padding-left: 30px;
    padding-bottom: 18px;
    border-bottom: 2px solid rgba(59,59,59,0.5);
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    display: block;
`;

const LogOutLink = styled(Link)`
    font-family: BrixSansRegular;
    font-size: 1rem;
    padding-left: 30px;
    color: ${themed('colors.background')};
    margin-top: 1rem;
    margin-bottom: 1.5rem;
`;

/**
 * Side panel components reside here (ie: sign in etc). If side panel need to be closed
 * after  certain action, this.props.updateUserDetailsPanelStatus(!this.props.isDrawerOpenisDrawerOpen) need to be triggered
 */
export class UserDetailsPage extends Component {

  render() {

    const {user} = this.props;
    const welcomeMessage = user ? "Hi " + (user.firstName ? user.firstName : user.lastName) + "!" : null;

    return (
      <div className="userDetailsPanel">
        <WelcomeLabel small neutral italic>{welcomeMessage}</WelcomeLabel>
        <LogOutLink to="#" onClick={() => {
          this.props.logoutUser();
          this.props.updateUserDetailsPanelStatus(!this.props.isDrawerOpen);
        }}>Log Out</LogOutLink>
      </div>
    );
  }
}

function select(state) {
  return {
    isDrawerOpen: state.user.isDrawerOpen,
    user: state.user.loggedInUser
  };
}

export default connect(select, {showLoginModal, updateUserDetailsPanelStatus, logoutUser})(withRouter(UserDetailsPage));
