/* eslint-disable import/no-named-as-default */
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Grid, Row} from 'lib/ui';
import PaymentLayout from 'components/checkout/PaymentLayout';
import styled from 'styled-components';
import CartLayout from 'components/cart/CartLayout';
import RestaurantMap from 'components/restaurant/RestaurantMap';
import RestaurantDetails from 'components/restaurant/RestaurantDetails';
import Sidebar from 'components/global/Sidebar';
import Content from 'components/global/Content';
import * as AppPropTypes from 'utils/proptypes';
import {clearCart} from 'store/cart';
import {CONSTANT, trackPageView} from '../../analytics/analyticsUtils';
import RemoveCardOverlay from '../../../components/card/RemoveCardOverlay';
import Link from 'components/app/router/Link';
import GlobalHeader from "../../global/GlobalHeader";


const MobileRestaurantDetailContainer = styled.div`
    display: none;
    -webkit-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    -moz-box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    box-shadow: inset 0px -9px 10px -5px rgba(0,0,0,0.1);
    padding-bottom: 20px;
    margin-top: -11px;
    @media (max-width: 767px) {
        height: calc(100vh - 80px);            
        display: block;
        position: absolute;
        background-color: #fff;
        z-index: 4;
    }
`;
const CloseLink = styled(Link)`
    font-family: BrixSansLight;
    font-size: 0.9rem;
    padding-bottom: 10px;
    text-align: center;
    width:106px;
    display: flex;
    margin-top: 30px;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
&:focus {
  outline: none;
}
`;

export class CheckoutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
    this.handleClick = this.handleClick.bind(this)
  }

  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    cart: AppPropTypes.cart,
    clearCart: PropTypes.func.isRequired,
    user: PropTypes.any,
    hours: PropTypes.array,
  };

  componentWillReceiveProps(nextProps) {
    const {cart, history, restaurant} = this.props;
    const {order} = nextProps.cart;

    // go to confirmation page after order submit
    if ((order && order.id && (order !== cart.order)) || (order && order.isAlreadyProcessed)) {
      this.props.clearCart();
      if (window.history.replaceState) {
        window.history.replaceState(null, null, `/${restaurant.accountId}`);
      }
      return history.push('confirmation');
    }

    // if user empties the cart from the checkout
    // page, take them back to the menu page
    if (!cart.empty && nextProps.cart.empty) {
      return history.replace(`/${restaurant.accountId}`);
    }
  }

  componentDidMount() {
    trackPageView(CONSTANT.CHECKOUT);
  }

  handleClick() {
    this.setState({
      condition: !this.state.condition
    })
  }

  render() {
    const { restaurant, hours, cart, isMobile } = this.props;
    const { condition } = this.state;
    const showCart = restaurant.isCakeOloAvailable && !cart.empty;

    return (
      <Grid className="checkoutPage">
        <GlobalHeader customTitle={"Checkout"} subHead={restaurant.name}/>
        <Row className="checkoutRow">
          <Content>
            {/* <a ref="btn" onClick={this.handleClick.bind(this)} role={"button"}
               className="showOnMobile infoIcon"><InfoIcon/></a> */}
            { condition &&
              <MobileRestaurantDetailContainer className={"showOnMobile"}>
                <RestaurantMap restaurant={restaurant} compact={showCart}/>
                <RestaurantDetails restaurant={restaurant} hours={hours}/>
                <CloseLink to="#" onClick={this.handleClick.bind(this)}>Close Details</CloseLink>
              </MobileRestaurantDetailContainer>
            }
            <PaymentLayout/>
            <RemoveCardOverlay/>
          </Content>
          <Sidebar>
            <div className="hideFromMobile">
              { !isMobile && <RestaurantMap restaurant={restaurant} compact={!cart.empty}/>}
              <div className="mobileOrderHeader showOnMobile">
                My Order
              </div>
              <CartLayout canCheckout={false}/>
            </div>
          </Sidebar>
        </Row>
      </Grid>
    );
  }
}

function select(state) {
  return {
    restaurant: state.app.restaurantInfo,
    cart: state.cart,
    user: state.user.loggedInUser,
    isMobile: state.app.isMobile
  };
}

export default connect(select, {clearCart})(withRouter(CheckoutPage));
