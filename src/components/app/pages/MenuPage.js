import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Grid, Row } from 'lib/ui';
import MenuLayout from 'components/menu/MenuLayout';
import RestaurantMap from 'components/restaurant/RestaurantMap';
import RestaurantDetails from 'components/restaurant/RestaurantDetails';
import CartLayout from 'components/cart/CartLayout';
import Sidebar from 'components/global/Sidebar';
import Content from 'components/global/Content';
import * as AppPropTypes from 'utils/proptypes';
import {restaurantHoursSelector} from 'store/utils/selectors';
import queryString from 'query-string';
import {getPasswordResetUser, updateUserDetailsPanelStatus} from 'store/user';
import {scrollToElement} from '../../../utils/commonUtil';
import GlobalHeader from 'components/global/GlobalHeader';
import {resetSearch} from "../../../store/search";
import {
  HOME_ROUTE,
  CloseButton,
  MobileRestaurantDetailContainer
} from './constants/CommonPageConstants';

export class MenuPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleSideOverlayView = this.handleSideOverlayView.bind(this);
  }

  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    hours: PropTypes.array,
    cart: AppPropTypes.cart,
    user: PropTypes.any
  };

  componentWillMount() {
    const parsed = queryString.parse(this.props.location.search);
    if (parsed.resetPassword && parsed.token) {
      this.props.getPasswordResetUser(parsed.token);
    }
  }

  componentDidUpdate(prevProps) {
    if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).addEventListener('click', this.handleSideOverlayView);
    } else if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && !this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).removeEventListener('click', this.handleSideOverlayView);
    }
  }

  componentDidMount() {
    document.body.classList.remove('restaurantDetailVisible');
    document.body.classList.remove('mobileCartVisible');
  }

  handleSideOverlayView() {
    this.props.updateUserDetailsPanelStatus(false)
  }

  handleClick() {
    if (!this.state.condition) {
      document.body.classList.add('restaurantDetailVisible');
    } else {
      document.body.classList.remove('restaurantDetailVisible');
    }
    this.setState({
      condition: !this.state.condition
    });
    scrollToElement('.menuPage');
  }

  renderMenu(restaurant) {
    if (!this.props.isCartOpenedInMobile) {
      return (<MenuLayout restaurant={restaurant}/>);
    }
  }

  goBackToHome(isFromMobile) {
    const {history} = this.props;
    history.push(HOME_ROUTE);
    if (!isFromMobile) {
      this.props.resetSearch();
    }
  }

  render() {

    const { restaurant, hours, cart, user, isMobile } = this.props;
    const { condition } = this.state;
    const showCart = restaurant.isCakeOloAvailable && !cart.empty;

    return (
      <div>
        <Grid className="menuPage">
          <GlobalHeader onBackClicked={this.goBackToHome.bind(this)}/>
          { !isMobile &&
            <div className="menuMapContainer">
              <RestaurantMap restaurant={restaurant} compact={showCart} headerMap={true}/>
            </div>
          }
          <Row className="menuRow">
            <Content>
              { condition &&
                <MobileRestaurantDetailContainer className={"showOnMobile"}>
                  <RestaurantMap restaurant={restaurant} compact={showCart} mobileHeader={true}/>
                  <RestaurantDetails restaurant={restaurant} hours={hours}/>
                  <CloseButton threeQtr primary onPress={this.handleClick.bind(this)}>
                    Close Details
                  </CloseButton>
                </MobileRestaurantDetailContainer>
              }
              <MenuLayout restaurant={restaurant} user={user} onTitleClick={this.handleClick}/>
            </Content>
            <Sidebar>
              <div className="hideFromMobile">
                {!showCart && <RestaurantDetails restaurant={restaurant} hours={hours}/>}
              </div>
              {<CartLayout/>}
            </Sidebar>
          </Row>
        </Grid>
      </div>
    );
  }
}

function select(state) {
  return {
    restaurant: state.app.restaurantInfo,
    hours: restaurantHoursSelector(state),
    cart: state.cart,
    user: state.user.loggedInUser,
    isDrawerOpen: state.user.isDrawerOpen,
    isMobile: state.app.isMobile
  };
}

const actions = {getPasswordResetUser, updateUserDetailsPanelStatus, resetSearch};


export default connect(select, actions)(MenuPage);
