/**
 * Created by achintha on 11/7/17.
 */

import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import styled from 'styled-components';
import backgroundImage from 'assets/404-bg.png';
import {Heading, Paragraph} from 'lib/ui';
import mixpanel from 'mixpanel-browser';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10rem;
  width: 100%;
  height: 100%;
  background: url(${backgroundImage}) no-repeat center center;
  background-size: cover;
`;

export default class MerchantWithIncompleteData extends Component {

  componentWillMount() {
    //region mixpanel
    const path = window.location.pathname;
    mixpanel.track("MERCHANT_WITH_INCOMPLETE_DATA_LOADED", {
      "PATH": path ? path : 'null'
    });
    //endregion
  }

  render() {
    return (
      <DocumentTitle title="Merchant Inactive">
        <Wrapper>
          <Heading xxlarge>Sorry!</Heading>
          <Paragraph>The restaurant is not available for ordering.</Paragraph>
        </Wrapper>
      </DocumentTitle>
    );
  }
}
