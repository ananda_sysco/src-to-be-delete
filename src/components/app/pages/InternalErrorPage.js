import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import styled from 'styled-components';
import backgroundImage from 'assets/404-bg.png';
import {Heading, Paragraph} from 'lib/ui';
import mixpanel from 'mixpanel-browser';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10rem;
  width: 100%;
  height: 100%;
  background: url(${backgroundImage}) no-repeat center center;
  background-size: cover;
`;

export default class InternalErrorPage extends Component {

  componentWillMount() {
    //region mixpanel
    mixpanel.track("INTERNAL_ERROR_PAGE_LOADED", {
      "PATH": window.location.pathname
    });
    //endregion
  }

  render() {
    return (
      <DocumentTitle title="Page Not Found">
        <Wrapper>
          <Heading xxlarge>Oops!</Heading>
          <Paragraph>Internal error has occurred.</Paragraph>
        </Wrapper>
      </DocumentTitle>
    );
  }
}
