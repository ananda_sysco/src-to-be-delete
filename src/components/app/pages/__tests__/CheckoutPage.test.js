import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import CheckoutPage from '../CheckoutPage';
import configureStore from 'redux-mock-store';
import * as app from 'fixtures/appStructure';
import * as user from 'fixtures/user';

const mockStore = configureStore();

const initialState = {
  app: app.default,
  user: user.default
};

describe('src/components/CheckoutPage', () => {

  it('should render correctly', () => {
    const setRouteLeaveHook = jest.fn();
    const store = mockStore(initialState);
    let wrapper = shallow(<CheckoutPage store={store}/>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
