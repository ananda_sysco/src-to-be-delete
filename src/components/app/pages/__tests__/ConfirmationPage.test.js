import React from 'react';
import {shallow} from 'enzyme';
import ConfirmationPage from '../ConfirmationPage';
import configureStore from 'redux-mock-store';
import * as testData from 'fixtures/appStructure';
import * as user from 'fixtures/user';

const mockStore = configureStore();

const initialState = {app: testData.default.app,
  user: user.default
};
describe('src/components/CheckoutPage',()=>{
  it('should render correctly', ()=> {
    const store = mockStore(initialState);
    let wrapper = shallow(<ConfirmationPage store={store}/>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
