import "test_helper";
import React from 'react';
import {shallow} from 'enzyme';
import MenuPage from '../MenuPage';
import configureStore from 'redux-mock-store';
import theme from 'lib/ui/themes/cake';
import {ThemeProvider} from 'styled-components';
import * as testData from 'fixtures/appStructure';
import * as user from 'fixtures/user';

const mockStore = configureStore();

const initialState = {
  app: testData.default.app,
  user: user.default
};

describe('src/components/CheckoutPage', () => {
  it('should render correctly', () => {
    const setRouteLeaveHook = jest.fn();
    const store = mockStore(initialState);
    let wrapper = shallow(<ThemeProvider theme={theme}>
        <MenuPage store={store} router={setRouteLeaveHook}/>
      </ThemeProvider>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
