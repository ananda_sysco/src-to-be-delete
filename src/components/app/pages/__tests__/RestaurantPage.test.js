import React from 'react';
import {shallow} from 'enzyme';
import RestaurantPage from '../RestaurantPage';
import configureStore from 'redux-mock-store';
import { appStructure as testData } from '../../../../../test/fixtures';

const mockStore = configureStore();
const initialState = testData;

describe('src/components/CheckoutPage', () => {

  it('should render correctly', () => {
    const store = mockStore(initialState);
    let wrapper = shallow(<RestaurantPage store={store}/>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
