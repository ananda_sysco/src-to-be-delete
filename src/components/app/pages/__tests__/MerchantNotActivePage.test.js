import React from 'react';
import renderer from 'react-test-renderer';
import MerchantNotActivePage from '../MerchantNotActivePage';
import theme from 'lib/ui/themes/cake';
import {ThemeProvider} from 'styled-components';

describe('src/components/CheckoutPage', () => {
  it('should render correctly', () => {
    let snapshot = renderer.create(<ThemeProvider theme={theme}>
        <MerchantNotActivePage/>
      </ThemeProvider>
    );
    expect(snapshot).toMatchSnapshot();
  });
});
