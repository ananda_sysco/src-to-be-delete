import React, {Component} from 'react';
import DocumentTitle from 'react-document-title';
import styled from 'styled-components';
import backgroundImage from 'assets/404-bg.png';
import {Heading, Paragraph} from 'lib/ui';
import mixpanel from 'mixpanel-browser';
import config from 'config';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 10rem;
  width: 100%;
  height: 100%;
  background: url(${backgroundImage}) no-repeat center center;
  background-size: cover;
`;

export default class NotFoundPage extends Component {

  componentWillMount() {
    const path = window.location.pathname;
    const hostname = window.location.hostname;
    if (hostname && window.location.hostname.includes(config.giftCardUrl)) {
      window.location.href = 'https://' + config.giftCardUrl + '/balance';
      return;
    }
    //region mixpanel
    mixpanel.track("NOT_FOUND_PAGE_LOADED", {
      "PATH": path ? path : 'null'
    });
    //endregion
  }

  render() {
    return (
      <DocumentTitle title="Page Not Found">
        <Wrapper>
          <Heading xxlarge>Oops!</Heading>
          <Paragraph>We couldn't find the page you were looking for.</Paragraph>
        </Wrapper>
      </DocumentTitle>
    );
  }
}
