import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import {connect} from 'react-redux';
import {get} from 'lodash';
import {withRouter} from 'react-router-dom';
import { Grid, Row } from 'lib/ui';
import MenuLayout from 'components/menu/MenuLayout';
import Masthead from 'components/global/Masthead';
import RestaurantMap from 'components/restaurant/RestaurantMap';
import RestaurantDetails from 'components/restaurant/RestaurantDetails';
import ReceiptCartLayout from 'components/cart/ReceiptCartLayout';
import Sidebar from 'components/global/Sidebar';
import Content from 'components/global/Content';
import Loading from 'components/global/Loading';
import GlobalHeader from 'components/global/GlobalHeader';
import {getReceipt} from 'store/receipt';
import {getPasswordResetUser, updateUserDetailsPanelStatus} from 'store/user';
import * as AppPropTypes from 'utils/proptypes';
import {restaurantHoursSelector} from 'store/utils/selectors';
import queryString from 'query-string';
import { scrollToElement, getCurrencySymbol } from '../../../utils/commonUtil';
import {resetSearch} from '../../../store/search';
import AuthenticationSection from '../../user/AuthenticationSection';
import NotFoundPage from './NotFoundPage';
import {
  HOME_ROUTE,
  CloseButton,
  MobileRestaurantDetailContainer
} from './constants/CommonPageConstants';

export class ReceiptPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      condition: false
    };
    this.handleSideOverlay = this.handleSideOverlay.bind(this);
  }

  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    hours: PropTypes.array,
    cart: AppPropTypes.cart,
    user: PropTypes.any,
    match: PropTypes.object,
    getReceipt: PropTypes.func.isRequired
  };

  componentWillMount() {
    const parsed = queryString.parse(this.props.location.search);
    if (parsed.resetPassword && parsed.token) {
      this.props.getPasswordResetUser(parsed.token);
    }
  }

  componentDidMount() {
    document.body.classList.remove('restaurantDetailVisible');
    document.body.classList.remove('mobileCartVisible');

    const receiptId = get(this.props.match, 'params.id');

    if (receiptId) {
      this.props.getReceipt(receiptId);
    }
  }

  componentDidUpdate(prevProps) {
    if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).addEventListener('click', this.handleSideOverlay);
    } else if ((this.props.isDrawerOpen !== prevProps.isDrawerOpen) && !this.props.isDrawerOpen) {
      ReactDOM.findDOMNode(this).removeEventListener('click', this.handleSideOverlay);
    }
  }

  handleClick() {
    if (!this.state.condition) {
      document.body.classList.add('restaurantDetailVisible');
    } else {
      document.body.classList.remove('restaurantDetailVisible');
    }
    this.setState({
      condition: !this.state.condition
    });
    scrollToElement('.receiptPage');
  }

  handleSideOverlay() {
    this.props.updateUserDetailsPanelStatus(false);
  }

  renderMenu(restaurant) {
    if (!this.props.isCartOpenedInMobile) {
      return (<MenuLayout restaurant={restaurant}/>);
    }
    return null;
  }

  goBackToHome(isFromMobile) {
    const {history} = this.props;
    history.push(HOME_ROUTE);
    if (!isFromMobile) {
      this.props.resetSearch();
    }
  }

  renderReceipt(restaurant, user, cart, hours, condition) {
    const showCart = restaurant && restaurant.isCakeOloAvailable && !cart.empty;
    return (
      <Grid className='receiptPage'>
        <GlobalHeader onBackClicked={this.goBackToHome.bind(this)} isReceiptView={true}/>
        <div className={'menuMapContainer'}>
          <RestaurantMap restaurant={restaurant} compact={showCart} headerMap={true}/>
        </div>
        <Row className='menuRow'>
          <Content>
            {
              condition &&
              <MobileRestaurantDetailContainer className={'showOnMobile'}>
                <RestaurantMap restaurant={restaurant} compact={showCart} mobileHeader={true}/>
                <RestaurantDetails restaurant={restaurant} hours={hours}/>
                <CloseButton threeQtr primary onPress={this.handleClick.bind(this)}>
                  Close Details
                </CloseButton>
              </MobileRestaurantDetailContainer>
            }
            <Masthead
              heading={restaurant.name}
              user={user}
              showAddress={true}
              showLogo={true}
              orderEnabled={restaurant.isCakeOloAvailable}
              restaurant={restaurant}
              isReceiptView={true}
            />
            <ReceiptCartLayout
              timezone={restaurant.timeZone}
              currency={getCurrencySymbol(restaurant.countryCode)}
            />
          </Content>
          <Sidebar>
            <div className='hideFromMobile'>
              <RestaurantDetails restaurant={restaurant} hours={hours}/>
            </div>
          </Sidebar>
        </Row>
      </Grid>
    );
  }

  render() {
    const { restaurant, cart, user, hours, error } = this.props;
    const { condition } = this.state;

    if (error) {
      return <NotFoundPage />;
    }

    if (!restaurant) {
      return <Loading />;
    }

    const parts = [
      get(restaurant, 'name'),
      get(restaurant, 'address.city'),
      'Online Ordering',
    ];

    const title = parts.filter(val => !!val).join(' | ');
    return (
      <DocumentTitle title={title}>
        <Fragment>
          <AuthenticationSection/>
          {this.renderReceipt(restaurant, user, cart, hours, condition)}
        </Fragment>
      </DocumentTitle>
    );
  }
}

function select(state) {
  return {
    restaurant: state.app.restaurantInfo,
    error: state.receipt.error,
    hours: restaurantHoursSelector(state),
    cart: state.cart,
    user: state.user.loggedInUser,
    isDrawerOpen: state.user.isDrawerOpen
  };
}

const actions = {
  getPasswordResetUser,
  updateUserDetailsPanelStatus,
  resetSearch,
  getReceipt
};


export default connect(select, actions)(withRouter(ReceiptPage));
