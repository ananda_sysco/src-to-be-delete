import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Col} from "lib/ui";
import {selectSession, disableCheckout} from "store/app";
import Menu from "./Menu";
import ProductModal from "components/product/ProductModal";
import {createErrorNotification} from "../../store/productModal";
import {validatePrepDeliverTime} from 'store/utils/selectors';
import * as AppPropTypes from "utils/proptypes";
import ForgotPasswordModal from "components/user/ForgotPasswordModal";
import PasswordResetModal from "components/user/PasswordResetModal";
import Masthead from 'components/global/Masthead';
import {withAvailability} from 'utils/sessionUtil';

const posOffLineError = 'The restaurant cannot take orders at this time. Please try again later.';
const ieError = 'The browser you are using is not supported. We recommend using the latest version of Google Chrome, Firefox or Microsoft Edge for the best results';
const allSessionCompletedError = 'The restaurant is closed for ordering. Check hours to see when you can order next.';
const orderTypeError = 'Delivery or takeout order options are not available for this restaurant';
const oloFlagDisabledError = 'Sorry, online ordering is currently unavailable.';
const lpgError = "The restaurant cannot take orders at this time. Please contact the restaurant for further information.";

export class MenuLayout extends Component {


  static propTypes = {
    restaurant: AppPropTypes.restaurant,
    catalog: AppPropTypes.catalog,
    sessions: PropTypes.object,
    selectSession: PropTypes.func.isRequired,
    selectedSession: PropTypes.string,
    activeSession: PropTypes.string,
  };

  handleCloseModal = () => {
    this.props.deselectProduct();
  };

  componentWillMount = () => {
    if (!this.props.sessions) {
      return;
    }
    let sessions = withAvailability(this.props.sessions, this.props.restaurant.timeZone, false);
    if (sessions) {
      for (let key in sessions) {
        if (sessions.hasOwnProperty(key)) {
          if (sessions[key].isAvailable !== this.props.sessions[key].isAvailable || sessions[key].isPending !== this.props.sessions[key].isPending) {
            //Reload if sessions has timed out after initial loading
            window.location.reload();
            return;
          }
        }
      }
    }
  };


  isPosOnline = (restaurant) => {
    let isPosOnline = false;
    if (restaurant && restaurant.queueConsumers && restaurant.queueConsumers > 0) {
      isPosOnline = true;
    }
    return isPosOnline;
  };

  areOrderingTypesAvailable = (restaurant) => {
    let areOrderingTypesAvailable = true;
    if (restaurant && !restaurant.deliver && !restaurant.pickup) {
      areOrderingTypesAvailable = false;
    }
    if (!areOrderingTypesAvailable) {
      this.props.createErrorNotification(orderTypeError, true);
    }
    return areOrderingTypesAvailable;
  };

  allSessionsCompleted = (sessions) => {
    let areAllSessionsCompleted = true;
    if (sessions) {
      for (let key in sessions) {
        if (sessions.hasOwnProperty(key)) {
          let session = sessions[key];
          if (session.isPending || session.isAvailable) {
            areAllSessionsCompleted = false;
            return areAllSessionsCompleted;
          }
        }
      }
    }
    return areAllSessionsCompleted;
  };

  /**
   * Check for internet explorer
   * @returns {boolean}
   */
  isIE() {
    if (window.navigator) {
      const ua = window.navigator.userAgent;
      const msie = ua.indexOf("MSIE ");

      if (msie > 0) {
        // IE 10 or older => return version number
        return true;
      }

      const trident = ua.indexOf('Trident/');
      return trident > 0;
    } else {
      return false;
    }
  }

  render() {
    const {
      restaurant,
      user,
      sessions,
      selectedSession,
      currentSession,
      selectSession,
      currentDay,
      createErrorNotification,
      orderType,
      onTitleClick
    } = this.props;

    const areAllSessionsEnded = this.allSessionsCompleted(sessions);

    const isPosOnLIne = this.isPosOnline(restaurant);
    const orderEnabled = restaurant.isCakeOloAvailable
      && selectedSession
      && (sessions[selectedSession].isAvailable || sessions[selectedSession].isPending)
      && isPosOnLIne
      && !areAllSessionsEnded
      && this.areOrderingTypesAvailable(restaurant);

    let disableAdding = false;
    let disableCheckout = false;

    const isPGAvailable = restaurant.activeCCPaymentGateway === 'LPG';

    let shouldHideError = true;
    if (this.isIE()) {
      createErrorNotification(ieError, true);
      disableAdding = true;
      shouldHideError = false;
      disableCheckout = true;
    } else if (!restaurant.isCakeOloAvailable) {
      createErrorNotification(oloFlagDisabledError, true);
      disableAdding = true;
      shouldHideError = false;
      disableCheckout = true;
    } else if (!isPosOnLIne) {
      createErrorNotification(posOffLineError, true);
      disableAdding = true;
      shouldHideError = false;
      disableCheckout = true;
    } else if (areAllSessionsEnded) {
      createErrorNotification(allSessionCompletedError, true);
      disableAdding = true;
      shouldHideError = false;
      disableCheckout = true;
    } else if (!isPGAvailable) {
      createErrorNotification(lpgError, true);
      disableAdding = true;
      shouldHideError = false;
      disableCheckout = true;
    } else if (validatePrepDeliverTime(restaurant, selectedSession, sessions, orderType, null).isTimeOver) {
      disableAdding = true;
      disableCheckout = false;
    }

    if (disableCheckout) {
      this.props.disableCheckout();
    }

    return (
      <Col>
        <Masthead
          heading={restaurant.name}
          user={user}
          showAddress={true}
          showLogo={true}
          currentDay={currentDay}
          sessions={sessions}
          selectedSession={selectedSession}
          activeSession={currentSession}
          sessionChange={selectSession}
          orderEnabled={restaurant.isCakeOloAvailable}
          shouldShowError={shouldHideError}
          onTitleClick={onTitleClick}
        />
        <Menu orderEnabled={orderEnabled} disableAdding={disableAdding}/>
        <ProductModal/>
        <ForgotPasswordModal/>
        <PasswordResetModal/>
      </Col>
    );
  }
}

function mapState(state) {
  const restaurant = state.app.restaurantInfo;
  const {currentSession, selectedSession, sessions, currentDay} = state.app;
  const {orderType} = state.cart;
  return {
    sessions,
    currentSession,
    selectedSession,
    restaurant,
    orderType,
    currentDay,
  };
}

export default connect(mapState, {selectSession, createErrorNotification, disableCheckout})(MenuLayout);
