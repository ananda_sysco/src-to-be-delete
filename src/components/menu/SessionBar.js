import { map, find } from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MdAccessTime from 'react-icons/lib/md/access-time';
import MdKeyboardArrowDown from 'react-icons/lib/md/keyboard-arrow-down';
import MdCheck from 'react-icons/lib/md/check';
import { Row, Col, Heading, Select, Label, themed } from 'lib/ui';
import { contains24Hours } from 'utils/toolbox';
import * as constants from "../../lib/constants";
import {DATE_TIME_FORMAT} from 'utils/sessionUtil';

const Wrapper = styled(Row)`
  padding-right: 15px;
  height: 100%;
`;
const SessionHeaderLabel = styled(Heading)`
font-family: BrixSansMedium;
font-size: 18px;
  @media (max-width: 767px) {
    font-size: 0.96rem;
}
`;

const SessionName = styled(Label)`
  margin-bottom: ${themed('layout.gutter.xsmall')}rem;
`;

const SessionTime = styled(Label)`
  color: ${themed('colors.foreground')};
  margin-bottom: ${themed('layout.gutter.xsmall')}rem;
`;

const TimeIcon = styled(MdAccessTime)`
  color: ${themed('colors.foreground')};
  margin-right: ${themed('layout.gutter.xxsmall')}rem;
`;

const CheckIcon = styled(MdCheck)`
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.xlarge')}rem;
`;

const DownIcon = styled(MdKeyboardArrowDown)`
  font-size: ${themed('text.size.large')}rem;
  vertical-align: inherit !important;
  position: relative;
  top:4px;
  @media (max-width: 767px) {
    text-align: left;
    top: 5px;
}
`;

const UnavailableLabel = styled(Label)`
  margin-bottom: ${themed('layout.gutter.xxsmall')}rem;
  text-align: right;
  display: block;
  @media (max-width: 767px) {
    text-align: left;
}
`;

const OptionSlot = styled.span`
  float:none;
`;
const HeaderSlot = styled.span`
  float:right;
  margin-right: 24px;
  @media (max-width: 767px) {
    float: none;
    margin-right: auto;
}
`;

const renderArrow = () => null;

export default class SessionBar extends Component {

  static propTypes = {
    currentDay: PropTypes.number.isRequired,
    sessions: PropTypes.object.isRequired,
    selectedSession: PropTypes.string,
    activeSession: PropTypes.string,
    sessionChange: PropTypes.func,
    orderEnabled: PropTypes.bool,
    isReceiptView: PropTypes.bool,
  };

  getSlotToday(sessionId) {
    const { sessions, currentDay } = this.props;

    if (!sessionId) {
      return null;
    }

    const { slots } = sessions[sessionId];
    return find(slots, { day: currentDay.toString() });
  }

  handleSessionChange = ({value}) => {
    this.props.sessionChange(value);
  };

  isStartEndTimesValid = (slot) => {
    let startTime, endTime;

    if (contains24Hours(slot)) {
      return true;
    }

    if (slot) {
      startTime = moment(slot.start, DATE_TIME_FORMAT);
      endTime = moment(slot.end, DATE_TIME_FORMAT);
    }

    return startTime && endTime && startTime.isBefore(endTime);
  };

  renderSlotTimes = (slot) => {
    if (contains24Hours(slot)) {
      return 'Available 24 hours';
    }

    const inFmt = 'HH:mm:ss';
    const outFmt = 'h:mm A';

    const startTime = moment(slot.start, inFmt);
    const endTime = moment(slot.end, inFmt);

    return `${startTime.format(outFmt)} - ${endTime.format(outFmt)}`;
  };

  renderValue = ({ label }) => {
    const { selectedSession } = this.props;

    if (!selectedSession) {
      return null;
    }

    const slotToday = this.getSlotToday(selectedSession);

    return (
      <Col>
        <SessionHeaderLabel xsmall uppercase>{label} <DownIcon /></SessionHeaderLabel>
        <HeaderSlot>{this.renderSlot(slotToday)}</HeaderSlot>
      </Col>
    )
  };

  renderSlot = (slot, isChecked) => {
    if (!slot || !this.isStartEndTimesValid(slot) || (isChecked && !this.props.isSessionValidForOrdering)) {
      return null;
    }

    return (
      <SessionTime small>
        <TimeIcon />
        {this.renderSlotTimes(slot)}
      </SessionTime>
    );
  };

  renderOption = ({ value, label }) => {
    const {selectedSession, isSessionValidForOrdering} = this.props;
    const isChecked = selectedSession === value;
    const slotToday = this.getSlotToday(value);
    const {isAvailable, isPending} = this.props.sessions[value];

    return (
      <Row>
        <Col xs={10}>
          <SessionName large>{label}</SessionName>
          <OptionSlot>{this.renderSlot(slotToday, isChecked)}</OptionSlot>
          {(!(isAvailable || isPending) || !this.isStartEndTimesValid(slotToday) || (isChecked && !isSessionValidForOrdering)) &&
          <Label small italic error>Currently unavailable</Label>}
        </Col>
        <Col xs={2}>
          <Row center="xs" middle="xs">
            {isChecked && <CheckIcon />}
          </Row>
        </Col>
      </Row>
    )
  };

  render() {
    const {orderEnabled, sessions, selectedSession, isSessionValidForOrdering, isReceiptView} = this.props;
    const options = map(sessions, (session) => ({
      value: session.id,
      label: session.name,
    }));
    const currentSlot = this.getSlotToday(selectedSession);

    const isSessionAvailable = selectedSession && (sessions[selectedSession].isAvailable || sessions[selectedSession].isPending);

    const RawComponent = (props) => {
      return (
        <Wrapper end="sm" start="xs">
          <Col xs={12}>
                {props.message &&
                <UnavailableLabel small italic error>
                    {props.message}
                </UnavailableLabel>
                }
                <Select
                    className="session-select"
                    clearable={false}
                    searchable={false}
                    value={selectedSession}
                    options={options}
                    onChange={this.handleSessionChange}
                    optionRenderer={this.renderOption}
                    valueRenderer={this.renderValue}
                    arrowRenderer={renderArrow}
                />
          </Col>
        </Wrapper>
      )
    };
    if (isReceiptView) {
        return null;
    } else if (!orderEnabled) {
      return <RawComponent message='Online ordering is currently unavailable'></RawComponent>;
    } else if (!isSessionAvailable || !this.isStartEndTimesValid(currentSlot) || !isSessionValidForOrdering) {
      return <RawComponent message={constants.SESSION_NOT_AVAILABLE}> </RawComponent>;
    } else {
      return <RawComponent></RawComponent>
    }
  }
}
