import React from 'react';
import renderer from 'react-test-renderer';
import MenuLayout from '../MenuLayout';
import theme from 'lib/ui/themes/cake';
import {ThemeProvider} from 'styled-components';
import {shallow} from 'enzyme';

import configureStore from 'redux-mock-store';
import * as app from 'fixtures/appStructure';
import * as cart from 'fixtures/cart';

const mockStore = configureStore();

const initialState = {app: app.default, cart: cart.default}

describe('src/components/menu/MenuItemLoader', () => {
  it('it should render correctly', () => {
    const store = mockStore(initialState);
    const wrapper = shallow(
      <MenuLayout store={store}/>);
    expect(wrapper).toMatchSnapshot();
  });
});
