import React from 'react';
import { shallow } from 'enzyme';
import {
  Heading,
} from '../../../lib/ui';
import theme from 'lib/ui/themes/cake';
// only use for creating a snapshot
import renderer from 'react-test-renderer';
import { ThemeProvider } from 'styled-components';
import MenuItem from '../MenuItem';

const testProps = {
  id: '1',
  name: 'test',
  description: 'This is a test component',
  price: 1,
  onPress: jest.fn(),
};

describe('src/components/menu/MenuItem', () => {
  it('should render correctly', () => {
    const snapshot = renderer.create(
      <ThemeProvider theme={theme}>
        <MenuItem {...testProps} />
      </ThemeProvider>
    ).toJSON();
    expect(snapshot).toMatchSnapshot();
  });

  it('should render name with heading', () => {
    const wrapper = shallow(<MenuItem {...testProps} />);
    expect(wrapper.contains(<Heading xxsmall>{testProps.name}</Heading>)).toBeTruthy();
  });

  // FIXME: wrapper.find('button') says it's finding 0 nodes
  // Does this not work with shallow rendering?
  //
  // it('should attach a click handler', () => {
  //   const wrapper = shallow(<MenuItem {...testProps} />);
  //   wrapper.find('button').simulate('click');
  //
  //   expect(testProps.onPress).toBeCalled();
  // });

  /*it('should format price', () => {
    const wrapper = shallow(<MenuItem {...testProps} />);
    expect(wrapper.contains(<Label neutral>{'1.00'}</Label>)).toBeTruthy();
  });*/
});
