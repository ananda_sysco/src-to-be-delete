import React from 'react';
import renderer from 'react-test-renderer';
import SessionBar from '../SessionBar';
import theme from 'lib/ui/themes/cake';
import { ThemeProvider } from 'styled-components';
import * as sessionData from 'fixtures/sessions';
const testProps = sessionData;

describe('src/components/menu/MenuItemLoader',()=>{
  it('it should render correctly',()=>{
    const snapshot = renderer.create(
      <ThemeProvider theme={theme}>
      <SessionBar {...testProps}/>
      </ThemeProvider>).toJSON();
    expect(snapshot).toMatchSnapshot();
  });
});
