import { times } from 'lodash';
import React from 'react';
import styled, { withTheme } from 'styled-components';
import { lighten } from 'polished';
import ContentLoader, { Rect } from 'react-content-loader'
import { themed, Col, Row } from 'lib/ui';

const ItemWrapper = styled(Col)`
  border-bottom: 1px solid ${themed('colors.border')};
  margin-bottom: 1rem;
  padding-bottom: 0.5rem;
  min-height: 4rem;
`;

const Wrapper = styled(Row)`
  padding: ${themed('layout.gutter.medium')}rem;
`;

const Item = ({ theme }) => (
  <ItemWrapper xs={12} md={5}>
    <ContentLoader
      height={70}
      speed={1}
      primaryColor={theme.colors.foregroundLight}
      secondaryColor={lighten(0.05, theme.colors.foregroundLight)}
    >
      <Rect x={0} y={0} height={15} radius={5} width={200} />
      <Rect x={204} y={0} height={15} radius={5} width={50} />

      <Rect y={25} height={10} radius={5} width={350} />
      <Rect y={40} height={10} radius={5} width={350} />
    </ContentLoader>
  </ItemWrapper>
)

export function MenuItemLoader({ theme }) {
  return (
    <Wrapper between="xs">
      {times(12, (idx) => <Item key={idx} theme={theme} />)}
    </Wrapper>
  );
}

export default withTheme(MenuItemLoader);
