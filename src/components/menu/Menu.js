import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Heading, Row, themed } from 'lib/ui';
import MenuItem from './MenuItem';
import MenuItemLoader from './MenuItemLoader';
import { selectMenuItem } from 'store/productModal';
import { isEven } from '../../utils/commonUtil';


const Section = styled.div`
`;

const MenuDescription = styled.div`
  padding: .8rem;
  font-style: italic;
  font-size: 13px;
  font-family: "BrixSansLight";
  background-color: ${themed('colors.background')};
  color: ${themed('colors.neutral')};
  word-break: break-word;
  white-space: pre-wrap;
  word-wrap: break-word;
  line-height: 1.2rem;
  position: relative;
  text-align: center;
  @media (max-width: 767px) {
    margin-bottom: 0px;
    font-size: inherit;  
    text-align: left;
    position: static;  
    }
`;

const MenuHeader = styled.div`
  text-align: center;
  padding: 10px 10px 10px 10px;
  background-color: transparent;
  @media (max-width: 767px) {
    padding: 10px;
    background-color: ${themed({
        loading: 'colors.border',
        default: 'colors.foreground',
        }, 'default')};  
    }
`;

const CategoryTitle = styled(Heading)`
  font-family: "BrixSansExtraLight";
  font-size: 36px;
  margin: 0;
  color: #5f5f5f;
  position: relative;
  @media (max-width: 767px) {
    text-align:left;
    font-size: 1rem;
    color: ${themed('colors.background')};
    font-family: "DuplicateSlabRegular";
    position: static;    
    }
`;

const Wrapper = styled(Row)`
  padding: ${themed('layout.gutter.medium')}rem;
  padding-left: 22px;
  margin-top: 40px;
`;

export class Menu extends Component {

  static propTypes = {
    categories: PropTypes.object.isRequired,
    sessionCategories: PropTypes.arrayOf(PropTypes.object).isRequired,
    products: PropTypes.object,
    isLoading: PropTypes.bool,
    orderEnabled: PropTypes.bool,
    disableAdding: PropTypes.bool
  };

  static defaultProps = {
    isLoading: false,
    orderEnabled: true,
    disableAdding: false
  };

  handleItemPress = (itemId) => {
    this.props.selectMenuItem(itemId);
  };

  handleSubCategoryPress = (itemId) => {
    this.props.selectMenuItem(itemId, true);
  };

  static renderLoading() {
    return (
      <Section>
        <MenuHeader loading>
          <CategoryTitle>&nbsp;</CategoryTitle>
        </MenuHeader>
        <MenuItemLoader/>
      </Section>
    )
  }

  static renderDescription(category) {
    if (!category.description) {
      return null;
    }

    return (
      <MenuDescription>
        {category.description}
      </MenuDescription>
    );
  }

  static checkSubCategory(subCategory) {
    return subCategory.isActive && subCategory.products && subCategory.products.length > 0;
  }

  renderSubCategories(category) {
    const {categories} = this.props;

    if (!category.subCategories.length) {
      return null;
    }

    const subCategories = category.subCategories.map(subC => categories[subC]);

    return subCategories.filter(subCategory => Menu.checkSubCategory(subCategory)).map(subCategory => {

      return (
        <React.Fragment>
          <MenuItem
            key={subCategory.id}
            id={subCategory.id}
            name={subCategory.name}
            description={Menu.renderDescription(subCategory)}
            enabled={true}
            isSubCategory={true}
          />
          {this.renderItems(subCategory)}
        </React.Fragment>
      );
    });
  }

  renderItems(category) {
    const {products, orderEnabled, disableAdding} = this.props;
    const categoryProducts = category.products ? category.products.map((p) => products[p]) : null;

    if (!categoryProducts) {
      return null;
    }

    return categoryProducts.map((item, index) => (
      <MenuItem
        key={item.id}
        isEven={isEven(index)}
        id={item.id}
        onPress={this.handleItemPress}
        name={item.name}
        description={item.description || ''}
        price={item.price}
        enabled={orderEnabled && !disableAdding}
      />
    ));
  }

  renderCategories() {
    const {sessionCategories, categories} = this.props;

    return sessionCategories.map(category => {

      if ((category.products && category.subCategories && category.products.length === 0 && category.subCategories.length === 0)
        || !category.isActive) {
        return null;
      }

      //region return if there are not active subcategires and top level items
      const isActiveSubCategoriesAvailable = category.subCategories.map(subC => categories[subC]).some(subC => Menu.checkSubCategory(subC));
      if (!isActiveSubCategoriesAvailable && (!category.products || category.products.length === 0)) {
        return null;
      }
      //endregion


      return (
        <Section className="menuContent" key={category.id}>
          <MenuHeader>
            <CategoryTitle>{category.name}</CategoryTitle>
          </MenuHeader>
          {Menu.renderDescription(category)}
          <Wrapper between="xs">
            {this.renderItems(category)}
            {this.renderSubCategories(category)}
          </Wrapper>
        </Section>
      );
    });
  }

  render() {
    const {isLoading} = this.props;

    return (
      <div>
        {isLoading ? Menu.renderLoading() : this.renderCategories()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    categories: state.app.categories,
    sessionCategories: state.app.sessionCategories.map(id => state.app.categories[id]),
    products: state.app.products,
    isLoading: state.app.catalogIsLoading
  };
}

export default connect(mapStateToProps, {
  selectMenuItem,
})(Menu);
