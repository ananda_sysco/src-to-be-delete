import React, { Component } from 'react';
import { isString } from 'lodash';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Row,
  Col,
  Heading,
  Label,
  Paragraph,
  Button,
  AddIcon,
} from 'lib/ui';
import { formatPrice } from 'utils/toolbox';

const Wrapper = styled(Col)`
  margin-bottom: 1rem;
  padding-bottom: 0.5rem;
  min-height: 4rem;
  border-bottom: ${props => props.subCategory ? 'none' : '1px solid #ececec'};
`;

const PriceText = styled(Label)`
  word-break: break-all;
  white-space: pre-wrap;
`;

const Divider = styled(Label)`
  margin: 0 .5rem;
`;

const CategoryHeading = styled(Heading)`
  font-family: 'BrixSansLightItalic';
  font-weight: bold;
  font-size: 0.9em;
  color: #363532;
  &:before {
    content: " ";
    position: relative;
    top: 10px;
    right: 10px;
    border-top: 2px solid #363532;
    padding-left: 40px;
  }
  &:after {
    content: " ";
    position: relative;
    top:10px;
    left: 10px;
    border-top: 2px solid #363532;
    padding-right: 40px;
}`;

const DescriptionParagraph = styled(Paragraph)`
  font-size: 13px;
`;
const MenuColumnDivider = styled.div`
  width: 30px;
  height: 100%;
`;
export default class MenuItem extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    enabled: PropTypes.bool,
    isSubCategory: PropTypes.bool,
    onPress: PropTypes.func,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    index: PropTypes.number,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  };

  static defaultProps = {
    price: null,
    enabled: true,
    isSubCategory: false,
  };

  handlePress = () => {
    const { onPress, id } = this.props;

    if (onPress) {
      onPress(id);
    }
  };

  static renderTitle(name, price, isSubCategory) {

    if (price === null || isSubCategory) {
      return (
        <Row center={isSubCategory ? 'xs' : ''}>
          <CategoryHeading subCategoryHeading={isSubCategory} xxsmall>{name}</CategoryHeading>
        </Row>
      );
    }

    return (
      <Row>
        <Heading xxsmall>{name}</Heading>
        <Divider>|</Divider>
        <PriceText neutral>{isString(price) ? price : formatPrice(price)}</PriceText>
      </Row>
    );
  }

  static generateClassName(isEven, isSubCategory) {
    return `${isSubCategory ? 'subCategory' : 'menuItem'} ${isEven ? 'oddItemRow' : 'evenItemRow'}`;
  }

  render() {
    const { name, price, description, enabled, isSubCategory, isEven } = this.props;

    return (
      <Wrapper subCategory={isSubCategory} xs={12} sm={isSubCategory ? 12 : 6} md={isSubCategory ? 12 : 6} className={MenuItem.generateClassName(isEven, isSubCategory)}>
        <Row>
          <Col xs={isSubCategory ? 12 : 10}>
            {MenuItem.renderTitle(name, price, isSubCategory)}
            <Row center={isSubCategory ? 'xs' : ''}>
              <DescriptionParagraph>{description}</DescriptionParagraph>
            </Row>
          </Col>
          {!isSubCategory && <Col xs={2}>
            <Row end='xs'>
              {enabled &&
                <Button transparent onPress={this.handlePress}>
                  <AddIcon />
                </Button>
              }
            </Row>
          </Col>
        }
        </Row>
        <MenuColumnDivider className="menuColumnDivider" />
      </Wrapper>
    );
  }
}
