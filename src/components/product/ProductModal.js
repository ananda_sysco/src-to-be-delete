import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect, Provider} from 'react-redux';
import Modal from 'react-modal';
import {deselectProduct} from 'store/productModal';
import {dismissNotification} from 'store/app';
import ProductLayout from './ProductLayout';
import store from 'store';
import {modalStyle} from 'lib/StyledUI';
import {isMobile} from '../../utils/commonUtil';


export class ProductModal extends Component {

  static propTypes = {
    deselectProduct: PropTypes.func.isRequired,
    isProductModalOpen: PropTypes.bool.isRequired,
    isCartOpenedInMobile: PropTypes.bool.isRequired
  };

  handleCloseModal = () => {
    this.props.deselectProduct();
    if (this.props.notifications.length > 0) {
      this.props.dismissNotification();
    }
  };

  onHashChange(e) {
    if (this.props.isProductModalOpen) {
      if ((e.oldURL.includes('#cartItem') || e.oldURL.includes('#item')) && !e.newURL.includes('#cartItem') && !e.newURL.includes('#item')) {
        this.handleCloseModal();
      }
    } else {
      if ((e.newURL.includes('#cartItem') || e.newURL.includes('#item'))) {
        window.history.replaceState(null, null, window.location.pathname);
      }
    }
  }

  componentDidMount() {
    if (isMobile()) {
      window.addEventListener("hashchange", this.onHashChange.bind(this), false);
    }
  }

  componentWillUnmount() {
    if (isMobile()) {
      window.removeEventListener("hashchange", this.onHashChange.bind(this), false);
    }
  }


  componentWillReceiveProps(nextProps) {
    if (!this.props.isProductModalOpen && nextProps.isProductModalOpen && isMobile()) {
      if (this.props.isCartOpenedInMobile) {
        window.location = "#cartItem";
      } else {
        window.location = "#item";
      }
    }
  }

  render() {
    const {isProductModalOpen} = this.props;

    return (
      <Modal
        isOpen={isProductModalOpen}
        className="ProductModal"
        contentLabel="Product Customization Modal"
        onRequestClose={this.handleCloseModal}
        style={modalStyle}
      >
        <Provider store={store}>
          <ProductLayout/>
        </Provider>
      </Modal>
    );
  }
}

function mapState(state) {
  return {
    isProductModalOpen: state.productModal.isOpen,
    notifications: state.app.notifications,
    isCartOpenedInMobile: state.cart.isCartOpenedInMobile
  };
}

export default connect(mapState, {deselectProduct, dismissNotification})(ProductModal);
