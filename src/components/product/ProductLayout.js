import React, {Component} from 'react';
import {get, sortBy, toPairs} from 'lodash';
import styled from 'styled-components';
import {CONSTANT, trackPageView} from '../analytics/analyticsUtils';
import FaTrash from 'react-icons/lib/fa/trash';
import {connect} from 'react-redux';
import {
  AddIconFilled,
  BackIcon,
  Button,
  CheckIconFilled,
  Col,
  Label,
  Counter,
  Heading,
  Paragraph,
  RadioButton,
  Row,
  TextArea,
  themed
} from 'lib/ui';
import {CloseButton, ExitRow, StyledContainer, TitleRow} from 'lib/StyledUI';
import AttributeForm from 'components/product/forms/AttributeForm';
import Loading from 'components/global/Loading';
import PropTypes from 'prop-types';
import {addToCart, createErrorNotification, removeFromCart, updateCart, updateCartViewMode} from 'store/cart';
import {dismissNotification, setAttributeMessages} from 'store/app';
import {
  clearAttributes,
  deselectProduct,
  selectProduct,
  updateAttributeGroups,
  updateProductQuantity
} from 'store/productModal';
import {formatPrice} from 'utils/toolbox';
import {isMobile, scrollToElement} from "../../utils/commonUtil";

// helpers
const foreground = ({theme}) => theme.colors.foreground;
const primary = ({theme}) => theme.colors.primary;

const isSpecialInstruction = id => id === '1';

// Header Components
const Header = styled(Row)`
  background-color: ${themed('colors.background')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
`;
const RemoveButton = styled(Button)`
  @media (max-width: 767px) {
    margin-top: -80px;
    padding-bottom: 80px;
  }
`;
const ProductMobileClose = styled.div`
  font-size: ${themed('text.size.large')}rem;
  color: ${themed('colors.neutral')};
  padding-top: ${themed('layout.gutter.large')}rem;
  padding-right: 1rem;
  padding-left: 1.4rem;
  cursor: pointer;
`;

const DescriptionRow = styled(Row)`
  padding-bottom: ${themed('layout.gutter.small')}rem;
  font-size: ${themed('text.size.small')}rem;
`;

const ProductName = styled.div`
  color: ${themed('colors.foreground')};
  font-family: BrixSansMedium;
  font-size: 1.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
  word-break: break-word;
  white-space: pre-wrap;
`;

const Controls = styled(Col)`
  padding-top: ${themed('layout.gutter.medium')}rem;
  @media (max-width: 1200px) {
    padding-top: 0px !important;
  }
  @media (max-width: 480px) {
    padding-right: 0px;
  }
`;

const CloseWrapper = styled(({loading, ...props}) => <Col {...props} />)`
  background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;

const StyledCounter = styled(Counter)`
  padding-top: ${themed('layout.gutter.large')}rem;
  padding-right: ${themed('layout.gutter.medium')}rem;
`;

// Option Components
const SectionDivider = styled(Col)`
  padding-left: ${themed('layout.gutter.xlarge')}rem;
  background-color: ${themed('colors.foreground')};
`;

const SectionTitle = styled(Heading)`
  font-size: ${themed('text.size.medium')}rem;
  color: ${themed('colors.background')};
  padding-top: ${themed('layout.gutter.small')}rem;
  padding-bottom: ${themed('layout.gutter.small')}rem;
  margin: 0;
`;

const AttributeTitle = styled.div`
  font-size: ${themed('text.size.small')}rem;
  font-family: BrixSansMedium;
  color: ${themed('colors.neutral')};
  padding-top: ${themed('layout.gutter.small')}rem;
  padding-bottom: ${themed('layout.gutter.small')}rem;
  text-transform: uppercase;
`;

// Footer Components
const Footer = styled(Row)`
  border-color: ${foreground};
  background-color: ${themed('colors.foregroundLight')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
`;

const SubCategoryWrapper = styled(Row)`
  background-color: ${themed('colors.foregroundLight')};
  padding: ${themed('layout.gutter.medium')}rem;
`;

const FooterContent = styled(Row)`
  height: 100%;
  margin-top: ${themed('layout.gutter.medium')}rem;
`;

const AddToCartBtn = styled(Button)`
  font-size: 4rem;
  color: ${primary};
  -webkit-backface-visibility: hidden;
`;

const TrashIcon = styled(FaTrash)`
  color: ${themed('colors.neutral')};
  margin-right: ${themed('layout.gutter.xsmall')}rem;
`;

const ProductChoices = styled(Col)`
  padding-top: ${themed('layout.gutter.small')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
`;

const ProductRadioWrapper = styled(Col)`
  margin-bottom: ${themed('layout.gutter.small')}rem;
  color: ${themed('colors.foreground')};
  font-family: BrixSansLight;
`;

const AttributeFormWrapper = styled(Row)`
  margin-right: ${themed('layout.gutter.xlarge')}rem;
  margin-left: ${themed('layout.gutter.xlarge')}rem;
`;
const AttributeFormWrapperBottomSpace = styled(Row)`
  margin: 0 ${themed('layout.gutter.xlarge')}rem 40px ${themed('layout.gutter.xlarge')}rem;
`;
const SubCategoryDescription = styled(Col)`

`;
const SubCategoryDescriptionLabel = styled(Label)`
  font-family: "BrixSansLight";
  font-size: 0.9rem;
  padding-bottom: 28px;
  padding-left: 2.57rem;
  padding-right: 2.57rem;
  word-break: break-word;
  white-space: pre-wrap;
  word-wrap: break-word;
`;

const ATTRIBUTE_SEPARATOR_VALUE = 3;
const ATTRIBUTE_SEPARATOR_VALUE_FOR_MOBILE = 2;

export class ProductLayout extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    isSubCategory: PropTypes.bool,
  };

  static defaultProps = {
    id: '',
    isSubCategory: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      attributeGroups: {},
      loading: true,
      error: false,
      quantity: 1,
      isValidFormSubmission: {},
      isMobile: isMobile()
    }
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateMobileStatus.bind(this));
    this.updateProductState(this.props);
    trackPageView(CONSTANT.ADD_ITEMS);
  }

  componentWillReceiveProps(nextProps) {
    this.updateProductState(nextProps);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateMobileStatus.bind(this));
  }

  /**
   * Calculate & Update state of new dimensions, whether its mobile or not
   */
  updateMobileStatus() {
    this.setState({isMobile: isMobile()});
  }

  updateProductState(props) {
    const {product, attributeSet, attributeGroups} = props;
    const stateAttributeGroups = this.state.attributeGroups;

    const tempAttributeGroups = {...props.attributeGroups};
    if (attributeGroups) {
      if (stateAttributeGroups && stateAttributeGroups["1"]) {    //Copy special instructions from current state
        tempAttributeGroups["1"] = stateAttributeGroups["1"];
      }
      this.setState({attributeGroups: tempAttributeGroups, loading: false});
    }

    if (product && product.attributeSet) {

      // attribute group state will be kept by attributeGroupId: { attributeValueId: Boolean }
      if (attributeGroups && Object.keys(attributeGroups).length > 0) {
        let temp = {};
        attributeSet.attributes.forEach((attr) => {
          if (attr.minSelections > 0) {
            const temp2 = attributeGroups[attr.id];
            temp[attr.id] = toPairs(temp2).filter(k => (k.length === 2 && k[1] === true)).length >= attr.minSelections;
          }
        });
        this.setState({isValidFormSubmission: temp});
        return;
      }

      const initalAttributeGroups = {};

      let isValidFormSubmission = this.state.isValidFormSubmission;

      attributeSet.attributes.forEach((attr) => {
        // perhaps a better way to handle special instructions?
        if (attr.minSelections > 0) {
          isValidFormSubmission[attr.id] = false;
        }
        if (isSpecialInstruction(attr.id)) {
          initalAttributeGroups[attr.id] = '';
        } else {
          initalAttributeGroups[attr.id] = {};
        }
      });

      this.setState({attributeGroups: initalAttributeGroups, loading: false, error: false, isValidFormSubmission});
    } else {
      this.setState({loading: false, error: true});
    }
  }

  handleProductChange = (attributeFormState) => {
    this.props.updateAttributeGroups(attributeFormState);
  };

  updateProductQuantity = (value) => {
    if (value !== this.props.quantity) {
      this.props.updateProductQuantity(value);
    }
  };

  generateAttributeValidationMessage = () => {
    const {attributeSetMinMaxMessages} = this.props;
    const {isValidFormSubmission} = this.state;
    let validationMessage = [];
    if (attributeSetMinMaxMessages && isValidFormSubmission) {
      for (let key in isValidFormSubmission) {
        if (isValidFormSubmission.hasOwnProperty(key)) {
          if (!isValidFormSubmission[key] && attributeSetMinMaxMessages[key]) {
            if (!attributeSetMinMaxMessages[key].message || !attributeSetMinMaxMessages[key].name) {
              continue;
            }
            validationMessage.push(attributeSetMinMaxMessages[key].message + " from " + attributeSetMinMaxMessages[key].name);
          }
        }
      }
    }

    return validationMessage;
  };

  createAttributeValidationNotification = () => {
    let minMaxValidationMessage = this.generateAttributeValidationMessage();
    let constructedMessage;
    if (minMaxValidationMessage.length > 1) {
      for (let message of minMaxValidationMessage) {
        constructedMessage = constructedMessage ? (constructedMessage + "- " + message + "\n") : "- " + message + "\n"
      }
    } else {
      constructedMessage = minMaxValidationMessage[0];
    }
    if (this.props.notifications.length > 0) {
      this.props.dismissNotification();
    }
    this.props.createErrorNotification(constructedMessage);
  };

  changeHashInMobile = (forced) => {
    if (this.props.isCartOpenedInMobile && !forced) {
      window.location = "#cart";
    } else {
      window.history.replaceState(null, null, window.location.pathname);
    }
  };

  handleAddToCart = () => {

    if (!this.isValid()) {
      //region navigate to the erroneous area
      scrollToElement('.validationMessage');
      //endregion

      this.createAttributeValidationNotification();
      return;
    }

    const {index} = this.props;
    const {specialInstructionsDirty, attributeGroups} = this.state;
    const specialInstructions = specialInstructionsDirty
      ? attributeGroups[1]
      : get(this.props, 'attributeGroups.1', '');
    const item = {
      menuId: this.props.menuId,
      productId: this.props.selectedProduct,
      quantity: this.props.quantity,
      cost: this.props.cost,
      attributeGroups: {...this.props.attributeGroups, 1: specialInstructions},
      isSubCategory: this.props.isSubCategory
    };

    let {name, kitchenName} = this.props.product;
    if (item.isSubCategory) {
      name = this.props.subCategory.name + '-' + name;
    }

    if (index === null) {
      this.props.addToCart(item);
    } else {
      this.props.updateCart(index, {...item, name, kitchenName});
    }
    if (this.props.notifications.length > 0) {
      this.props.dismissNotification();
    }
    this.changeHashInMobile(false);
  };

  handleRemoveFromCart = () => {
    if (!this.props.disableCheckout) {
      const {index, items, isCartOpenedInMobile} = this.props;
      this.props.removeFromCart(index);
      this.props.deselectProduct();
      if (isCartOpenedInMobile && items && items.length === 1) {
        this.changeHashInMobile(true);
        this.props.updateCartViewMode(false);
        document.body.classList.remove('mobileCartVisible');
      } else {
        this.changeHashInMobile(false);
      }
    }
  };

  handleClose = () => {
    this.props.deselectProduct();
  };

  handleCloseOnMobile = () => {
    this.changeHashInMobile();
    this.handleClose();
  };



  handleInstructions = (val) => {
    const instructionState = {1: val};
    const newState = Object.assign({}, this.state.attributeGroups, instructionState);
    this.setState({attributeGroups: newState, specialInstructionsDirty: true});
  };

  handleSelectProduct = (productId) => {
    this.setState({attributeGroups: null, loading: true, isValidFormSubmission: {}}, () => {
      this.props.clearAttributes();
      this.props.selectProduct(productId);
    });
  };

  handleAttributeGroupChange(data, minReached, id) {
    this.props.updateAttributeGroups(data);
    if (this.state.isValidFormSubmission[id] !== minReached) {
      let temp = Object.assign({}, this.state.isValidFormSubmission);
      temp[id] = minReached;
      this.setState({isValidFormSubmission: temp});
    }
  }

  renderClose() {
    const {loading} = this.state;

    return (
      <CloseWrapper loading={loading} xs={12}>
        <ExitRow start="xs" end="sm">
          <Col smOffset={11} xs={2} sm={1}>
            <CloseButton className="hideFromMobile" onClick={this.handleClose}/>
            <ProductMobileClose className="showOnMobile productBack" onClick={this.handleCloseOnMobile}>
              <BackIcon/>
            </ProductMobileClose>
          </Col>
        </ExitRow>
      </CloseWrapper>
    );
  }

  renderOptions() {
    const {attributeSet: {attributes}} = this.props;

    // sort special instructions to the bottom
    const sorted = sortBy(attributes, attr => isSpecialInstruction(attr.id));

    return sorted.map((a) => {
      if (isSpecialInstruction(a.id)) {
        return (
          <AttributeFormWrapperBottomSpace key={a.id}>

            <AttributeTitle>{a.name}</AttributeTitle>
            <Col xs={11}>
                <TextArea
                  name={a.name}
                  placeHolder="Substitutions or additions that affect price will be charged at pickup/delivery"
                  value={(this.props.attributeGroups && this.props.attributeGroups['1']) || ''}
                  onChange={this.handleInstructions}
                  maxlength="255"
                />
            </Col>

          </AttributeFormWrapperBottomSpace>
        );
      }

      return (
        <AttributeFormWrapper key={a.id}>
          <AttributeForm
            attribute={a}
            onChange={this.handleAttributeGroupChange.bind(this)}
            initialValues={this.props.attributeGroups ? this.props.attributeGroups[a.id] : null}
            showMinErrorMessage={this.state.isValidFormSubmission[a.id] === false}
            attributeMessageMapper={this.props.setAttributeMessages}
          />
        </AttributeFormWrapper>
      );
    });
  }

  renderDescription(subCategory, product, isSubCategory) {
    if ((subCategory && subCategory.description) || (product && product.description)) {
      return (
        <DescriptionRow>
          <Paragraph>{isSubCategory ? subCategory.description : product.description}</Paragraph>
        </DescriptionRow>)
    }
  }

  renderHeader() {
    const {isSubCategory} = this.props;
    const {subCategory, product} = this.props;

    return (
      <Header>
        <Col xs={7} sm={8} md={8} lg={9}>
          <TitleRow>
            <ProductName xlarge>{isSubCategory ? subCategory.name : product.name}</ProductName>
          </TitleRow>
          {this.renderDescription(subCategory, product, isSubCategory)}
        </Col>
        <Controls xs={5} sm={4} md={4} lg={3}>
          <StyledCounter
            value={this.props.quantity}
            onChange={this.updateProductQuantity}
          />
        </Controls>
      </Header>
    )
  }

  renderProducts() {
    const {subCategoryProducts} = this.props;
    const separatorValue = this.state.isMobile ? ATTRIBUTE_SEPARATOR_VALUE_FOR_MOBILE : ATTRIBUTE_SEPARATOR_VALUE;
    let products = [];
    let categoryList = [];
    let selectedCategoryItem;
    let selectedCategoryItemDescription;
    for (let categoryCount = 0; categoryCount < subCategoryProducts.length; categoryCount++) {
      if (categoryCount !== 0 && categoryCount % separatorValue === 0) {
        products.push(<SubCategoryWrapper>{categoryList.slice()}</SubCategoryWrapper>);
        categoryList.length = 0;
      }
      categoryList.push(
        <ProductRadioWrapper xs={6} sm={4}>
          <RadioButton
            checked={this.props.selectedProduct === subCategoryProducts[categoryCount].id}
            label={`${subCategoryProducts[categoryCount].name} (${formatPrice(subCategoryProducts[categoryCount].price)})`}
            value={subCategoryProducts[categoryCount].id}
            onChange={this.handleSelectProduct.bind(this)}
            group={this.props.subCategory.id}/>
        </ProductRadioWrapper>);
    }
    products.push(<SubCategoryWrapper>{categoryList.slice()}</SubCategoryWrapper>);
    selectedCategoryItem = subCategoryProducts.filter(category => category.id === this.props.selectedProduct);
    selectedCategoryItemDescription = selectedCategoryItem && selectedCategoryItem.length > 0 ? selectedCategoryItem[0].description : null;
    return (
      <div>
        <ProductChoices xs={12}>
          <AttributeTitle>Choose One (Required)</AttributeTitle>
          {products}
        </ProductChoices>
        {selectedCategoryItemDescription &&
        <SubCategoryDescription
          xs={12}><SubCategoryDescriptionLabel>{selectedCategoryItemDescription}</SubCategoryDescriptionLabel></SubCategoryDescription>}
      </div>
    );
  }

  isValid() {
    for (let temp of Object.keys(this.state.isValidFormSubmission)) {
      if (this.state.isValidFormSubmission[temp] === false) {
        return false;
      }
    }
    return true;
  }

  renderFooter() {
    const {index} = this.props;

    return (
      <Footer>
        <Col xs={10} sm={10} md={8}>
          <FooterContent middle="xs">
            {index !== null &&
            <RemoveButton link onPress={this.handleRemoveFromCart}>
              <TrashIcon/> Remove from order
            </RemoveButton>
            }
          </FooterContent>
        </Col>
        <Col xs={2} sm={2} md={4}>
          <FooterContent end="xs" middle="xs">
            <AddToCartBtn className="OverlayBtn" disabled={!this.props.selectedProduct} transparent
                          onPress={this.handleAddToCart}>
              {index === null ? <AddIconFilled/> : <CheckIconFilled/>}
            </AddToCartBtn>
          </FooterContent>
        </Col>
      </Footer>
    );
  }

  render() {
    const {loading, error} = this.state;
    const {isSubCategory, isProductPending} = this.props;

    if (loading || isProductPending) {
      return (
        <StyledContainer>
          {this.renderClose()}
          <Loading/>
        </StyledContainer>
      );
    }
    if (error) {
      return (
        <StyledContainer>
          {this.renderClose()}
          <Col xs={12}>
            <Header>
              <Col xs={12} sm={8} md={8} lg={9}>
                <TitleRow>
                  <ProductName xlarge>Item is not available</ProductName>
                </TitleRow>
              </Col>
            </Header>
          </Col>
        </StyledContainer>
      )
    }
    if (this.props.isSubCategory && !this.props.selectedProduct && this.props.subCategoryProducts.length) {
      let product = this.props.subCategoryProducts[0];
      this.props.selectProduct(product.id);
      return null;
    }

    return (
      <StyledContainer>
        {this.renderClose()}
        <Col xs={12}>
          {this.renderHeader()}
        </Col>
        {isSubCategory && this.renderProducts()}
        <SectionDivider>
          <SectionTitle>Customizations</SectionTitle>
        </SectionDivider>
        <Col xs={12}>
          {this.renderOptions()}
          {this.renderFooter()}
        </Col>
      </StyledContainer>
    )
  }
}

function mapStateToProps(state, props) {
  const {menuId, isSubCategory, index, attributeGroups, quantity, cost, selectedProduct, isProductPending} = state.productModal;
  const {
    restaurantInfo, attributeSets, attributes, attributeValues, products, categories, selectedSession,
    attributeSetMinMaxMessages, notifications, disableCheckout
  } = state.app;
  let subCategoryProducts = null;
  let product = null;
  let subCategory;

  if (isSubCategory) {
    subCategory = categories[menuId];
    subCategoryProducts = subCategory.products.map(pId => products[pId]);
    if (subCategoryProducts.length) {
      if (selectedProduct) {
        product = subCategoryProducts.find(k => k.id === selectedProduct);
      } else {
        product = subCategoryProducts[0];
      }
    }
  } else {
    product = products[selectedProduct];
  }

  const attributeSet = product && attributeSets[product.attributeSet] ? Object.assign({}, attributeSets[product.attributeSet]) : null;

  if (attributeSet) {
    attributeSet.attributes = attributeSet.attributes.map((attributeId) => {
      const attribute = Object.assign({}, attributes[attributeId]);
      attribute.values = attribute.values.map((valueId) => attributeValues[valueId]);
      return attribute;
    });
  }

  const {items, isCartOpenedInMobile} = state.cart;

  return {
    index,
    attributeGroups,
    quantity,
    cost,
    product,
    attributeSet,
    attributeValues,
    subCategoryProducts,
    menuId,
    selectedProduct,
    subCategory,
    isSubCategory,
    restaurantInfo,
    isProductPending,
    selectedSession,
    attributeSetMinMaxMessages,
    notifications,
    items,
    isCartOpenedInMobile,
    disableCheckout
  };
}

export default connect(mapStateToProps, {
  deselectProduct,
  updateProductQuantity,
  updateAttributeGroups,
  addToCart,
  removeFromCart,
  updateCart,
  selectProduct,
  clearAttributes,
  setAttributeMessages,
  createErrorNotification,
  dismissNotification,
  updateCartViewMode
})(ProductLayout);
