import React, {Component} from 'react';
import styled from 'styled-components';
import {forEach, isEqual, noop, reduce, toPairs} from 'lodash';
import {Col, Label, RadioButton, Row, themed} from 'lib/ui';
import * as AppPropTypes from 'utils/proptypes';
import {formatPrice} from 'utils/toolbox';
import PropTypes from 'prop-types';
import {StyledCheckbox} from 'lib/StyledUI';

const FormWrapper = styled(Row)`
  background-color: ${themed('colors.foregroundLight')};
  padding-top: ${themed('layout.gutter.small')}rem;
  padding-bottom: ${themed('layout.gutter.small')}rem;
`;

const FormAttribute = styled(Col)`
  padding-top: ${themed('layout.gutter.medium')}rem;
  color: ${themed('colors.foreground')};
  font-family: BrixSansLight;
  font-size: ${themed('text.size.medium')}rem;
  background-color: transparent;
  @media (max-width: 767px) {
    padding-top: 1.5rem;
}
`;

const SectionTitle = styled.div`
  color: ${themed('colors.neutral')};
  font-size: ${themed('text.size.small')}rem;
  font-family: BrixSansMedium;
  padding: ${themed('layout.gutter.small')}rem;
  text-transform: uppercase;
  word-break: break-word;
  white-space: pre-wrap;
  word-wrap: break-word;
`;

const StyledRadioButton = styled(RadioButton)`
  padding-top: ${themed('layout.gutter.medium')}rem;
  padding-bottom: ${themed('layout.gutter.medium')}rem;
`;

const MaxReachedLabel = styled(Label)`
  margin-bottom: ${themed('layout.gutter.xxsmall')}rem;
  line-height: 33px;
  margin-bottom: 0px !important;
`;

export default class AttributeForm extends Component {

  static propTypes = {
    attribute: AppPropTypes.attribute.isRequired,
    showMinErrorMessage: PropTypes.bool,
    attributeMessageMapper: PropTypes.func
  };

  static defaultProps = {
    onChange: noop,
  };

  constructor(props) {
    super(props);

    const {attribute, initialValues} = props;
    const values = {};
    let tempMaxReached = false;
    let tempSelectedCount = 0;

    // values will be set to true when selected
    if (initialValues) {
      Object.assign(values, initialValues);
      if (attribute && attribute.maxSelections > 0) {
        let length = toPairs(initialValues).filter(k => k.length === 2 && k[1] === true).length;
        tempMaxReached = length === attribute.maxSelections;
        tempSelectedCount = length;
      }
    } else if (attribute.values) {
      attribute.values.forEach((v) => {
        values[v.attributeValueId] = false;
      });
    }

    this.state = {
      dirtied: false,
      isValid: true,
      values,
      selectedCount: tempSelectedCount,
      maxReached: tempMaxReached,
      minReached: true
    };

  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!isEqual(this.props.attribute, nextProps.attribute)) {
      return true;
    } else if (this.props.showMinErrorMessage !== nextProps.showMinErrorMessage) {
      return true;
    }

    return !(isEqual(this.state.values, nextState.values) && (this.state.minReached === nextState.minReached) &&
      (this.state.maxReached === nextState.maxReached));
  }

  componentWillUpdate(nextProps, nextState) {
    const {id} = this.props.attribute;
    this.notifyParent({[id]: nextState.values}, nextState.minReached, id);
    if (!this.props.showMinErrorMessage && nextProps.showMinErrorMessage) {
      this.updateAttributeMessageStore(true);
    }
  }

  componentDidMount() {
    this.updateAttributeMessageStore(false);
  }

  updateAttributeMessageStore = (isForced) => {
    const {minSelections, maxSelections, values, id, name} = this.props.attribute;
    const count = values ? values.length : 0;
    const message = this.constructRequiredInstructions(minSelections, maxSelections, count, isForced);
    this.props.attributeMessageMapper(id, name, message ? message : ""); //sending an empty string just in case a null value gets through
  };

  notifyParent(formState, minReached, id) {
    const {onChange} = this.props;

    if (onChange) {
      onChange(formState, minReached, id);
    }
  }

  handleCheckboxClick = (id) => {
    const {maxSelections, minSelections} = this.props.attribute;
    const currentValue = this.state.values[id];
    let selectedCount = this.state.selectedCount;

    let maxReached = false;
    let minReached = minSelections <= 0 ? true : false;

    if (!currentValue) {
      selectedCount++;
      if (selectedCount === maxSelections) {
        maxReached = true;
      }
      if (selectedCount >= minSelections) {
        minReached = true;
      }
    } else {
      selectedCount--;
      maxReached = false;
      if (selectedCount < minSelections) {
        minReached = false;
      }
    }

    const newValues = Object.assign({}, this.state.values, {[id]: !currentValue});
    this.setState({
      dirtied: true,
      values: newValues,
      minReached: minReached,
      maxReached: maxReached,
      selectedCount: selectedCount
    });

  };

  handleRadioClick = (id) => {
    const {minSelections} = this.props;
    const newValues = {};

    if (this.state.values[id] && minSelections === 1) {
      return;
    }

    forEach(this.state.values, (val, key) => {
      newValues[key] = key === id;
    });

    this.setState({values: newValues});
  };

  isValid = () => {
    const {minSelections, maxSelections} = this.props.attribute;
    let selected = 0;

    if (this.state.values) {
      selected = reduce(this.state.values, (acc, val) => {
        if (val) {
          return acc + 1;
        }
        return acc;
      }, 0);
    }

    return (selected >= minSelections) && (selected <= maxSelections);
  };


  renderChoices() {
    const {id, maxSelections, minSelections, values} = this.props.attribute;

    return values.map((v) => {

      if (maxSelections > 1 || minSelections < 1) {
        return (
          <FormAttribute xs={12} key={`${id}-${v.attributeValueId}`}>
            <StyledCheckbox
              group={id}
              checked={this.state.values[v.attributeValueId]}
              onChange={this.handleCheckboxClick}
              value={v.attributeValueId}
              label={v.price ? `${v.value} (${formatPrice(v.price)})` : `${v.value}`}
              disabled={this.state.maxReached && !this.state.values[v.attributeValueId]}
            />
          </FormAttribute>
        );
      }
      return (
        <FormAttribute xs={12} key={`${id}-${v.attributeValueId}`}>
          <StyledRadioButton
            group={id}
            onChange={this.handleRadioClick}
            checked={this.state.values[v.attributeValueId]}
            value={v.attributeValueId}
            label={v.price ? `${v.value} (${formatPrice(v.price)})` : `${v.value}`}
          />
        </FormAttribute>
      );
    });
  }

  constructRequiredInstructions(minSelections, maxSelections, count, isForced) {
    let messageComponent = null;
    if ((this.props.showMinErrorMessage && minSelections > 0) || isForced) {
      if (minSelections === maxSelections) {
        messageComponent = 'Select ' + minSelections;
      } else {
        if (this.state.maxReached) {
          messageComponent = 'Max value reached';
        } else {
          messageComponent = 'Select at least ' + minSelections;
        }
      }
    }

    if (this.state.maxReached && minSelections !== maxSelections && count !== maxSelections) {
      messageComponent = 'Maximum number of selections reached';
    }
    return messageComponent;
  }

  render() {
    const {name, minSelections, maxSelections, values} = this.props.attribute; //maxSelections already available

    if (minSelections === 0 && maxSelections === 0) {
      return null;
    }

    const count = values ? values.length : 0;
    return (
      <FormWrapper>
        <SectionTitle>{name}</SectionTitle>
        <MaxReachedLabel small italic error>
          <span className={'validationMessage'}>
            {this.constructRequiredInstructions(minSelections, maxSelections, count, false)}
          </span>
        </MaxReachedLabel>
        {this.renderChoices()}
      </FormWrapper>
    );
  }
}
