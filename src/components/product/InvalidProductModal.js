import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import FaCircle from 'react-icons/lib/fa/check-circle';
import {
  Col,
  Paragraph,
  Row,
  themed,
  Button,
} from 'lib/ui';
import {
  StyledContainer,
  modalStyle,
  TitleRow
} from 'lib/StyledUI'
import CartItem from 'components/cart/CartItem';
import {removeInvalidItems, hideInvalidItemsPopup, updateCartViewMode} from 'store/cart';

const foreground = ({theme}) => theme.colors.foreground;
const primary = ({theme}) => theme.colors.primary;

const Header = styled(Row)`
  background-color: ${themed('colors.background')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
`;

const Title = styled.div`
  color: ${themed('colors.foreground')};
  font-family: BrixSansMedium;
  font-size: 1.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  background-color: ${themed('colors.foregroundLight')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
`;

const DescriptionRow = styled(Row)`
  padding-bottom: ${themed('layout.gutter.small')}rem;
  font-size: ${themed('text.size.small')}rem;
`;

const FooterContent = styled(Row)`
  height: 100%;
  margin-top: ${themed('layout.gutter.medium')}rem;
`;

const OkIcon = styled(FaCircle)`
  font-size: 4rem;
  color: ${primary};
`;

const defaultErrorMessage = "Items that are not available now were removed from your cart.";

const InvalidItemRow = styled(Col)`
  background-color: ${themed('colors.foregroundLight')};
  padding-top: ${themed('layout.gutter.xlarge')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
`;

export class InvalidProductModal extends Component {

  static propTypes = {
    handleClose: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    invalidItems: PropTypes.any.isRequired,
    isShown: PropTypes.any.bool,
    // isProductModalOpen: PropTypes.bool.isRequired,
  };


  handleCloseModal = () => {
    this.props.hideInvalidItemsPopup();
  };

  handleOk = () => {
    this.props.removeInvalidItems(this.props.invalidItems);
    this.handleCloseModal();
    const {items, isCartOpenedInMobile} = this.props;
    if (isCartOpenedInMobile && items && items.length === 1) {
      this.props.updateCartViewMode(false);
      document.body.classList.remove('mobileCartVisible');
    }
  };

  renderItems(items) {

    return (
      <InvalidItemRow>
        {items.map(item => (
          <CartItem
            key={item.lineItemId}
            item={item}
            onEdit={null}
            onRemove={null}
            hideActions={true}
          />
        ))}
      </InvalidItemRow>
    );
  }

  render() {

    const {invalidItems, isShown} = this.props;

    return (
      <Modal
        isOpen={isShown}
        className="ProductModal"
        contentLabel="Invalid products"
        onRequestClose={this.handleCloseModal}
        style={modalStyle}
        shouldCloseOnOverlayClick={false}
      >
        <StyledContainer>
          <Col xs={12}>
            <Header>
              <Col>
                <TitleRow>
                  <Title xlarge>Invalid Items in the cart</Title>
                </TitleRow>
                <DescriptionRow>
                  <Paragraph>{defaultErrorMessage}</Paragraph>
                </DescriptionRow>
              </Col>
            </Header>
          </Col>
          {this.renderItems(invalidItems)}
          <Col xs={12}>
            <Footer>
              <Col xs={8}>
              </Col>
              <Col xs={4}>
                <FooterContent end="xs" middle="xs">
                  <Button transparent onPress={this.handleOk}>
                    <OkIcon/>
                  </Button>
                </FooterContent>
              </Col>
            </Footer>
          </Col>
        </StyledContainer>
      </Modal>);

  };
}


function mapState(state) {
  return {
    invalidItems: state.cart.invalidCartItems,
    isShown: state.cart.isInvalidItemsModalShown,
    sessionBasedInvalidItemsInCart: state.cart.sessionBasedInvalidItemsInCart,
    items: state.cart.items,
    isCartOpenedInMobile: state.cart.isCartOpenedInMobile
  };
}

const actions = {removeInvalidItems, hideInvalidItemsPopup, updateCartViewMode};

export default connect(mapState, actions)(InvalidProductModal);
