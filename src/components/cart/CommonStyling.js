import styled from 'styled-components';
import { themed } from 'lib/ui';
import { options } from 'lib/ui/utils/theme';

export const ReceiptLine = styled.div`
  color: ${themed('colors.foreground')};
  margin-top: ${themed('layout.gutter.medium')}rem;
  font-family: ${options({
    bold: 'BrixSansMedium',
    light: 'BrixSansRegular',
  }, 'light')};
  font-size: 1rem;
  text-align: right;
`;

export const DiscountLine = styled(ReceiptLine)`
  font-weight: bold;
  font-style: italic;
`;

export const GrandTotalLine = styled(ReceiptLine)`
  font-weight: bold;
  font-size: 1.25rem;
`;
