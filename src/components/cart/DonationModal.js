/**
 * @author Tharuka Jayalath
 * (C) 2019, Sysco Labs
 * Created: 3/23/20. Mon 2020 19:48
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components';
import Modal from 'react-modal';
import { Button, Col, Row, themed } from 'lib/ui';
import { addDonation, setDonationOverlayDisplayCondition } from 'store/cart';
import {
  CloseButton, ExitRow, FormInput,
  ResponsiveModal, StyledContainer,
  StyledAnchor
} from 'lib/StyledUI';
import { options } from 'lib/ui/utils/theme';
import theme from 'lib/ui/themes/cake';
import { checkNullOrUndefined } from '../../utils/commonUtil';

const foreground = ({ theme }) => theme.colors.foreground;

const CloseWrapper = styled(({ loading, ...props }) => <Col {...props} />)`
  background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;

const DonationButton = styled(Button)`
  width: 89px;
  margin-top: 20px;
  height: 89px;
  border-radius: 6px;
  background-color: ${options({
  unselected: '#F2700F',
  selected: '#A74B00',
}, 'unselected')};
  font-size: ${options({
  normal: '2rem',
  custom: '0.95rem',
}, 'normal')};
`;

const CustomAmountInput = styled(FormInput)`
  padding: 0.5rem;
  border-right: ${options({
  empty: '1px solid #F2700F !important',
  filled: '0px',
}, 'filled')};
`;

const Title = styled.h1`
  font-family: BrixSansMedium;
  font-size: 2.5rem;
  margin-bottom: 1rem;
  color: ${themed('colors.foreground')};
  margin-top: 1.8rem;
  margin-bottom: 5px;
  text-align: center;
  word-break: break-word;
`;

const DetailWrapper = styled(Col)`
  padding: 2% 12% 0 12%;
  font-family: BrixSansLight;
  font-size: 1rem;
  line-height: 1.7rem;
  text-align: center;
  color: #5F5F5F;
  margin-bottom: 20px;
`;

const CustomInputWrapper = styled(Col)`
  margin-top: 37px;
  text-align: center;
  align-items: center;
  justify-content: center;
  display: flex;
`;

const SmallTextWrapper = styled(Col)`
  padding: 15px 20px 20px 20px;
  padding-top: ${options({
  empty: '30px',
  full: '15px',
}, 'empty')};
  font-family: BrixSansLight;
  font-size: 0.8rem;
  color:#484542;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  margin-top: 34px;
  margin-bottom: 30px;
`;

const ActionButton = styled(Button)`
  padding:1rem;
  font-size:1em;1.2em;
`;

const ActionButtonOutline = styled(Button)`
  padding:1rem;
  font-size:1em;1.2em;
  color: #EE7E04;
`;

const MIN_DONATION_AMOUNT = 1;
const MAX_DONATION_AMOUNT = 50;
const CURRENCY_SYMBOL = '$';

const giftingTermsContent = <p>100% of your contribution will go directly to the restaurant. By giving a gift, you agree to the&nbsp;
    <StyledAnchor href="https://www.trycake.com/legal/restaurant-gift-terms/" target="_blank" rel="noopener noreferrer">
      Restaurant Gift Terms.
    </StyledAnchor>
  &nbsp;<span className={'termsBoldText'}>Gifts once made are nonrefundable.</span><br/>
  In the event your restaurant order is canceled (for any reason), the gift amount you selected will continue to be processed.
</p>;

class DonationModal extends Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState(props);
  }

  componentDidUpdate(prevProps) {
    const { donationAmount } = this.props;
    if (prevProps.donationAmount !== donationAmount
      && donationAmount === 0) {
      this.setState(this.getInitialState(this.props));
    }
  }

  getInitialState = props => ({
    donationAmount: props.donationAmount,
    isCustomDonation: props.isCustomDonation,
    isCustomInputVisible: props.isCustomDonation,
    donationValues: props.restaurant ?
      props.restaurant.donationInfo.fixedDonationValues
      .map(value => ({
        value,
        selected: !props.isCustomDonation ? value === props.donationAmount : false
      })) : []
  });

  donationButtonsOnPress = (value) => {
    let { donationValues } = this.state;
    donationValues = donationValues.map(data => ({ ...data, selected: data.value === value }));
    this.setState({
      isCustomDonation: false,
      isCustomInputVisible: false,
      donationAmount: value,
      donationValues
    });
  };

  handleCustomDonationButtonOnPress = () => {
    const { donationValues } = this.state;
    this.setState({
      isCustomInputVisible: true,
      donationAmount: 0,
      donationValues: donationValues.map(data => ({...data, selected: false}))
    });
  };

  renderPredefinedDonationValues = () => {
    const { donationValues, isCustomDonation } = this.state;
    return donationValues.map(data =>(
      <DonationButton selected={!isCustomDonation ? data.selected : false}
                      onPress={()=> this.donationButtonsOnPress(data.value)}>
        ${data.value}
      </DonationButton>
    ));
  };

  validateCustomDonation = (value, validationState) => {
    if (value) {
      const match = value.match(/[0-9]{1,2}/);
      if (!checkNullOrUndefined(match)) {
        const matchedValue = Number.parseInt(match[0], 10);
        const minDonationAmount = this.props.restaurant.donationInfo.minDonationAmount || MIN_DONATION_AMOUNT;
        const maxDonationAmount = this.props.restaurant.donationInfo.maxDonationAmount || MAX_DONATION_AMOUNT;
        if (matchedValue < minDonationAmount) {
          validationState.donationAmount = minDonationAmount;
        } else if (matchedValue > maxDonationAmount) {
          validationState.donationAmount = maxDonationAmount;
        } else {
          validationState.donationAmount = matchedValue;
        }
        validationState.isCustomDonation = true;
      } else {
        document.querySelector('#customDonationInput').value = null;
      }
    }
  };

  customDonationOnChange = ({ target: { value } }) => {
    const validationState = {
      donationAmount: 0,
      isCustomDonation: false
    };
    this.validateCustomDonation(value, validationState);
    this.setState(validationState);
  };

  handleAddGift = () => {
    const { donationAmount, isCustomDonation } = this.state;
    const { addDonation, setDonationOverlayDisplayCondition } = this.props;
    if (donationAmount) {
      addDonation({ donationAmount, isCustomDonation });
    }
    setDonationOverlayDisplayCondition(false);
  };

  handleModalClose = () => {
    const { donationValues } = this.state;
    const { donationAmount, isCustomDonation } = this.props;
    this.setState({
      donationValues: !isCustomDonation
        ? donationValues.map(data => ({ ...data, selected: data.value === donationAmount }))
        : donationValues,
      isCustomInputVisible: isCustomDonation,
      donationAmount,
      isCustomDonation
    });
    this.props.setDonationOverlayDisplayCondition(false)
  };

  render() {
    const { isShown, restaurant } = this.props;
    let donationOverlayMainText = '';
    if (restaurant) {
      donationOverlayMainText = restaurant.donationInfo.donationOverlayMainText;
    }
    const {
      donationAmount, isCustomDonation, isCustomInputVisible
    } = this.state;
    return (
      <ThemeProvider theme={theme}>
        <Modal
        isOpen={isShown}
        className={'DonationModal'}
        contentLabel={'Donation'}
        style={ResponsiveModal}
        shouldCloseOnOverlayClick={true}
        onRequestClose={this.handleModalClose}
        >
          <StyledContainer>
            <CloseWrapper xs={12}>
              <ExitRow end="xs">
                <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                  <CloseButton onClick={this.handleModalClose} />
                </Col>
              </ExitRow>
              <Title xlarge>We Can’t Thank You Enough</Title>
              <DetailWrapper xs={12}>{donationOverlayMainText}</DetailWrapper>
              <Row center="xs">
                <Col xs={9}>
                  <Row around="xs"  center="xs">
                    {this.renderPredefinedDonationValues()}
                    <DonationButton custom
                                    selected={isCustomInputVisible}
                                    onPress={this.handleCustomDonationButtonOnPress}
                    >
                      SET YOUR AMOUNT
                    </DonationButton>
                  </Row>
                </Col>
              </Row>
              {
                isCustomInputVisible &&
                <Row center="xs" className="customInput">
                  <CustomInputWrapper xs={4} sm={3} md={3} lg={2}>
                    <span className="currencySign">{CURRENCY_SYMBOL}</span>
                    <CustomAmountInput id={'customDonationInput'}
                                       name={'custom'}
                                       defaultValue={isCustomDonation ? donationAmount : null}
                                       value={isCustomDonation ? donationAmount : null}
                                       onChange={this.customDonationOnChange}
                                       maxlength={2}
                    />
                    <span className="decimalSign">.00</span>
                  </CustomInputWrapper>
                </Row>
              }
              {
                (donationAmount > 0) &&
                <Footer center="xs">
                  <Col xs={9}>
                    <Row around="xs"  center="xs">
                      <Col xs={6}>
                        <ActionButtonOutline full primary outline
                                             onPress={this.handleModalClose}
                        >
                          Cancel
                        </ActionButtonOutline>
                      </Col>
                      <Col xs={6}>
                        <ActionButton full primary
                                      onPress={this.handleAddGift}
                        >
                          Add Gift
                        </ActionButton>
                      </Col>
                    </Row>
                  </Col>
                </Footer>
              }
              {
                (donationAmount > 0)
                  ? <SmallTextWrapper xs={12} full>{giftingTermsContent}</SmallTextWrapper>
                  : <SmallTextWrapper xs={12} empty>{giftingTermsContent}</SmallTextWrapper>
              }
            </CloseWrapper>
          </StyledContainer>
        </Modal>
      </ThemeProvider>
    )
  }
}

function mapState(state) {
  return {
    isShown: state.cart.isDonationOverlayShown,
    donationAmount: state.cart.donationAmount,
    isCustomDonation: state.cart.isCustomDonation,
    restaurant: state.app.restaurantInfo
  }
}

const actions = { addDonation, setDonationOverlayDisplayCondition };

export default connect(mapState, actions)(DonationModal);
