import moment from 'moment-timezone';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MdCheck from 'react-icons/lib/md/check';
import { Row, Col, Select, Label, themed } from 'lib/ui';
import * as AppPropTypes from 'utils/proptypes';
import {selectOrderTime, orderTypes, orderTimeManualSelect} from 'store/cart';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";

const CheckIcon = styled(MdCheck)`
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.large')}rem;
`;

export class OrderTimeSelect extends Component {

  static propTypes = {
    cart: AppPropTypes.cart.isRequired,
    restaurant: AppPropTypes.restaurant.isRequired,
    options: PropTypes.array,
    selectOrderTime: PropTypes.func.isRequired,
    orderTimeManualSelect: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { cart, options } = this.props;

    if (!options.length) {
      return;
    }

    // default to first order time if none is set
    // TODO: this should be in store logic, but will do
    // just fine right here for now
    const autoselectAsap = !cart.orderTime || options.indexOf(cart.orderTime) === -1;

    if (autoselectAsap) {
      this.props.selectOrderTime(options[0]);
    }
  }

  handleChange = ({ value }) => {
    this.props.selectOrderTime(value);
    this.props.orderTimeManualSelect(true);
    trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_ETA_DROP_DOWN, this.props.restaurant, 1);
  };

  getOptions() {
    const {options} = this.props;

    return options.map((option, index) => {
      const time = moment(option).startOf('minute'); // remove seconds & milis
      return { value: option, label: index === 0 ? `${time.format('h:mma')} (ASAP)` : time.format('h:mma')};
    });
  }

  renderValue = ({ label }) => {
    return (
      <Col>
        <Label small>{label}</Label>
      </Col>
    )
  };

  renderOption = ({ value, label }) => {
    const { orderTime } = this.props.cart;
    const isChecked = value === orderTime;

    return (
      <Row>
        <Col xs={10}>
          <Label>{label}</Label>
        </Col>
        <Col xs={2}>
          <Row center="xs" middle="xs">
            {isChecked && <CheckIcon />}
          </Row>
        </Col>
      </Row>
    )
  };

  render() {
    const options = this.getOptions();
    const { orderTime, orderType } = this.props.cart;
    const disabled = !options.length;
    const placeholder = orderType === orderTypes.delivery ? 'Delivery time' : 'Pick-up time';

    return (
      <Select
        placeholder={disabled ? 'Currently not available' : placeholder}
        clearable={false}
        searchable={false}
        value={orderTime}
        options={options}
        disabled={disabled}
        onChange={this.handleChange}
        optionRenderer={this.renderOption}
        valueRenderer={this.renderValue}
      />
    );
  }
}

function mapState(state) {
  return {
    cart: state.cart,
    restaurant: state.app.restaurantInfo,
    options: state.cart.readyByOptions
  };
}

const actions = {selectOrderTime, orderTimeManualSelect};

export default connect(mapState, actions)(OrderTimeSelect);
