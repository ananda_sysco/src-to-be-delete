import React from 'react';
import theme from 'lib/ui/themes/cake';
import {shallow} from 'enzyme';
import {ThemeProvider} from 'styled-components';
import {TotalsLoader} from "../TotalsLoader";

describe('src/components/cart/TotalsLoader', () => {
  it('should render correctly', () => {
    const snapshot = shallow(
      <ThemeProvider theme={theme}>
        <TotalsLoader numLines={3}/>
      </ThemeProvider>
    );
    expect(snapshot).toMatchSnapshot();
  });
});
