import React from 'react';
import { shallow } from 'enzyme';
import {
  Heading,
  Label,
} from '../../../lib/ui';

// only use for creating a snapshot
import renderer from 'react-test-renderer';
import { ThemeProvider } from 'styled-components';
import theme from 'lib/ui/themes/cake';
import CartItem from '../CartItem';
import {findIndex} from "lodash";
import {editItem } from '../../../store/productModal';
import {removeFromCart} from '../../../store/cart';




const handleItemEdit = (item) => {
  editItem(item.lineItemId);
};

const handleItemRemove = (removed) => {
  // FIXME: doing this by index when we have a
  // perfectly good lineItemId seems silly to me
  const items = [
    {
      "sessionId": "88e528cc-89a1-48bd-e920-509c217e84e2",
      "lineItemId": "b24045f0-187f-11e8-b5b6-5fcb2a461f3a",
      "menuId": "fd701f7f-d4e9-412f-9689-414eda004557",
      "productId": "fd701f7f-d4e9-412f-9689-414eda004557",
      "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
      "quantity": 1,
      "name": "dell3",
      "isSubCategory": false,
      "totalCost": 3,
      "attributeGroups": {
        "1": ""
      },
      "instructions": [],
      "specialInstructions": "",
      "invalidCartItems": []
    }
  ];
  const index = findIndex(items, item => item.lineItemId === removed.lineItemId);
  removeFromCart(index);
};

const testProps = {
  key:"b24045f0-187f-11e8-b5b6-5fcb2a461f3a" ,
  item: {
    "sessionId": "88e528cc-89a1-48bd-e920-509c217e84e2",
    "lineItemId": "b24045f0-187f-11e8-b5b6-5fcb2a461f3a",
    "menuId": "fd701f7f-d4e9-412f-9689-414eda004557",
    "productId": "fd701f7f-d4e9-412f-9689-414eda004557",
    "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
    "quantity": 1,
    "name": "dell3",
    "isSubCategory": false,
    "totalCost": 3,
    "attributeGroups": {
      "1": ""
    },
    "instructions": [],
    "specialInstructions": "",
    "invalidCartItems": []
  },
  onEdit:handleItemEdit,
  onRemove:handleItemRemove
};

describe('src/component/cart/CartItem',()=>{
  it('should render correctly',()=>{
    const snapshot = renderer.create(
      <ThemeProvider theme={theme}>
      <CartItem {...testProps}/>
      </ThemeProvider>
    ).toJSON();
    expect(snapshot).toMatchSnapshot();
  });

});
