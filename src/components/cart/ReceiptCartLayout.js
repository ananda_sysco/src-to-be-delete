import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled, {withTheme} from 'styled-components';
import {connect} from 'react-redux';
import {Col, Heading, Row, themed, Label} from 'lib/ui';
import CartItem from 'components/cart/CartItem';
import * as AppPropTypes from 'utils/proptypes';
import {formatPrice} from 'utils/toolbox';
import {withRouter} from 'react-router';
import {Section} from 'lib/StyledUI';
import {getReceiptFormattedTime} from '../../utils/commonUtil';
import { DiscountLine, GrandTotalLine, ReceiptLine } from './CommonStyling';

const Wrapper = styled(Row)`
  padding: 0rem;
`;

const ReceiptSection = styled(Row)`
  padding: 0rem;
`;
const SectionHeaderContainer = styled(Row)`
  margin-left: 0rem;
  margin-right: 0rem;
`;

const SummarySection = styled(Section)`
  padding-right: 20px;
`;

const OrderDescriptionContainer = styled(Row)`
  padding: 0 0.625rem 0 0.625rem;
  padding-bottom: 40px;
  margin-left: 0.625rem;
  margin-right: 0.625rem;
`;
const CartContainer = styled(Col)`
  padding: 0rem;
`;

const ReceiptHeading = styled(Heading)`
  margin-bottom: ${themed('layout.gutter.medium')}rem;
  text-align:left;
  font-size: 1rem;
  color: ${themed('colors.background')};
  font-family: "DuplicateSlabRegular";
  position: static;
  padding: 10px 20px;
  width: 100%;
  background-color: ${themed({
      loading: 'colors.border',
      default: 'colors.foreground',
      }, 'default')};
  }
`;

const OrderDescriptionLabel = styled(Label)`
  font-size: 0.88rem;
  font-style: italic;
`;

export class ReceiptCartLayout extends Component {

  static propTypes = {
    receipt: AppPropTypes.receipt.isRequired,
    timezone: PropTypes.string.isRequired,
    currency: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  renderItems() {
    const {lineItems = [], createdTime, orderNumber} = this.props.receipt;
    const { currency } = this.props;
    return (
    <ReceiptSection receiptView={true}>
        <CartContainer receiptView={true} xs={12}>
          <SectionHeaderContainer receiptView={true}>
            <ReceiptHeading xxsmall uppercase neutral>Order Details</ReceiptHeading>
          </SectionHeaderContainer>
          <OrderDescriptionContainer>
            <Col xs={6}>
              <Row start={'xs'}>
                <OrderDescriptionLabel>Order Accepted: {getReceiptFormattedTime(createdTime, this.props.timezone)}</OrderDescriptionLabel>
              </Row>
            </Col>
            <Col xs={6}>
              <Row end={'xs'}>
                <OrderDescriptionLabel>Order #{orderNumber}</OrderDescriptionLabel>
              </Row>
            </Col>
          </OrderDescriptionContainer>
          {lineItems.map(item => (
            <CartItem
              key={item.id}
              item={item}
              onEdit={()=>{}}
              onRemove={()=>{}}
              isReceiptView={true}
              currency={currency}
            />
          ))}
        </CartContainer>
      </ReceiptSection>
    );
  }

  renderReceipt() {
    const {
      subTotal,
      tax,
      total,
      discount,
      tipAmount,
    } = this.props.receipt;
    const { currency } = this.props;

    return (
      <SummarySection end={'xs'}>
        <Col xs={12}>
          <Row>
            <Col xs={8} sm={9} md={10}>
              <Row end={'xs'}>
                <ReceiptLine>Sub-Total:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={4} sm={3} md={2}>
              <Row end={'xs'}>
                <ReceiptLine>{currency}{formatPrice(subTotal)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col xs={8} sm={9} md={10}>
              <Row end={'xs'}>
                <ReceiptLine>Sales Tax:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={4} sm={3} md={2}>
              <Row end={'xs'}>
                <ReceiptLine>{currency}{formatPrice(tax)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          {tipAmount > 0 &&
          <Row>
            <Col xs={8} sm={9} md={10}>
              <Row end={'xs'}>
                <ReceiptLine>Tip:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={4} sm={3} md={2}>
              <Row end={'xs'}>
                <ReceiptLine>{currency}{formatPrice(tipAmount)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          }
          {discount > 0 &&
          <Row>
            <Col xs={8} sm={9} md={10}>
              <Row end={'xs'}>
                <DiscountLine>Discount:</DiscountLine>
              </Row>
            </Col>
            <Col xs={4} sm={3} md={2}>
              <Row end={'xs'}>
                <DiscountLine>-{currency}{discount}</DiscountLine>
              </Row>
            </Col>
          </Row>
          }
          <Row>
            <Col xs={8} sm={9} md={10}>
              <Row end={'xs'}>
                <GrandTotalLine receiptView={true}>Total:</GrandTotalLine>
              </Row>
            </Col>
            <Col xs={4} sm={3} md={2}>
              <Row end={'xs'}>
                <GrandTotalLine receiptView={true}>{currency}{formatPrice(total)}</GrandTotalLine>
              </Row>
            </Col>
          </Row>
        </Col>
      </SummarySection>
    );
  }

  render() {
    const {receipt} = this.props;
    if (!receipt.lineItems.length) {
      return null;
    }

    return (
      <Wrapper receiptView={true}>
        <Col xs={12}>
          {this.renderItems()}
          {this.renderReceipt()}
        </Col>
      </Wrapper>
    );
  }
}

function mapState(state) {
  return {
    receipt: state.receipt
  };
}

export default withRouter(connect(mapState, {})(withTheme(ReceiptCartLayout)));
