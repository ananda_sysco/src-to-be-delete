import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { themed, Row, Col, Label, Button } from 'lib/ui';
import { formatPrice } from 'utils/toolbox';

const CartItemWrapper = styled(Row)`
  padding-bottom: ${themed('layout.gutter.medium')}rem;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
  border-color: ${themed('colors.border')};
  border-bottom-width: 1px;
  border-style: solid;
  padding-left: ${props => props.receiptView ? "0.625rem" : "0rem"};
  padding-right: ${props => props.receiptView ? "0.625rem" : "0rem"};
  margin-left: ${props => props.receiptView ? "0px" : "-0.5rem"};
  margin-right: ${props => props.receiptView ? "0.625rem" : "-0.5rem"};
`;

const CenterRow = styled(Row)`
  padding: 0 ${themed('layout.gutter.small')}rem;
`;

const InstructionRow = styled(Row)`
  margin-top: ${themed('layout.gutter.xxsmall')}rem;
  padding: 0 ${themed('layout.gutter.small')}rem;
`;

const Instruction = styled(Label)`
  color: ${themed('colors.neutral')};
  line-height: 150%;
`;


const InstructionLong = styled(Label)`
  color: ${themed('colors.neutral')};
`;

const EditButton = styled(Button)`
  padding-left: 0;
  padding-right: ${themed('layout.gutter.xsmall')}rem;
  @media (max-width: 767px) {
    font-size: 1rem;
}
`;

const RemoveButton = styled(Button)`
  padding-left: ${themed('layout.gutter.xsmall')}rem;
  padding-right: 0;
  @media (max-width: 767px) {
    font-size: 1rem;
}
`;

export default class CartItem extends Component {

  static propTypes = {
    item: PropTypes.shape({
      quantity: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      totalCost: PropTypes.number.isRequired,
    }).isRequired,
    onEdit: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
    hideActions: PropTypes.bool.isOptional,
    isReceiptView: PropTypes.bool,
    currency: PropTypes.string.isOptional
  };

  renderInstructions(item, isReceiptView) {
    const xsSmMdOffset = isReceiptView ? 2 : null;
    const lgOffset = isReceiptView ? 1 : null;
    const xsSmMd = isReceiptView ? 10 : 12;
    const lg = isReceiptView ? 11 : 12;
    const attributes = item.instructions.map((i, index) => {
      return (
        <Col
          xsOffset={xsSmMdOffset}
          smOffset={xsSmMdOffset}
          mdOffset={xsSmMdOffset}
          lgOffset={lgOffset}
          xs={xsSmMd}
          sm={xsSmMd}
          md={xsSmMd}
          lg={lg}
          key={index}
        >
          <Instruction light small>{`- ${i}`}</Instruction>
        </Col>
      );
    });

    if (item.specialInstructions) {
      attributes.push(
        <Col xs={lg} key={attributes.length}>
          <InstructionLong light small>{`- Special Instructions: ${item.specialInstructions}`}</InstructionLong>
        </Col>
      );
    }

    return attributes.length ? attributes : null;
  }

  render() {
    const {item, hideActions, isReceiptView} = this.props;
    const currency = this.props.currency || '';
    let actions = null;
    if (hideActions !== true && !isReceiptView) {
      actions = <CenterRow middle="xs">
        <EditButton
          small
          link
          onPress={this.props.onEdit}
          bindData={this.props.item}
        >
          edit
        </EditButton> |
        <RemoveButton
          small
          link
          onPress={this.props.onRemove}
          bindData={this.props.item}
        >
          remove
        </RemoveButton>
      </CenterRow>;
    }
    return (
      <CartItemWrapper receiptView={isReceiptView}>
        {!isReceiptView && <Col xs={2} sm={2} md={2} lg={2}>
          <Row start="xs">
            <Label>{`x${item.quantity}`}</Label>
          </Row>
        </Col>
        }
        <Col xs={!isReceiptView ? 7 : 9} sm={!isReceiptView ? 6 : 8} md={!isReceiptView ? 6 : 8} lg={!isReceiptView ? 6 : 8}>
          <CenterRow start="xs">
          {isReceiptView && <Col lg={1} md={2} sm={2} xs={2}>
                <Row start="xs">
                    <Label>{`x${item.quantity}`}</Label>
                </Row>
            </Col>
          }
           {isReceiptView ? <Col lg={11} md={10} sm={10} xs={10}>
                <Label bold>{item.name}</Label>
            </Col> : <Label bold>{item.name}</Label>
            }
          </CenterRow>
          <InstructionRow>
            {this.renderInstructions(item, isReceiptView)}
          </InstructionRow>
          {actions}
        </Col>
        <Col xs={3} sm={4} md={4} lg={4}>
          <Row end="xs">
            <Label>{`${currency}${formatPrice(item.totalCost * item.quantity)}`}</Label>
          </Row>
        </Col>
      </CartItemWrapper>
    );
  }
}
