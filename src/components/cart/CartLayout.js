import React, {Component} from 'react';
import {findIndex, debounce, isNil} from 'lodash';
import PropTypes from 'prop-types';
import styled, {withTheme, css} from 'styled-components';
import {connect} from 'react-redux';
import config from 'config';
import {BasketIcon, Button, Col, CrossIcon, Heading, RadioButton, Row, themed, TextArea, Paragraph, SupportUsIcon} from 'lib/ui';
import {StyledCheckbox, Section} from 'lib/StyledUI';
import CartItem from 'components/cart/CartItem';
import {withAvailability} from 'utils/sessionUtil';
import * as AppPropTypes from 'utils/proptypes';
import {formatPrice} from 'utils/toolbox';
import {editItem} from 'store/productModal';
import {
  createErrorNotification,
  orderTypes,
  removeFromCart,
  resetErroneousSession,
  selectOrderTime,
  selectOrderType,
  setContactTypeAttribute,
  setContactTypeMessage,
  setCartDisplayCondition,
  showInvalidItemsPopup,
  updateCartViewMode,
  updateInvalidItems,
  removeDonation,
  setDonationOverlayDisplayCondition,
  PICKUP_FROM_CURBSIDE,
  LEAVE_FOOD_AT_DOOR,
  ATTR_TYPE_NO_CONTACT,
  ATTR_TYPE_NO_CONTACT_DESCRIPTION
} from 'store/cart';
import {getRestaurantBasic, dismissNotification} from 'store/app';
import OrderTimeSelect from './OrderTimeSelect';
import TotalsLoader from './TotalsLoader';
import {withRouter} from 'react-router';
import {fetchSessionSpecificItems, validatePrepDeliverTime} from 'store/utils/selectors';
import {checkNullOrUndefinedOrNotANumber} from "../../utils/commonUtil";
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";
import { DiscountLine, GrandTotalLine, ReceiptLine } from './CommonStyling';

const Wrapper = styled(Row)`
  padding: ${themed('layout.gutter.large')}rem ${themed('layout.gutter.small')}rem;
`;

const BasketIconCol = styled(Col)`
  height: 60px;
  display: flex;
  font-size: 2em;
  align-items: center;
  padding-left: 10px;
  justify-content: center;
`;

const ViewOrderHeader = styled.div`
  height: 60px;
  color: #fff;
  display: none;
  display: flex;
  align-items: center;
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;`;

  const DeliveryWrapper = styled.div`
  margin-top: 15px;
  margin-left: 5px;
  margin-right: 5px;
  textarea {
      margin-bottom: 0px;
      height: 78px;
  }
  label {
      font-size: 0.875rem;
  }
  `;


const Divider = styled(Row)`
  ${props => props.adjustTop && css`
    margin-top: 38px;
  `}
  border-bottom: 1px solid ${themed('colors.border')};
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const SectionHeading = styled(Heading)`
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const HorizontalList = styled.ul`
  display: block;
  width: 100%;
`;

const HorizontalListItem = styled.li`
  &:first-child {
    margin-bottom: 10px;
  }
  padding: ${themed('layout.gutter.small')}rem 8px;
  border-radius: 5px;
  border: 1px solid ${themed('colors.borderLight')};
  background-color: ${themed('colors.background')};

`;
const ViewOrderLink = styled.a`
  font-family: BrixSansExtraLight;
  font-size: 1.25rem;
  color: ${themed('colors.background')};
  height: 60px;
  display: flex;
  align-items: center;
  padding-left: 1rem;
  padding-right: 1rem;
&:focus {
  outline: none;
}
`;
const ItemCountWrapper = styled.div`
  height: 34px;
  display: flex;
  align-items: center;
  font-family: BrixSansExtraLight;
  font-size: 1rem;
  border-radius: 21px;
  background-color: rgba(0,0,0,0.5);
  margin: 12px 20px 10px 5px;
  justify-content: center;
  max-width: 63px;
`;

const orderTypeNotSelectedError = "Please select order type";

const SupportUsSection = styled(Col)`
background-color:#F8F3F0;
padding:1.5rem;
`;
const DonationWrapper = styled(Col)`
margin-top: -20px;
`;

const SupportHeader = styled.h1`
font-family: BrixSansMedium;
font-size: 1.2rem;
text-align: center;
padding:.5rem;
`;

const SupportContent = styled(Row)`
font-family: BrixSansLight;
font-size: 1rem;
line-height: 1.3rem;
margin-bottom: 20px;
padding:.5rem;
`;

const SupportIconWrapper = styled(Col)`
    text-align:center;
    margin:5px 0 20px 0;
`;

export const SupportUsButton = styled(Button)`
  color: #EE7E04;
  @media (min-width: 320px) {
    font-size:0.9em;
  }
  @media (min-width: 480px) {
    font-size:0.9em;
  }
  @media (min-width: 767px) {
    font-size:0.9em;
  }
  @media screen and (min-width: 992px) {
    font-size:1em;
  }
  @media screen and (min-width: 1200px) {
    font-size:1em;
  }
`;

export class CartLayout extends Component {

  static propTypes = {
    cart: AppPropTypes.cart.isRequired,
    restaurant: AppPropTypes.restaurant.isRequired,
    editItem: PropTypes.func.isRequired,
    updateInvalidItems: PropTypes.func.isRequired,
    selectOrderType: PropTypes.func.isRequired,
    canCheckout: PropTypes.bool,
    isLoading: PropTypes.bool,
    sessions: PropTypes.any.isRequired,
    mobileVisibility: PropTypes.bool,
    donationViewEnabled: PropTypes.bool
  };

  static defaultProps = {
    canCheckout: true,
    isLoading: false,
    donationViewEnabled: config.donationViewEnabled
  };

  constructor(props) {
    super(props);

    this.state = {
      invalidItems: [],
      cartInvalid: false,
      buttonMessage: 'Checkout',
      checkoutButtonEnabled: true,
      condition: false
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {erroneousSession} = nextProps;
    let itemsToBeRemoved = [];
    if (erroneousSession) {
      itemsToBeRemoved = fetchSessionSpecificItems(erroneousSession, nextProps.cart.items);
      if (itemsToBeRemoved.length > 0) {
        this.props.updateInvalidItems(itemsToBeRemoved);
        this.props.showInvalidItemsPopup();
      }
      this.props.resetErroneousSession();
    }
    if (nextProps.cart.orderType !== this.props.cart.orderType && nextProps.options && nextProps.options.length > 0) {
      this.props.selectOrderTime(nextProps.options[0]);
    }
  }

  handleOrderTypeChanged = (orderType) => {
    const { notifications, restaurant, disableCheckout } = this.props;
    const isPickup = (orderTypes.pickup === orderType);
    const isDelivery = (orderTypes.delivery === orderType);
    let contactType = '';
    let contactTypeValue = false;

    if (isDelivery) {
      contactType = LEAVE_FOOD_AT_DOOR;
      contactTypeValue = isDelivery;
      trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_DELIVERY_RADIO, restaurant, 1);
    } else if (isPickup) {
      contactType = PICKUP_FROM_CURBSIDE;
      contactTypeValue = restaurant.isCurbsidePickupMandatory;
      trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_PICK_UP_RADIO, restaurant, 1);
    }
    //This is to disable tax calculation during restaurant offline mode, which re-enables the checkout button (RNG-6631)
    if (!disableCheckout) {
      this.props.selectOrderType(orderType);
      this.props.setContactTypeAttribute(contactType, contactTypeValue);

      if (notifications && notifications.length > 0) {
        const {message} = notifications[0];
        if (message && message === orderTypeNotSelectedError) {
          this.props.dismissNotification();
        }
      }
    }
  };

  handleDeliveryContactMethodChanged = (e) => {
    this.props.setContactTypeAttribute(LEAVE_FOOD_AT_DOOR, e.target.checked);
  };

  handleDeliveryMessageChanged = debounce((deliverMessage) => {
    this.props.setContactTypeMessage(deliverMessage);
  }, 200);

  handlePickupMethodChanged = (e) => {
    this.props.setContactTypeAttribute(PICKUP_FROM_CURBSIDE, e.target.checked);
  };

  handlePickupMessageChanged = debounce((pickupMessage) => {
    this.props.setContactTypeMessage(pickupMessage);
  }, 200);

  handleOrderTimeChanged = (orderTime) => {
    this.props.selectOrderTime(orderTime);
  };

  handleItemEdit = (item) => {
    if (!this.props.disableCheckout) {
      trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_EDIT_SHOPPING_CART, this.props.restaurant, 1);
      this.props.editItem(item.lineItemId);
    }
  };

  handleItemRemove = (removed) => {
    // FIXME: doing this by index when we have a
    // perfectly good lineItemId seems silly to me
    if (!this.props.disableCheckout) {
      trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_REMOVE_SHOPPING_CART, this.props.restaurant, 1);
      const {items} = this.props.cart;
      const index = findIndex(items, item => item.lineItemId === removed.lineItemId);
      if (this.state.condition && items.length === 1) {
        document.body.classList.remove('mobileCartVisible');
        this.props.updateCartViewMode(!this.state.condition);
        this.setState({condition: !this.state.condition})
      }
      this.props.removeFromCart(index);
    }
  };

  handleClick() {
    if (!this.state.condition) {
      document.body.classList.add('mobileCartVisible');
      window.location = "#cart";
    } else {
      document.body.classList.remove('mobileCartVisible');
      window.history.replaceState(null, null, window.location.pathname);
    }
    this.props.updateCartViewMode(!this.state.condition);
    this.setState({
      condition: !this.state.condition
    });
  }

  renderDeliveryInfo(deliver) {
    const {orderType, attributes = []} = this.props.cart;
    if (!deliver || orderType !== orderTypes.delivery) {
      return null;
    }

    const hasNoContact = !isNil(attributes.find(attr => attr.key === ATTR_TYPE_NO_CONTACT));
    return (
      <React.Fragment>
        <Paragraph small>
          <StyledCheckbox
            id={'checkBoxToDoor'}
            label={'Leave order at the door'}
            checked={orderType === orderTypes.delivery && hasNoContact}
            value={'deliveryLeaveFoodAtTheDoor'}
            group={'deliveryContactMethod'}
            onClick={this.handleDeliveryContactMethodChanged} />
        </Paragraph>
      </React.Fragment>
    );
  }

  renderCurbsidePickup() {
    const { orderType, attributes = [] } = this.props.cart;
    const { pickup, isCurbsidePickupEnabled, isCurbsidePickupMandatory } = this.props.restaurant;
    if (!pickup || orderType !== orderTypes.pickup || !isCurbsidePickupEnabled) {
      return null;
    }

    const curbsidePickup = !isNil(attributes.find(attr => attr.key === ATTR_TYPE_NO_CONTACT));
    return (
      <React.Fragment>
        <Paragraph small>
          <StyledCheckbox
            id={'checkBoxCurbsidePickup'}
            label={'Pickup from curbside'}
            checked={curbsidePickup || isCurbsidePickupMandatory}
            value={'isCurbsidePickup'}
            group={'pickupMethod'}
            disabled={isCurbsidePickupMandatory}
            onClick={this.handlePickupMethodChanged} />
        </Paragraph>
      </React.Fragment>
    );
  }

  renderOrderType() {
    const {deliver, pickup, deliveryFee} = this.props.restaurant;

    if (!deliver && !pickup) {
      return null;
    }

    const {orderType, attributes = []} = this.props.cart;
    const contactMessage = attributes.find(attr => attr.key === ATTR_TYPE_NO_CONTACT_DESCRIPTION);

    return (
      <div>
        <Section>
          <Col xs={12} sm={12}>
            <Row>
              <SectionHeading xxsmall uppercase neutral>Order type</SectionHeading>
            </Row>
            <Row>
              <HorizontalList className="orderTypeWrapper">
                {pickup &&
                <HorizontalListItem xs={6}>
                  <Row className="orderTypeDelivery">
                    <Col xs={5}>
                      <RadioButton
                        label={'Pick-up'}
                        group={'orderType'}
                        value={orderTypes.pickup}
                        checked={orderType === orderTypes.pickup}
                        onChange={this.handleOrderTypeChanged}
                      />
                    </Col>
                    <Col xs={7}>
                      {orderType === orderTypes.pickup && this.renderOrderTime()}
                    </Col>
                  </Row>
                  <DeliveryWrapper>
                    { this.renderCurbsidePickup() }
                  </DeliveryWrapper>
                  { orderType === orderTypes.pickup &&
                    <TextArea
                        onChange={this.handleDeliveryMessageChanged}
                        name={'deliveryMessage'}
                        value={contactMessage ? contactMessage.value : ''}
                        maxlength={'100'}
                        placeHolder={'Additional instructions (e.g. no utensils, meet me at curb etc)'} />
                    }
                </HorizontalListItem>
                }
                {deliver &&
                <HorizontalListItem xs={6}>
                <Row className="orderTypeDelivery">
                  <Col xs={5}>
                    <RadioButton
                        label={`Delivery (+${formatPrice(deliveryFee)})`}
                        group={'orderType'}
                        value={orderTypes.delivery}
                        checked={orderType === orderTypes.delivery}
                        onChange={this.handleOrderTypeChanged}
                    />
                  </Col>
                  <Col xs={7}>
                    {orderType === orderTypes.delivery && this.renderOrderTime()}
                  </Col>
                  </Row>
                  <DeliveryWrapper>
                    {this.renderDeliveryInfo(deliver)}
                  </DeliveryWrapper>
                  { orderType === orderTypes.delivery &&
                    <TextArea
                      onChange={this.handleDeliveryMessageChanged}
                      name={'deliveryMessage'}
                      value={contactMessage ? contactMessage.value : ''}
                      maxlength={'100'}
                      placeHolder={'Additional instructions (e.g. no utensils, meet me at curb etc)'} />
                  }
                </HorizontalListItem>
                }

              </HorizontalList>
            </Row>
            <Row>

            </Row>
          </Col>
        </Section>
      </div>
    );
  }

  renderOrderTime = () => {
    const {cart} = this.props;

    if (!cart.orderType) {
      return null;
    }

    return (

            <OrderTimeSelect/>
    )
  };

  renderItems() {
    const {items = []} = this.props.cart;

    return (
      <Section>
        <Col xs={12}>
          <Row>
            <SectionHeading xxsmall uppercase neutral>Order Details</SectionHeading>
          </Row>
          {items.map(item => (
            <CartItem
              key={item.lineItemId}
              item={item}
              onEdit={this.handleItemEdit}
              onRemove={this.handleItemRemove}
            />
          ))}
        </Col>
      </Section>
    );
  }

  renderDonationItem() {
    const {
      cart: { donationAmount , empty },
      restaurant: { donationInfo : { donationName }}
    } = this.props;
    if (donationAmount) {
      return (
        <Section>
          <DonationWrapper xs={12}>
            {empty && <Divider adjustTop={true}/>}
            <CartItem key={'donation_item'}
                      item={{ quantity: 1, name: donationName, totalCost: donationAmount, instructions: [] }}
                      onEdit={() => this.props.setDonationOverlayDisplayCondition(true)}
                      onRemove={() => this.props.removeDonation()}
            />
          </DonationWrapper>
        </Section>
      );
    }
    return null;
  }
  renderReceipt() {
    const {
      totalCost,
      deliveryFee,
      tax,
      grandTotal,
      discount,
      tip,
      orderType,
    } = this.props.cart;

    return (
      <Section end={'xs'}>
        <Col xs={12}>
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <ReceiptLine>Subtotal:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <ReceiptLine>{formatPrice(totalCost)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          {(orderType === orderTypes.delivery) &&
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <ReceiptLine>Delivery Fee:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <ReceiptLine>{formatPrice(deliveryFee)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          }
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <ReceiptLine>Sales Tax:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <ReceiptLine>{formatPrice(tax)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          {discount > 0 &&
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <DiscountLine>Discount:</DiscountLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <DiscountLine>-{discount}</DiscountLine>
              </Row>
            </Col>
          </Row>
          }
          {tip > 0 &&
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <ReceiptLine>Tip:</ReceiptLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <ReceiptLine>{formatPrice(tip)}</ReceiptLine>
              </Row>
            </Col>
          </Row>
          }
          <Row>
            <Col xs={7}>
              <Row end={'xs'}>
                <GrandTotalLine>Total:</GrandTotalLine>
              </Row>
            </Col>
            <Col xs={5}>
              <Row end={'xs'}>
                <GrandTotalLine>{formatPrice(grandTotal)}</GrandTotalLine>
              </Row>
            </Col>
          </Row>
        </Col>
      </Section>
    );
  }

  renderLoading() {
    const {
      totalCost,
      tax,
      tip,
      orderType,
      discount,
      grandTotal,
    } = this.props.cart;

    // count lines in receipt so our loader matches
    const numLines = [
      totalCost > 0,
      tax > 0,
      tip > 0,
      orderType === orderTypes.delivery, // fee
      discount > 0,
      grandTotal > 0,
    ].filter(line => line !== false).length;

    return (
      <Section end={'xs'}>
        <TotalsLoader numLines={Math.max(3, numLines)}/>
      </Section>
    );
  }

  /**
   * This method is duplicated in Payment Layout. Need to make this a pure fucntion. Until that. Both places need to be changed.
   * @returns {boolean}
   */
  revalidateSessions() {
    let invalidItems = [];
    let tempInvalidCart = false;
    if (this.props.sessions && this.props.restaurant) {
      let sessions = withAvailability(this.props.sessions, this.props.restaurant.timeZone, false);

      const etaTimeValidations = {};

      if (sessions) {
        const {items = []} = this.props.cart;
        items.forEach(item => {
          let tempSession = sessions[item.sessionId];
          // get session's preparation and deliver time validation
          etaTimeValidations[item.sessionId] = etaTimeValidations[item.sessionId] || validatePrepDeliverTime(this.props.restaurant, item.sessionId, this.props.sessions, this.props.cart.orderType, false);

          if (!tempSession || !(tempSession.isAvailable || tempSession.isPending) || etaTimeValidations[item.sessionId].isTimeOver) {
            invalidItems.push(item);
          }
        });
        if (!(invalidItems.length === 0 && this.state.invalidItems.length === 0)) {
          this.setState({invalidItems});
          this.props.updateInvalidItems(invalidItems);
        }
      } else {
        tempInvalidCart = true;
        this.setState({cartInvalid: true});
        if (this.state.cartInvalid === false) {
          this.props.updateInvalidItems(null, true);
        }
      }
    }
    return tempInvalidCart || invalidItems.length > 0;
  }

  handleCheckoutPressed = () => {
    const { cart, restaurant } = this.props;
    if (checkNullOrUndefinedOrNotANumber(cart.totalCost) || checkNullOrUndefinedOrNotANumber(restaurant.minimumOrderValue) || checkNullOrUndefinedOrNotANumber(restaurant.minDeliveryOrderAmount)) {
      this.props.createErrorNotification("Something went wrong. Please retry.");
    } else if (!cart.orderType) {
      this.props.createErrorNotification(orderTypeNotSelectedError);
    } else {
      let minimumOrderAmountError = null;
      const minimumOrderValue = Math.max(restaurant.minimumOrderValue, restaurant.minDeliveryOrderAmount);
      if (cart.orderType === 'DELIVERY') {
        if (cart.totalCost < minimumOrderValue) {
          minimumOrderAmountError = 'Minimum order amount for Delivery orders is $' + minimumOrderValue + '.'
        }
      } else if (cart.orderType === 'PICKUP' && cart.totalCost < restaurant.minimumOrderValue) {
        minimumOrderAmountError = 'Minimum order amount for Pickup orders is $' + this.props.restaurant.minimumOrderValue + '.'
      }

      if (minimumOrderAmountError === null) {
        this.setState({buttonMessage: 'Please wait'});
        this.props.getRestaurantBasic(this.props.restaurant.accountId).then(restaurnat => {
          if (restaurnat.queueConsumers < 1 || !restaurnat.isCakeOloAvailable) {
            this.setState({checkoutButtonEnabled: false});
            //TODO show error currently reloading page
            window.location.reload();
            return;
          }
          if (this.revalidateSessions()) {
            this.props.showInvalidItemsPopup();
            return;
          } else {
            this.props.history.push(`/${this.props.restaurant.accountId}/checkout`);
          }
          this.setState({buttonMessage: 'Checkout'});
        }).catch(e => {
          this.setState({buttonMessage: 'Checkout'});
          this.props.createErrorNotification("Something went wrong. Please retry.");
        });
      } else {
        this.props.createErrorNotification(minimumOrderAmountError);
      }
    }
  };

  showCart() {
    this.setState({condition: true});
    this.props.updateCartViewMode(true);
    document.body.classList.add('mobileCartVisible');
  }

  hideCart() {
    this.setState({condition: false});
    this.props.updateCartViewMode(false);
    document.body.classList.remove('mobileCartVisible');
  }

  onHashChange(e) {
    if (e.oldURL.includes('#cart') && !e.newURL.includes('#cart')) {
      //region RNG-5811
      const element = document.querySelector('.mobileCartExpanded');
      if (element) {
        element.scrollTop = 0;
      }
      //endregion
      this.hideCart();
    } else if (!this.props.isCartOpenedInMobile && e.newURL.includes('#cart') && !e.oldURL.includes('#cart')) {
      window.history.replaceState(null, null, window.location.pathname);
    }
  }

  componentDidMount() {
    if (!this.props.cart.isEmpty && this.props.cart.condition) {
      this.showCart();
    } else {
      this.hideCart();
    }
    window.addEventListener("hashchange", this.onHashChange.bind(this), false);
  }

  componentWillUnmount() {
    this.props.setCartDisplayCondition(false);
    window.removeEventListener("hashchange", this.onHashChange.bind(this), false);
  }

  renderSupportUsView = () => {
    const {
      cart: { donationAmount, empty },
      restaurant: { isCakeOLODonationEnabled, queueConsumers,
        donationInfo: { supportUsText } },
      donationViewEnabled
    } = this.props;

    if (!donationViewEnabled || !isCakeOLODonationEnabled || queueConsumers < 1) {
      return null;
    }
    if (donationAmount) {
      if (empty) {
        return this.renderDonationItem();
      }
      return null;
    }

    return (
      <Section>
        <SupportUsSection xs={12}>
        <SupportIconWrapper>
          <SupportUsIcon/>
        </SupportIconWrapper>
          <Row>
            <SupportHeader>Showing Some Love</SupportHeader>
          </Row>
          <SupportContent>{supportUsText}</SupportContent>
          <SupportUsButton full primary outline onPress={() => this.props.setDonationOverlayDisplayCondition(true)}>
            ADD A GIFT
          </SupportUsButton>
        </SupportUsSection>
      </Section>
    );
  };


  render() {
    const { cart, restaurant, isLoading, canCheckout, disableCheckout, totalItems } = this.props;

    if (!restaurant.isCakeOloAvailable) {
      return null;
    }

    if (!cart.items.length) {
      return this.renderSupportUsView();
    }

    return (
      <Wrapper className={this.state.condition ? "mobileCartExpanded" : "mobileCartCollapsed"}>
        <ViewOrderHeader className={this.state.condition ? "mobileOrderHeader" : "mobileViewOrderHeader"}
                         xs={12}>
          <Row center="xs">
            <BasketIconCol xs={2} sm={1} md={1}><ViewOrderLink role="button"
                                                               onClick={this.handleClick.bind(this)}>{this.state.condition ?
              <CrossIcon/> :
              <BasketIcon/>}</ViewOrderLink></BasketIconCol>
            <Col xs={7} sm={10} md={10}><ViewOrderLink role="button"
                                                       onClick={this.handleClick.bind(this)}>{this.state.condition ? "My Order" : "View Order"}</ViewOrderLink></Col>
            <Col className={'itemCountCol'} xs={3} sm={1}
                 md={1}
                 onClick={this.handleClick.bind(this)}><ItemCountWrapper>{totalItems ? totalItems : 0}</ItemCountWrapper></Col>
          </Row>
        </ViewOrderHeader>
        <Col xs={12}>
          {this.renderOrderType()}
          <Divider/>
          {this.renderItems()}
          {this.renderDonationItem()}
          {isLoading ? this.renderLoading() : this.renderReceipt()}
          {this.renderSupportUsView()}
          {canCheckout && this.state.checkoutButtonEnabled && !disableCheckout &&
          <Section center={'xs'}>
            <Button full primary onPress={this.handleCheckoutPressed} id={'cart_checkout_btn'}>
              {this.state.buttonMessage}
            </Button>
          </Section>
          }
        </Col>
      </Wrapper>
    );
  }
}

function mapState(state) {
  return {
    cart: state.cart,
    restaurant: state.app.restaurantInfo,
    isLoading: state.cart.totalIsLoading,
    erroneousSession: state.cart.erroneousSession,
    sessions: state.app.sessions,
    disableCheckout: state.app.disableCheckout,
    options: state.cart.readyByOptions,
    isCartOpenedInMobile: state.cart.isCartOpenedInMobile,
    totalItems: state.cart.totalItems,
    notifications: state.app.notifications
  };
}

const actions = {
  selectOrderType,
  setContactTypeAttribute,
  setContactTypeMessage,
  editItem,
  removeFromCart,
  updateInvalidItems,
  showInvalidItemsPopup,
  getRestaurantBasic,
  resetErroneousSession,
  selectOrderTime,
  createErrorNotification,
  updateCartViewMode,
  setCartDisplayCondition,
  dismissNotification,
  removeDonation,
  setDonationOverlayDisplayCondition
};

export default withRouter(connect(mapState, actions)(withTheme(CartLayout)));
