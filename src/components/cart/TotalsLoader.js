import { times, random } from 'lodash';
import React from 'react';
import styled, { withTheme } from 'styled-components';
import { lighten } from 'polished';
import ContentLoader, { Rect } from 'react-content-loader'
import { themed, Col } from 'lib/ui';

const TOTAL_WIDTH = 400;
const LINE_HEIGHT = 16;
const LINE_SPACE = 36;
const LINE_WIDTH_MIN = 175;
const LINE_WIDTH_MAX = 275;

const Wrapper = styled(Col)`
  margin-top: ${themed('layout.gutter.medium')}rem;
`

export function TotalsLoader({ theme, numLines }) {
  const height = (LINE_HEIGHT * numLines) + (LINE_SPACE * (numLines - 1));

  return(
    <Wrapper xs={12}>
      <ContentLoader
        height={height}
        speed={1}
        primaryColor={theme.colors.border}
        secondaryColor={lighten(0.05, theme.colors.border)}
      >
        {times(numLines, (index) => {
          const width = random(LINE_WIDTH_MIN, LINE_WIDTH_MAX, false);
          const x = TOTAL_WIDTH - width;
          const y = (LINE_HEIGHT + LINE_SPACE) * index;

          return <Rect
            key={index}
            x={x}
            y={y}
            width={width}
            height={LINE_HEIGHT}
            radius={5}
          />
        })}
      </ContentLoader>
    </Wrapper>

  )
}

export default withTheme(TotalsLoader);
