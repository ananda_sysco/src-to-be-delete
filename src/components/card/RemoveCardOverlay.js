import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import Loading from 'components/global/Loading';
import {
  Col,
  themed,
  Button,
  Row
} from 'lib/ui';
import {
  StyledContainer,
  modalStyle,
  CloseButton,
  ExitRow
} from 'lib/StyledUI';

import {hideCardRemoveConfirmationModal, confirmCardRemoval} from '../../store/user';

const foreground = ({theme}) => theme.colors.foreground;

const Title = styled.div`
font-family: DuplicateSlabThin;
  font-size: 2.5rem;
  margin-bottom: 1rem;
  color: ${themed('colors.foreground')};
  margin-top: 1rem;
  margin-bottom: 1.5rem;
  text-align: center;
`;

const SubTitle = styled.div`
font-family: BrixSansLight;
  font-size: 0.88rem;
  text-align: center;
  color: ${themed('colors.foreground')};
`;

const CardNumber = styled.div`
font-family: BrixSansMedium;
  font-size: 1rem;
  text-align: center;
  color: ${themed('colors.foreground')};
  margin-top: 3.5rem;
  margin-bottom: 3rem;
`;
const Footer = styled(Row)`
  border-color: ${foreground};
  padding: ${themed('layout.gutter.small')}rem;
  padding-right: 5rem;
  padding-left: 5rem;
  padding-bottom: 3rem;
`;

const CloseWrapper = styled(({loading, ...props}) => <Col {...props} />)`
  background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;

export class RemoveCardOverlay extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    isShown: PropTypes.bool,
    hideCardRemoveConfirmationModal: PropTypes.func,
    confirmCardRemoval: PropTypes.func
  };

  handleCloseModal = () => {
    this.props.hideCardRemoveConfirmationModal();
  };

  formatCardNumber = (value) => {
    if (value) {
      let temp;
      if (value.length === 16) {
        temp = value.match(/.{1,4}/g);
      } else if (value.length === 15) {
        let temp2 = value.match(/.{1,2}/g);
        temp = [];
        temp.push(temp2[0] + temp2[1]);
        temp.push(temp2[2] + temp2[3] + temp2[4]);
        temp.push(temp2[5] + temp2[6] + temp2[7]);
      } else {
        temp = [value];
      }
      let result = '';
      temp.forEach(function (element) {
        result += element + '-';
      });
      return result.substring(0, result.length - 1);
    }
    return null;
  };

  render() {

    const {isShown} = this.props;
    return (
      <Modal
        isOpen={isShown}
        className="RemoveCardModal"
        onRequestClose={this.handleCloseModal}
        style={modalStyle}
        shouldCloseOnOverlayClick={true}
      >
        <StyledContainer>
          <CloseWrapper xs={12}>
            <ExitRow end="xs">
              <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                <CloseButton onClick={this.handleCloseModal}/>
              </Col>
            </ExitRow>
          </CloseWrapper>
          <Title xlarge>Remove Card</Title>
          <SubTitle xlarge>Please confirm you would like to remove the card</SubTitle>
          {this.props.cardDeletedPending && <Loading/>}
          {(this.props.removingCard && !this.props.cardDeletedPending) &&
          <CardNumber>{this.formatCardNumber(this.props.removingCard.number)}</CardNumber>}
          <Col xs={12}>
            <Footer>
              {(this.props.removingCard && !this.props.cardDeletedPending) &&
              <Button primary full onPress={this.props.confirmCardRemoval}>Confirm</Button>}
            </Footer>
          </Col>
        </StyledContainer>
      </Modal>
    );
  };
}


function mapState(state) {
  return {
    isShown: state.user.cardRemoveConfirmationModalShown,
    removingCard: state.user.removingCard,
    cardDeletedPending: state.user.cardDeletedPending,
  };
}

const actions = {hideCardRemoveConfirmationModal, confirmCardRemoval};

export default connect(mapState, actions)(RemoveCardOverlay);
