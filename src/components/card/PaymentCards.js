import {map} from 'lodash';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MdKeyboardArrowDown from 'react-icons/lib/md/keyboard-arrow-down';
import {Select, Label, themed} from 'lib/ui';
import CardOptionComponent from './CardOptionComponent';
import {getFormattedCardNumber} from '../../utils/commonUtil';
import {ADD_CARD_LABEL} from '../../lib/constants';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";


const Wrapper = styled.div`
  display: flex;
`;

const InnerWrapper = styled.div`
  width:100%;
  flex:1;
`;

const DropdownContent = styled.div`
  width:100%;
  display:flex;
  flex-direction: row;
  align-items: center; 
`;

const DropdownHeader = styled.div`
padding:0;
height:100%;
display: flex;
align-items: center;
padding : 0 10px;
`;
const DropdownSelect = styled(Select) `
  height:auto !important;
  padding:0;
`;

const DropdownHeaderText = styled.div`
margin-bottom: 0;
flex:1;
font-family: BrixSansRegular;
color: ${themed('colors.foreground')};
font-size: 1rem;

`;

const DownIcon = styled(MdKeyboardArrowDown) `
  font-size: ${themed('text.size.large')}rem;
  vertical-align: inherit !important;
`;

const UnavailableLabel = styled(Label) `
  margin-bottom: ${themed('layout.gutter.xxsmall')}rem;
`;

const renderArrow = () => null;

export default class PaymentCards extends Component {

  static propTypes = {
    currentDay: PropTypes.number.isRequired,
    cardSet: PropTypes.object.isRequired,
    orderEnabled: PropTypes.bool,
    showCardRemoveConfirmationModal: PropTypes.bool,
  };

  handleCardChange = ({value, label}) => {
    this.props.changeSaveCardStatus(false);
    if (value === ADD_CARD_LABEL) {
      //User wants to add a card
      this.props.switchPaymentInfoEnteringForm(true);
      trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].REGISTERED_CLICK_ON_PLACE_ORDER, this.props.restaurant, 1);
    } else {
      //User selecting an existing card
      this.props.switchPaymentInfoEnteringForm(false);
    }
    this.props.changeSelectedCardIdentifier({value, label});
  };

  componentDidMount = () => {
    this.props.changeSelectedCardIdentifier(this.getSavedCards()[0]);
    this.props.switchPaymentInfoEnteringForm(false);
  };

  renderCardNumberText = ({label}) => {
    return (
      <DropdownHeader>
        <DropdownHeaderText xsmall uppercase>{getFormattedCardNumber(label)} </DropdownHeaderText>
        <DownIcon/>
      </DropdownHeader>
    )
  };

  getSavedCards = () => {
    const currentCard = this.props.cardSelection ? this.props.cardSelection : null;
    const currentCardData = (currentCard && currentCard.value) ? currentCard.value : null;
    const options = map(this.props.cardSet, (card) => ({
      value: card.id,
      label: card.number,
      card: card,
      removeCard: this.props.showCardRemoveConfirmationModal,
      isSelected: (currentCardData && currentCardData === card.id)
    }));

    options.push({value: ADD_CARD_LABEL, label: ADD_CARD_LABEL});
    return options;
  };

  render() {
    const options = this.getSavedCards();
    let selectedCard = this.props.cardDropDownOption;
    if (!selectedCard) {
      selectedCard = options[0];
    }
    const RawComponent = (props) => {
      return (
        <Wrapper>
          <InnerWrapper>
            {props.message &&
            <DropdownContent disabled={props.isPending}>
              <UnavailableLabel small italic error>
                {props.message}
              </UnavailableLabel>
            </DropdownContent>
            }
            <DropdownContent>
              <DropdownSelect
                className="custom-select"
                clearable={false}
                searchable={false}
                value={selectedCard}
                options={options}
                onChange={this.handleCardChange}
                optionComponent={CardOptionComponent}
                valueRenderer={this.renderCardNumberText}
                arrowRenderer={renderArrow}
                disabled={this.props.isPending}
              />
            </DropdownContent>
          </InnerWrapper>
        </Wrapper>
      )
    };
    return <RawComponent></RawComponent>
  }
}
