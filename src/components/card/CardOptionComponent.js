import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MdTrash from 'react-icons/lib/io/trash-a';
import {Button, Label, themed} from 'lib/ui';
import MdCheck from 'react-icons/lib/md/check';
import {getFormattedCardNumber} from '../../utils/commonUtil';
import {ADD_CARD_LABEL} from '../../lib/constants';


const DropdownContent = styled.div`
  width:100%;
  display:flex;
  flex-direction: row;
  align-items: center; 
`;

const ItemWrapper = styled.div`
  width:100%;
  flex:1;
`;

const TrashIcon = styled(MdTrash) `
color: ${themed('colors.neutral')};
  font-size: ${themed('text.size.large')}rem;  
`;
const DeleteButton = styled(Button) `

  background-color: transparent;
`;

const CheckIcon = styled(MdCheck) `
  color: ${themed('colors.primary')};
  font-size: ${themed('text.size.large')}rem;
`;

const CustomDropdownName = styled(Label) `
  font-family: BrixSansRegular;
  font-size:1.1em;
  align-items:
  align-items: center;
`;


export default class CardOptionComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }


  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    isSelected: PropTypes.bool,
    onFocus: PropTypes.func,
    onSelect: PropTypes.func,
    option: PropTypes.object.isRequired,
  };

  handleMouseDown = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onSelect(this.props.option, event);
  };

  handleMouseEnter = (event) => {
    this.props.onFocus(this.props.option, event);
  };

  handleMouseMove = (event) => {
    if (this.props.isFocused) return;
    this.props.onFocus(this.props.option, event);
  };

  handleDeletion = (e) => {
    this.props.option.removeCard(this.props.option.card);
    e.persist();
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
  };

  render() {
    if (this.props.option.value !== ADD_CARD_LABEL) {
      return (
        <DropdownContent
          className={this.props.className}
          onMouseDown={this.handleMouseDown}
          onMouseEnter={this.handleMouseEnter}
          onMouseMove={this.handleMouseMove}
        >
          <ItemWrapper>
            <CustomDropdownName large>{getFormattedCardNumber(this.props.option.label)}</CustomDropdownName>
          </ItemWrapper>
          {this.props.option.isSelected &&
          <div>
            <CheckIcon/>
          </div>
          }
          <div id={this.props.option.label}>
            <DeleteButton onMouseDown={this.handleDeletion}>
              <TrashIcon/>
            </DeleteButton>
          </div>
        </DropdownContent>
      )
    }
    else {
      return (
        <div className="add-item-wrapper">
          <CustomDropdownName
            large
            className={this.props.className}
            onMouseDown={this.handleMouseDown}
            onMouseEnter={this.handleMouseEnter}
            onMouseMove={this.handleMouseMove}
          >
            {this.props.option.label}
          </CustomDropdownName>
        </div>
      )
    }
  };

}
