import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import {withRouter} from 'react-router-dom';
import {Button, Col, NotificationBar, Row, themed} from 'lib/ui';
import {CloseButton, ExitRow, FormInput, ResponsiveModal, StyledContainer} from 'lib/StyledUI';
import {hideResetPasswordModal, showForgotPasswordModal, updatePassword} from 'store/user';
import Loading from 'components/global/Loading';

const foreground = ({theme}) => theme.colors.foreground;

const Title = styled.div`
  color: ${themed('colors.foreground')};
  font-family: DuplicateSlabThin;
  font-size: 2.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
  text-align: center;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  padding: ${themed('layout.gutter.small')}rem 5rem 5rem 5rem;
`;


const CloseWrapper = styled(({loading, ...props}) => <Col {...props} />)`
background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;

const CreatePasswordModalContent = styled.div`
padding: 1rem 5rem;
`;


export class PasswordResetModal extends Component {

  constructor(props) {
    super(props);
    this.state = {};

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  validate(state) {
    return true; //TODO implement
  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };

    this.setState({...tempState});
  }


  static propTypes = {
    handleClose: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    isShown: PropTypes.any.bool,
    isPending: PropTypes.any.bool,
    isSuccess: PropTypes.any.bool,
    accountId: PropTypes.any.string,
    errorCode: PropTypes.any.string
  };


  handleCloseModal = () => {
    this.setState({});
    if (this.props.accountId) {
      this.props.history.replace('/' + this.props.accountId);
    } else {
      this.props.history.replace('/');
    }
    this.props.hideResetPasswordModal();
  };

  handleForgetPassword = () => {
    this.handleCloseModal();
    this.props.showForgotPasswordModal();
  };

  handleOk = () => {
    if (this.state.password && this.state.password.length < 6) {
      this.setState({retypePasswordError: 'Password should contain at least 6 characters'});
    } else if (this.state.password && this.state.password === this.state.retypePassword) { //TODO add Validations
      this.props.updatePassword(this.state.password);
    } else {
      this.setState({retypePasswordError: 'Passwords do not match'});
    }
  };

  renderForm(isPending) {
    if (isPending) {
      return <Loading/>;
    } else {
      return (<form>
        <FormInput name={'password'} placeholder={'Type a new password'} value={this.state.password} type={"password"}
                   onChange={this.handleInputChange}/>
        <FormInput name={'retypePassword'} placeholder={'Retype new password'} type={'password'}
                   value={this.state.retypePassword}
                   onChange={this.handleInputChange} error={this.state.retypePasswordError}/>
      </form>);
    }
  }

  render() {

    const {isShown, isPending, isSuccess, errorCode, errorMessage} = this.props;

    return (
      <Modal
        isOpen={isShown}
        className="LoginModal"
        contentLabel="Create Your New Password"
        onRequestClose={this.handleCloseModal}
        style={ResponsiveModal}
        shouldCloseOnOverlayClick={true}
      >
        <StyledContainer>
          <CloseWrapper xs={12}>
            <ExitRow end="xs">
              <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                <CloseButton onClick={this.handleCloseModal}/>
              </Col>
            </ExitRow>
          </CloseWrapper>
          <div>
            <CreatePasswordModalContent>
              <Title xlarge>Create Your New Password</Title>
            </CreatePasswordModalContent>
            <CreatePasswordModalContent>
              {this.renderForm(isPending)}
            </CreatePasswordModalContent>
            {!isPending && <Col xs={12}>
              <Footer>
                {!isSuccess && <Button full primary onPress={this.handleOk}>
                  Update
                </Button>}

              </Footer>
            </Col>}
          </div>

          {errorCode &&
          <NotificationBar
            type={'error'}
            message={errorMessage ? errorMessage : "Something went wrong please retry"}
            actions={null}
          />}
        </StyledContainer>
      </Modal>);

  };
}


function mapState(state) {
  return {
    isShown: state.user.isResetPasswordModalShown,
    isPending: state.user.isFlowWaiting,
    isSuccess: state.user.isPasswordUpdateSuccess,
    accountId: state.app.restaurantInfo ? state.app.restaurantInfo.accountId : null,
    errorCode: state.user.resetPasswordErrorCode,
    errorMessage: state.user.resetPasswordErrorMessage,
    pwdResetTokenErrorMessage: state.user.pwdResetTokenErrorMessage,
    isPwdResetTokenError: state.user.isPwdResetTokenError
  };
}

const actions = {hideResetPasswordModal, updatePassword, showForgotPasswordModal};

export default connect(mapState, actions)(withRouter(PasswordResetModal));
