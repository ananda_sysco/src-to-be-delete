import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import {Button, Col, NotificationBar, PoweredByLogo, Row, themed} from 'lib/ui';
import {CloseButton, ExitRow, FormInput, ResponsiveModal, StyledContainer} from 'lib/StyledUI'
import {clearResetPasswordStatus, hideForgotPasswordModal, sendResetPasswordEmail} from 'store/user';
import Loading from 'components/global/Loading';
import validator from 'validator';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";

const foreground = ({theme}) => theme.colors.foreground;

const Title = styled.div`
  color: ${themed('colors.foreground')};
  font-family: DuplicateSlabThin;
  font-size: 2.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
  text-align: center;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  padding: ${themed('layout.gutter.small')}rem 5rem 5rem 5em;
`;

const CloseWrapper = styled(({loading, ...props}) => <Col {...props} />)`
background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;

const ForgotPasswordModalContent = styled.div`
padding: 1rem 5rem;
@media (max-width: 480px) {
    padding: 1rem 1rem;
}
@media (max-width: 767px) {
    padding: 1rem 1rem;
}
`;

const LogoWrapper = styled.div`
  width:100%;
  text-align:center;
  position: relative;
  top: 65px;
`;

export class ForgotPasswordModal extends Component {

  static propTypes = {
    handleClose: PropTypes.func,
    clearResetPasswordStatus: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    isShown: PropTypes.any.bool,
    isPending: PropTypes.any.bool,
    accountId: PropTypes.any.string,
    errorCode: PropTypes.any.integer
  };


  constructor(props) {
    super(props);
    this.state = {
      email: null
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleOk = this.handleOk.bind(this);
  }


  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };

    this.setState({...tempState});
  }


  handleCloseModal = () => {
    this.setState({'email': null, 'emailError': null});
    this.props.hideForgotPasswordModal();
    this.props.clearResetPasswordStatus();
  };

  handleOk = () => {
    trackEvent(SCENARIOS.FORGOT_PASSWORD_PROCESS, EVENTS[SCENARIOS.FORGOT_PASSWORD_PROCESS].CLICK_ON_FORGOT_PASSWORD, this.props.restaurant, 1);
    if (this.state.email && validator.isEmail(this.state.email)) {
      this.props.sendResetPasswordEmail(this.state.email, this.props.accountId);
    } else {
      this.setState({'emailError': 'Invalid Email'});
    }
  };

  handleOnBlur = () => {
    if (validator.isEmail(this.state.email ? this.state.email : '')) {
      this.setState({'emailError': null});
    } else {
      this.setState({'emailError': 'Invalid Email'});
    }
  };

  renderForm(isPending) {
    if (isPending) {
      return <Loading/>;
    } else {
      return (
        <FormInput name={'email'} placeholder={'Email'} value={this.state.email} onChange={this.handleInputChange}
                   onBlur={this.handleOnBlur} error={this.state.emailError}/>
      );
    }
  }

  renderNotifications = (errorCode) => {
    let notificationType = 'error';
    let notificationMessage = null;
    if (errorCode && errorCode === 404) {
      notificationType = 'error';
      notificationMessage = 'There is no account associated with this email address.';
    } else if (errorCode && errorCode !== 404) {
      notificationType = 'error';
      notificationMessage = 'Something went wrong please retry';
    }
    return (
      errorCode ?
        <NotificationBar
          type={notificationType}
          message={notificationMessage}
          actions={null}
        /> : null
    )
  };


  render() {

    const {isShown, isPending, errorCode} = this.props;

    return (
      <Modal
        isOpen={isShown}
        className="LoginModal"
        contentLabel="Recover Password"
        onRequestClose={this.handleCloseModal}
        style={ResponsiveModal}
        shouldCloseOnOverlayClick={true}
      >
        <StyledContainer>
          <CloseWrapper xs={12}>
            <ExitRow end="xs">
              <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                <CloseButton onClick={this.handleCloseModal}/>
              </Col>
            </ExitRow>
          </CloseWrapper>
          <ForgotPasswordModalContent>
            <Title xlarge>Recover Password</Title>
          </ForgotPasswordModalContent>
          <ForgotPasswordModalContent>
            {this.renderForm(isPending)}
          </ForgotPasswordModalContent>
          <Col xs={12}>
            <Footer>
              {(!isPending) && <Button full primary onPress={this.handleOk}>
                SUBMIT
              </Button>}
              <LogoWrapper>
                <PoweredByLogo/>
              </LogoWrapper>
            </Footer>
          </Col>
          {this.renderNotifications(errorCode)}
        </StyledContainer>
      </Modal>);
  };
}


function mapState(state) {
  return {
    isShown: state.user.isForgotPasswordModalShown,
    isPending: state.user.isFlowWaiting,
    accountId: state.app.restaurantInfo ? state.app.restaurantInfo.accountId : null,
    errorCode: state.user.resetPasswordEmailErrorCode
  };
}

const actions = {hideForgotPasswordModal, sendResetPasswordEmail, clearResetPasswordStatus};

export default connect(mapState, actions)(ForgotPasswordModal);
