import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import {Button, Col, NotificationBar, PoweredByLogo, Row, themed} from 'lib/ui';
import {CloseButton, ExitRow, FormInput, ResponsiveModal, StyledContainer} from 'lib/StyledUI';
import {authenticateUser, continueAsGuest, hideLoginModal, showForgotPasswordModal} from 'store/user';
import Link from 'components/app/router/Link';
import Loading from 'components/global/Loading';
import validator from 'validator';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";
import {isMobile, scrollToElement} from "../../utils/commonUtil";


const foreground = ({theme}) => theme.colors.foreground;

const Title = styled.div`
  font-family: DuplicateSlabThin;
  font-size: 2.5rem;
  margin-bottom: 1rem;
  color: ${themed('colors.foreground')};
  margin-top: 1rem;
  margin-bottom: 1.5rem;
  text-align: center;
  word-break: break-word;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  padding: ${themed('layout.gutter.small')}rem;
  padding-right: 5rem;
  padding-left: 5rem;
  @media (max-width: 480px) {
    padding-right: 1rem;
    padding-left: 1rem;
  }
  @media (max-width: 767px) {
    padding-right: 1rem;
    padding-left: 1rem;
  }
`;

const ForgotLink = styled(Link)`
  display: block;
  text-align: center;
  width:140px;
  font-family: BrixSansLight;
  margin: 3em auto 4rem auto;
`;

const SignInModalContent = styled.div`
  padding: 1rem 5rem;
  @media (max-width: 480px) {
    padding: 1rem 1rem;
  }
  @media (max-width: 767px) {
    padding: 1rem 1rem;
  }
`;

const LogoWrapper = styled.div`
  width:100%;
  text-align:center;
`;

const CloseWrapper = styled(({loading, ...props}) => <Col {...props} />)`
  background: ${themed({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;


const EMPTY_EMAIL = "Email is required";
const INVALID_EMAIL = "Invalid email";
const EMPTY_PWD = "Password is required";

export class LoginModal extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  static propTypes = {
    handleClose: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    isShown: PropTypes.any.bool,
    isPending: PropTypes.any.bool,
    errorCode: PropTypes.any.int
  };

  handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };
    this.setState({...tempState});
  };

  handleCloseModal = () => {
    this.setState({email: undefined, password: undefined, emailError: null, pwdError: null});
    this.props.hideLoginModal();
  };

  handleForgetPassword = () => {
    this.handleCloseModal();
    this.props.showForgotPasswordModal();
  };

  handleGuestFlow = () => {
    this.handleCloseModal();
    this.props.continueAsGuest();
  };

  validateField = (field) => {
    const value = this.state[field];
    let emailError, passwordError;
    switch (field) {
      case "email":
        if (!value) {
          emailError = EMPTY_EMAIL;
        } else if (!validator.isEmail(value)) {
          emailError = INVALID_EMAIL;
        }
        this.setState({isEmailValid: !emailError, emailError: emailError});
        break;
      case "password":
        if (!value) {
          passwordError = EMPTY_PWD;
        }
        this.setState({isPwdValid: !passwordError, pwdError: passwordError});
        break;
      default:
        return;
    }
  };

  handleBlur = (component) => {
    let target = component.target;
    const field = target.name;
    this.validateField(field);
  };


  handleOk = () => {
    this.validateField('email');
    this.validateField('password');
    let isEmailValid = this.state.isEmailValid;
    let isPasswordValid = this.state.isPwdValid;
    if (isEmailValid && isPasswordValid) {
      trackEvent(SCENARIOS.SIGN_IN, EVENTS[SCENARIOS.SIGN_IN].CLICK_ON_SIGN_IN, this.props.restaurant, 1);
      this.props.authenticateUser(this.state.email, this.state.password, true);
    } else {
      if (!this.state.email) {
        this.setState({isEmailValid: !EMPTY_EMAIL, emailError: EMPTY_EMAIL});
      }
      if (!this.state.password) {
        this.setState({isPwdValid: !EMPTY_PWD, pwdError: EMPTY_PWD});
      }
      //TODO check password
    }
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleOk();
    }
  };

  renderForm(isPending) {
    if (isPending) {
      return <Loading/>;
    } else {
      return (<form>
        <FormInput name={'email'} placeholder={'Email'} value={this.state.email} onChange={this.handleInputChange}
                   onBlur={this.handleBlur} error={this.state.emailError} maxlength={'40'}/>
        <FormInput name={'password'} placeholder={'Password'} type={'password'} value={this.state.password}
                   onChange={this.handleInputChange} onKeyPress={this.handleKeyPress} onBlur={this.handleBlur}
                   error={this.state.pwdError}/>
      </form>);
    }
  }

  render() {

    const {isShown, isPending, errorCode} = this.props;

    let errorMessage = errorCode === 401 ? "Invalid email or password" : "Unexpected Error, Please Retry";

    return (
      <Modal
        isOpen={isShown}
        className="LoginModal"
        contentLabel="Login"
        onRequestClose={this.handleCloseModal}
        style={ResponsiveModal}
        shouldCloseOnOverlayClick={true}
      >
        <StyledContainer>
          <CloseWrapper xs={12}>
            <ExitRow end="xs">
              <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                <CloseButton onClick={this.handleCloseModal}/>
              </Col>
            </ExitRow>
          </CloseWrapper>
          <SignInModalContent>
            <Title xlarge>Sign In</Title>

          </SignInModalContent>
          <SignInModalContent>
            {this.renderForm(isPending)}
          </SignInModalContent>
          {!isPending && <Col xs={12} sm={12}>
            <Footer>
              <Button full primary onPress={this.handleOk}>
                Sign In
              </Button>
              <Col xs={12}>
                <ForgotLink to="#" onClick={this.handleForgetPassword}>
                  Forgot Password?
                </ForgotLink>
              </Col>
              <LogoWrapper>
                <PoweredByLogo/>
              </LogoWrapper>
            </Footer>
          </Col>}

          {(errorCode && !isPending) &&
          <NotificationBar
            id={"loginModalNotification"}
            type={'error'}
            message={errorMessage}
            actions={null}
          />}

        </StyledContainer>
      </Modal>);

  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!prevProps.errorCode && this.props.errorCode) {
      if (!isMobile()) {
        return;
      }
      setTimeout(function () {
        scrollToElement('#loginModalNotification')
      }, 600);
    }
  }

}

function mapState(state) {
  return {
    isShown: state.user.isLoginModalShown,
    isPending: state.user.isFlowWaiting,
    errorCode: state.user.loginModalErrorCode,
    restaurant: state.app.restaurantInfo
  };
}

const actions = {hideLoginModal, authenticateUser, showForgotPasswordModal, continueAsGuest};

export default connect(mapState, actions)(LoginModal);
