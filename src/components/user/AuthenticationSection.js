import React from 'react';
import LoginModal from "components/user/LoginModal";
import ForgotPasswordModal from "components/user/ForgotPasswordModal";
import PasswordResetModal from "components/user/PasswordResetModal";
import InvalidProductModal from "components/product/InvalidProductModal";
import DonationModal from "components/cart/DonationModal";


export default function AuthenticationSection() {
  return [<LoginModal/>, <ForgotPasswordModal/>, <PasswordResetModal/>, <InvalidProductModal/>, <DonationModal/>];
}
