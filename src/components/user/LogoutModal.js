import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Modal from 'react-modal';
import FaCircle from 'react-icons/lib/fa/check-circle';
import {
  Col,
  Row,
  themed,
  Button,
  Grid,
  StyledContainer,
  modalStyle,
  TitleRow
} from 'lib/ui';
import {hideLogoutModal} from 'store/user';

const foreground = ({theme}) => theme.colors.foreground;
const primary = ({theme}) => theme.colors.primary;

const Header = styled(Row)`
  background-color: ${themed('colors.background')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
`;

const Title = styled.div`
  color: ${themed('colors.foreground')};
  font-family: BrixSansMedium;
  font-size: 1.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
`;

const Footer = styled(Row)`
  border-color: ${foreground};
  background-color: ${themed('colors.foregroundLight')};
  padding: ${themed('layout.gutter.small')}rem;
  padding-right: ${themed('layout.gutter.xlarge')}rem;
  padding-left: ${themed('layout.gutter.xlarge')}rem;
`;

const FooterContent = styled(Row)`
  height: 100%;
  margin-top: ${themed('layout.gutter.medium')}rem;
`;

const OkIcon = styled(FaCircle)`
  font-size: 4rem;
  color: ${primary};
`;

export class LogoutModal extends Component {

  static propTypes = {
    handleClose: PropTypes.func,
    // handleOK: PropTypes.func.isRequired,
    isShown: PropTypes.any.bool
  };


  handleCloseModal = () => {
    this.props.hideLogoutModal();
  };

  handleOk = () => {
    this.props.hideLogoutModal();
  };


  render() {

    const {isShown} = this.props;

    return (
      <Modal
        isOpen={isShown}
        className="LoginModal"
        contentLabel="Logout"
        onRequestClose={this.handleCloseModal}
        style={modalStyle}
        shouldCloseOnOverlayClick={true}
      >
        <StyledContainer>
          <Col xs={12}>
            <Header>
              <Col>
                <TitleRow>
                  <Title xlarge>Logout</Title>
                </TitleRow>

              </Col>
            </Header>
          </Col>
          <Col xs={12}>
            <Footer>
              <Col xs={8}>
              </Col>
              <Col xs={4}>
                <FooterContent end="xs" middle="xs">
                  <Button transparent onPress={this.handleOk}>
                    <OkIcon/>
                  </Button>
                </FooterContent>
              </Col>
            </Footer>
          </Col>
        </StyledContainer>
      </Modal>);

  };
}


function mapState(state) {
  return {
    isShown: state.user.isLoginModalShown
  };
}

const actions = {hideLogoutModal};

export default connect(mapState, actions)(LogoutModal);
