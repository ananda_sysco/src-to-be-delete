import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {
  Col,
  Row,
  themed,
  Button,
  Heading,
  Label,
  PoweredByLogo
} from 'lib/ui';
import {FormInput} from 'lib/StyledUI';
import Link from 'components/app/router/Link';
import {
  updateContact,
  checkUser,
  authenticateUser,
  continueAsGuest,
  clearGuestState,
  showForgotPasswordModal
} from 'store/user';
import Loading from 'components/global/Loading';
import validator from 'validator';
import {NotificationBar} from 'lib/ui';


const LoadingWrapper = styled.div`
  padding:100px;
`;
//Style definitions for login starts from here
const LogoWrapper = styled.div`
  width:100%;
`;
const SignInSection = styled(Row)`
  background-color: #F7F7F7;
  padding: ${themed('layout.gutter.large')}rem ${themed('layout.gutter.small')}rem ${themed('layout.gutter.small')}rem ${themed('layout.gutter.small')}rem;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

const GuestButton = styled(Button)`
  color: ${themed('colors.primary')};
  @media (max-width: 1024px) {
    display: none !important;
}
`;

const PoweredByWrapper = styled.div`
  margin: ${themed('layout.gutter.medium')}rem 0 ${themed('layout.gutter.medium')}rem 10px;
`;
const SectionHeading = styled(Heading)`
  margin: ${themed('layout.gutter.medium')}rem 0 ${themed('layout.gutter.medium')}rem 0;
`;

const ForgotLink = styled(Link)`
  display: block;
  text-align: right;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
  float: right;
`;
const GuestMobileLoginLink = styled(Link)`
  text-align: center;
  width:40%;
  font-family: BrixSansLight;
  margin: 2em auto 1rem auto;
  display: none;
  @media (max-width: 1024px) {
    display: block;
  }
`;
//style definitions for login ends here


export class LoginSection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isCreateUserSuccess: false,
      validating: false,
      email: (props.user) ? props.user.email : null
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleGuestLogin = this.handleGuestLogin.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.emailError && nextProps.emailError) {
      if (!this.state.email) {
        this.setState({emailError: 'Email is required'});
      } else {
        this.setState({emailError: 'Invalid email'});
      }
    }

    if (nextProps.loggedInUser && nextProps.user && this.props.loggedInUser === null) {
      this.setState({email: nextProps.user.email, emailError: null})
    }
    if (this.props.loggedInUser && !nextProps.loggedInUser) {
      this.setState({email: null})
    }
  }

  static propTypes = {
    isPending: PropTypes.any.bool,
    hasAccount: PropTypes.any.bool,
    loggedInUser: PropTypes.any.object,
    errorCode: PropTypes.any.int
  };

  /**
   *
   * This is structured according to the rc-form validators
   * @param rule
   * @param value
   * @param callback
   */
  handleBlur() {
    let value = this.state.email;
    if (value) {
      if (validator.isEmail(value)) {
        if (!this.props.user || (this.props.user && this.props.user.email !== value)) {
          this.props.clearGuestState();
          this.props.checkUser(value);
          this.props.updateContact("email", value);
          this.setState({emailError: null});
        } else {
          return;
        }
      } else {
        this.setState({emailError: 'Invalid email'});
        this.props.updateContact("email", null);
      }
    } else {
      this.props.updateContact("email", null);
      this.setState({emailError: 'Email cannot be empty'});
    }
  }

  handleBlurPass() {
    let value = this.state.password;
    if (value) {
      this.setState({passwordError: null});
    } else {
      this.setState({passwordError: 'Password cannot be empty'});
    }
  }

  handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    let tempState = {
      [name]: value
    };
    this.setState({...tempState});
  };

  handleSignIn = () => {
    if (this.state.emailError || this.state.passwordError) {
      return;
    } else {
      this.props.authenticateUser(this.state.email, this.state.password, false);
    }
  };

  handleGuestLogin() {
    this.props.continueAsGuest();
  }

  handleForgotPassword = () => {
    this.props.showForgotPasswordModal();
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSignIn();
    }
  };

  render() {
    const {isPending, hasAccount, loggedInUser, errorCode} = this.props;

    if (isPending) {
      return (
        <LoadingWrapper>
          <Loading/>
        </LoadingWrapper>);
    }

    const DynamicSection = (hasAccount && !loggedInUser) ? SignInSection : Col;
    let errorMessage = errorCode === 401 ? "Invalid email or password" : "Unexpected Error, Please Retry";

    return (
      <div>
      <DynamicSection start={'xs'}>
        <Col xs={12}>
          {(hasAccount && !loggedInUser) &&
          <Row>
            <Col xs={12}>
              <SectionHeading xxsmall uppercase neutral>SIGN IN WITH YOUR EXISTING ACCOUNT</SectionHeading>
            </Col>
          </Row>
          }
          {(hasAccount && !loggedInUser) ?
          <Row>
            <Col xs={12}>
              <FormInput name={'email'} placeholder={'Email'} onBlur={this.handleBlur} value={this.state.email}
                         disabled={loggedInUser}
                         onChange={this.handleInputChange} maxlength={'40'} error={this.state.emailError}/>
            </Col>
          </Row>
          :
          <Row>
            <FormInput name={'email'} placeholder={'Email'} onBlur={this.handleBlur} value={this.state.email}
                       disabled={loggedInUser}
                       onChange={this.handleInputChange} maxlength={'40'} error={this.state.emailError}/>
          </Row>}

          {(hasAccount && !loggedInUser) &&
          <Row>
            <Col xs={12}>
              <FormInput name={'password'} placeholder={'Password'} value={this.state.password}
                         onChange={this.handleInputChange} type={'password'} onKeyPress={this.handleKeyPress}
                         error={this.state.passwordError}/>
            </Col>
          </Row>
          }
        </Col>
        <Col xs={12}>
          <Row>
            {(hasAccount && !loggedInUser) ? <LogoWrapper></LogoWrapper> : ''}
            {errorCode &&
            <NotificationBar
              type={'error'}
              message={errorMessage}
              actions={null}
              xs={12}
            />}
          </Row>
        </Col>
        {(hasAccount && !loggedInUser) &&
          <Col xs={12}>
            <Row>
              <Col xs={12}>
                <ForgotLink to="#">
                  <Label small neutral italic onClick={this.handleForgotPassword}>Forgot Password?</Label>
                </ForgotLink>
              </Col>
              <Col xs={12}>
                <Row>
                  <Col xs={12} sm={12} md={6}>
                    <Button full primary onPress={this.handleSignIn}>Sign In</Button>
                  </Col>
                  <Col xs={12} sm={12} md={6}>
                    <GuestButton className="hideFromMobile" full primary outline onPress={this.handleGuestLogin}>CHECKOUT AS A GUEST</GuestButton>
                    <GuestMobileLoginLink to="#" onClick={this.handleGuestLogin}>
                        Checkout as a Guest
                    </GuestMobileLoginLink>
                  </Col>
                  <PoweredByWrapper><PoweredByLogo/></PoweredByWrapper>
                </Row>
              </Col>
          </Row>
        </Col>
        }
      </DynamicSection>
      </div>
    );
  };
}


function mapState(state) {
  return {
    isPending: state.user.isFlowWaiting,
    hasAccount: state.user.hasAccount && !state.user.isGuest,
    loggedInUser: state.user.loggedInUser,
    user: state.user.currentUser,
    isGuest: state.user.isGuest,
    errorCode: state.user.loginSectionErrorCode,
  };
}

const actions = {checkUser, updateContact, authenticateUser, continueAsGuest, clearGuestState, showForgotPasswordModal};

export default connect(mapState, actions)(LoginSection);
