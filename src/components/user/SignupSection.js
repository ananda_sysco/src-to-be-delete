import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {
  Button,
  themed,
  Row,
  Col,
  Paragraph,
  PoweredByLogo,
} from 'lib/ui';
import {signUpUser} from 'store/user';
import Loading from 'components/global/Loading';
import {FormInput} from 'lib/StyledUI';
import {EVENTS, SCENARIOS, trackEvent} from "../analytics/analyticalHandler";

const LoadingWrapper = styled.div`
  padding:100px;
`;

const Wrapper = styled(Row)`
  padding-bottom: ${themed('layout.gutter.xlarge')}rem;
`;
const LogoWrapper = styled.div`
  width:100%;
  padding: 50px 10px 10px 10px;
`;
const EmailText = styled(Paragraph)`
font-family: BrixSansMedium;
margin-bottom: 20px;
`;

export class SignupSection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isCreateUserSuccess: false,
      password: '',
      confPassword: '',
      passwordMismatchError: null,
      passwordError: null
    };


    this.handlePassword = this.handlePassword.bind(this);
    this.handleConfirmPassword = this.handleConfirmPassword.bind(this);
    this.passwordMismatchError = 'Password mismatch';
  }

  constructPasswordLength(passwordLength) {
    if (this.state.password === '') {
      return;
    }
    if (this.state.password.length < passwordLength) {
      return 'Password should contain at least ' + passwordLength + ' characters';
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSignUpSuccess && nextProps.signUpErrorCode === null) {
      this.setState({isCreateUserSuccess: true});
    }
  }


  handlePassword(event) {
    const value = event.target.value;
    if (this.state.passwordError && value) {
      this.setState({passwordError: null});
    }
    if (this.state.password === '') {//This is the first time user entering the password
      if (this.state.confPassword === '') {
        this.setState({password: value});
      } else {
        if (this.state.confPassword === value) {
          this.setState({password: value, passwordMismatchError: null});
        } else {
          this.setState({password: value, passwordMismatchError: this.passwordMismatchError});
        }
      }
    } else {
      if (value !== this.state.confPassword) {
        this.setState({password: value, passwordMismatchError: this.passwordMismatchError});
      } else {
        this.setState({password: value, passwordMismatchError: null});
      }
    }
  }

  handleConfirmPassword(event) {
    const value = event.target.value;
    if (this.state.passwordMismatchError && value) {
      this.setState({passwordMismatchError: null});
    }
    if (this.state.password !== value) {
      this.setState({confPassword: value, passwordMismatchError: this.passwordMismatchError});
    } else {
      this.setState({confPassword: this.state.password, passwordMismatchError: null});
    }
  }

  static propTypes = {
    isPending: PropTypes.any.bool
  };

  renderForm() {
    return (
      <form>
        <EmailText>{this.props.user.email}</EmailText>
        <FormInput name={'password'} placeholder={'Password'} type={'password'}
                   onBlur={this.handlePassword} error={this.constructPasswordLength(6) || this.state.passwordError}/>
        <FormInput name={'retypePassword'} placeholder={'Retype Password'} type={'password'}
                   onBlur={this.handleConfirmPassword} error={this.state.passwordMismatchError}/>
      </form>
    );
  }

  handleOk = () => {
    if (!this.props.isFlowWaiting) {
      let tempUser = this.props.user;
      tempUser = Object.assign({}, tempUser, {password: this.state.password});
      if (this.state.confPassword && this.state.password.length >= 6 && this.state.passwordMismatchError === null) {
        trackEvent(SCENARIOS.ORDER_CONFIRMATION, EVENTS[SCENARIOS.ORDER_CONFIRMATION].NEW_USER_CLICK_ON_CREATE_ACCOUNT, this.props.restaurant, 1);
        this.setState({password: '', confPassword: ''});
        this.props.signUpUser(tempUser);
      } else {
        if (!this.state.password) {
          this.setState({passwordError: 'Password is required'});
        }
        if (!this.state.confPassword) {
          this.setState({passwordMismatchError: 'Confirmation password is required'});
        }
      }
    }
  };

  renderSignUpSection() {
    if (this.props.isFlowWaiting) {
      return (<LoadingWrapper>
        <Loading/>
      </LoadingWrapper>);
    } else {
      return (<div>
        <Wrapper>
          <Col xs={12}>
            <Paragraph>Create an account to save your information and speed up future orders:</Paragraph>
          </Col>
          <Col xs={12} md={6}>
            {this.renderForm()}<Button primary full onPress={this.handleOk}>
            CREATE ACCOUNT
          </Button>
          </Col>
          <LogoWrapper className="hideFromMobile">
            <PoweredByLogo/>
          </LogoWrapper>
        </Wrapper>
      </div>);
    }
  }

  render() {
    if (!this.state.isCreateUserSuccess) {
      return this.renderSignUpSection();
    } else {
      return null;
    }
  };
}


function mapState(state) {

  return {
    isSignUpSuccess: state.user.isSignUpCompleted,
    isSignUpRequested: state.user.isSignUpRequested,
    signUpErrorCode: state.user.signUpErrorCode,
    restaurant: state.app.restaurantInfo,
    email: state.user.email,
    password: state.user.password,
    user: state.user.currentUser,
    isFlowWaiting: state.user.isFlowWaiting
  };
}

const actions = {signUpUser};

export default connect(mapState, actions)(SignupSection);
