import {cloneDeep, isEqual} from 'lodash';
import {removeEmptyElementsFromObject} from '../utils/commonUtil';
import {PhoneNumberUtil, PhoneNumberFormat} from 'google-libphonenumber';

const phoneUtils = PhoneNumberUtil.getInstance();

export function updatedUserAddress(currentUser, loggedInUser) {
  if (!currentUser || !loggedInUser || !currentUser.contacts || !loggedInUser.contacts) {
    return null;
  }
  let addressUpdated = false;

  let cleanedCurrentUserAddress = removeEmptyElementsFromObject(currentUser.contacts[0]);
  let cleanedLoggedInUserAddress = removeEmptyElementsFromObject(loggedInUser.contacts[0]);

  if (cleanedCurrentUserAddress && cleanedLoggedInUserAddress && !isEqual(cleanedCurrentUserAddress, cleanedLoggedInUserAddress)) {
    loggedInUser.contacts[0] = currentUser.contacts[0];
    addressUpdated = true;
  }
  return {isUpdated: addressUpdated, user: loggedInUser};
}

export function updatedBasicUserInfo(currentUser, loggedInUser, comparisonArray) {
  if (!currentUser || !loggedInUser || !comparisonArray || comparisonArray.length === 0) {
    return null;
  }
  let infoUpdated = false;

  for (let key of comparisonArray) {
    if (key === 'phone' && currentUser.contacts && loggedInUser.contacts) {
      let currentPhoneNumber = currentUser.contacts.length !== 0 ? currentUser.contacts[0].phone : null;
      let loggedInUserPhone = loggedInUser.contacts.length !== 0 ? loggedInUser.contacts[0].phone : null;
      if (!isEqual(currentPhoneNumber, loggedInUserPhone)) {
        loggedInUser.contacts[0].phone = currentUser.contacts[0].phone;
        infoUpdated = true;
      }
      continue;
    }
    if (!isEqual(currentUser[key], loggedInUser[key])) {
      loggedInUser[key] = currentUser[key];
      infoUpdated = true;
    }
  }

  return {isUpdated: infoUpdated, user: loggedInUser};
}

export function mapUserPayloadForUpdateRequest(loggedInUser) {
  const contacts = loggedInUser.contacts;
  let mappedUser = cloneDeep(loggedInUser);
  let preparedContacts;
  if (loggedInUser && contacts && contacts.length !== 0) {
    mappedUser.contacts = {contact: []};
    preparedContacts = prepareContactForUserBackend(contacts[0]);
    preparedContacts.id = contacts[0].contactId;
    if (preparedContacts.address) {
      preparedContacts.address.id = contacts[0].addressId;
    }
    if (preparedContacts.phone) {
      preparedContacts.phone.id = contacts[0].phoneId;
    }
    mappedUser.contacts.contact[0] = preparedContacts
  }
  return mappedUser;
}

export function mapPaymentTypesForUpdateRequest(currentUser, loggedInUser) {
  const paymentType = (currentUser.creditCards && currentUser.creditCards.length !== 0) ? currentUser.creditCards : [];
  let mappedUser = loggedInUser;
  let preparedPaymentType;
  if (paymentType.length !== 0) {
    if (!mappedUser.paymentTypes || (mappedUser.paymentTypes && !mappedUser.paymentTypes.credit_card)) {
      mappedUser.paymentTypes = {credit_card: []}
    }
    preparedPaymentType = {
      paymentMethod: 'credit_card',
      nameOnCard: paymentType[0].nameOnCard,
      number: paymentType[0].cardNumber,
      secCode: paymentType[0].securityCode,
      expire: paymentType[0].expiration.replace('/', ''),
      billingAddress: { zipCode: paymentType[0].zipCode }
    };
    mappedUser.paymentTypes.credit_card.push(preparedPaymentType);
  }
  return mappedUser;
}

export function mapGivenUserToStore(user) {
  const userForStore = Object.assign({}, user);
  if (user && user.contacts && user.contacts.contact.length !== 0) {
    const contact = user.contacts.contact[0];
    const contactId = (contact.id) ? contact.id : null;
    const phoneId = (contact.phone && contact.phone.id) ? contact.phone.id : null;
    const tempObject = {contactId, phoneId};

    if (contact.phone) {
      tempObject.phone = contact.phone.areaCode + contact.phone.exchangeCode + contact.phone.subscriberNumber;
    } else {
      tempObject.phone = null;
    }

    if (contact.address) {
      const address = contact.address;
      tempObject.address1 = address.address1 || '';
      tempObject.address2 = address.address2 || '';
      tempObject.city = address.city || '';
      tempObject.zipCode = address.zipCode || '';
      tempObject.state = address.state || null;
      tempObject.addressId = address.id ? address.id : null;
    }
    userForStore.contacts = [tempObject];
  }
  return userForStore;
}

export function prepareContactForUserBackend(contact) {
  const {phone, address1, address2, city, state, zipCode} = contact;
  // prepare phone number for API
  const parsedPhone = phoneUtils.parse(phone, 'US');
  const formattedPhone = phoneUtils.format(parsedPhone, PhoneNumberFormat.NATIONAL).match(/[0-9]+/g);
  const [areaCode, exchangeCode, subscriberNumber, extension] = formattedPhone;
  const preparedPhone = {areaCode, exchangeCode, subscriberNumber, extension};
  const result = {phone: preparedPhone};
  if (address1) {
    result.address = {
      address1,
      address2,
      city,
      state,
      zipCode,
    };
  }
  return result;
}
