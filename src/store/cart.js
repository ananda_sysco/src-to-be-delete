import {cloneDeep, concat, filter, findIndex, forEach, get, keys, values, isEmpty} from 'lodash';
import {PhoneNumberFormat, PhoneNumberUtil} from 'google-libphonenumber';
import moment from 'moment-timezone';
import uuid from 'uuid';
import mixpanel from 'mixpanel-browser';
import creditCardType from 'credit-card-type';
import ConsumerApi from 'lib/ConsumerApi';
import config from 'config';
import {getExpiration, reset} from './utils/persist';
import {getOrderErrorMessage, ORDER_ERRORS} from './utils/errors';
import {addNotification, disableCheckout, dismissNotification, setSessionValidityByPrepOrderType} from './app';
import {orderTimesSelector} from 'store/utils/selectors';
import {handleClickOrderPlaceEvent, handleOrderPlacementSuccessEvent} from '../components/analytics/analyticsOrderHandler';
import APIError from "../lib/APIError";
import {ADD_CARD_LABEL} from 'lib/constants';
import {setPhoneNumberValid, validateAndUpdateUser} from 'store/user';
import {formatPhoneNumber} from '../utils/toolbox';
import {fpSplice, replace, countTotalItems, extractInstructions, calculateTip} from './utils/helpers';

const phoneUtils = PhoneNumberUtil.getInstance();

// action constants
const ADD_TO_CART = 'add_to_cart';
const REMOVE_FROM_CART = 'remove_from_cart';
const REMOVE_ALL_FROM_CART = 'remove_all_from_cart';
const UPDATE_CART = 'update_cart';
const CALCULATE_TOTAL = 'calculate_total';
const UPDATE_TOTAL = 'update_total';
const SELECT_ORDER_TYPE = 'select_order_type';
const SELECT_ORDER_ATTRIBUTE = 'select_order_attribute';
const SELECT_ORDER_TIME = 'select_order_time';
const ORDER_TIME_MANUAL_SELECT = 'order_time_manual_select';
const ADD_TIP = 'add_tip';
const ADD_DONATION = 'add_donation';
const SUBMIT_ORDER = 'submit_order';
const CREATE_ORDER = 'create_order';
const INVALIDATE_ORDER = 'invalidate_order';
const GET_RESTAURANT = 'get_restaurant';
const SET_READY_BY_OPTIONS = 'set_ready_by_options';
const ADD_TO_INVALID_ITEMS = 'add_to_invalid_items';
const SHOW_HIDE_INVALID_ITEMS_MODAL = 'show_hide_invalid_items_modal';
const RESET_ERRONEOUS_SESSION = 'reset_erroneous_session';
const RESET_CART_OPENED_IN_MOBILE = 'reset_cart_open_in_mobile';
const SELECT_SESSION = 'select_session';
const SET_CART_DISPLAY_CONDITION = 'set_cart_display_condition';
const SET_DONATION_OVERLAY_DISPLAY_CONDITION = 'set_donation_overlay_display_condition';

export const ATTR_TYPE_NO_CONTACT = 'no-contact';
export const ATTR_TYPE_NO_CONTACT_DESCRIPTION = 'no-contact-description';

export const orderTypes = {
  pickup: 'PICKUP',
  delivery: 'DELIVERY',
};

export const tipTypes = {
  percentage: 'PERCENTAGE',
  value: 'VALUE',
};

export const LEAVE_FOOD_AT_DOOR = 'Leave order at the door';
export const PICKUP_FROM_CURBSIDE = 'Pickup from curbside';

const DEFAULT_ATTRIBUTES = [];
const ATTRIBUTE_ORDER = {
  [ATTR_TYPE_NO_CONTACT]: 0,
  [ATTR_TYPE_NO_CONTACT_DESCRIPTION]: 1
};

const initialState = {
  items: [],
  empty: true,
  totalCost: 0,
  totalItems: 0,
  orderType: null,
  order: null,
  attributes: DEFAULT_ATTRIBUTES,
  tax: 0,
  tip: null,
  donationAmount: 0,
  isCustomDonation: false,
  tipPercentage: null,
  discountCode: null,
  discountType: null,
  discount: 0,
  grandTotal: 0,
  orderTime: null,
  deliveryAddress: null,
  deliveryFee: 0,
  totalIsLoading: false,
  orderIsSending: false,
  readyByOptions: [],
  invalidCartItems: [],
  isInvalidItemsModalShown: false,
  isDonationOverlayShown: false,
  erroneousSession: null,
  sessionBasedInvalidItemsInCart: false,
  readyByTimeSelected: false,
  isCartOpenedInMobile: false,
  condition: false,
  accountId: null, //account cart items associated with
  accountName: null
};

const consumerApi = new ConsumerApi(config);

// reducer
export default function cart(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_TO_CART: {
      const {item, accountId, accountName} = action.payload;
      const items = concat(state.items, item);

      return {
        ...state,
        items,
        empty: items.length === 0,
        totalItems: countTotalItems(items),
        lastAddedItemSession: item.sessionId,
        ...getExpiration(),
        accountId,
        accountName
      };
    }

    case REMOVE_FROM_CART: {
      const {index} = action.payload;
      const items = fpSplice(state.items, index);
      const empty = items.length === 0;
      const accountId = empty ? null : state.accountId;
      const accountName = empty ? null : state.accountName;
      const orderType = !empty ? state.orderType : null;
      const attributes = !empty ? state.attributes : [];

      return {
        ...state,
        items,
        empty,
        orderType,
        attributes,
        totalItems: countTotalItems(items),
        ...getExpiration(),
        accountId,
        accountName
      };
    }

    case REMOVE_ALL_FROM_CART: {

      return {
        ...state,
        items: [],
        empty: true,
        tip: null,
        tipPercentage: null,
        totalItems: countTotalItems([]),
        ...getExpiration(),
        accountId: null,
        accountName: null,
        orderType: null,
        order: null,
        attributes: []
      };
    }

    case SET_READY_BY_OPTIONS: {
      const {readyByOptions, erroneousSession} = action.payload;

      return {
        ...state,
        readyByOptions,
        erroneousSession,
        sessionBasedInvalidItemsInCart: !(erroneousSession === null)
      };
    }

    case UPDATE_CART: {
      const {index, item} = action.payload;
      const items = replace(state.items, index, item);

      return {
        ...state,
        items,
        empty: items.length === 0,
        totalItems: countTotalItems(items),
        ...getExpiration(),
      };
    }

    case CALCULATE_TOTAL:
      return {
        ...state,
        totalIsLoading: true,
      };

    case UPDATE_TOTAL: {
      const {tax} = action.payload;
      let totalItems = 0;
      let totalCost = 0;

      state.items.forEach((item) => {
        totalItems += item.quantity;
        totalCost += item.quantity * item.totalCost;
      });

      const tip = calculateTip(totalCost, state.tip, state.tipPercentage);
      const grandTotal = totalCost + tip + state.donationAmount - state.discount + tax + ((state.orderType === orderTypes.delivery) ? state.deliveryFee : 0);

      return {
        ...state,
        totalItems,
        totalCost,
        tax,
        grandTotal,
        tip,
        totalIsLoading: false,
      };
    }

    case SELECT_ORDER_TYPE: {
      const {orderType} = action.payload;
      return {...state, orderType};
    }

    case SELECT_ORDER_ATTRIBUTE: {
      const { attrKey, attrValue, adding } = action.payload || {};
      let attributes = cloneDeep(state.attributes);
      if (adding) {
        const attrs = attributes.filter(attr => attr.key === attrKey);
        if (isEmpty(attrs)) {
          attributes.push({
            key: attrKey,
            displayName: 'Instruction',
            value: attrValue || ''
          });
        } else {
          attrs[0].value = attrValue || '';
        }
      } else {
        attributes = attributes.filter(attr => attr.key !== attrKey)
      }

      return {
        ...state,
        attributes
      };
    }

    case SELECT_ORDER_TIME: {
      const {orderTime} = action.payload;
      return {...state, orderTime};
    }

    case ORDER_TIME_MANUAL_SELECT: {
      const readyByTimeSelected = action.payload;
      return {...state, readyByTimeSelected};
    }

    case ADD_TIP: {
      const {tipType, value} = action.payload;
      const tipPercentage = tipType === tipTypes.percentage ? value : null;
      const tip = calculateTip(state.totalCost, value, tipPercentage);
      const grandTotal = state.grandTotal - state.tip + tip;

      return {
        ...state,
        tip,
        tipPercentage,
        grandTotal,
      };
    }

    case ADD_DONATION: {
      const { donationAmount, isCustomDonation } = action.payload;
      const grandTotal = state.grandTotal - state.donationAmount  + donationAmount;
      return {
        ...state,
        grandTotal,
        donationAmount,
        isCustomDonation
      }
    }

    case SUBMIT_ORDER: {
      return {
        ...state,
        orderIsSending: true,
      };
    }

    case CREATE_ORDER: {
      let donationAmount = state.donationAmount;
      const { order } = action.payload;
      if (order && (order.orderNumber || order.isAlreadyProcessed)) {
        donationAmount = 0;
      }
      return {
        ...state,
        orderIsSending: false,
        order,
        donationAmount
      };
    }

    case GET_RESTAURANT: {
      let restaurant = null;
      let orderType = null;
      let deliveryFee = 0;

      if (!action.error) {
        const {restaurantInfo} = action.payload;
        restaurant = restaurantInfo.entities.restaurant[restaurantInfo.result];
      }

      if (restaurant) {
        if (state.orderType) {
          orderType = state.orderType;
        }
        deliveryFee = restaurant.deliveryFee;
      }

      return {
        ...state,
        orderType,
        deliveryFee
      };
    }

    case  ADD_TO_INVALID_ITEMS: {
      return {
        ...state,
        invalidCartItems: action.payload.items,
      };
    }

    case SHOW_HIDE_INVALID_ITEMS_MODAL: {
      return {
        ...state,
        isInvalidItemsModalShown: action.payload.status,
      };
    }


    case RESET_ERRONEOUS_SESSION: {
      return {
        ...state,
        erroneousSession: null,
        sessionBasedInvalidItemsInCart: false
      };
    }

    case RESET_CART_OPENED_IN_MOBILE: {
      return {
        ...state,
        isCartOpenedInMobile: action.payload.isCartOpenedInMobile
      };
    }

    case SELECT_SESSION: {
      let isOrderTimeNullable = state.empty;
      let currentOrderTime = state.orderTime;
      return {
        ...state,
        orderTime: isOrderTimeNullable ? null : currentOrderTime,
      };
    }

    case SET_CART_DISPLAY_CONDITION: {
      return {
        ...state,
        condition: action.payload.value
      }
    }

    case SET_DONATION_OVERLAY_DISPLAY_CONDITION: {
      return {
        ...state,
        isDonationOverlayShown: action.payload
      }
    }

    default:
      return state;
  }
}

// action creators
export function addToCart(item) {
  return async (dispatch, getState) => {
    const categories = getState().app.categories;
    const product = getState().app.products[item.productId];
    const {specialInstructions, instructions} = extractInstructions(item.attributeGroups, getState());

    if (product) {
      const cartItem = {
        sessionId: getState().app.selectedSession,
        lineItemId: uuid.v1(),
        menuId: item.menuId,
        productId: item.productId,
        variationId: product.variationId,
        quantity: item.quantity,
        name: item.isSubCategory ? `${categories[item.menuId].name} - ${product.name}` : product.name,
        isSubCategory: item.isSubCategory,
        totalCost: item.cost,
        attributeGroups: item.attributeGroups,
        instructions,
        specialInstructions,
        invalidCartItems: []
      };

      dispatch({
        type: ADD_TO_CART,
        payload: {
          item: cartItem,
          accountId: getState().app.restaurantInfo.accountId,
          accountName: getState().app.restaurantInfo.name
        }
      });
      dispatch(setReadyByOptions());
      dispatch(calculateTotal());
    }
  };
}

export function updateInvalidItems(items) {
  return (dispatch, getState) => {
    dispatch({type: ADD_TO_INVALID_ITEMS, payload: {items}});
  }
}

export function updateCartViewMode(value) {
  return (dispatch, getState) => {
    dispatch({type: RESET_CART_OPENED_IN_MOBILE, payload: {isCartOpenedInMobile: value}});
  }
}


export function showInvalidItemsPopup() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_HIDE_INVALID_ITEMS_MODAL, payload: {status: true}});
  }
}

export function hideInvalidItemsPopup() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_HIDE_INVALID_ITEMS_MODAL, payload: {status: false}});
  }
}

export function resetErroneousSession() {
  return (dispatch) => {
    dispatch({type: RESET_ERRONEOUS_SESSION});
  }
}

function removeItem(dispatch, index) {
  dispatch({type: REMOVE_FROM_CART, payload: {index}});
  dispatch(setReadyByOptions());
  dispatch(calculateTotal());
}

function setDefaultReadyByTime() {
  return (dispatch, getState) => {
    const readyByOption = getState().cart.readyByOptions ? getState().cart.readyByOptions[0] : null;
    const isTimeSetManually = getState().cart.readyByTimeSelected;
    const items = getState().cart.items;
    if (!isTimeSetManually && items && items.length >= 1) {
      dispatch(selectOrderTime(readyByOption ? readyByOption : null));
    }
    if (!items || items.length === 0) {
      dispatch(selectOrderTime(null));
      dispatch(orderTimeManualSelect(false));
    }
  };
}

export function removeFromCart(index) {
  return (dispatch, getState) => {
    removeItem(dispatch, index);
    dispatch(setDefaultReadyByTime());
  };
}

export function removeInvalidItems(items) {
  return (dispatch, getState) => {
    items.forEach(tempItem => {
      const index = findIndex(getState().cart.items, item => item.lineItemId === tempItem.lineItemId);
      removeItem(dispatch, index);
    });
  };
}

export function updateCart(index, item) {
  return (dispatch, getState) => {
    const cartItems = getState().cart.items;
    const {specialInstructions, instructions} = extractInstructions(item.attributeGroups, getState());
    const existingItem = cartItems[index];

    const itemToUpdate = {
      ...existingItem,
      ...item,
      totalCost: item.cost || existingItem.totalCost,
      specialInstructions,
      instructions: instructions || existingItem.instructions,
    };

    dispatch({type: UPDATE_CART, payload: {index, item: itemToUpdate}});
    dispatch(calculateTotal());
  };
}

export function setContactTypeAttribute(attrType, adding) {
  return (dispatch) => {
    dispatch({ type: SELECT_ORDER_ATTRIBUTE, payload: {
        attrKey: ATTR_TYPE_NO_CONTACT,
        attrValue: attrType,
        adding
      }
    })
  }
}

export function setContactTypeMessage(attrMessage) {
  return (dispatch) => {
    dispatch({ type: SELECT_ORDER_ATTRIBUTE, payload: {
        attrKey: ATTR_TYPE_NO_CONTACT_DESCRIPTION,
        attrValue: attrMessage,
        adding: true
      }
    })
  }
}

export function selectOrderType(orderType) {
  return async (dispatch) => {
    dispatch({type: SELECT_ORDER_TYPE, payload: {orderType}});
    dispatch(setReadyByOptions());
    dispatch(calculateTotal());
    dispatch(setSessionValidityByPrepOrderType());
  }
}

export function setReadyByOptions() {
  return async (dispatch, getState) => {
    const {options, erroneousSession} = orderTimesSelector(getState());
    dispatch({type: SET_READY_BY_OPTIONS, payload: {readyByOptions: options, erroneousSession}});
    dispatch(setDefaultReadyByTime());
  }
}

export function selectOrderTime(orderTime) {
  return {type: SELECT_ORDER_TIME, payload: {orderTime}};
}

export function orderTimeManualSelect(status) {
  return {type: ORDER_TIME_MANUAL_SELECT, payload: status};
}

export function addTip(value, type = tipTypes.value) {
  return {type: ADD_TIP, payload: {tipType: type, value}};
}

export function addDonation(donation) {
  return { type:ADD_DONATION, payload: donation };
}

export function removeDonation() {
  return addDonation({ donationAmount : 0, isCustomDonation: false });
}

export function setDonationOverlayDisplayCondition(condition) {
  return { type: SET_DONATION_OVERLAY_DISPLAY_CONDITION, payload: condition };
}

export function calculateTotal() {
  return async (dispatch, getState) => {
    dispatch(disableCheckout(true));
    const { restaurantInfo } = getState().app;
    const { items, orderType } = getState().cart;

    if (!restaurantInfo || !items || !items.length) {
      return 0;
    }

    dispatch({type: CALCULATE_TOTAL});

    const cart = {
      merchantId: restaurantInfo.id,
      accountId: restaurantInfo.accountId,
      orderType,
      lineItems: {
        lineItem: items.map(item => ({
          productId: item.productId,
          variationId: item.variationId,
          qty: item.quantity,
          attributeValueIds: getAttributeValueIds(item),
        })),
      }
    };

    try {
      const result = await consumerApi.calculateTax(cart);
      dispatch({type: UPDATE_TOTAL, payload: {tax: result.totalTax || 0}});
      dispatch(disableCheckout(false));
    } catch (e) {
      dispatch(addNotification({type: 'error', message: "Something went wrong please refresh the page."}));
    }
  }
}

export function prepareContactForApi(contact) {
  const {phone, address1, address2, city, state, zipCode} = contact;

  // prepare phone number for API
  const parsedPhone = phoneUtils.parse(phone, 'US');
  const formattedPhone = phoneUtils.format(parsedPhone, PhoneNumberFormat.NATIONAL).match(/[0-9]+/g);
  const [areaCode, exchangeCode, subscriberNumber, extension] = formattedPhone;
  const preparedPhone = {areaCode, exchangeCode, subscriberNumber, extension};

  const result = {phone: preparedPhone};

  if (address1) {
    // API expects street and unit like 1200-A
    const [streetNumber, ...streetName] = address1.trim().split(/\s+/g);

    result.address = {
      address1: filter([streetNumber, address2]).join('-'),
      address2: streetName.join(' '),
      city,
      state,
      zipCode,
    };
  }

  return result;
}

const prepareNewPaymentCard = function (paymentCard) {
  return {
    credit_card: [
      {
        paymentMethod: 'credit_card',
        type: getCardType(paymentCard.cardNumber),
        number: paymentCard.cardNumber,
        nameOnCard: paymentCard.nameOnCard.trim(),
        expire: (paymentCard.expiration || '').replace(/[^0-9]+/g, ''),
        secCode: paymentCard.securityCode,
        billingAddress: { zipCode: paymentCard.zipCode }
      },
    ]
  }
};

const prepareExistingPaymentCard = function (savedCard) {
  return {
    credit_card: [{
      paymentMethod: 'credit_card',
      id: savedCard.value,
      number: savedCard.label
    }]
  }
};

const updateOrderWithDonationInfo = (cart) => {
  const { donationAmount, grandTotal } = cart;
  const updatedOrderInfo = {};
  if (donationAmount) {
    updatedOrderInfo.donation = { amount: donationAmount };
    updatedOrderInfo.total = grandTotal - donationAmount;
  }
  return updatedOrderInfo;
};

export function createOrder() {
  return async (dispatch, getState) => {
    const restaurant = getState().app.restaurantInfo;
    const user = getState().user.currentUser;
    const products = getState().app.products;
    const attributeValues = getState().app.attributeValues;
    const cart = getState().cart;
    const temporaryCard = get(user, 'creditCards[0]');
    const currentlySelectedCard = user.cardDropDownOption;
    const loggedInUser = getState().user.loggedInUser;
    const hasAccount = getState().user.hasAccount;

    if (restaurant) {
      try {
        if (getState().app.notifications && getState().app.notifications.length > 0 &&
          (getState().app.notifications[0].message === ORDER_ERRORS.TERMS_OF_USE_ERROR ||
            getState().app.notifications[0].message === ORDER_ERRORS.TERMS_OF_USE_WITH_GIFT_TERMS_ERROR)) {
          dispatch(dismissNotification());
        }
        dispatch({type: SUBMIT_ORDER});

        const contact = get(user, 'contacts[0]');
        let paymentCard;
        if (loggedInUser) {
          //Using a temporary card for order placement of logged in user with saved cards
          if (temporaryCard && currentlySelectedCard && (currentlySelectedCard.label === ADD_CARD_LABEL)) {
            paymentCard = prepareNewPaymentCard(temporaryCard);
          }
          //Using a saved card for order placement of logged in user
          else if (currentlySelectedCard && (currentlySelectedCard.label !== ADD_CARD_LABEL)) {
            //TODO Not tested this branch, need to test this once the payment on-boarding is done
            paymentCard = prepareExistingPaymentCard(currentlySelectedCard);
          }
          //Using a temporary card for order placement of logged in user without saved cards
          else {
            paymentCard = prepareNewPaymentCard(temporaryCard);
          }
        } else {
          paymentCard = prepareNewPaymentCard(temporaryCard);
        }

        let orderTime = null;

        if (cart.orderTime) {
          // API requires a unix timestamp in UTC
          orderTime = adjustedReadyByTime(cart, restaurant).unix() * 1000;
        }

        const orderedAttrs = cloneDeep(cart.attributes)
          .sort((a, b) => ((ATTRIBUTE_ORDER[a.key] || 0) - (ATTRIBUTE_ORDER[b.key] || 0)));

        const order = {
          accountId: restaurant.accountId,
          lineItems: {
            lineItem: cart.items.map(item => ({
              productId: item.productId,
              qty: item.quantity,
              price: item.totalCost,
              variationId: item.variationId,
              attributeSetInstance: getAttributesForLineItem(item, products[item.productId], attributeValues),
            })),
          },
          orderType: cart.orderType,
          attributes: orderedAttrs,
          subTotal: cart.totalCost,
          tax: cart.tax,
          discount: cart.discount,
          discountCode: cart.discountCode,
          deliveryFee: cart.orderType === orderTypes.delivery ? restaurant.deliveryFee : null,
          tipAmount: cart.tip,
          total: cart.grandTotal,
          orderReadyTime: orderTime,
          customer: {
            id: loggedInUser ? loggedInUser.id : null,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            sendMail: true,
            contacts: {
              contact: [
                prepareContactForApi(contact)
              ],
            },
            paymentTypes: paymentCard
          },
          textMessageConfirmation: user.textMessageConfirmation,
          ...updateOrderWithDonationInfo(cart)
        };

        const result = await consumerApi.createOrder(restaurant.accountId, order);
        const tempUserForUpdate = cloneDeep(getState().user.currentUser);

        try {
          if (!loggedInUser && !hasAccount) {
            let isPhoneNumberValid = await consumerApi.checkPhone(contact.phone, user.email);
            dispatch(setPhoneNumberValid(isPhoneNumberValid));
          }
        } catch (e) {
          //DO nothing
        }


        handleClickOrderPlaceEvent(cart, restaurant);
        dispatch({type: CREATE_ORDER, payload: {order: result}});

        if (result instanceof APIError) {
          if (!result.data.data) {
            throw new Error(result.data);
          }
          throw new Error(result.data.data.code);
        }
        //region update user if the order is successful
        dispatch(validateAndUpdateUser(tempUserForUpdate));
        //endregion

        if (result) {
          mixpanel.track("ORDER_PLACED", {
            "ORDER_NUMBER": result.orderNumber,
            "ORDER_TYPE": result.orderType,
            "ORDER_TOTAL": result.total
          });
          handleOrderPlacementSuccessEvent(result, restaurant);
        }
      } catch (error) {
        if (error.message && error.message === '40420') {
          let minimumOrderAmountError = null;
          if (cart.orderType === 'DELIVERY') {
            minimumOrderAmountError = 'Minimum order amount for Delivery orders is $' + Math.max(restaurant.minimumOrderValue, restaurant.minDeliveryOrderAmount) + '.'
          } else if (cart.orderType === 'PICKUP') {
            minimumOrderAmountError = 'Minimum order amount for Pickup orders is $' + restaurant.minimumOrderValue + '.'
          }
          dispatch(addNotification({type: 'error', message: minimumOrderAmountError}));
          mixpanel.track("MINIMUM_ORDER_AMOUNT_ERROR", {
            "ORDER_TYPE": cart.orderType
          });
        } else if (error.message && error.message === '20070') {
          const embedVals = {
            phone: formatPhoneNumber(restaurant.phone)
          };
          dispatch(addNotification({type: 'error', message: getOrderErrorMessage(error.message, embedVals)}));
          mixpanel.track("ORDER_ERROR", {
            "ORDER_TYPE": cart.orderType
          });
        } else {
          dispatch(addNotification({type: 'error', message: getOrderErrorMessage(error.message)}));
          mixpanel.track("ORDER_ERROR", {
            "ORDER_TYPE": cart.orderType
          });
        }
        dispatch({type: CREATE_ORDER, payload: error, error: true});
      }
    }
  };
}

export function invalidateOrder() {
  return async (dispatch) => {
    await reset();
    dispatch({type: INVALIDATE_ORDER});
  }
}

export function createErrorNotification(message) {
  return (dispatch) => {
    dispatch(addNotification({type: 'error', message: message}));
  }
}

export function clearCart() {
  return (dispatch) => {
    dispatch({type: REMOVE_ALL_FROM_CART});
  }
}

export function setCartDisplayCondition(value) {
  return (dispatch) => {
    dispatch({type: SET_CART_DISPLAY_CONDITION, payload: {value: value}});
  }
}

function getCardType(cardNumber) {
  const type = get(creditCardType(cardNumber), '[0].type');

  switch (type) {
    case 'visa':
      return 'VISA';
    case 'master-card':
      return 'MASTER';
    case 'american-express':
      return 'AMEX';
    case 'discover':
      return 'DISCOVER';
    default:
      return '';
  }
}

function getAttributesForLineItem(item, product, attributeValues) {
  const attributeGroups = {...item.attributeGroups};
  const attributeSet = [];

  if (attributeGroups['1']) {
    attributeSet.push({
      value: attributeGroups['1'],
      attributeId: "1",
      attributeValueId: null,
    });
    delete attributeGroups['1'];
  }

  forEach(keys(attributeGroups), (attributeId) => {
    const attributeGroup = attributeGroups[attributeId];
    forEach(keys(attributeGroup), (key) => {
      const attributeValue = attributeGroup[key];
      if (attributeValue === true) {
        attributeSet.push({
          value: attributeValues[key].price,
          attributeId: attributeId,
          attributeValueId: key,
        });
      }
    });
  });

  const attributeSetInstance = {
    atttributeSetId: product.attributeSet,
    attributes: {
      attribute: attributeSet,
    },
  };

  return attributeSetInstance;
}

function getAttributeValueIds(item) {
  const attributeGroups = {...item.attributeGroups};
  delete attributeGroups['1'];

  const attributeValueIds = [];

  forEach(values(attributeGroups), (attributeGroup) => {
    forEach(keys(attributeGroup), (key) => {
      const attributeValue = attributeGroup[key];
      if (attributeValue === true) {
        attributeValueIds.push(key);
      }
    });
  });

  return attributeValueIds;
}

function adjustedReadyByTime(cart, restaurantInfo) {
  let {orderType, readyByOptions} = cart;
  let orderTime = moment(cart.orderTime);

  const {timeZone, orderPrepTime, deliveryTime} = restaurantInfo;
  const now = moment.tz(timeZone);
  const minimumAllowedTime = moment(now.format('HH:mm:ss'), 'HH:mm:ss');

  // account for delivery and prep times
  minimumAllowedTime.add(orderPrepTime, 'm');

  if (orderType === orderTypes.delivery) {
    minimumAllowedTime.add(deliveryTime, 'm');
  }
  //order time can't be earlier than now
  if (orderTime.isSameOrBefore(moment(minimumAllowedTime)) && readyByOptions) {
    for (let option of readyByOptions) {
      if (moment(option).isSameOrAfter(moment(minimumAllowedTime))) {
        orderTime = moment(option);
        break;
      }
    }
  }

  let obj = {
    year: now.year(),
    month: now.month(),
    day: now.date(),
    hour: orderTime.hour(),
    minute: orderTime.minute(),
    second: orderTime.second()
  };

  return moment.tz(obj, timeZone);
}

