import { filter, sortBy, uniqWith, isEqual } from 'lodash';
import moment from 'moment-timezone';
import { orderTypes } from '../cart';
import {getStartTimeMoment, getEndTimeMoment, withAvailability, DATE_TIME_FORMAT} from 'utils/sessionUtil';

function roundToFiveMins(time, method = 'add') {
  const remainder = (time.minute() % 5);
  const deficit = 5 - remainder;

  if (remainder !== 5) {
    return moment(time)[method](method === 'add' ? deficit : remainder, 'm').startOf('minute'); // remove seconds & milis
  }

  return time.startOf('minute'); // remove seconds & milis
}

/**
 * remove already expired sessions from the array
 * @returns {*}
 * @param sessions - list of unique sessions
 * @param sessionsData
 * @param timeZone
 */
export const removeExpiredSessions = (sessions, sessionsData, timeZone) => {
  let validUniqueSessions = [];
  let updatedSessions = withAvailability(sessionsData, timeZone, false);

  for (let session of sessions) {
    if (updatedSessions[session] && (updatedSessions[session].isAvailable || updatedSessions[session].isPending)) {
      validUniqueSessions.push(session);
    }
  }
  return validUniqueSessions;
};

/**
 * Identify whether two time ranges are overlapped
 * @returns {*}
 * @param t1_start
 * @param t1_end
 * @param t2_start
 * @param t2_end
 */
const areRangesOverlapped = (t1_start, t1_end, t2_start, t2_end) => {
  if (t1_start <= t2_start && t2_start <= t1_end) return true;
  if (t1_start <= t2_end && t2_end <= t1_end) return true;
  if (t2_start < t1_start && t1_end < t2_end) return true;
  return false;
};

/**
 * get min/max value of a given start/end time array
 * @returns {*}
 * @param timeArray
 * @param operation min/max arithmetic operations
 */
const getMinMaxTime = (timeArray, operation) => {

  let filteredTime = timeArray.reduce((t1, t2) => {
    return Math[operation](t1, t2);
  });

  return moment(filteredTime).format('HH:mm:ss');
};


/**
 * Get start/end times based on overlapped sessions. Returns error for
 * non overlapping scenarios
 * @returns {*}
 * @param items
 * @param sessions
 * @param timeZone
 */
export const mapTimeBasedOnSessions = (items, sessions, timeZone) => {
  let itemSession = [];
  let startTimes = [], endTimes = [];
  let uniqueSessions, uniqueValidSessions;
  let overlapped = true;
  let currentSessionRange;
  const localTime = moment.tz(timeZone);

  for (let item of items) {
    itemSession.push(item.sessionId);
  }

  uniqueSessions = [...new Set(itemSession)];
  uniqueValidSessions = removeExpiredSessions(uniqueSessions, sessions, timeZone);

  for (let session of uniqueValidSessions) {
    currentSessionRange = sessions[session].slots.find(slot => slot.day === (localTime.day() + 1).toString());
    startTimes.push(getStartTimeMoment(currentSessionRange.start));
    endTimes.push(getEndTimeMoment(currentSessionRange.end));
  }

  if (startTimes.length > 1) {
    for (let i = 0; i < startTimes.length && overlapped; i++) {
      for (let k = i + 1; k < startTimes.length; k++) {
        if (!areRangesOverlapped(startTimes[i], endTimes[i], startTimes[k], endTimes[k])) {
          overlapped = false;
          break;
        }
      }
    }
  }

  return (overlapped && startTimes.length !== 0) ? {
    start: getMinMaxTime(startTimes, 'max'),
    end: getMinMaxTime(endTimes, 'min')
  } : {error: true}

};

// returns {isTimeOver, options, isError}
export const validatePrepDeliverTime = (restaurantInfo, selectedSession, sessions, orderType, items) => {
  const options = [];

  const { timeZone, orderPrepTime, deliveryTime } = restaurantInfo;
  const now = moment.tz(timeZone);
  const localTime = moment(now.format(DATE_TIME_FORMAT), DATE_TIME_FORMAT);

  let isTimeOver = false;
  let isError = false;

  let start;
  let end;
  //if items are defined, get the start and end times from all the sessions which those items belong to
  if (items) {
    const timeRange = mapTimeBasedOnSessions(items, sessions, timeZone);

    start = timeRange.start;
    end = timeRange.end;
    const error = timeRange.error;

    if (error) {
      isError = true;
      return {isTimeOver, options, isError};
    }
  } else {
    // get the start and end time from the current session
    const currentSessionRange = sessions[selectedSession].slots.find(slot => slot.day === (now.day() + 1).toString());
    if (!currentSessionRange) { //Session is closed for today
      return {isTimeOver: true, options: null, isError: false};
    }
    start = getStartTimeMoment(currentSessionRange.start);
    end = getEndTimeMoment(currentSessionRange.end);
  }


  let startTime = moment(start, DATE_TIME_FORMAT);
  let endTime = moment(end, DATE_TIME_FORMAT);

  //start time can't be earlier than now
  if (startTime.isBefore(moment(localTime))) {
    startTime = localTime;
  }

  // account for delivery and prep times
  startTime.add(orderPrepTime, 'm');

  if (orderType === orderTypes.delivery) {
    startTime.add(deliveryTime, 'm');
  }
  // round start & end to nearest 5 mins
  startTime = roundToFiveMins(startTime, 'add');
  endTime = roundToFiveMins(endTime, 'subtract');
  while (startTime.isSameOrBefore(endTime)) {
    options.push(startTime.format());
    startTime.add(5, 'm');
  }
  // if no options, then start time is after the endtime
  isTimeOver = options.length === 0;
  return {isTimeOver, options, isError};
};

export const orderTimesSelector = (state) => {
  const {restaurantInfo, selectedSession, sessions} = state.app;
  const {orderType, lastAddedItemSession, items} = state.cart;
  let erroneousSession = null;

  if (!restaurantInfo || !orderType || !selectedSession) {
    return [];
  }

  const {isTimeOver, options, isError} = validatePrepDeliverTime(restaurantInfo, selectedSession, sessions, orderType, items);

  if (isError || isTimeOver) {
    erroneousSession = lastAddedItemSession;
  }

  return {options, erroneousSession};
};

export const restaurantHoursSelector = (state) => {
  /*
  This fix is for safari Version 10.0.3 (12602.4.8) as this version/s does not have Object.values function
   */
  const sessions = Object.keys(state.app.sessions).map(function(key) {
    return state.app.sessions[key];
  });
  const slots = sessions.reduce((result, session) => result.concat(session.slots), []);

  // get unique slots and filter out isavailable=false
  const uniques = filter(uniqWith(slots, isEqual), { isavailable: true });

  return sortBy(uniques, ['day', 'start', 'end']);
};

/**
 * Fetch items belonging to a specific session
 * @returns {*}
 * @param session
 * @param cart
 */
export const fetchSessionSpecificItems = (session, cart) => {
  let sessionSpecificItems = [];
  for (let item of cart) {
    if (item.sessionId === session) {
      sessionSpecificItems.push(item);
    }
  }
  return sessionSpecificItems;
};

