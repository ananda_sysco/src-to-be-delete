const DEFAULT_ORDER_ERROR = 'An error occurred while trying to place your order. Please try again later.';

export const ORDER_ERRORS = {
  '10000': '',
  '10010': '',
  '10060': '',
  '11100': '',
  '20020': '',
  '20050': 'Please enter delivery address details.',
  '20060': 'Unable to locate delivery address.',
  '20070': 'Delivery address is not within our delivery range. Please call us on restaurant phone number if you have any questions.',
  '30000': '',
  '40190': '',
  '40200': '',
  '40210': '',
  '40280': 'The restaurant cannot take orders at this time. Please try again later.',
  '40410': '',
  '40420': 'The restaurant made a recent menu update that affected the items in your cart. Please review your cart and submit your order again.',
  '40440': '',
  '40460': '',
  '40470': '',
  '40490': '',
  '40530': '',
  '40550': '',
  '40560': '',
  '40570': '',
  '40590': '',
  '40600': '',
  '40610': '',
  '40620': '',
  '40630': '',
  '40640': '',
  '40710': '',
  '40720': '',
  '40730': '',
  '40750': '',
  '40760': '',
  '40770': '',
  '40800': '',
  '40840': '',
  '40850': '',
  '40900': '',
  '40920': 'Your order ready time is incorrect. Please check your date/time settings.',
  '40941': 'The restaurant is not accepting gifts at the moment. Please remove the gift from the cart.',
  '41050': 'Credit card transaction failed. Please make sure you entered your credit card info correctly and try again.',
  '41080': '',
  '41090': 'Credit card transaction failed. Please make sure you entered your credit card info correctly and try again.',
  '41100': 'Credit card transaction failed. Please make sure you entered your credit card info correctly and try again.',
  '41250': '',
  '50010': '',
  '70110': '',
  '80000': '',
  '40940': 'The restaurant made a recent menu update that affected the items in your cart. Please review your cart and submit your order again.',
  '10303': 'The restaurant made a recent menu update that affected the items in your cart. Please review your cart and submit your order again.',
  TERMS_OF_USE_ERROR: 'You must accept the Terms of Use & Privacy Policy.',
  TERMS_OF_USE_WITH_GIFT_TERMS_ERROR: 'You must accept the Terms of Use, Privacy Policy and Restaurant Gift Terms.'
};

export function getOrderErrorMessage(code = null, value = null) {
  if (!code) {
    return DEFAULT_ORDER_ERROR;
  }

  let error = ORDER_ERRORS[code.toString()];

  if (code === '20070' && value && value.phone) {
    error = error.replace("restaurant phone number", value.phone);
  }

  return error && error.length ? error : DEFAULT_ORDER_ERROR;
}
