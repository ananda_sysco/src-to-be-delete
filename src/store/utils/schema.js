import {normalize, schema} from 'normalizr';
// category schema
const attributeValues = new schema.Entity('attributeValues', {}, {
  idAttribute: 'attributeValueId',
});

const attributes = new schema.Entity('attributes', {
  values: [attributeValues],
}, {
  idAttribute: (entity, parent, key) => {
    return entity.id + '_' + parent.id;
  },
  processStrategy: (entity, parent, key) => {
    const newEntity = Object.assign({}, entity);
    newEntity.values = entity.values.value || [];
    return newEntity;
  }
});

const attributeSet = new schema.Entity('attributeSet', {
  attributes: [attributes],
}, {
  processStrategy: (entity, parent, key) => {
    const newEntity = Object.assign({}, entity);
    newEntity.attributes = entity.attributes.attribute || [];
    return newEntity;
  }
});

const products = new schema.Entity('products', {
  attributeSet: attributeSet
});

const category = new schema.Entity('categories', {
  products: [products],
}, {
  processStrategy: (entity, parent, key) => {
    const {products, subCategories} = entity;

    return {
      ...entity,
      products: products ? products.product.slice() : [],
      subCategories: subCategories ? subCategories.category.slice() : [],
      isSubCategory: key === 'subCategories',
    };
  },
});

category.define({
  subCategories: [category],
});

const categoriesSchema = new schema.Array(category);

export function normalizeCategories(target) {
  return normalize(target, categoriesSchema);
}

// session schema
const session = new schema.Entity('sessions', {}, {
  processStrategy: (entity, parent, key) => {
    const {sessionhours, ...newEntity} = entity;
    return {...newEntity, slots: sessionhours.ranges.slice()};
  },
});

// restaurant info schema
const restaurantSchema = new schema.Entity('restaurant', {
  sessions: [session],
}, {
  processStrategy: (entity, parent, key) => {
    const newEntity = Object.assign({}, entity);
    newEntity.sessions = entity.sessions ? entity.sessions.session.slice() : null;
    newEntity.cuisines = entity.cuisines ? entity.cuisines.cuisine.slice() : null;
    newEntity.images = entity.images ? entity.images.image.slice() : null;
    return newEntity;
  },
});

export function normalizeRestaurant(target) {
  return normalize(target, restaurantSchema);
}

export function normalizeSession(target) {
  return normalize(target, session);
}
