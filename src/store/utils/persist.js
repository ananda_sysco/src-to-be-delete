import localForage from 'localforage';
import moment from 'moment';
import {persistStore, purgeStoredState, createTransform} from 'redux-persist';
import createExpirationTransform from 'redux-persist-transform-expire';

export const REHYDRATE = 'persist/REHYDRATE';

const expirationConfig = {
  expireKey: 'persistExpiresAt',
};



// expire parts of the persisted store
const expirationTransform = createExpirationTransform(expirationConfig);


const myTransform = createTransform(
  (inboundState, key) => {
    return {...inboundState};
  },
  (outboundState, key) => {
    if (outboundState.totalIsLoading) {
      return {};
    }
    return {...outboundState};
  }
);

// keyPrefix defaults to 'reduxPersist'
const persistConfig = {
  storage: localForage,
  whitelist: ['cart'],
  transforms: [
    expirationTransform,
    myTransform
  ],
};

export function definePrefix(prefix) {
  persistConfig.keyPrefix = prefix;
}

export function getPrefix() {
  return persistConfig.keyPrefix;
}

export function getExpiration() {
  return {
    [expirationConfig.expireKey]: moment().add(1, 'hour').toDate(),
  };
}

// returns persistor object
export function persist(store, cb) {
  if (!getPrefix()) {
    throw new Error('Must have a defined keyPrefix');
  }

  return persistStore(store, persistConfig, cb);
}

// returns a promise
export function reset() {
  if (!getPrefix()) {
    throw new Error('Must have a defined keyPrefix');
  }

  return purgeStoredState(persistConfig);
}
