import { concat, round } from 'lodash';

export function fpSplice(array, index) {
    return array.slice(0, index).concat(array.slice(index + 1));
}

export function replace(array, index, item) {
    return concat(array.slice(0, index), item, array.slice(index + 1));
}

export function countTotalItems(items) {
    return items.reduce((count, item) => count + item.quantity, 0);
}

export function extractInstructions(attributeGroups, state) {
    const attributeValues = state.app.attributeValues;
    const instructions = [];
    let specialInstructions = null;

    for (let attributeGroupId in attributeGroups) {
        if (attributeGroupId === '1') {
            specialInstructions = attributeGroups[attributeGroupId];
            continue;
        }

        const attributeGroup = attributeGroups[attributeGroupId];

        for (let attributeValueId in attributeGroup) {
            if (attributeGroup[attributeValueId]) {
                instructions.push(attributeValues[attributeValueId].value);
            }
        }
    }

    return { specialInstructions, instructions };
}

export function formatAddress(address) {
    if (!address) {
        return '';
    }

    return `${address.address1} ${address.address2}, ${address.city}, ${address.state} ${address.zipCode}`;
}

export function calculateTip(totalCost, value, percentage) {
    return percentage ? Number(round((totalCost * percentage), 2).toFixed(2)) : value;
}
