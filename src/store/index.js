import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {autoRehydrate} from 'redux-persist';
import logger from 'redux-logger';
import env from '../utils/env';

import app from 'store/app';
import cart from 'store/cart';
import user from 'store/user';
import productModal from 'store/productModal';
import wallet from 'store/wallet';
import search from 'store/search';
import receipt from 'store/receipt';

const rootReducer = combineReducers({app, cart, user, productModal, wallet, search, receipt});
const middlewares = [thunkMiddleware];

if (env('NODE_ENV') !== 'production') {
  middlewares.push(logger);
}

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(...middlewares),
  autoRehydrate(),
));

export default store;
