import ConsumerApi from 'lib/ConsumerApi';
import GoogleApi from "lib/GoogleApi";
import config from 'config';
import {getOpeningTime} from "../utils/sessionUtil";
import {normalizeSession} from "./utils/schema";
import {createErrorNotification} from 'store/productModal';
import {clearNotifications} from "store/app";
import {clone} from 'lodash';

const SET_LOCATION = 'set_location';
const SET_TEXT = 'set_text';
const SET_RESULTS = 'set_results';
const SET_PAGE = 'set_page';
const SET_REQUEST_TIMESTAMP = 'set_request_timestamp';
const APPEND_RESULTS = 'append_results';
const SET_ADDRESS = 'set_address';
const SET_ACTIVE_RESTAURANT = 'set_active_restaurant';
const SET_ALL_RESULTS_LOADED = 'set_all_results_loaded';
const SET_SEARCH_RESULTS_PENDING = 'set_search_results_pending';
const SET_NEW_SEARCH_REQUESTED = 'set_new_search_requested';
const RESET_SEARCH = 'reset_search';
const SWITCH_MAP_VIEW = 'switch_map_view';
const SET_DISPLAY_REDO_SEARCH_BUTTON = 'set_display_redo_search_button';
const SET_RESULTS_WAITING_LIST = 'set_results_waiting_list';

const pageSize = config.searchResultsPageSize || 30;
const displayPageSize = config.searchDisplayPageSize || 10;
const searchRadius = config.searchRadius || "50km";
const maxOperatorCount = config.searchResultsMaxOperatorCount || 200;
const localStorageKey = 'olo_address';
const esQueryDistanceType = 'plane';

const initialState = {
  location: null,
  text: null,
  results: null,
  address: null,
  page: 0,
  requestTimestamp: 0,
  allResultsLoaded: false,
  activeRestaurant: null,
  searchResultsPending: false,
  newSearchRequested: false,
  isMapView: false,
  maxOperatorCount: maxOperatorCount,
  displayRedoSearchButton: false,
  resultsWaitingList: []
};

const consumerApi = new ConsumerApi(config, true);
const googleApi = new GoogleApi(config, true);

export default function searchReducer(state = clone(initialState), action = {}) {
  switch (action.type) {

    case SET_LOCATION: {
      const {location} = action.payload;

      return {
        ...state,
        location
      };
    }

    case SET_TEXT: {
      const {text} = action.payload;

      return {
        ...state,
        text
      };
    }

    case SET_ADDRESS: {
      const {address} = action.payload;

      return {
        ...state,
        address
      };
    }

    case SET_PAGE: {
      const {page} = action.payload;

      return {
        ...state,
        page
      };
    }

    case SET_REQUEST_TIMESTAMP: {
      const {requestTimestamp} = action.payload;

      return {
        ...state,
        requestTimestamp
      };
    }

    case SET_RESULTS: {
      const {results} = action.payload;

      return {
        ...state,
        results
      };
    }

    case APPEND_RESULTS: {
      const payload = action.payload.results;
      const {results} = state;
      let temp = results;
      if (results !== null) {
        temp = results.concat(payload);
      }

      return {
        ...state,
        results: temp
      };
    }

    case RESET_SEARCH: {
      return {
        ...clone(initialState)
      }
    }

    case SET_ACTIVE_RESTAURANT: {
      const {activeRestaurant} = action.payload;

      return {
        ...state,
        activeRestaurant
      };
    }

    case SET_ALL_RESULTS_LOADED: {
      const {allResultsLoaded} = action.payload;
      return {
        ...state,
        allResultsLoaded: allResultsLoaded
      };
    }

    case SET_SEARCH_RESULTS_PENDING: {
      const {searchResultsPending} = action.payload;
      return {
        ...state,
        searchResultsPending: searchResultsPending
      };
    }

    case SET_NEW_SEARCH_REQUESTED: {
      const {newSearchRequested} = action.payload;
      return {
        ...state,
        newSearchRequested: newSearchRequested
      };
    }

    case SWITCH_MAP_VIEW: {
      const {isMapView} = state;
      return {
        ...state,
        isMapView: !isMapView
      };
    }

    case SET_DISPLAY_REDO_SEARCH_BUTTON: {
      return {
        ...state,
        displayRedoSearchButton: action.payload
      };
    }

    case SET_RESULTS_WAITING_LIST: {
      return {
        ...state,
        resultsWaitingList: action.payload
      }
    }

    default:
      return state;
  }
}

export function setLocation(location) {
  return {type: SET_LOCATION, payload: {location}};
}

export function setSearchText(text) {
  return {type: SET_TEXT, payload: {text}};
}

export function setSearchResults(values) {
  return {type: SET_RESULTS, payload: values};
}

export function setAddress(address) {
  return {type: SET_ADDRESS, payload: {address}};
}

export function setActiveRestaurant(activeRestaurant) {
  return async (dispatch) => {
    dispatch({type: SET_ACTIVE_RESTAURANT, payload: {activeRestaurant}});
  }
}

export function setAllResultsLoaded(allResultsLoaded) {
  return async (dispatch) => {
    dispatch({type: SET_ALL_RESULTS_LOADED, payload: {allResultsLoaded}});
  };
}

export function appendResults(results) {
  return async (dispatch) => {
    dispatch({type: APPEND_RESULTS, payload: {results}});
  };
}

export function setPage(page) {
  return async (dispatch) => {
    dispatch({type: SET_PAGE, payload: {page}});
  };
}

export function resetSearch(page) {
  return async (dispatch) => {
    dispatch({type: RESET_SEARCH, payload: {}});
  };
}

export function switchMapView() {
  return async (dispatch) => {
    dispatch({type: SWITCH_MAP_VIEW});
  };
}

export function setDisplayRedoSearchButton(status) {
  return async (dispatch) => {
    dispatch({type: SET_DISPLAY_REDO_SEARCH_BUTTON, payload: status})
  }
}

export function setResultsWaitingList(list) {
  return {type: SET_RESULTS_WAITING_LIST, payload: list};
}

export function doSearch(location, address, text) {
  return async (dispatch) => {
    dispatch(clearNotifications());
    dispatch(setLocation(location));
    dispatch(setSearchText(text));
    dispatch(setAddress(address));
    dispatch(setResultsWaitingList([]));
    dispatch({type: SET_PAGE, payload: {page: 0}});
    dispatch({type: SET_ALL_RESULTS_LOADED, payload: {allResultsLoaded: false}});
    dispatch({type: SET_NEW_SEARCH_REQUESTED, payload: {newSearchRequested: true}});
    dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: true}});

    const results = await fetchOperators(location, 0, pageSize);

    if (results instanceof Error) {
      dispatch(createErrorNotification('We are experiencing some technical difficulties. Please try again later.'));
      dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: false}});
      return;
    }
    let displayResults = [];
    let tempWaitingResultsList = [];

    if (results instanceof Array) {
      const {length} = results;
      if (length > 0) {
        if (length >= displayPageSize) {
          displayResults = results.slice(0, displayPageSize);
          tempWaitingResultsList = results.slice(displayPageSize);
        } else {
          displayResults = results;
          dispatch({type: SET_ALL_RESULTS_LOADED, payload: {allResultsLoaded: true}});
        }
      } else {
        dispatch({type: SET_ALL_RESULTS_LOADED, payload: {allResultsLoaded: true}});
      }
    }
    dispatch(setResultsWaitingList(tempWaitingResultsList));
    dispatch({type: SET_RESULTS, payload: {results: displayResults}});
    dispatch({type: SET_NEW_SEARCH_REQUESTED, payload: {newSearchRequested: false}});
    dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: false}});
  }
}

export function loadNextPage() {
  return async (dispatch, getState) => {
    dispatch(clearNotifications());
    const resultsWaitingList = getState().search.resultsWaitingList;
    dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: true}});
    if (resultsWaitingList.length < displayPageSize) {
      const requiredResultOffset = displayPageSize - resultsWaitingList.length;
      const page = getState().search.page + 1;
      const previousTimestamp = getState().search.requestTimestamp;
      const d = new Date();
      const timestamp = d.getTime();

      //region skip immediate requests
      if (timestamp - previousTimestamp < 1000) {
        return;
      }
      //endregion

      dispatch({type: SET_REQUEST_TIMESTAMP, payload: {requestTimestamp: timestamp}});

      const results = await fetchOperators(getState().search.location, (page * pageSize) + 1, pageSize);

      if (results instanceof Error) {
        dispatch(createErrorNotification('We are experiencing some technical difficulties. Please try again later.'));
        dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: false}});
        return;
      }

      let displayResults = [];
      let tempWaitingResultsList = [];
      if (results instanceof Array) {
        const {length} = results;
        if (length > 0) {
          displayResults = results.slice(0, requiredResultOffset);
          displayResults = resultsWaitingList.concat(displayResults);
          tempWaitingResultsList = results.slice(requiredResultOffset);
        } else {
          displayResults = resultsWaitingList;
          dispatch({type: SET_ALL_RESULTS_LOADED, payload: {allResultsLoaded: true}});
        }
      }
      dispatch(setResultsWaitingList(tempWaitingResultsList));
      dispatch({type: SET_PAGE, payload: {page}});
      dispatch({type: APPEND_RESULTS, payload: {results: displayResults}});
    } else {
      const displayResults = resultsWaitingList.slice(0, displayPageSize);
      const waitingResultsList = resultsWaitingList.slice(displayPageSize);
      dispatch({type: APPEND_RESULTS, payload: {results: displayResults}});
      dispatch(setResultsWaitingList(waitingResultsList));
    }
    dispatch({type: SET_SEARCH_RESULTS_PENDING, payload: {searchResultsPending: false}});
  }
}

async function fetchOperators(geoLocation, page, pageSize) {

  const source = {
    "from": page,
    "size": pageSize,
    "query": {
      "bool": {
        "filter": [
          {"term": {"isOLOAvailable": true}}
        ],
        "must": {
          "geo_distance": {
            "distance": searchRadius,
            "distance_type": esQueryDistanceType,
            "geoLocation": {
              "lat": geoLocation.lat,
              "lon": geoLocation.lng
            }
          }
        }
      }
    },
    "sort": [
      {
        "_geo_distance": {
          "geoLocation": {
            "lat": geoLocation.lat,
            "lon": geoLocation.lng
          },
          "order": "asc",
          "unit": "km",
          "distance_type": esQueryDistanceType
        }
      }
    ]
  };

  try {

    let results = await consumerApi.getOperators(JSON.stringify(source));
    if (results && results.length > 0) {
      results = results.map(operator => {

        let openingTime = null;
        if (operator.sessions && operator.sessions.length > 0) {
          let tempSessions = [];
          for (let session of operator.sessions) {
            const sessionInfo = normalizeSession(session);
            tempSessions.push(sessionInfo.entities.sessions[session.id]);
          }
          openingTime = getOpeningTime(tempSessions, operator);
        }

        return {
          name: operator.name,
          address: operator.address,
          cuisines: operator.cuisines ? operator.cuisines : [],
          lat: operator.geoLocation.lat,
          lon: operator.geoLocation.lon,
          id: operator.accountId,
          openingTime,
          isPickupAvailable: operator.isPickupAvailable,
          isDeliveryAvailable: operator.isDeliveryAvailable,
          isOnline: operator.isOnline
        };

      });

    }

    return results;

  } catch (e) {
    return e;
  }
}

export function setDecodedAddress(geoLocation) {
  return async (dispatch) => {
    let result = await googleApi.getDecodedAddress(geoLocation);
    if (result instanceof Error || result === null) {
      result = geoLocation.lat.toFixed(6) + ',' + geoLocation.lng.toFixed(6);
    }
    dispatch(setAddress(result));
    if (localStorage) {
      localStorage.setItem(localStorageKey, result);
    }
  };

}
