import { addNotification } from './app';

const initialState = {
  isOpen: false,
  menuId: null,
  isSubCategory: false,
  selectedProduct: null,
  index: null,
  attributeGroups: null,
  quantity: 1,
  cost: 0,
  isProductPending: false
};

// action constants
const ADD_TO_CART = 'add_to_cart';
const DESELECT_PRODUCT = 'deselect_product';
const EDIT_ITEM = 'edit_item';
const SELECT_MENU_ITEM = 'select_menu_item';
const SELECT_PRODUCT = 'select_product';
const UPDATE_CART = 'update_cart';
const UPDATE_PRODUCT_QUANTITY = 'update_product_quantity';
const UPDATE_ATTRIBUTE_GROUPS = 'update_attribute_groups';
const CLEAR_ATTRIBUTE_GROUPS = 'clear_attribute_groups';
const SET_PRODUCT_PENDING = 'set_product_pending';

// reducer
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_TO_CART:
      return { ...initialState };

    case UPDATE_CART:
      return { ...initialState };

    case DESELECT_PRODUCT:
      return { ...initialState };

    case CLEAR_ATTRIBUTE_GROUPS:
      return {...state, attributeGroups: null};

    case SET_PRODUCT_PENDING: {
      const {state: value} = action.payload;
      return {...state, isProductPending: value};
    }

    case EDIT_ITEM:  {
     /*
      item =  { lineItemId, menuId, productId, quantity, name, instructions, totalCost, attributeGroups }
     */
      const { item, index } = action.payload;
      const { attributeGroups, quantity, cost, productId, menuId, isSubCategory } = item;

      return {
        ...state,
        index,
        isSubCategory,
        menuId,
        attributeGroups,
        quantity,
        cost,
        selectedProduct: productId,
        isOpen: true,
      }
    }

    case SELECT_MENU_ITEM: {
      const { menuId, isSubCategory, products } = action.payload;
      const selectedProduct = isSubCategory ? null : menuId;
      const cost = isSubCategory ? 0 : products[selectedProduct] ? products[selectedProduct].price : 0;

      return {
        ...state,
        isOpen: true,
        menuId,
        isSubCategory,
        selectedProduct,
        cost,
      }
    }

    case SELECT_PRODUCT: {
      const { attributeValues, products, productId } = action.payload;
      const { attributeGroups } = state;
      const product = products[productId];

      let cost = product ? product.price : 0;

      // use case where a user has already selected attributes, calculate cost
      if (attributeGroups) {
        for (let attributeGroupId in attributeGroups) {
          if (attributeGroups.hasOwnProperty(attributeGroupId)) {
            let attributeGroup = attributeGroups[attributeGroupId];
            for (let attributeValueId in attributeGroup) {
              if (attributeGroup[attributeValueId] && attributeValues[attributeValueId]) {
                cost += attributeValues[attributeValueId].price || 0;
              }
            }
          }
        }
      }

      return {
        ...state,
        selectedProduct: productId,
        cost,
      }
    }

    case UPDATE_PRODUCT_QUANTITY:
      return {
        ...state,
        quantity: action.payload.quantity,
      };

    case UPDATE_ATTRIBUTE_GROUPS: {
      const { attributeGroup, attributeValues, products } = action.payload;
      const newAttributeGroups = Object.assign({}, state.attributeGroups, attributeGroup);

      let cost = state.selectedProduct ? products[state.selectedProduct].price : 0;

      for (let attributeGroupId in newAttributeGroups) {
        if (newAttributeGroups.hasOwnProperty(attributeGroupId)) {
          let attGroup = newAttributeGroups[attributeGroupId];
          for (let attributeValueId in attGroup) {
            if (attGroup[attributeValueId] && attributeValues[attributeValueId]) {
              cost += attributeValues[attributeValueId].price !== undefined ? attributeValues[attributeValueId].price : 0;
            }
          }
        }
      }

      return {
        ...state,
        attributeGroups: newAttributeGroups,
        cost,
      }
    }

    default:
      return state;
  }
}

// action creators
export function deselectProduct() {
  return { type: DESELECT_PRODUCT, payload: null };
}

export function clearAttributes() {
  return {type: CLEAR_ATTRIBUTE_GROUPS, payload: null};
}

export function editItem(lineItemId) {
  return (dispatch, getState) => {
    const cart = getState().cart;
    let item, index;
    cart.items.forEach((i, cartIndex) => {
      if (i.lineItemId === lineItemId) {
        index = cartIndex;
        item = i;
      }
    });

    if (index !== undefined) {
      dispatch({ type: EDIT_ITEM, payload: { index, item } });
    } else {
      dispatch({ type: EDIT_ITEM, payload: { item, index }, error: true });
    }
  }
}

export function selectMenuItem(menuId, isSubCategory) {
  return async (dispatch, getState) => {
    const state = getState();

    dispatch({ type: SELECT_MENU_ITEM, payload: { menuId, isSubCategory, products: state.app.products } });

  }
}

export function selectProduct(productId) {
  return (dispatch, getState) => {
    const { attributeValues, products } = getState().app;

    dispatch({ type: SELECT_PRODUCT, payload: { productId, attributeValues, products } });
  }
}

export function createErrorNotification(message, disableActions = false) {
  return  (dispatch) => {
    dispatch(addNotification({type: 'error', message, disableActions}));
  }
}

export function updateAttributeGroups(attributeGroup) {
  return (dispatch, getState) => {
    const { attributeValues, products } = getState().app;

    dispatch({ type: UPDATE_ATTRIBUTE_GROUPS, payload: { attributeGroup, attributeValues, products } });
  }
}

export function updateProductQuantity(quantity) {
  return { type: UPDATE_PRODUCT_QUANTITY, payload: { quantity } };
}

