import 'test_helper';
import nock from 'nock';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import config from 'config';
import cart, {
  addToCart,
  removeFromCart,
  updateCart,
  selectOrderType,
  calculateTotal,
  prepareContactForApi,
} from 'store/cart';
import {
  product as productFixture,
  appStructure,
} from 'fixtures';

const mockStore = configureStore([thunk]);

// Attribute Set: 'Additions', Attribute Value: { name: 'Add Avocado', price: 1.5 }
const mockAttributeGroups = () => ({
  'd6f39d19-e593-4ad7-ce77-8e8730b4b9da': {
    '46fffebb-7a89-45fd-8ca4-6e07361a01af': true,
    'a083a089-b4f3-4174-face-7b0b69681cd6': false,
    'ca549d5f-ac76-4077-d13a-9738020ad498': false,
    'c0f95e6d-6820-40f1-c735-f15b3fedde0c': false,
    'a92efeef-6692-41e8-9113-a5d62e91f20e': false,
    '1e778980-b619-4b4d-80e4-9e2e4c28ee79': false,
    '29738529-9522-4c18-cb26-0ea9578b845d': false,
    '0b3b0a7c-5578-4a9c-b31e-f93cfdab3ac6': false,
    'ccd303c4-d85b-42e9-82f3-e192135018be': false,
  },
  '1': 'Extra toast',
});

const mockItem = () => ({
  attributeGroups: mockAttributeGroups(),
  menuId: productFixture.id,
  productId: productFixture.id,
  quantity: 1,
  cost: productFixture.price,
});

const getState = () => ({
  auth: {
    token: 'abc123',
  },
  app: {
    ...appStructure.app,
    products: {
      [productFixture.id]: productFixture,
    },
  },
  cart: {
    items: [],
  },
});

const mockCartWithItems = async (items) => {
  const store = mockStore(getState);

  // add some items
  for (let i = 0; i < items.length; i++) {
    await store.dispatch(addToCart(items[i]));
  }

  // return updated state
  return store.getActions().reduce((state, action) => {
    return cart(state, action);
  }, undefined);
};

describe('reducers/cart', () => {
  beforeAll(() => {
    nock(config.backendUrl)
      .persist()
      .post('/oauth/token')
      .reply(200, { access_token: 'success_token', expires_in: 300 });

    nock(config.backendUrl)
      .persist()
      .post('/consumer/service/tax')
      .reply(200, { totalTax: 1, totalCost: 1 });

    nock(config.backendUrl)
    .persist()
    .post('/consumer/calculateTax')
    .reply(200, {data:{subTotal: 18, totalTax: 1}});
  });

  afterAll(() => {
    nock.cleanAll();
  });

  it('should create an add to cart action', async () => {
    const item = mockItem();
    const attributeGroups = mockAttributeGroups();
    const store = mockStore(getState);

    // dispatch action
    await store.dispatch(addToCart(item));
    const [ action ] = store.getActions();

    expect(action.payload.item).toMatchObject({
      name: productFixture.name,
      attributeGroups,
      instructions: ['Add Avocado'],
      specialInstructions: 'Extra toast',
    });
  });

  it('should add an item', async () => {
    const item = mockItem();
    const store = mockStore(getState);
    await store.dispatch(addToCart(item));

    // dispatch action
    const [ action ] = store.getActions();
    const reduced = cart(undefined, action);

    expect(reduced).toMatchObject({
      items: [action.payload.item],
      empty: false,
      totalItems: 1,
    });
  });

  it('should add multiple items', async () => {
    const item = mockItem();
    const store = mockStore(getState);

    // add two items
    await store.dispatch(addToCart(item));
    await store.dispatch(addToCart(item));

    // dispatch action
    const actions = store.getActions();

    const reduced = actions.reduce((state, action) => {
      return cart(state, action);
    }, undefined);

    // this is due to set_ready_by_options, disable_checkout, select_order_time and order_time_manual_select actions being fired
    expect(actions.length).toEqual(10);
    expect(reduced).toMatchObject({
      items: [actions[0].payload.item, actions[5].payload.item],
      empty: false,
      totalItems: 2,
    });
  });

  it('should remove items', async () => {
    const item = mockItem();
    const store = mockStore(getState);

    // add an item
    await store.dispatch(addToCart(item));
    await store.dispatch(removeFromCart(0));

    const actions = store.getActions();

    const reduced = actions.reduce((state, action) => {
      return cart(state, action);
    }, undefined);

    expect(reduced).toMatchObject({
      items: [],
      empty: true,
      totalItems: 0,
    });
  });

  it('should update an item', async () => {
    const cartState = await mockCartWithItems([mockItem(), mockItem(), mockItem()]);
    const store = mockStore(() => ({
      ...getState(),
      cart: cartState,
    }));

    const updatedItem = {...cartState.items[2], quantity: 10, cost: 60};
    await store.dispatch(updateCart(2, updatedItem));
    const actions = store.getActions();

    const reduced = actions.reduce((state, action) => {
      return cart(state, action);
    }, cartState);

    expect(reduced).toMatchObject({
      items: [
        cartState.items[0],
        cartState.items[1],
        {...cartState.items[2], ...updatedItem, ...{totalCost: 60}},
      ],
      totalItems: 12,
    });
  });

  it('should select orderType', async () => {
    const store = mockStore(getState);
    await store.dispatch(selectOrderType('PICKUP'));

    const [ action ] = store.getActions();
    const reduced = cart(undefined, action);
    expect(reduced).toMatchObject({
      orderType: 'PICKUP',
    });
  });

  it('should calculate totals', async () => {
    const cartState = await mockCartWithItems([mockItem(), mockItem(), mockItem()]);
    const store = mockStore(() => ({
      ...getState(),
      cart: cartState,
    }));

    await store.dispatch(calculateTotal());
    const actions = store.getActions();
    const reduced = actions.reduce((state, action) => {
      return cart(state, action);
    }, cartState);

    expect(reduced).toMatchObject({
      totalCost: 18,
      tax: 1,
      grandTotal: 19,
    });
  });

  it('should prepare a contact for the API', async () => {
    const contact = {
      firstName: 'Homer',
      lastName: 'Simpson',
      email: 'homer@springfield.com',
      phone: '6504507211#123',
      address1: '1110 Franklin Street Apartments',
      address2: 'F',
      city: 'Redwood City',
      state: 'CA',
      zipCode: '94063',
    };

    expect(prepareContactForApi(contact)).toEqual({
      address: {
        state: 'CA',
        address1: '1110-F',
        address2: 'Franklin Street Apartments',
        city: 'Redwood City',
        zipCode: '94063'
      },
      phone: {
        areaCode: '650',
        exchangeCode: '450',
        subscriberNumber: '7211',
        extension: '123'
      }
    });
  });

  it('should prepare a contact for the API without address', async () => {
    const contact = {
      firstName: 'Homer',
      lastName: 'Simpson',
      email: 'homer@springfield.com',
      phone: '6504507211#123',
    };

    expect(prepareContactForApi(contact)).toEqual({
      phone: {
        areaCode: '650',
        exchangeCode: '450',
        subscriberNumber: '7211',
        extension: '123'
      }
    });
  });
});
