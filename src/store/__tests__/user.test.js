import user, { addContact, addPaymentInfo } from 'store/user';
import 'test_helper';

describe('reducers/user', () => {
  it('should add contact', () => {
    const contact = {
      firstName: 'Homer',
      lastName: 'Simpson',
      email: 'homer@springfield.com',
      phone:  '6504561234',
      address1: '1234 Some Street',
      address2: 'A',
      city: 'Austin',
      state: 'TX',
      zipCode: '78704',
      textMessageConfirmation: true
    };
    const action = addContact(contact);
    const reduced = user(undefined, action);

    expect({currentUser: reduced['currentUser']}).toMatchObject({
      currentUser: {
        firstName: contact.firstName,
        lastName: contact.lastName,
        textMessageConfirmation: true,
        contacts: [
          {
            phone: contact.phone,
            address1: contact.address1,
            address2: contact.address2,
            city: contact.city,
            state: contact.state,
            zipCode: contact.zipCode,
            email: contact.email,
          },
        ],
      },
    });
  });

  it('should add payment info', () => {
    const paymentCard = {
      cardNumber: '4111111111111111',
      nameOnCard: 'Homer Simpson',
      securityCode: '447',
      expiration: '0624'
    };

    const action = addPaymentInfo(paymentCard);
    const reduced = user(undefined, action);

    expect(reduced).toMatchObject({
      currentUser: {
        creditCards: [paymentCard],
      },
    });
  });
});
