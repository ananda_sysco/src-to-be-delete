import faker from 'faker';
import 'test_helper';
import nock from 'nock';
import config from 'config';
import appReducer, { addNotification, dismissNotification, formatPriceRange } from '../app';

const accessToken = faker.random.uuid();
const subCategory = {
  "id": "347379b7-3940-45d6-f736-f6be0784b117",
  "name": "Auto_Category_Sub2",
  "description": "",
  "merchantId": "10213688",
  "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
  "isActive": true,
  "lineNo": null,
  "parentId": "dcdf638c-a5ac-4613-a16c-77e76ed876bd",
  "products": [
    "abce4546-9d08-4468-9bc7-3bcad64c07c6"
  ],
  "subCategories": [],
  "isSubCategory": true
};
const products = {"abce4546-9d08-4468-9bc7-3bcad64c07c6": {
    "id": "abce4546-9d08-4468-9bc7-3bcad64c07c6",
    "name": "Item AAC- $99.99",
    "description": "",
    "merchantId": "10213688",
    "reference": "82",
    "price": 99.99,
    "kitchenName": "Item AAC- $99.99",
    "receiptName": "Item AAC- $99.99",
    "taxCategoryId": "018c6568-c60e-41b8-faa0-3b3aab3d99f8",
    "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
    "isAuxiliary": false,
    "attributeSet": "36e4f7a0-220b-11e8-8a49-efe5e898a52f"
  }};

describe('reducers/app', () => {
  beforeAll(() => {
    nock(config.backendUrl)
      .persist()
      .post('/oauth/token')
      .reply(200, { access_token: accessToken });
  });

  afterAll(() => {
    nock.cleanAll();
  });

  it('should add a notification', async () => {
    const initialState = {
      notifications: [],
    };

    const action = addNotification({ type: 'error' });
    const reduced = appReducer(initialState, action);

    expect(reduced.notifications[0]).toEqual({ type: 'error', 'idx': 0 });
  });

  it('should NOT add a duplicate notification', async () => {
    const initialState = {
      notifications: [{ type: 'error' }],
    };

    const action = addNotification({ type: 'error' });
    const reduced = appReducer(initialState, action);

    expect(reduced.notifications[0]).toEqual({ type: 'error', idx: 0 });
    expect(reduced.notifications.length).toEqual(1);
  });

  it('should dismiss a notification', async () => {
    const initialState = {
      notifications: [
        { type: 'current' },
        { type: 'next' },
        { type: 'waiting' },
      ],
    };

    const action = dismissNotification();
    const reduced = appReducer(initialState, action);

    expect(reduced.notifications).toEqual([
      { type: 'next' },
      { type: 'waiting' },
    ]);
  });

  it('should format the price',async ()=>{
    const json = formatPriceRange(subCategory, products);
    expect(json).toEqual("99.99");
  })
});
