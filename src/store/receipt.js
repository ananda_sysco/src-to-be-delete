import ConsumerApi from 'lib/ConsumerApi';
import config from 'config';
import {getRestaurant} from './app';

// action constants
const GET_RECEIPT = 'get_receipt';
const FETCH_ERROR = 'fetch_error';

// state
const initialState = {
  id: null,
  orderNumber: null,
  customer: null,
  accountId: null,
  createdTime: null,
  total: null,
  subTotal: null,
  tax: null,
  discount: null,
  tipAmount: null,
  lineItems: [],
  error: null
};

const consumerApi = new ConsumerApi(config, true);

function formatItemList(items) {
  const itemsArray = [];

  let auxItems = [];
  let mainItem = null;
  let priceWithAux = 0;
  if (items) {
    // lets sort items according to the line number first
    items.sort((a,b) => a.lineno - b.lineno);
    items.forEach(item => {
      const formattedItem = {
        id: item.productId,
        name: item.productName,
        quantity: item.qty,
        totalCost: item.price,
        instructions: item.instructions || []
      };

      if (item.isAuxiliary) {
        auxItems.push(formattedItem);
        priceWithAux += item.price;
      } else {
        if (mainItem) {
          mainItem.auxItems = auxItems;
          mainItem.priceWithAux = priceWithAux;
          itemsArray.push(mainItem);
        }
        mainItem = formattedItem;
        priceWithAux = item.price;
        auxItems = [];
      }
    });

    if (mainItem) {
      mainItem.auxItems = auxItems;
      mainItem.priceWithAux = priceWithAux;
      itemsArray.push(mainItem);
    }
  }
  return itemsArray;
}
// reducer
export default function receiptReducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_RECEIPT:
      return {
        ...state,
        ...action.payload,
      };

    case FETCH_ERROR:
      return {
        ...state,
        error: action.payload,
      };

    default:
      return state;
  }
}

export function getReceipt(receiptId) {
  return async (dispatch) => {
    try {
      const receiptJson = await consumerApi.getReceiptInfo(receiptId);
      dispatch(getRestaurant(receiptJson.accountId, false));

      if (receiptJson.lineItems) {
        receiptJson.lineItems = formatItemList(receiptJson.lineItems.lineItem);
      }
      if (receiptJson.discount) {
        receiptJson.discount = Math.abs(receiptJson.discount);
      }

      dispatch({type: GET_RECEIPT, payload: receiptJson});
    } catch (e) {
      dispatch({type: FETCH_ERROR, payload: {body: e, error: true, errorStatus: e.status}});
    }

  };
}
