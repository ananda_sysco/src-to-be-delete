import {cloneDeep, pick} from 'lodash';
import AuthApi from 'lib/AuthApi';
import APIError from 'lib/APIError';
import config from 'config';
import {addNotification} from './app';
import ConsumerApi from "../lib/ConsumerApi";
import {
  mapGivenUserToStore,
  mapPaymentTypesForUpdateRequest,
  mapUserPayloadForUpdateRequest,
  prepareContactForUserBackend,
  updatedBasicUserInfo,
  updatedUserAddress
} from "../managers/userManager";
import {EVENTS, SCENARIOS, trackEvent} from "../components/analytics/analyticalHandler";

const ADD_CONTACT = 'add_contact';
const UPDATE_CONTACT = 'update_contact';
const ADD_PAYMENT_INFO = 'add_payment_info';
const SHOW_LOGIN_MODAL = 'show_login_modal';
const HIDE_LOGIN_MODAL = 'hide_login_modal';
const SHOW_LOGOUT_MODAL = 'show_logout_modal';
const HIDE_LOGOUT_MODAL = 'hide_logout_modal';
const SHOW_FORGOT_PASSWORD_MODAL = 'show_forgot_password_modal';
const HIDE_FORGOT_PASSWORD_MODAL = 'hide_forgot_password_modal';
const SHOW_RESET_PASSWORD_MODAL = 'show_reset_password_modal';
const HIDE_RESET_PASSWORD_MODAL = 'hide_reset_password_modal';
const AUTHENTICATED_USER = 'authenticate_user';
const LOGOUT_USER = "logout_user";
const SET_FLOW_WAITING = "set_flow_waiting";
const PASSWORD_REST_SUCCESS = "password_reset_success";
const PASSWORD_UPDATE_SUCCESS = "password_update_success";
const SET_PASSWORD_REST_USER = "set_password_reset_user";
const SET_HAS_ACCOUNT = "set_has_account";
const SET_IS_GUEST = "set_is_guest";
const SET_LOGIN_MODAL_ERROR_CODE = "set_login_modal_error_code";
const SET_LOGIN_SECTION_ERROR_CODE = "set_login_section_error_code";
const SET_PASSWORD_RESET_ERROR_CODE = "set_password_reset_error_code";
const SET_PASSWORD_RESET_EMAIL_ERROR_CODE = "set_password_reset_email_error_code";
const CLEAR_LOGIN_ERROR = "clear_login_section_error_code";
const CLEAR_RESET_PASSWORD_MODAL = "clear_reset_password_modal";
const REQUEST_SIGN_UP_USER = 'request_sign_up_user';
const COMPLETED_SIGN_UP_USER = 'completed_sign_up_user';
const SET_SIGN_UP_ERROR_CODE = 'set_sign_up_error_code';
const SET_LOGGED_IN_USER = 'set_logged_in_user';
const SET_LOGGED_IN_USER_ONLY = 'set_logged_in_user_only';
const SET_CURRENT_USER_ONLY = 'set_current_user_only';
const SHOW_CARD_REMOVE_CONFIRMATION_MODAL = 'show_card_remove_confirmation_modal';
const HIDE_CARD_REMOVE_CONFIRMATION_MODAL = 'hide_card_remove_confirmation_modal';
const SWITCH_PAYMENT_INFO_ENTERING_FORM = 'switch_payment_info_entering_form';
const SET_PAYMENT_INFO = 'set_payment_info';
const PWD_RESET_TOKEN_ERROR = 'pwd_reset_token_error';
const CHANGE_SELECTED_CARD_IDENTIFIER = 'change_selected_card_identifier';
const CHANGE_SAVE_CARD_STATUS = 'change_save_card_status';
const SET_STATE = 'set_state';
const SET_CARD_DELETE_PENDING = 'card_delete_Pending';
const PWD_RESET_TOKEN_EXPIRED_MSG = 'This link expired. Please click the \'Forgot Password?\' link from the Sign In flow to request a new Password Reset link.';
const UNEXPECTED_ERROR_MESSAGE = 'Something went wrong! please retry';
const CLEAR_SIGN_UP_STATE = 'clear sign up state';
const SET_PHONE_NUMBER_VALID = 'set_phone_number_valid';
const CLEAR_GUEST_PAYMENT_INFO = 'clear_guest_payment_info';
const UPDATE_USER_DETAIL_PANEL_STATUS = 'update_user_detail_panel_status';

const initialState = {
  currentUser: null,
  loggedInUser: null,
  isLoginModalShown: false,
  isForgotPasswordModalShown: false,
  isResetPasswordModalShown: false,
  isLogoutModalShown: false,
  isFlowWaiting: false,
  isPasswordResetSuccess: false,
  passwordResetUser: null,
  hasAccount: false,
  isGuest: false,
  isPhoneNumberValid: true,
  loginModalErrorCode: null,
  loginSectionErrorCode: null,
  resetPasswordErrorCode: null,
  resetPasswordEmailErrorCode: null,
  isSignUpCompleted: false,
  isSignUpRequested: false,
  signUpErrorCode: null,
  cardRemoveConfirmationModalShown: false,
  paymentInfoEnteringFormShown: false,
  selectedCardIdentifier: null,
  isPwdResetTokenError: false,
  pwdResetTokenErrorMessage: null,
  selectedState: null,
  updateUserErrorCode: null,
  updateUserErrorMessage: null,
  cardDeletedPending: false,
  isDrawerOpen: false
};

const authApi = new AuthApi(config, true);
const consumerApi = new ConsumerApi(config, true);

export default function userReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_CONTACT: {
      const {firstName, lastName, textMessageConfirmation, ...contact} = action.payload;
      const previousContacts = (state.currentUser && state.currentUser.contacts) ? state.currentUser.contacts : [];

      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          firstName,
          lastName,
          contacts: [{...previousContacts[0], ...contact}],
          textMessageConfirmation: textMessageConfirmation,
        }
      };
    }

    case UPDATE_CONTACT: {
      const {key, value} = action.payload;

      let currentUser = {
        ...state.currentUser,
      };

      currentUser[key] = value;
      return {
        ...state,
        currentUser
      };
    }

    case ADD_PAYMENT_INFO: {
      const card = action.payload;

      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          creditCards: [card],
        },
      };
    }

    case SHOW_LOGIN_MODAL: {
      return {
        ...state,
        isLoginModalShown: true
      }
    }

    case HIDE_LOGIN_MODAL: {
      return {
        ...state,
        isLoginModalShown: false
      }
    }

    case SET_PHONE_NUMBER_VALID: {
      return {
        ...state,
        isPhoneNumberValid: action.payload.status
      }
    }

    case SHOW_FORGOT_PASSWORD_MODAL: {
      return {
        ...state,
        isForgotPasswordModalShown: true
      }
    }

    case HIDE_FORGOT_PASSWORD_MODAL: {
      return {
        ...state,
        isForgotPasswordModalShown: false,
        resetPasswordEmailErrorCode: null,
      }
    }

    case SHOW_RESET_PASSWORD_MODAL: {
      return {
        ...state,
        isResetPasswordModalShown: true
      }
    }

    case HIDE_RESET_PASSWORD_MODAL: {
      return {
        ...state,
        isResetPasswordModalShown: false
      }
    }

    case SET_PASSWORD_RESET_ERROR_CODE: {
      return {
        ...state,
        resetPasswordErrorCode: action.payload.status,
        resetPasswordErrorMessage: action.payload.message
      }
    }

    case SET_PASSWORD_RESET_EMAIL_ERROR_CODE: {
      return {
        ...state,
        resetPasswordEmailErrorCode: action.payload.status
      }
    }

    case REQUEST_SIGN_UP_USER: {
      return {
        ...state,
        isSignUpRequested: action.payload.status
      }
    }

    case COMPLETED_SIGN_UP_USER: {
      return {
        ...state,
        isSignUpCompleted: action.payload.status
      }
    }

    case SET_SIGN_UP_ERROR_CODE: {
      return {
        ...state,
        signUpErrorCode: action.payload.status
      }
    }

    case LOGOUT_USER: {
      return {
        ...state,
        loggedInUser: null,
        currentUser: null,
        hasAccount: null,
        isSignUpRequested: false,
        isSignUpCompleted: false,
        signUpErrorCode: null,
        selectedState: null
      }
    }

    case AUTHENTICATED_USER: {
      return {
        ...state,
        loggedInUser: action.payload,
        currentUser: action.payload
      }
    }

    case SET_FLOW_WAITING: {
      return {
        ...state,
        isFlowWaiting: action.payload.status
      }
    }

    case PASSWORD_REST_SUCCESS: {
      return {
        ...state,
        isPasswordResetSuccess: action.payload.status
      }
    }

    case CLEAR_RESET_PASSWORD_MODAL: {
      return {
        ...state,
        resetPasswordErrorCode: null,
        isPasswordResetSuccess: false
      }
    }

    case PASSWORD_UPDATE_SUCCESS: {
      return {
        ...state,
        isPasswordUpdateSuccess: action.payload.status
      }
    }

    case SET_HAS_ACCOUNT: {
      return {
        ...state,
        hasAccount: action.payload.status
      }
    }

    case SET_IS_GUEST: {
      return {
        ...state,
        isGuest: action.payload.status
      }
    }

    case SET_PASSWORD_REST_USER: {
      return {
        ...state,
        passwordResetUser: action.payload
      }
    }

    case SET_CARD_DELETE_PENDING: {
      return {
        ...state,
        cardDeletedPending: action.payload.status
      }
    }

    case PWD_RESET_TOKEN_ERROR: {
      return {
        ...state,
        isPwdResetTokenError: action.payload.status,
        pwdResetTokenErrorMessage: action.payload.message ? action.payload.message : null,
      }
    }

    case SET_LOGIN_MODAL_ERROR_CODE: {
      return {
        ...state,
        loginModalErrorCode: action.payload.status
      }
    }

    case SET_LOGIN_SECTION_ERROR_CODE: {
      return {
        ...state,
        loginSectionErrorCode: action.payload.status
      }
    }

    case CLEAR_LOGIN_ERROR: {
      return {
        ...state,
        loginSectionErrorCode: null,
        loginModalErrorCode: null
      }
    }

    case SET_LOGGED_IN_USER: {
      return {
        ...state,
        loggedInUser: action.payload.user,
        currentUser: action.payload.user
      }
    }

    case SET_LOGGED_IN_USER_ONLY: {
      return {
        ...state,
        loggedInUser: action.payload
      }
    }

    case SET_CURRENT_USER_ONLY: {
      return {
        ...state,
        currentUser: action.payload
      }
    }

    case SHOW_CARD_REMOVE_CONFIRMATION_MODAL: {
      return {
        ...state,
        removingCard: action.payload.card,
        cardRemoveConfirmationModalShown: true,
      }
    }

    case HIDE_CARD_REMOVE_CONFIRMATION_MODAL: {
      return {
        ...state,
        removingCard: null,
        cardRemoveConfirmationModalShown: false,
      }
    }

    case SWITCH_PAYMENT_INFO_ENTERING_FORM: {
      return {
        ...state,
        paymentInfoEnteringFormShown: action.payload.status,
      }
    }

    case UPDATE_USER_DETAIL_PANEL_STATUS: {
      return {
        ...state,
        isDrawerOpen: action.payload.status,
      }
    }


    case SET_PAYMENT_INFO: {
      const {field, value} = action.payload;
      let currentUser = {
        ...state.currentUser
      };
      if (currentUser.creditCards === undefined || currentUser.creditCards === null) {
        currentUser['creditCards'] = [];
        let paymentCard = {};
        paymentCard[field] = value;
        currentUser['creditCards'].push(paymentCard);
      } else {
        currentUser['creditCards'][0][field] = value;
      }
      return {
        ...state,
        currentUser
      };
    }

    case CHANGE_SELECTED_CARD_IDENTIFIER: {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          cardDropDownOption: action.payload.cardIdentifier
        }
      }
    }

    case CHANGE_SAVE_CARD_STATUS: {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          saveCurrentCard: action.payload.saveCardStatus
        }
      }
    }

    case SET_STATE: {
      const selectedState = action.payload;
      return {...state, selectedState};
    }

    case CLEAR_SIGN_UP_STATE: {
      return {
        ...state,
        isSignUpRequested: false,
        isSignUpCompleted: false
      }
    }

    case CLEAR_GUEST_PAYMENT_INFO: {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          creditCards: null
        }
      }
    }

    default:
      return state;
  }
}

export function addContact(values) {
  const payload = pick(values, [
    'firstName',
    'lastName',
    'email',
    'phone',
    'address1',
    'address2',
    'city',
    'state',
    'zipCode',
    'textMessageConfirmation'
  ]);

  return {type: ADD_CONTACT, payload};
}

export function updateContact(key, value) {
  return {type: UPDATE_CONTACT, payload: {key, value}};
}

export function showLoginModal() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_LOGIN_MODAL, payload: {}});
  }
}

export function hideLoginModal() {
  return (dispatch, getState) => {
    dispatch({type: HIDE_LOGIN_MODAL, payload: {}});
    dispatch({type: CLEAR_LOGIN_ERROR, payload: {}});
  }
}

export function showLogoutModal() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_LOGOUT_MODAL, payload: {}});
  }
}

export function hideLogoutModal() {
  return (dispatch, getState) => {
    dispatch({type: HIDE_LOGOUT_MODAL, payload: {}});
  }
}


export function showForgotPasswordModal() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_FORGOT_PASSWORD_MODAL, payload: {status: true}});
  }
}

export function hideForgotPasswordModal() {
  return (dispatch, getState) => {
    dispatch({type: HIDE_FORGOT_PASSWORD_MODAL, payload: {}});
  }
}

export function showResetPasswordModal() {
  return (dispatch, getState) => {
    dispatch({type: SHOW_RESET_PASSWORD_MODAL, payload: {status: true}});
  }
}

export function hideResetPasswordModal() {
  return (dispatch, getState) => {
    dispatch({type: HIDE_RESET_PASSWORD_MODAL, payload: {}});
  }
}

export function setSelectedState(selectedState) {
  return {type: SET_STATE, payload: selectedState};
}

export function setSelectedStateToStore(selectedState) {
  return (dispatch, getState) => {
    dispatch(setSelectedState(selectedState));
  }
}

export function authenticateUser(email, password, loginModal) {

  return async (dispatch, getState) => {
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    let temp;

    try {
      temp = await authApi.authenticateUser(email, password);
    } catch (error) {
      temp = new APIError(500, error.message);
    }


    if (temp instanceof APIError) {
      if (loginModal) {
        dispatch({type: SET_LOGIN_MODAL_ERROR_CODE, payload: {status: temp.code}});
      } else {
        dispatch({type: SET_LOGIN_SECTION_ERROR_CODE, payload: {status: temp.code}});
      }
    } else {
      let userForStore = null;
      if (temp.contacts) {
        userForStore = mapGivenUserToStore(temp);
        dispatch(setSelectedState(userForStore.contacts[0].state || null));
      } else {
        userForStore = temp;
      }
      dispatch({type: AUTHENTICATED_USER, payload: userForStore});
      dispatch({type: HIDE_LOGIN_MODAL, payload: {}});
      dispatch({type: SET_IS_GUEST, payload: {status: false}});
      dispatch({type: CLEAR_LOGIN_ERROR, payload: userForStore});
      dispatch({type: SET_HAS_ACCOUNT, payload: {status: true}});
    }
    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
  }
}

export function signUpUser(user) {
  return async (dispatch, getState) => {
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    dispatch({type: REQUEST_SIGN_UP_USER, payload: {status: true}});
    let temp;
    const contact = user.contacts[0];
    let tempUser = {
      "contacts": {
        "contact": [
          prepareContactForUserBackend(contact),
        ]
      },
      "email": user.email,
      "firstName": user.firstName,
      "lastName": user.lastName,
      "password": user.password,
      "sendMail": true
    };

    try {
      temp = await authApi.signUp(tempUser);
    } catch (err) {
      temp = new APIError(500, 'Unexpected Error');
    }

    if (temp instanceof APIError) {
      dispatch(addNotification({type: 'error', message: UNEXPECTED_ERROR_MESSAGE, disableActions: false}));
      dispatch({type: SET_SIGN_UP_ERROR_CODE, payload: {status: temp.message}});
    } else {

      let receivedUser;

      try {
        receivedUser = await authApi.authenticateUser(temp.email, user.password);
      } catch (error) {
        receivedUser = new APIError(500, error.message);
      }

      if (receivedUser instanceof APIError) {
        dispatch({type: SET_LOGIN_SECTION_ERROR_CODE, payload: {status: receivedUser.code}});
      } else {

        let userForStore = cloneDeep(receivedUser);
        if (receivedUser.contacts) {
          userForStore = mapGivenUserToStore(receivedUser);
          dispatch(setSelectedState(userForStore.contacts[0].state || null));
        } else {
          userForStore = receivedUser;
        }
        dispatch({type: AUTHENTICATED_USER, payload: userForStore});
        dispatch({type: HIDE_LOGIN_MODAL, payload: {}});
        dispatch({type: SET_IS_GUEST, payload: {status: false}});
        dispatch({type: CLEAR_LOGIN_ERROR, payload: userForStore});
        dispatch({type: SET_HAS_ACCOUNT, payload: {status: true}});
      }
      dispatch({type: COMPLETED_SIGN_UP_USER, payload: {status: true}});
    }
    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
  }

}

export function logoutUser(disableNotifications) {
  return async (dispatch, getState) => {
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    try {
      const result = await authApi.logoutUser();
      if (result) {
        dispatch({type: LOGOUT_USER, payload: {}});
      }
    } catch (error) {
      if (!disableNotifications) {
        dispatch(addNotification({
          type: 'error',
          message: 'Something went wrong. Please retry.',
          disableActions: false
        }));
      }
    }
    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
  }
}

export function sendResetPasswordEmail(email, accountId) {
  return async (dispatch, getState) => {
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    if (getState().user.resetPasswordEmailErrorCode !== null) {
      dispatch({type: SET_PASSWORD_RESET_EMAIL_ERROR_CODE, payload: {status: null}});
    }
    let temp;
    try {
      temp = await consumerApi.sendResetPasswordEmail(email, accountId);
    } catch (error) {
      temp = new APIError(500, error.message);
    }

    if (temp instanceof APIError) {
      let responseBody = temp;
      if (responseBody.code === 404) {
        dispatch({type: SET_PASSWORD_RESET_EMAIL_ERROR_CODE, payload: {status: 404}});
      } else {
        dispatch({type: SET_PASSWORD_RESET_EMAIL_ERROR_CODE, payload: {status: 500}});
      }
    } else {
      dispatch({type: SET_PASSWORD_RESET_EMAIL_ERROR_CODE, payload: {status: null}});
      dispatch(hideForgotPasswordModal());
      dispatch(addNotification({
        type: 'success',
        message: `We sent an email to ${email} with instructions on how to reset your password.`,
        disableActions: false
      }));
    }
    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
  }
}

export function getPasswordResetUser(token) {
  return async (dispatch, getState) => {

    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    let response;
    try {
      response = await authApi.checkPasswordResetToken(token);
    } catch (error) {
      response = new APIError(500, error.message);
    }

    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
    if (!response || response instanceof APIError) {
      dispatch(addNotification({type: 'error', message: UNEXPECTED_ERROR_MESSAGE, disableActions: false}));
      //TODO clear input field
    } else {
      dispatch({type: PWD_RESET_TOKEN_ERROR, payload: {status: !response.isValid, message: response.message}});
      if (response.isValid) {
        dispatch({type: SET_PASSWORD_REST_USER, payload: {...response.data, token}});
        dispatch(showResetPasswordModal());
      } else {
        dispatch(addNotification({
          type: 'error',
          message: PWD_RESET_TOKEN_EXPIRED_MSG,
          disableActions: false,
          addToTop: true
        }));
      }
    }
  }
}

export function checkUser(email) {
  return async (dispatch, getState) => {
    if (getState().user.loginSectionErrorCode) {
      dispatch({type: SET_LOGIN_SECTION_ERROR_CODE, payload: {status: false}});
    }
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    let temp;
    try {
      temp = await consumerApi.checkUser(email);
    } catch (error) {
      temp = new APIError(500, error.message);
    }

    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
    if (!temp || temp instanceof APIError) {
      dispatch(addNotification({type: 'error', message: UNEXPECTED_ERROR_MESSAGE, disableActions: false}));
      //TODO clear input field
    } else if (temp.isAvailable) {
      dispatch({type: SET_HAS_ACCOUNT, payload: {status: true}});
      dispatch({type: SET_IS_GUEST, payload: {status: false}});
    } else {
      dispatch({type: SET_HAS_ACCOUNT, payload: {status: false}});
      continueAsGuest();
    }
  }
}

export function continueAsGuest() {
  return (dispatch, getState) => {
    dispatch({type: SET_IS_GUEST, payload: {status: true}});
  }
}

export function validateAndUpdateUser(currentUser) {
  return async (dispatch, getState) => {

    let keySet = ['firstName', 'lastName', 'email', 'phone'];
    let loggedInUser = cloneDeep(getState().user.loggedInUser);
    let isCardToBeSaved = currentUser.saveCurrentCard;
    let userUpdated = false;
    let cardUpdated = false;
    let addressUpdatedUser = null;
    let basicInfoUpdatedUser = null;
    let response;

    if (currentUser && loggedInUser) {
      addressUpdatedUser = updatedUserAddress(currentUser, loggedInUser);
      basicInfoUpdatedUser = updatedBasicUserInfo(currentUser, addressUpdatedUser ? addressUpdatedUser.user : loggedInUser, keySet);
      userUpdated = addressUpdatedUser && basicInfoUpdatedUser && (addressUpdatedUser.isUpdated || basicInfoUpdatedUser.isUpdated);

      if (isCardToBeSaved) {
        loggedInUser = mapPaymentTypesForUpdateRequest(currentUser, loggedInUser);
        cardUpdated = true;
      }

      if (userUpdated || cardUpdated) {
        let mappedUser = mapUserPayloadForUpdateRequest(loggedInUser);
        try {
          response = await consumerApi.updateUser(mappedUser);
        } catch (error) {
          response = new APIError(500, error.message);
        }
        if (response.isUpdated) {
          dispatch({type: AUTHENTICATED_USER, payload: mapGivenUserToStore(response.user ? response.user : null)});
        }
      }
    }
  }
}


export function clearGuestState() {
  return (dispatch, getState) => {
    //TODO validate the response
    dispatch({type: SET_IS_GUEST, payload: {status: false}});
  }
}


export function addPaymentInfo({cardNumber, nameOnCard, expiration, securityCode}) {
  return {type: ADD_PAYMENT_INFO, payload: {cardNumber, nameOnCard, expiration, securityCode}};
}

export function setPhoneNumberValid(status) {
  return {type: SET_PHONE_NUMBER_VALID, payload: {status}};
}

export function showCardRemoveConfirmationModal(card) {
  return (dispath, getState) => {
    trackEvent(SCENARIOS.CHECKOUT_SCREEN, EVENTS[SCENARIOS.CHECKOUT_SCREEN].REGISTERED_CLICK_ON_REMOVE_CARD, getState().app.restaurantInfo, 1);
    dispath({type: SHOW_CARD_REMOVE_CONFIRMATION_MODAL, payload: {card}})
  };
}

export function hideCardRemoveConfirmationModal() {
  return (dispatch) => {
    dispatch({type: HIDE_CARD_REMOVE_CONFIRMATION_MODAL, payload: {}})
  }
}

export function confirmCardRemoval() {
  return async (dispatch, getState) => {
    dispatch({type: SET_CARD_DELETE_PENDING, payload: {status: true}});
    let tempLoggedInUser = cloneDeep(getState().user.loggedInUser);
    let tempCurrentUser = cloneDeep(getState().user.currentUser);
    let tempCreditCards = tempLoggedInUser.paymentTypes.credit_card;
    let index = tempCreditCards.findIndex(card => card.id === getState().user.removingCard.id);
    tempCreditCards.splice(index, 1);
    tempCurrentUser.paymentTypes.credit_card = tempCreditCards;

    const result = await consumerApi.updateUser(mapUserPayloadForUpdateRequest(tempLoggedInUser));

    dispatch({type: HIDE_CARD_REMOVE_CONFIRMATION_MODAL, payload: {}});
    if (!(result instanceof APIError)) {
      dispatch({type: SET_LOGGED_IN_USER_ONLY, payload: tempLoggedInUser});
      dispatch({type: SET_CURRENT_USER_ONLY, payload: tempCurrentUser});
      if (tempLoggedInUser && tempLoggedInUser.paymentTypes && tempLoggedInUser.paymentTypes.credit_card.length > 0) {
        const card = tempLoggedInUser.paymentTypes.credit_card[0];
        dispatch({
          type: CHANGE_SELECTED_CARD_IDENTIFIER, payload: {
            cardIdentifier: {
              value: card.id,
              label: card.number,
              card: card
            }
          }
        });
      } else {
        dispatch({type: CHANGE_SELECTED_CARD_IDENTIFIER, payload: {cardIdentifier: null}});
        dispatch({type: SWITCH_PAYMENT_INFO_ENTERING_FORM, payload: {status: true}})
      }
    } else {
      dispatch(addNotification({
        type: 'error',
        message: UNEXPECTED_ERROR_MESSAGE,
        disableActions: false
      }));
    }
    dispatch({type: SET_CARD_DELETE_PENDING, payload: {status: false}});
  }
}

export function switchPaymentInfoEnteringForm(status) {
  return (dispatch) => {
    dispatch({type: SWITCH_PAYMENT_INFO_ENTERING_FORM, payload: {status: status}})
  }
}

export function clearResetPasswordStatus() {
  return (dispatch) => {
    dispatch({type: CLEAR_RESET_PASSWORD_MODAL, payload: {}})
  }
}

export function updateUserDetailsPanelStatus(status) {
  return (dispatch) => {
    dispatch({type: UPDATE_USER_DETAIL_PANEL_STATUS, payload: {status: status}})
  }
}


export function updatePassword(password) {
  return async (dispatch, getState) => {
    let resetPwdUser = getState().user.passwordResetUser;
    const userEmail = resetPwdUser.email;
    const token = resetPwdUser.token;
    let response;
    dispatch({type: SET_FLOW_WAITING, payload: {status: true}});
    clearResetPwdErrorState();

    try {
      response = await consumerApi.updatePassword(userEmail, password, token);
    } catch (error) {
      response = new APIError(500, error.message);
    }

    dispatch({type: SET_FLOW_WAITING, payload: {status: false}});
    if (!response || response instanceof APIError) {
      dispatch({type: SET_PASSWORD_RESET_ERROR_CODE, payload: {status: response.code, message: response.message}});
    }
    else if (response.statusCode === 204) {
      dispatch({type: HIDE_RESET_PASSWORD_MODAL, payload: {}});
      dispatch({type: SET_PASSWORD_REST_USER, payload: null});
      clearResetPwdErrorState();
      dispatch(addNotification({
        type: 'success',
        message: 'You have successfully changed your password',
        disableActions: false
      }));
    } else {
      dispatch({
        type: SET_PASSWORD_RESET_ERROR_CODE,
        payload: {status: response.statusCode ? response.statusCode : 500, message: response.message}
      });
    }
    window.history.replaceState(null, null, window.location.pathname);
  }
}

export function setGuestPaymentInfo(field, value) {
  return (dispatch) => {
    dispatch({type: SET_PAYMENT_INFO, payload: {field: field, value: value}});
  }
}

export function changeSelectedCardIdentifier(cardIdentifier) {
  return (dispatch) => {
    dispatch({type: CHANGE_SELECTED_CARD_IDENTIFIER, payload: {cardIdentifier}});
  }
}

export function changeSaveCardStatus(saveCardStatus) {
  return (dispatch) => {
    dispatch({type: CHANGE_SAVE_CARD_STATUS, payload: {saveCardStatus}});
  }
}

export function clearResetPwdErrorState() {
  return (dispatch) => {
    dispatch({type: SET_PASSWORD_RESET_ERROR_CODE, payload: {status: null, message: null}});
  }
}

export function clearSignUpState() {
  return (dispatch) => {
    dispatch({type: CLEAR_SIGN_UP_STATE, payload: {}});
  }
}

export function clearGuestPaymentInfo() {
  return (dispatch) => {
    dispatch({type: CLEAR_GUEST_PAYMENT_INFO, payload: {}});
  }
}
