import config from 'config';
import WalletApi from "../lib/WalletApi";
import APIError from "../lib/APIError";

const SET_WALLET_DATA = 'set_wallet_data';
const SET_WALLET_REQUEST_PENDING = 'set_wallet_request_pending';
const SET_WALLET_ERROR = 'set_wallet_error';


const initialState = {
  isWalletRequestPending: false,
  walletData: null,
  walletError: null
};


const walletApi = new WalletApi(config, true);

export default function walletReducer(state = initialState, action = {}) {
  switch (action.type) {

    case SET_WALLET_DATA: {
      return {
        ...state,
        walletData: action.payload
      };
    }

    case SET_WALLET_REQUEST_PENDING: {
      return {
        ...state,
        isWalletRequestPending: action.payload.status
      };
    }

    case SET_WALLET_ERROR: {
      return {
        ...state,
        walletError: action.payload
      };
    }

    default:
      return state;
  }
}

export function getWalletDetails(cardNo, pin, captchaValue) {
  return async (dispatch, getState) => {
    dispatch({type: SET_WALLET_REQUEST_PENDING, payload: {status: true}});
    dispatch({type: SET_WALLET_ERROR, payload: null});
    try {
      const result = await walletApi.getWalletData(cardNo, pin, captchaValue);
      if (result instanceof APIError) {
        let errorMessage = 'Error while loading wallet data. Please try again later.';
        const apiResponse = result.data;
        if (apiResponse && apiResponse.data && apiResponse.data.code) {
          const code = apiResponse.data.code;
          if (code === '10500' || code === '60000') {
            errorMessage = 'Sorry, that card number or PIN isn\'t valid. Please try again.';
          } else if (code === "60300" || code === "60020") {
            errorMessage = 'Sorry, card is not in active state.';
          }
        }
        dispatch({type: SET_WALLET_ERROR, payload: errorMessage});
      } else if (result) {
        dispatch({type: SET_WALLET_DATA, payload: result});
      }
    } catch (error) {
      dispatch({type: SET_WALLET_ERROR, payload: 'Error while loading wallet data. Please try again later.'});
    }
    dispatch({type: SET_WALLET_REQUEST_PENDING, payload: {status: false}});
  }
}

export function clearWalletError() {
  return (dispatch, getState) => {
    dispatch({type: SET_WALLET_ERROR, payload: null});
  }
}

