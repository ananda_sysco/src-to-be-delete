import moment from 'moment-timezone';
import ConsumerApi from 'lib/ConsumerApi';
import MenuApi from 'lib/MenuApi';
import AuthApi from 'lib/AuthApi';
import config from 'config';
import {cloneDeep, isEqual, max, min, uniqWith} from 'lodash';
import {normalizeCategories, normalizeRestaurant} from './utils/schema';
import {formatPrice} from 'utils/toolbox';
import {getEndTimeMoment, getStartTimeMoment, withAvailability} from '../utils/sessionUtil';
import * as constants from 'lib/constants';
import {setSelectedState} from "./user";
import {mapGivenUserToStore} from "../managers/userManager";
import {EVENTS, SCENARIOS, trackEvent} from "../components/analytics/analyticalHandler";
import {validatePrepDeliverTime} from 'store/utils/selectors';
import { isMobile } from '../utils/commonUtil';

const DONATION_INFO = {
  fixedDonationValues: [1, 5, 10, 20, 50],
  donationOverlayMainText: 'Your gift will help our restaurant to weather this difficult time. ' +
    'We thank you for supporting us. Hoping we can see you in person soon!',
  supportUsText: 'COVID-19 has greatly changed the way our restaurant is able to do business and serve the community. ' +
    'Add an additional gift of support to help us continue to do what we do best.',
  donationName: 'Gift to Restaurant',
  minDonationAmount: 1,
  maxDonationAmount: 50
};

// action constants
const FETCH_CATALOG = 'fetch_catalog';
const GET_CATALOG = 'get_catalog';
const GET_RESTAURANT = 'get_restaurant';
const SELECT_SESSION = 'select_session';
const ADD_NOTIFICATION = 'add_notification';
const DISMISS_NOTIFICATION = 'dismiss_notification';
const CLEAR_NOTIFICATIONS = 'clear_notifications';
const DISABLE_CHECKOUT = 'disable_checkout';
const SET_LOGGED_IN_USER = 'set_logged_in_user';
const SET_MIN_MAX_MESSAGES_IN_ATTRIBUTES = 'set_min_max_messages_in_attributes';
const RESET_STORE = 'reset_store';
const SET_SESSION_VALIDITY_BY_PREP_AND_ORDER_TYPE = 'set_session_validity_by_prep_and_order_type';

// helpers
const initialState = {
  attributes: {},
  attributeSets: {},
  attributeValues: {},
  attributeSetMinMaxMessages: {},
  categories: {},
  currentSession: null, // id of valid session if applicable
  currentDay: null, // index of current day (1-7)
  notifications: [],
  products: {},
  restaurantInfo: null,
  restaurantError: null,
  selectedSession: null, // id
  sessions: {},
  sessionCategories: [],
  catalogIsLoading: false,
  disableCheckout: false,
  isSessionValidForOrdering: true,
  isMobile: isMobile()
};

const DATE_TIME_FORMAT = 'HH:mm:ss';
const MAP_HEIGHT = 164;
const PARAM_MOBILE_MAP_SIZE = 'mobile-map-size';

const consumerApi = new ConsumerApi(config, true);
const menuApi = new MenuApi(config, true);
const authApi = new AuthApi(config, true);

function findAnySession(timezone, sessions) {
  if (sessions) {
    for (let s in sessions) {
      if (sessions.hasOwnProperty(s)) {
        return s;
      }
    }
  }
  return null;
}


/**
 * If there is no active session during the current time, but there is still a session that hasn’t started in the day (e.g. the time before Breakfast starts or the time between the Lunch and Dinner sessions), we need to default to the next session that will start soon
 * @param dateTime
 * @param sessions
 * @returns {*}
 */
function findCurrentNearestSession(dateTime, sessions = {}) {
  let diff = 24 * 3600 * 1000; //millies per day
  const day = dateTime.day() + 1;
  let currentSession = null;
  const momentTime = moment(dateTime.format(DATE_TIME_FORMAT), DATE_TIME_FORMAT);

  for (let s in sessions) {
    if (sessions.hasOwnProperty(s)) {
      let sessionSlot = sessions[s].slots.find(el => {
        return el.day === String(day);
      });

      if (sessionSlot && sessionSlot.isavailable) {
        let startTimeMoment = moment(sessionSlot.start, DATE_TIME_FORMAT);
        if (momentTime.isBefore(startTimeMoment)) {
          let temp = startTimeMoment.diff(momentTime);
          if (temp < diff) {
            currentSession = s;
            diff = temp
          }
        }
      }
    }
  }
  return currentSession;
}

function findCurrentSession(sessions, timezone) {
  if (!sessions) {
    sessions = {};
  }
  const dateTime = moment().tz(timezone);
  const momentTime = moment(dateTime.format(DATE_TIME_FORMAT), DATE_TIME_FORMAT);
  const day = dateTime.day() + 1;
  let currentSession = null;

  if (sessions) {
    for (let s in sessions) {
      // find matching day
      if (sessions.hasOwnProperty(s)) {
        let sessionSlot = sessions[s].slots.find(el => {
          return el.day === String(day);
        });

        if (sessionSlot && sessionSlot.isavailable) {

          let startTimeMoment = getStartTimeMoment(sessionSlot.start);
          let endTimeMoment = getEndTimeMoment(sessionSlot.end);

          // assign current session if current time is within session slot
          if (momentTime.isSameOrAfter(startTimeMoment) && momentTime.isSameOrBefore(endTimeMoment)) {
            currentSession = s;
          }
        }

        if (currentSession) {
          break;
        }
      }
    }
  }

  if (!currentSession) {
    currentSession = findCurrentNearestSession(dateTime, sessions)
  }

  if (!currentSession) {
    currentSession = findAnySession(timezone, sessions);
  }

  return currentSession;
}


// reducer
export default function appReducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_RESTAURANT: {
      if (action.error) {
        return {...state, restaurantError: action.errorStatus};
      }

      const {restaurantInfo} = action.payload;
      const restaurant = restaurantInfo.entities.restaurant[restaurantInfo.result];
      const {sessions} = restaurantInfo.entities;

      if (restaurant) {
        let mixpanel = null;
        try {
          mixpanel = require('mixpanel-browser');
        } catch (error) {
          //This check is needed for run tests.
        }
        mixpanel.register({
          "RESTAURANT": restaurant.name,
          "RESTAURANT_ID": restaurant.id
        });

      }

      return {
        ...state,
        restaurantInfo: restaurant,
        restaurantError: null,
        currentDay: moment().tz(restaurant.timeZone).day() + 1,
        sessions: withAvailability(sessions, restaurant.timeZone),
      };
    }

    case FETCH_CATALOG:
      return {
        ...state,
        selectedSession: action.payload.sessionId,
        catalogIsLoading: true,
      };

    case GET_CATALOG: {
      const {catalog} = action.payload;

      return {
        ...state,
        products: {...state.products, ...catalog.entities.products},
        categories: {...state.categories, ...catalog.entities.categories},
        attributeSets: {...state.attributeSets, ...catalog.entities.attributeSet},
        attributeValues: {...state.attributeValues, ...catalog.entities.attributeValues},
        attributes: {...state.attributes, ...catalog.entities.attributes},
        sessionCategories: catalog.result,
        catalogIsLoading: false,
      };
    }

    case RESET_STORE: {
      return cloneDeep(initialState);
    }

    case SELECT_SESSION: {
      const {sessionId} = action.payload;

      return {
        ...state,
        selectedSession: sessionId,
      };
    }

    case DISABLE_CHECKOUT: {
      const {state: value} = action.payload;
      return {
        ...state,
        disableCheckout: value
      }
    }

    case ADD_NOTIFICATION: {
      let notifications = uniqWith([
        action.payload,
        ...state.notifications,
      ], isEqual);

      const sortArray = notifications.map((obj, idx) => {
        obj.idx = idx;
        return obj;
      });

      sortArray.sort((a, b) => {
        if (a.addToTop && !b.addToTop) {
          return -1;
        } else if (!a.addToTop && b.addToTop) {
          return 1;
        } else {
          return a.idx - b.idx;
        }
      });

      return {
        ...state,
        notifications: sortArray,
      };
    }

    case DISMISS_NOTIFICATION: {
      return {
        ...state,
        notifications: state.notifications.slice(1),
      };
    }

    case CLEAR_NOTIFICATIONS: {
      return {
        ...state,
        notifications: []
      }
    }

    case SET_MIN_MAX_MESSAGES_IN_ATTRIBUTES: {
      const {id, name, message} = action.payload;
      return {
        ...state,
        attributeSetMinMaxMessages: {...state.attributeSetMinMaxMessages, [id]: {name, message}}
      };
    }

    case SET_SESSION_VALIDITY_BY_PREP_AND_ORDER_TYPE: {
      const {isSessionValidForOrdering} = action.payload;
      return {
        ...state,
        isSessionValidForOrdering: isSessionValidForOrdering
      };
    }

    default:
      return state;
  }
}

// action creators
export function selectSession(sessionId) {
  return async (dispatch, getState) => {
    try {

      if (getState().app.notifications && getState().app.notifications.length > 0 && getState().app.notifications[0].message === constants.SESSION_NOT_AVAILABLE) {
          dispatch(dismissNotification());
      }

      dispatch({type: SELECT_SESSION, payload: {sessionId}});

      const restaurant = getState().app.restaurantInfo;
      trackEvent(SCENARIOS.MENU_SCREEN, EVENTS[SCENARIOS.MENU_SCREEN].CLICK_ON_MENU_DROP_DOWN, restaurant, 1);
      await dispatch(getCatalog(restaurant.accountId, sessionId));
    } catch (e) {
      dispatch({type: SELECT_SESSION, payload: e, error: true});
    }
  }
}

export function getCatalog(accountId, sessionId) {
  return async (dispatch, getState) => {
    dispatch({type: FETCH_CATALOG, payload: {sessionId}});

    const catalogJson = await menuApi.getCatalog(accountId, sessionId);
    const catalog = normalizeCategories(catalogJson.categories.category);

    dispatch({type: GET_CATALOG, payload: {sessionId, catalog}});
    dispatch(setSessionValidityByPrepOrderType());
  }
}


export function clearNotifications(accountId, sessionId) {
  return async (dispatch, getState) => {
    dispatch({type: CLEAR_NOTIFICATIONS});
  }
}

export function loadLoggedInUser() {
  return async (dispatch, getState) => {
    try {
      let loggedInUser = await authApi.getLoggedInUser();
      loggedInUser = loggedInUser.data.user;
      let userForStore = null;
      if (loggedInUser.contacts) {
        userForStore = mapGivenUserToStore(loggedInUser);
        dispatch(setSelectedState(userForStore.contacts[0].state || null));
      } else {
        userForStore = loggedInUser;
      }
      dispatch({type: SET_LOGGED_IN_USER, payload: {user: userForStore}});
    } catch (e) {
      //Do nothing here since this a asynchronous operation
    }
  }
}

function getRestaurantMapQueryParams() {
  const containerHeight = window.innerHeight || document.body.clientHeight;
  const containerWidth = window.innerWidth || document.body.clientWidth;
  const MAP_WIDTH = containerWidth > containerHeight ? containerWidth : containerHeight;
  return [{ paramName: PARAM_MOBILE_MAP_SIZE, paramValue: `${MAP_WIDTH}x${MAP_HEIGHT}` }];
}

export function getRestaurant(accountId, withCurrentSession=true) {
  return async (dispatch, getState) => {

    if (getState().app.restaurantInfo) {
      if (getState().app.restaurantInfo.accountId === accountId) {
        return;
      } else {
        dispatch({type: RESET_STORE});
      }
    }

    try {
      const restaurantJson = await consumerApi.getRestaurantInfo(accountId, getRestaurantMapQueryParams());
      restaurantJson.accountId = accountId;
      restaurantJson.donationInfo = DONATION_INFO;
      const restaurantInfo = normalizeRestaurant(restaurantJson);
      dispatch({type: GET_RESTAURANT, payload: {restaurantInfo}});

      // fetch default session catalog
      if (withCurrentSession) {
        const restaurantId = restaurantJson['id'];
        const restaurant = restaurantInfo.entities.restaurant[restaurantId];
        const defaultSessionId = findCurrentSession(restaurantInfo.entities.sessions, restaurant.timeZone);

        if (defaultSessionId) {
          dispatch(getCatalog(accountId, defaultSessionId));
        }
      }

    } catch (e) {
      dispatch({type: GET_RESTAURANT, payload: e, error: true, errorStatus: e.status});
    }

  };
}

/**
 * Returns basic set of information about a restaurant
 * @param restaurantId
 * @returns {function(*, *)}
 */
export function getRestaurantBasic(restaurantId) {
  return async (dispatch, getState) => {
    try {
      return consumerApi.getRestaurantInfo(restaurantId);
    } catch (e) {
      return Promise.reject(e);
    }
  }
}

export function addNotification(notification) {
  return {type: ADD_NOTIFICATION, payload: notification};
}

export function dismissNotification() {
  return {type: DISMISS_NOTIFICATION};
}

export function resetStore() {
  return {type: RESET_STORE};
}

export function disableCheckout(state = true) {
  return {type: DISABLE_CHECKOUT, payload: {state}};
}

export function formatPriceRange(subCategory, products) {
  const prices = subCategory.products.map(productId => products[productId].price);

  const minPrice = min(prices);
  const maxPrice = max(prices);

  if (!prices.length) {
    return '';
  }

  if (!minPrice || !maxPrice || (minPrice === maxPrice)) {
    return formatPrice(minPrice || maxPrice);
  }


  return `${formatPrice(minPrice)} - ${formatPrice(maxPrice)}`;

}

export function setAttributeMessages(id, name, message) {
  return {type: SET_MIN_MAX_MESSAGES_IN_ATTRIBUTES, payload: {id, name, message}};
}

export function setSessionValidityByPrepOrderType() {
  return async (dispatch, getState) => {
    const {restaurantInfo, selectedSession, sessions} = getState().app;
    const {orderType} = getState().cart;
    const validationObj = validatePrepDeliverTime(restaurantInfo, selectedSession, sessions, orderType, null);
    const isSessionValidForOrdering = validationObj && !validationObj.isTimeOver;
    dispatch({type: SET_SESSION_VALIDITY_BY_PREP_AND_ORDER_TYPE, payload: {isSessionValidForOrdering}});
  }
}
