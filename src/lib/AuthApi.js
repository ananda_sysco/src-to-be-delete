import {executeGetApiCall, executePostApiCall} from './apiUtil';
import APIError from './APIError';

export default class AuthApi {

  constructor(config) {
    this.esbUrl = config.backendUrl;
  }

  async authenticateUser(username, password) {
    const url = `${this.esbUrl}/auth/login`;
    const response = await executePostApiCall(url, {username, password});

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Login Error');
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }
    let resultJson = await response.json();
    return resultJson.data;
  }

  async logoutUser() {
    const url = `${this.esbUrl}/auth/logout`;
    const response = await executeGetApiCall(url);
    if (response && response.ok) {
      return true;
    } else {
      throw new Error("Logout failed");
    }
  }

  async checkPasswordResetToken(token) {
    const url = `${this.esbUrl}/auth/checkPasswordResetToken?token-param=${token}`;
    const response = await executeGetApiCall(url);
    const responseJson = await response.json();

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Password reset token check error', responseJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return responseJson.data;
  }

  async signUp(user) {

    const url = `${this.esbUrl}/auth/signup`;
    const response = await executePostApiCall(url, user);
    let resultJson = await response.json();
    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'User Creation Error', resultJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }
    return resultJson.data;
  }

  async getLoggedInUser() {
    const url = `${this.esbUrl}/auth/getLoggedinUser`;
    const response = await executeGetApiCall(url).catch((error) => {
//Do nothing
    });

    if (!response || !response.ok) {
      let error = new Error('failed to get logged in user');
      if (response && response.status) {
        error.status = response.status;
      } else {
        error.status = 500;
      }
      throw error;
    }

    let resultJson = response.json();
    return resultJson;
  }


}
