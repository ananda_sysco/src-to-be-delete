/**
 * Created by Tharuka Jayalath on 09/06/2018
 */

import {executeGoogleGetApiCall} from './apiUtil';

export default class GoogleApi {

  constructor(config) {
    this.mapsApiUrl = config.mapsApiUrl;
    this.googleApiKey = config.googleApiKey;
  }

  async getDecodedAddress(geoLocation) {

    const url = `${this.mapsApiUrl}/geocode/json?latlng=${geoLocation.lat},${geoLocation.lng}&key=${this.googleApiKey}`;
    const response = await executeGoogleGetApiCall(url);
    let resultJson = response.json();
    let addressInfo = null;
    await resultJson.then((resultObject) => {
      if (resultObject && resultObject.status === 'OK' && resultObject.results.length > 0) {
        addressInfo = resultObject.results[0].formatted_address || null;
      }
    });

    return addressInfo;
  }

}
