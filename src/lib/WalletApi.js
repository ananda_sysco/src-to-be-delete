import APIError from "./APIError";

const {executePostApiCall} = require('./apiUtil');

export default class WalletApi {

  constructor(config) {
    this.esbUrl = config.backendUrl;
    this.applicationBaseUrl = config.applicationBaseUrl;
  }

  async getWalletData(cardNo, pin, captchaValue) {
    const url = `${this.esbUrl}/wallet/getWalletDetails`;
    const data = {
      "CARD_NO": cardNo,
      "PIN": pin,
      "CAPTCHA_VALUE": captchaValue
    };

    const response = await executePostApiCall(url, data);

    let walletInfo = await response.json();

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'getWalletData error', walletInfo);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return walletInfo;
  }
}
