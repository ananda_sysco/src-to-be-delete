
export default {
  style: 'light',

  layout: {
    gutter: {
      xxlarge: 2.5, //40px
      xlarge: 2, // 32px
      large: 1.25, // 20px
      medium: 1, // 16px
      small: 0.625, // 10px
      xsmall: 0.5, // 8px
      xxsmall: 0.25, // 4px
    },
  },

  colors: {
    primary: '#EE7E04', // cake orange
    primaryLight: '#f7b792',
    primaryDark: '#d9650d',
    background: '#FFFFFF', // white
    foreground: '#5F5F5F', // dark gray
    foregroundLight: '#F7F7F7', // light gray
    border: '#ECECEC', // border gray
    borderLight: '#d1ccc7',
    highlight: '#d1ccc7',
    transparent: 'transparent',
    neutral: '#a8a7a4', // medium gray
    success: '#84bd00',
    info: '#00b5e2',
    warning: '#f2a900',
    error: '#de2323',
  },

  text: {
    size: {
      xxlarge: 3,
      xlarge: 2,
      large: 1.25,
      medium: 1,
      small: 0.875,
      xsmall: 0.75,
    },
    font: {
      default: 'BrixSansLight',
    },
  },

  button: {
    radius: {
      rounded: 5,
      block: 0,
    },
    height: {
      medium: 45,
      small: 24,
    },
  },

  form: {
    input: {
      height: {
        large: 45,
        default: 24,
      },
    },
  },
};
