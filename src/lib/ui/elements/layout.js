import styled from 'styled-components';
import {
  Grid as FBGrid,
  Row as FBRow,
  Col as FBCol,
} from 'react-flexbox-grid';

export const Grid = styled(FBGrid)`

`;

export const Col = styled(FBCol)`

`;

export const Row = styled(FBRow)`

`;

export const Container = styled(Grid)`
  background-color: ${({ theme }) => theme.colors.background};
  color: ${({ theme }) => theme.colors.foreground};
  height: 100%;
`;
