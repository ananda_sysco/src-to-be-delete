import { find } from 'lodash';
import React from 'react';
import styled, { css } from 'styled-components';
import { options, themed, when } from '../utils/theme';

export const commonStyles = css`
  color: ${themed({
    default: 'colors.foreground',
    inverted: 'colors.background',
    neutral: 'colors.neutral',
    primary: 'colors.primary',
    info: 'colors.info',
    success: 'colors.success',
    warning: 'colors.warning',
    error: 'colors.error',
  }, 'default')};
  ${when('uppercase', 'text-transform: uppercase')};
  ${when('italic', 'font-style: italic')};
  ${when('centered', 'align-self: center')};
  word-break: break-word;
  white-space: pre-wrap;
  word-wrap: break-word;  
`;

export const familyStyles = css`
  font-family: ${options({
    bold: 'BrixSansMedium',
    light: 'BrixSansLight',
  }, 'light')};
`;

export const sizeStyles = css`
  font-size: ${themed({
    xlarge: 'text.size.xlarge',
    large: 'text.size.large',
    medium: 'text.size.medium',
    small: 'text.size.small',
    xsmall: 'text.size.xsmall',
  }, 'medium')}rem;
`;

export const Label = styled.span`
  display: flex;
  ${when('full', 'flex: 1')};
  ${familyStyles}
  ${sizeStyles}
  ${commonStyles}
`;

export const FormLabel = styled.label`
  ${familyStyles}
  ${sizeStyles}
  ${commonStyles}
`;

export const Paragraph = styled.p`
  margin-bottom: .5rem;
  line-height: 1.4rem;
  ${familyStyles}
  ${sizeStyles}
  ${commonStyles}
`;

const H1 = styled.h1`
  font-family: DuplicateSlabThin;
  font-size: 3.5rem;
  margin-bottom: 1rem;
  ${commonStyles}
`;

const H2 = styled.h2`
  font-family: DuplicateSlabThin;
  font-size: 3rem;
  margin-bottom: 1rem;
  ${commonStyles}
`;

const H3 = styled.h3`
  font-family: DuplicateSlabThin;
  font-size: 2.5rem;
  margin-top: .5rem;
  margin-bottom: 1rem;
  ${commonStyles}
`;

const H4 = styled.h4`
  font-family: BrixSansMedium;
  font-size: 2rem;
  margin-bottom: .5rem;
  ${commonStyles}
`;

const H5 = styled.h5`
  font-family: BrixSansMedium;
  font-size: 1.5rem;
  margin-bottom: .5rem;
  ${commonStyles}
`;

const H6 = styled.h6`
  font-family: BrixSansMedium;
  font-size: 1rem;
  margin-bottom: .5rem;
  ${commonStyles}
`;

const sizes = {
  xxlarge: H1,
  xlarge: H2,
  large: H3,
  small: H4,
  xsmall: H5,
  xxsmall: H6,
};

// Main entry point for element
// Finds the appropriate DOM element for the
// provided size prop (xlarge, large, etc.)
export function Heading(props) {
  const Element = find(sizes, (Elem, size) => (props[size] === true)) || H3;
  return <Element {...props} />;
}
