import {
  findOption,
  findOptions,
  themedFromProps,
  themed,
  options,
  when,
} from '../theme';

describe('chrome/utils', () => {
  const props = {
    theme: {
      colors: {
        primary: 'red',
        secondary: 'blue',
      },
      sizes: {
        large: 30,
        medium: 20,
        small: 10,
      },
    },
  };

  const themeOptions = {
    large: 'sizes.large',
    medium: 'sizes.medium',
    small: 'sizes.small',
  };

  const styleOptions = {
    large: 30,
    medium: 20,
    small: 10,
  };

  it('should find an object value by boolean props', () => {
    const obj = {
      large: 'foo',
      small: 'bar',
    };

    expect(findOption(obj, { large: true })).toEqual('foo');
    expect(findOption(obj, { small: true })).toEqual('bar');
    expect(findOption(obj, { })).toEqual(undefined);
  });

  it('should find multiple object values by boolean props', () => {
    const obj = {
      large: 'foo',
      small: 'bar',
    };

    expect(findOptions(obj, { large: true, small: true })).toMatchObject(['foo', 'bar']);
  });

  it('should get a theme value from props', () => {
    expect(themedFromProps(props, 'colors.primary')).toEqual('red');
    expect(themedFromProps(props, 'colors.secondary')).toEqual('blue');
  });

  it('should throw if theme value not found', () => {
    expect(() => themedFromProps(props, 'badprop')).toThrow();
  });

  it('should get a theme value from themeOptions', () => {
    expect(themedFromProps({ ...props, large: true }, themeOptions)).toEqual(30);
    expect(themedFromProps({ ...props, medium: true }, themeOptions)).toEqual(20);
    expect(themedFromProps({ ...props, small: true }, themeOptions)).toEqual(10);
    expect(themedFromProps(props, themeOptions, 'small')).toEqual(10);
  });

  it('should throw if theme option not found with no default', () => {
    expect(() => themedFromProps(props, themeOptions)).toThrow();
    expect(() => themedFromProps(props, themeOptions, 'badoption')).toThrow();
  });

  it('should create a styled-components function for a single value', () => {
    const func = themed('colors.primary');
    expect(func).toBeInstanceOf(Function);
    expect(func(props)).toEqual('red');
  });

  it('should create a styled-components function for options', () => {
    const func = themed(themeOptions, 'medium');

    expect(func(props)).toEqual(20);
    expect(func({ ...props, large: true })).toEqual(30);
  });

  it('should throw before creating themed function if default option not found', () => {
    expect(() => themed(themeOptions)).toThrow();
    expect(() => themed(themeOptions, 'baddefault')).toThrow();
  });

  it('should create a styled-components function for when()', () => {
    const func = when(themeOptions);
    expect(func({ large: true, medium: true })).toEqual('sizes.large\nsizes.medium');
  });

  it('should pick an option from options()', () => {
    const func = options(styleOptions, 'medium');
    expect(func({ large: true })).toEqual(30);
    expect(func({ medium: true })).toEqual(20);
    expect(func({ small: true })).toEqual(10);
    expect(func({ })).toEqual(20);
  });

  it('should throw before creating options function if default option not found', () => {
    expect(() => options(styleOptions)).toThrow();
    expect(() => options(styleOptions, 'baddefault')).toThrow();
  });
});
