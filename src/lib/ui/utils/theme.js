import { get, find, filter, values, isString, isObject } from 'lodash';

export function findOption(opts, flags) {
  return find(opts, (value, option) => (!!flags[option]));
}

export function findOptions(opts, flags) {
  return values(filter(opts, (value, option) => (!!flags[option])));
}

export function getOption(opts, option) {
  if (!(option in opts)) {
    throw new Error(`Theme option [${option}] not found in opts`);
  }

  return opts[option];
}

export function themedFromProps(props, opts, defaultOption = null) {
  if (isString(opts)) {
    if (!props.theme) {
      throw new Error('Theme not found in props');
    }

    const val = get(props.theme, opts);

    if (val === undefined) {
      throw new Error(`Theme key [${opts}] not found in theme`);
    }

    return val;
  }

  if (isObject(opts)) {
    const key = findOption(opts, props) || getOption(opts, defaultOption);

    if (key) {
      return themedFromProps(props, key);
    }

    throw new Error('No theme key matched and no default provided');
  }

  throw new TypeError('theme() input must be an object or a string');
}

export function themed(input, defaultVal = null) {
  if (isObject(input)) {
    // defaultVal must be an
    // option name that exists in input
    if (!(defaultVal in input)) {
      throw new Error(`Default option [${defaultVal}] not found in opts`);
    }
  }

  return (props) => themedFromProps(props, input, defaultVal);
}

export function options(input, defaultVal = null) {
  if (isObject(input)) {
    // defaultVal must be an
    // option name that exists in input
    if (!(defaultVal in input)) {
      throw new Error(`Default option [${defaultVal}] not found in opts`);
    }

    return (props) => findOption(input, props) || input[defaultVal];
  }

  throw new Error('Options input must be an object');
}

export function when(opts, style = null) {
  if (isString(opts)) {
    return (props) => (props[opts] === true ? style : null);
  }

  if (isObject(opts)) {
    return (props) => {
      const matched = findOptions(opts, props);
      return matched.join('\n');
    };
  }

  throw new TypeError('when() input must be an object or a string');
}
