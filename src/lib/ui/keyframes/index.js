import { keyframes } from 'styled-components';

export function fadeIn() {
  return keyframes`
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
    }
  `;
}

export default {
  fadeIn,
};
