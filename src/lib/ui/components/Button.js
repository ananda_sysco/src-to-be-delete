import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {noop} from 'lodash';
import {options, themed} from '../utils/theme';

const StyledButton = styled.button `
  display: ${options({
  default: 'inline-block !important',
  link: 'flex',
}, 'default')};
  flex-direction: ${options({
  vertical: 'column',
  horizontal: 'row',
}, 'horizontal')};
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-family: BrixSansMedium;
  outline: 0;
  cursor: pointer;
  text-transform: ${options({
  uppercase: 'uppercase',
  link: 'inherit',
}, 'uppercase')};
  font-size: ${options({
  large: '1.5rem',
  medium: '1rem',
  small: '0.75rem',
}, 'medium')};
  @media (max-width: 767px) {
    font-size: ${options({
  large: '1.5rem',
  medium: '1.2rem',
  small: '0.75rem',
}, 'medium')};
      padding: ${options({
  large: '1rem',
  medium: '0.9rem',
  small: '0.5rem',
  trim: '0',
}, 'medium')};
}
  padding: ${options({
  large: '1rem',
  medium: '0.5rem',
  small: '0.5rem',
  trim: '0',
}, 'medium')};
  color: ${themed({
  primary: 'colors.background',
  default: 'colors.background',
  outline: 'colors.foreground',
  link: 'colors.primary',
}, 'default')};
  background-color: ${themed({
  outline: 'colors.transparent',
  primary: 'colors.primary',
  info: 'colors.background',
  warning: 'colors.background',
  error: 'colors.background',
  success: 'colors.background',
  default: 'colors.foreground',
  link: 'colors.transparent',
  transparent: 'colors.transparent',
}, 'default')};
  border-style: ${options({
  outline: 'solid',
  default: 'none',
}, 'default')};
  border-radius: ${options({
  'square': '0',
  'rounded': '3px',
}, 'rounded')};
  border-width: ${options({
  outline: '1px',
  default: '0',
}, 'default')};
  border-color: ${themed({
  primary: 'colors.primary',
  default: 'colors.background',
  error: 'colors.background',
}, 'default')};
  width: ${options({
  full: '100%',
  half: '50%',
  default: 'auto',
  threeQtr: '75%'
}, 'default')};

  &:hover {
    opacity: 0.7;
  }
`;

export default class Button extends Component {

  static propTypes = {
    onPress: PropTypes.func,
    bindData: PropTypes.any,
    children: PropTypes.node,
  };

  static defaultProps = {
    onPress: noop,
    children: [],
  };

  handleClick = (event) => {
    if (this.props.onPress) {
      this.props.onPress(this.props.bindData, event);
    }
  };

  render() {
    return (
      <StyledButton
        {...this.props}
        onClick={this.handleClick}>

        {this.props.children}
      </StyledButton>
    );
  }
}
