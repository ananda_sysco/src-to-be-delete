// See https://github.com/JedWatson/react-select
import React from 'react';
import RToolTip from 'react-tooltip';

export default (props) => <RToolTip class="tooltip" {...props} />;
