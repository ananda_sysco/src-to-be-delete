import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {uniqueId} from 'lodash';
import {FormLabel} from '../elements/text';

const RadioWrapper = styled.span`
  background-color: transparent;
  text-align: left;
`;

const StyledInput = styled.input`
  position: fixed;
  width: 0;
  height: 0;
  opacity: 0;
  margin: 0;
  padding: 0;
`;

export default class RadioButton extends Component {

  static propTypes = {
    checked: PropTypes.bool,
    defaultChecked: PropTypes.bool,
    group: PropTypes.string,
    label: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    group: '',
    checked: false,
  };

  handleChange = () => {
    const { onChange, value } = this.props;

    if (onChange) {
      onChange(value);
    }
  };

  render() {
    const { checked, group, label, value } = this.props;
    const idValue = uniqueId(group);
    return (
      <RadioWrapper className="radioButtonWrapper">
          <StyledInput
            id={idValue}
            type="radio"
            name={group}
            value={value}
            checked={checked}
            onChange={this.handleChange}
            className="radioButtonInput"
          />
        <FormLabel htmlFor={idValue} className="radioButtonLabel" small>
          {label}
        </FormLabel>
      </RadioWrapper>
    );
  }
}
