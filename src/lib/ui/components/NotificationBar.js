import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import { transparentize } from 'polished';
import { MdCheckCircle, MdInfo, MdWarning, MdReport, } from 'react-icons/lib/md';
import { themed } from '../utils/theme';
import { Row, Col } from '../elements/layout';
import { Label, Paragraph } from '../elements/text';
import Button from './Button';

const marginKeyframes = keyframes`
  0% {
    opacity: 0;
    margin-top: -100px;
  }
  100% {
    opacity: 1;
    margin-top: 0;
  }
  `;

const Wrapper = styled(Col)`
  animation: ${marginKeyframes} .3s ease-out;
  padding: ${themed('layout.gutter.large')}rem ${themed('layout.gutter.xlarge')}rem;
  background-color: ${({ theme, type }) => {
    return theme.colors[type]
  }};
`;

const Body = styled(Col)`
  padding: 0 ${themed('layout.gutter.medium')}rem;
`;

const Actions = styled(Row)`
  margin-top: ${themed('layout.gutter.large')}rem;
`;

const ActionButton = styled(Button)`
  margin-right: ${themed('layout.gutter.small')}rem;
  margin-bottom: ${themed('layout.gutter.small')}rem;
`;
const MessageText = styled(Paragraph)`
  line-height: 2rem;
`;

const stylizeIcon = (Type) => styled(Type)`
  font-size: ${themed('text.size.xlarge')}rem;
  color: ${({ theme }) => transparentize(0.3, theme.colors.background)};
`;

const icons = {
  success: stylizeIcon(MdCheckCircle),
  info: stylizeIcon(MdInfo),
  warning: stylizeIcon(MdWarning),
  error: stylizeIcon(MdReport),
};

export default class NotificationBar extends Component {

  static propTypes = {
    type: PropTypes.oneOf([
      'success',
      'info',
      'warning',
      'error',
    ]).isRequired,
    message: PropTypes.node,
    timeout: PropTypes.number,
    children: PropTypes.node,
    onDismiss: PropTypes.func,
  };

  static defaultProps = {
    type: 'info',
  };

  timeoutId = null;

  componentWillMount() {
    const { timeout } = this.props;

    if (timeout) {
      this.timeoutId = setTimeout(() => {
        this.dismiss();
      }, timeout);
    }
  }

  dismiss = () => {
    clearTimeout(this.timeoutId);
    this.timeoutId = null;

    if (this.props.onDismiss) {
      this.props.onDismiss();
    }
  };

  renderAction(action) {
    const { primary, label, handler, data } = action;
    const { type } = this.props;
    const buttonProps = primary ? { [type]: true } : { outline: true };
    const labelProps = primary ? { [type]: true } : { inverted: true };

    return (
      <ActionButton {...buttonProps} onPress={handler} bindData={data} key={label}>
        <Label {...labelProps} small bold uppercase>{label}</Label>
      </ActionButton>
    );
  }

  render() {
    const {type, actions, ...props} = this.props;
    const Icon = icons[type];

    return (
      <Wrapper type={type} {...props}>
        <Row>
          <Col xs={1}>
            <Row center="xs">
              <Icon />
            </Row>
          </Col>
          <Body xs={11}>
            <Row>
              <MessageText inverted bold>{this.props.message}</MessageText>
            </Row>
            {actions &&
            <Actions>
              {actions && actions.map(action => this.renderAction(action))}
            </Actions>
            }
          </Body>
        </Row>
      </Wrapper>
    );
  }
}
