// See https://github.com/JedWatson/react-select
import React from 'react';
import RSelect from 'react-select';
import styled from 'styled-components';
import MdKeyboardArrowDown from 'react-icons/lib/md/keyboard-arrow-down';
import { themed } from '../utils/theme';

const DownIcon = styled(MdKeyboardArrowDown)`
  font-size: ${themed('text.size.large')}rem;
  vertical-align: inherit !important;
`;

const renderArrow = () => <DownIcon />;

export default (props) => (
  <RSelect
    arrowRenderer={renderArrow}
    {...props}
  />
)
