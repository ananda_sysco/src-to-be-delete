import React, {Component} from 'react';
import {omit} from 'lodash';
import PropTypes from 'prop-types';
import styled, {css} from 'styled-components';
import InputMask from 'react-input-mask';
import {themed} from '../utils/theme';
import {Label} from '../elements/text';

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-bottom: ${themed('layout.gutter.medium')}rem;
`;

export const commonStyles = css`
  font-family: BrixSansLight;
  color: ${themed('colors.foreground')};
  display: flex;
  flex: 1;
  height: 24px;
  padding: ${themed('layout.gutter.xsmall')}rem;
  font-size: ${themed('text.size.small')}rem;
  border-radius: 5px;
  border-color: ${themed({
    error: 'colors.error',
    default: 'colors.borderLight',
  }, 'default')};
  border-width: 1px;
  border-style: solid;
  outline: none;
  line-height: 24px;

  &:focus {
    border-color: ${themed({
      error: 'colors.error',
      default: 'colors.primary',
    }, 'default')};
    border-width: 1px;
  }
`;

const CleanInput = (props) => (
  <input {...omit(props, ['error'])} />
);

const StyledInput = styled(CleanInput)`
  ${commonStyles}
`;

const CleanMask = (props) => (
  <InputMask {...omit(props, ['error'])} />
);


const StyledMask = styled(CleanMask)`
  ${commonStyles}
`;

const ErrorLabel = styled(Label)`
  color: ${themed('colors.error')};
  margin-top: ${themed('layout.gutter.xxsmall')}rem;
`;

export default class Input extends Component {

  static propTypes = {
    mask: PropTypes.string,
    error: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.string),
      PropTypes.string,
    ]),
  };

  static defaultProps = {
    mask: null,
    error: null,
  };

  renderInputField() {
    if(this.props.mask) {
      return <StyledMask {...this.props} />;
    }

    return <StyledInput {...this.props} />;
  }

  render() {
    const { error } = this.props;
    const message = Array.isArray(error) ? error.length && error[0] : error;

    return (
      <Wrapper className="textInput">
        {this.renderInputField()}
        {message && <ErrorLabel xsmall>{message}</ErrorLabel>}
      </Wrapper>
    );
  }
}
