import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { noop } from 'lodash';
import { themed } from 'lib/ui';

const StyledTextArea = styled.textarea`
  font-family: "BrixSansLight";
  font-size: 1rem;
  margin-top: ${themed('layout.gutter.small')}rem;
  margin-bottom: ${themed('layout.gutter.small')}rem;
  padding: ${themed('layout.gutter.xsmall')}rem;
  width: 100%;
  box-sizing: border-box;
  outline: none;
  height:  50px;
  resize: none;
  border-radius: 5px;
  border-color: ${themed('colors.borderLight')};
  border-style: solid;

  &::placeholder {
    color: ${themed('colors.neutral')};
    font-style: italic;
  }

  &:focus {
    border-color: ${themed('colors.primary')};
    border-width: 1px;
    background-color: ${themed('colors.background')};
  }
`;

export default class TextArea extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    name: PropTypes.string,
    placeHolder: PropTypes.string,
    maxlength: PropTypes.string,
    value: PropTypes.any
  };

  static defaultProps = {
    onChange: noop,
    name: '',
    placeHolder: '',
    value: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
  }

  onChange = (e) => {
    const {onChange, maxlength} = this.props;
    const value = e.target.value;

    if (maxlength && value && value.length >= parseInt(maxlength, 10) + 1) {
      return;
    }

    this.setState({value});

    if (onChange) {
      onChange(value);
    }
  };

  render() {
    const {placeHolder, name} = this.props;
    return (
      <StyledTextArea
        name={name}
        value={this.state.value}
        placeholder={placeHolder}
        onChange={this.onChange}
        spellCheck={false}
      />
    );
  }
}
