import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {noop, uniqueId} from 'lodash';

const CheckboxWrapper = styled.span`
  background-color: transparent;
  text-align: left;
`;

const Label = styled.label`
  font-family: "BrixSansLight";
  @media (max-width: 767px) {
    font-size: 1.15rem;
}
`;

export default class Checkbox extends Component {
  static propTypes = {
    checked: PropTypes.bool,
    group: PropTypes.string,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onClick: PropTypes.func
  };

  static defaultProps = {
    onChange: noop,
    onClick: noop,
    checked: false,
    group: '',
  };

  onChange = () => {
    const { onChange, value } = this.props;

    if (onChange) {
      onChange(value);
    }
  };

  onClick = (e) => {
    const { onClick } = this.props;

    if (onClick) {
      onClick(e);
    }
  }

  render() {
    const {checked, group, label, value, disabled, onClick} = this.props;
    const idValue = uniqueId(group);
    return (
      <CheckboxWrapper className="checkBoxWrapper">
        <input
          type="checkbox"
          defaultChecked={checked}
          id={idValue}
          name={group}
          value={value}
          onChange={this.onChange}
          onClick={onClick}
          disabled={disabled}
          className="checkBoxInput"
        />
        <Label htmlFor={idValue} className="checkBoxLabel">{label}</Label>
      </CheckboxWrapper>
    );
  }
}
