import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {noop} from 'lodash';
import MdAddCircleOutline from 'react-icons/lib/md/add-circle-outline';
import MdRemoveCircleOutline from 'react-icons/lib/md/remove-circle-outline';
import {Col, Label, Row, themed} from 'lib/ui';

const primaryColor = ({theme}) => theme.colors.primary;

const StyledCounterWrapper = styled(Row)`
  background-color: transparent;
`;

const StyledLabel = styled(Label)`
  padding-top: ${themed('layout.gutter.small')}rem;
  @media (max-width: 767px) {
    padding-top: 0.925rem;
  }
  @media (max-width: 374px) {
    min-width: 2rem;
    justify-content: center;
  }
`;

const Remove = styled(MdRemoveCircleOutline)`
  color: ${primaryColor};
  font-size: ${themed('text.size.large')}rem;
  padding: ${themed('layout.gutter.small')}rem;
  @media (max-width: 767px) {
    font-size:1.875rem;
  }
  &:active {
    fill: #5f5f5f;
  }
`;

const Add = styled(MdAddCircleOutline)`
  color: ${primaryColor};
  font-size: ${themed('text.size.large')}rem;
  padding: ${themed('layout.gutter.small')}rem;
  @media (max-width: 767px) {
    font-size:1.875rem;
  }
  &:active {
    fill: #5f5f5f;
  }
`;

export default class Counter extends Component {

  static propTypes = {
    children: PropTypes.node,
    value: PropTypes.number,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    children: [],
    value: 1,
    onChange: noop,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
  }

  incrementUp = () => {
    const {onChange} = this.props;
    const nextState = this.state.value + 1;

    this.setState({value: nextState});

    if (onChange) {
      onChange(nextState);
    }
  }

  incrementDown = () => {
    const {onChange} = this.props;
    const nextState = this.state.value - 1;

    if (this.state.value > 1) {
      this.setState({value: nextState});
      if (onChange) {
        onChange(nextState);
      }
    }
  }

  render() {
    return (
      <StyledCounterWrapper end="xs">
        <Col>
          <Remove className="minusIcon" onClick={this.incrementDown}/>
        </Col>
        <Col>
          <StyledLabel large>{this.state.value}</StyledLabel>
        </Col>
        <Col>
          <Add className="plusIcon" onClick={this.incrementUp}/>
        </Col>
      </StyledCounterWrapper>
    );
  }
}
