module.exports = {
  US_STATES: ["AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "FM", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH", "MI", "MN", "MO", "MP", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY"],
  loginErrors: {
    '10010': 'Invalid Email or Password',
    '10020': 'Incorrect Password'
  },
  SESSION_NOT_AVAILABLE: 'This menu is currently unavailable',
  ADD_PAYMENT_OPTION: 'Add a new payment option',
  ADD_CARD_LABEL: '+ Add a new card',
  SAVE_THIS_CARD_LABEL: 'Save this card info for future orders.',
  DATE_FORMAT_STRINGS: {
    RECEIPT: 'MMM DD, YYYY @hh:mm A'
  },
  COUNTRY_CURRENCY_MAP: {
    US: '$',
    LK: 'Rs.'
  }
};
