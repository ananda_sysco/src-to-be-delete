import faker from 'faker';
import nock from 'nock';
import 'test_helper';
import config from 'config';
import AuthApi from '../AuthApi';

const authApi = new AuthApi(config);
/*const user = {
  "contacts": {
    "contact": [
      {
        "phone": {
          "areaCode": "378",
          "exchangeCode": "863",
          "subscriberNumber": "7286"
        }
      }
    ]
  },
  "email": "tharukaboss@yahoo.com",
  "firstName": "thar",
  "lastName": "jauya",
  "password": "12345678",
  "sendMail": true
};*/

const tokenParam = faker.random.uuid();


describe('lib/AuthApi',()=>{

  /*it('should create an user',async ()=>{
    nock.cleanAll();
    nock(`${config.backendUrl}`)
      .post(`/auth/signup`)
      .reply(200, {
        "id": "75dc8885-1aa3-4e63-993d-5f2fb9f617d4",
        "idmUserId": "bea2f3a6-ed9f-4ab0-aedb-0808f3f0f8d8",
        "firstName": "thar",
        "lastName": "jauya",
        "email": "tharukaboss@yahoo.com",
        "sendMail": true,
        "contacts": {
          "contact": [
            {
              "id": "ad77650e-9909-49ad-820d-bf0822ffa549",
              "phone": {
                "id": "e1c6c005-5b28-446c-ae79-e187693ebed9",
                "areaCode": "378",
                "exchangeCode": "863",
                "subscriberNumber": "7286"
              },
              "contactType": "PRIMARYPHONE"
            }
          ]
        },
        "paymentTypes": {},
        "firstOrder": true,
        "eReceiptNotification": true,
        "rewardsNotification": true,
        "dateOfBirthFormat": "yyyy-MM-dd"
      });

    const json = await authApi.signUp(user);

    //expect(json).toBeTruthy();
    expect(json).toEqual({
      "id": "75dc8885-1aa3-4e63-993d-5f2fb9f617d4",
      "idmUserId": "bea2f3a6-ed9f-4ab0-aedb-0808f3f0f8d8",
      "firstName": "thar",
      "lastName": "jauya",
      "email": "tharukaboss@yahoo.com",
      "sendMail": true,
      "contacts": {
        "contact": [
          {
            "id": "ad77650e-9909-49ad-820d-bf0822ffa549",
            "phone": {
              "id": "e1c6c005-5b28-446c-ae79-e187693ebed9",
              "areaCode": "378",
              "exchangeCode": "863",
              "subscriberNumber": "7286"
            },
            "contactType": "PRIMARYPHONE"
          }
        ]
      },
      "paymentTypes": {},
      "firstOrder": true,
      "eReceiptNotification": true,
      "rewardsNotification": true,
      "dateOfBirthFormat": "yyyy-MM-dd"
    });
  });*/

  it('should get null for logged in user',async ()=>{
    nock.cleanAll();
    nock(`${config.backendUrl}/auth`)
      .get(`/getLoggedinUser`)
      .reply(200, {user : null});

    const json = await authApi.getLoggedInUser();
    expect(json).toEqual({user: null});

  });

  it('should return valid and contain user email address',async()=>{
    nock.cleanAll();
    nock(`${config.backendUrl}/auth`)
      .get(`/checkPasswordResetToken?token-param=${tokenParam}`)
      .reply(200, {isValid: true, data :{email: 'resetcheckuser@gmail.com'}});

    const json = await authApi.checkPasswordResetToken(tokenParam);
    expect(json).toBeTruthy();
    expect(json.email).toEqual('resetcheckuser@gmail.com');
  });


});


