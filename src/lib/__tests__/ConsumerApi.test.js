import faker from 'faker';
import 'test_helper';
import nock from 'nock';
import config from 'config';
import * as restaurantTestData from 'fixtures/restaurant';
import * as catalogTestData from 'fixtures/catalog';
import * as taxRequestData from 'fixtures/tax-request';
import * as orderRequestData from 'fixtures/order-request';

import ConsumerApi from '../ConsumerApi';

const accessToken = faker.random.uuid();
const accountId = faker.random.uuid();
const sessionId = faker.random.uuid();
const productId = faker.random.uuid();
const orderId = faker.random.uuid();
const consumerApi = new ConsumerApi(config);

describe('lib/ConsumerApi', () => {
  beforeAll(() => {
    nock(`${config.backendUrl}/consumer`)
      .get(`/getRestaurantInfo?account-id=${accountId}`)
      .reply(200, {data:restaurantTestData});

    nock(`${config.backendUrl}/consumer`)
      .get(`/getCatalog?account-id=${accountId}&session-id=${sessionId}`)
      .reply(200, {data:catalogTestData});

    nock(`${config.backendUrl}/consumer`)
      .post(`/calculateTax`)
      .reply(200, {data:{
        subTotal: 22.49,
        totalTax: 1.63
      }});

    nock(`${config.backendUrl}/consumer`)
      .post(`/createOrder?account-id=${accountId}`)
      .reply(200, {data:Object.assign({}, orderRequestData, { id: orderId })});
  });

  afterAll(() => {
    nock.cleanAll();
  });

  it('should get restaurant info', async () => {
    const json = await consumerApi.getRestaurantInfo(accountId);
    expect(json).toBeTruthy();
    expect(json).toEqual(restaurantTestData);
  });

  it('should calculate tax', async () => {
    const json = await consumerApi.calculateTax(taxRequestData);
    expect(json).toBeTruthy();
    expect(json).toEqual({
      subTotal: 22.49,
      totalTax: 1.63
    });
  });

  it('should create order', async () => {
    const json = await consumerApi.createOrder(accountId, orderRequestData);
    expect(json).toBeTruthy();
    expect(json).toEqual(Object.assign({}, orderRequestData, { id: orderId }));
  });

  it('should throw empty object error when creating order', async () => {
    nock.cleanAll();
    nock(`${config.backendUrl}/consumer`)
    .post(`/createOrder?account-id=${accountId}`)
    .reply(404, {});
    try{
      const json = await consumerApi.createOrder(accountId, {abc:123});
    }catch (e){
      expect(e).toEqual({});
    }
  });

  it('should throw data error when creating order', async () => {
    nock.cleanAll();
    nock(`${config.backendUrl}/consumer`)
    .post(`/createOrder?account-id=${accountId}`)
    .reply(404, {data:'test data'});
    try{
      const json = await consumerApi.createOrder(accountId, {abc:123});
    }catch (e){
      expect(e).toEqual('test data');
    }
  });
});
