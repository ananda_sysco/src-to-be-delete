import {transparentize} from "polished";
import MdClose from "react-icons/lib/md/close";
import styled from 'styled-components';
import {Checkbox, Grid, Input, Row, themed} from './ui';
import {Label} from "./ui/elements/text";


export const FormInput = styled(Input)`
`;

export const StyledContainer = styled(Grid)`
  margin: 0;
  width: 100%;
`;

export const Section = styled(Row)`
  padding: ${themed('layout.gutter.small')}rem;
`;

export const modalStyle = {
  overlay: {
    backgroundColor: `${transparentize(.5, 'black')}`,
  }
};

export const ResponsiveModal = {
    overlay: {
        overflowY: 'auto',
        backgroundColor: `${transparentize(.5, 'black')}`,
      }
  };


export const CloseButton = styled(MdClose)`
  font-size: ${themed('text.size.large')}rem;
  color: ${themed('colors.neutral')};
  padding-top: ${themed('layout.gutter.large')}rem;
  margin-right: 1rem;
  cursor: pointer;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
`;

export const ExitRow = styled(Row)``;

export const TitleRow = styled(Row)`
  padding-bottom: ${themed('layout.gutter.small')}rem;
  user-select: none;
`;

export const StyledCheckbox = styled(Checkbox)`
  padding-top: ${themed('layout.gutter.medium')}rem;
  padding-bottom: ${themed('layout.gutter.medium')}rem;
`;

export const ErrorLabel = styled(Label)`
  color: ${themed('colors.error')};
  margin-top: ${themed('layout.gutter.xxsmall')}rem;
`;

export const StyledAnchor = styled.a`
  text-decoration: none;
  color:${themed('colors.primary')};
  &:hover{
    opacity: 0.7;
  }`;
