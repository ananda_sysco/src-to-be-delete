/**
 * Created by Salinda on 01/10/18.
 * Execute fetch calls
 */
import fetch from "isomorphic-fetch";

const csrfTokenElement = document.querySelector('meta[name="csrf-token"]');
const token = csrfTokenElement ? csrfTokenElement.getAttribute('content') :  null;

const handleUnauthorized = async response => {
  if (response.status === 401) {
    if (response.headers.get('X-olo-v3-unauthorized') === 'true') {
      window.location.href = window.location.protocol + '//' + window.location.hostname + '/' + window.location.pathname.split("/")[1];
    } else {
      return response;
    }
  }
  return response;
};

export const executeGetApiCall = (url) => {
  return fetch(url, {
    method: 'GET',
    headers: {
      'Request-Type': 'application/vnd.platform.api+json',
    },
    'Request-Type': 'data',
    credentials: 'include',
    redirect: 'manual'
  }).then(handleUnauthorized);
};

export const executePostApiCall = (url, data) => {
  return fetch(url, {
    method: 'POST',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
      'Request-Type': 'application/vnd.platform.api+json',
      'Content-Type': 'application/json',
      'x-csrf-token': token
    },
    body: JSON.stringify(data),
  }).then(handleUnauthorized);
};

export const executePutApiCall = (url, data) => {
  return fetch(url, {
    method: 'PUT',
    credentials: 'include',
    headers: {
      Accept: 'application/json',
      'Request-Type': 'application/vnd.platform.api+json',
      'Content-Type': 'application/json',
      'x-csrf-token': token
    },
    body: JSON.stringify(data),
  }).then(handleUnauthorized);
};

export const executeGoogleGetApiCall = (url) => {
  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  });
};
