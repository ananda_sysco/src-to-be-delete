import { executeGetApiCall, executePostApiCall, executePutApiCall } from './apiUtil';
import { updateUrlWithQueryParams } from '../utils/commonUtil';
import APIError from './APIError';

export default class ConsumerApi {

  constructor(config) {
    this.esbUrl = config.backendUrl;
    this.applicationBaseUrl = config.applicationBaseUrl;
  }


  async getRestaurantInfo(id, optionalParams) {
    const queryParamName = 'account-id';
    let url = `${this.esbUrl}/consumer/getRestaurantInfo?${queryParamName}=${id}`;
    if (optionalParams && optionalParams instanceof Array) {
      url = updateUrlWithQueryParams(url, optionalParams);
    }
    const response = await executeGetApiCall(url).catch(function (error) {
      //Do nothing
    });

    if (!response || !response.ok) {
      let error = new Error('failed to get restaurant info');
      if (response && response.status) {
        error.status = response.status;
      } else {
        error.status = 500;
      }
      throw error;
    }

    let resultJson = response.json();
    let restaurantInfo = null;
    await resultJson.then((resultObject) => {
      restaurantInfo = resultObject.data;
    });
    return restaurantInfo;
  }

  async getReceiptInfo(id) {
    const queryParamName = 'receipt-id';
    const url = `${this.esbUrl}/consumer/getReceiptInfo?${queryParamName}=${id}`;
    const response = await executeGetApiCall(url).catch(function (error) {
      //Do nothing
    });

    if (!response || !response.ok) {
      let error = new Error('failed to get receipt info');
      if (response && response.status) {
        error.status = response.status;
      } else {
        error.status = 500;
      }
      throw error;
    }

    let resultJson = response.json();
    let receiptInfo = null;
    await resultJson.then((resultObject) => {
      receiptInfo = resultObject.data;
    });
    return receiptInfo;
  }

  async calculateTax(cart) {
    const url = `${this.esbUrl}/consumer/calculateTax`;
    const response = await executePostApiCall(url, cart).catch(function (error) {
      //Do nothing
    });

    if (!response || !response.ok) {
      throw new Error('failed to calculate tax');
    }

    let resultJson = response.json();
    let taxInfo = null;
    await resultJson.then((resultObject) => {
      taxInfo = resultObject.data;
    });
    return taxInfo;
  }

  async createOrder(accountId, order) {
    const url = `${this.esbUrl}/consumer/createOrder?account-id=${accountId}`;
    const response = await executePostApiCall(url, order);
    const orderJson = await response.json();

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Order creation error', orderJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return orderJson.data;
  }

  async sendResetPasswordEmail(email, accountId) {
    const url = `${this.esbUrl}/consumer/sendResetPasswordEmail`;
    let redirectUrl;
    if (accountId) {
      redirectUrl = `${this.applicationBaseUrl}/${accountId}?resetPassword=true`;
    } else {
      redirectUrl = `${this.applicationBaseUrl}?resetPassword=true`;
    }
    const response = await executePostApiCall(url, {'email': email, 'redirectUrl': redirectUrl});

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Reset Password Email Error');
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }
    return null;
  }

  async checkUser(email) {
    const url = `${this.esbUrl}/consumer/checkUser?email-param=${email}`;
    const response = await executeGetApiCall(url);
    const responseJson = await response.json();

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Check user error', responseJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return responseJson.data;
  }

  async updateUser(user) {
    const url = `${this.esbUrl}/consumer/updateUser`;
    const response = await executePutApiCall(url, user);
    const responseJson = await response.json();

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Update user error', responseJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return responseJson.data;
  }

  async updatePassword(email, password, token) {
    const url = `${this.esbUrl}/consumer/resetPassword?email-param=${email}&token-param=${token}`;
    const response = await executePostApiCall(url, {password});
    let responseJson;

    if (response.status !== 204) {
      responseJson = await response.json();
    } else {
      responseJson = {data: {statusCode: 204, message: "No Content"}};
    }

    if (!response || !response.ok) {
      if (response) {
        return new APIError(response.status, 'Password reset token check error', responseJson);
      } else {
        return new APIError(500, 'Unexpected Error');
      }
    }

    return responseJson.data;
  }

  async checkPhone(phone, email) {
    const url = `${this.esbUrl}/consumer/checkPhone?phoneNumber=${encodeURIComponent(phone)}&email=${encodeURIComponent(email)}`;
    const response = await executeGetApiCall(url);

    if (!response || !response.ok) {
      return true;
    }

    const responseJson = await response.json();
    return !(responseJson.data && responseJson.data.isAvailable);
  }

  async getOperators(source){
    const url = `${this.esbUrl}/consumer/getOperators?source=${encodeURIComponent(source)}`;
    const response = await executeGetApiCall(url);

    if (!response || !response.ok) {
      let error = new Error('failed to get restaurant info');
      if (response && response.status) {
        error.status = response.status;
      } else {
        error.status = 500;
      }
      throw error;
    }

    let resultJson = response.json();
    let operators = null;
    await resultJson.then((resultObject) => {
      operators = resultObject.data;
    });
    return operators;
  }

}
