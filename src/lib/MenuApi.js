const {executeGetApiCall} = require('./apiUtil');

export default class MenuApi {

  constructor(config) {
    this.esbUrl = config.backendUrl;
  }


  async getCatalog(accountID, sessionId) {
    const url = `${this.esbUrl}/menu/getCatalog?account-id=${accountID}&session-id=${sessionId}`;

    const response = await executeGetApiCall(url);

    if (!response || !response.ok) {
      throw new Error(`failed to get catalog`);
    }
    let resultJson = response.json();
    let catalogInfo = null;
    await resultJson.then((resultObject) => {
      catalogInfo = resultObject.data;
    });
    return catalogInfo;
  }
}
