const getConfigsFromServer = require('./ConfigServer');
const express = require('express');
const helmet = require('helmet');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const url = require('url');
const uuidV4 = require('uuid/v4');
const { frameworkLogger } = require('./logger');
const logger = frameworkLogger.getDefaultLogger();
const csp = require('helmet-csp');
const csurf = require('csurf');
const cors = require('cors');
const session = require('express-session');
const redisStore = require('connect-redis')(session);
const { get } = require('lodash');
const cls = require('cls-hooked');
const GLOBAL_NAMESPACE = 'net.cake.global';
const namespace = cls.createNamespace(GLOBAL_NAMESPACE);
const buildDir = path.resolve(__dirname, '../../build');

const server = express();
// Function to check whether the app is running in production mode
const isProductionMode = () => server.get('env') === 'production';
const BAD_CSRF_ERROR_CODE = 'EBADCSRFTOKEN';

function setCSPConfigs(server, csp, isProduction) {
  const configuration = {
    directives: {
      defaultSrc: ["'self'", "https://api.mixpanel.com", "https://www.google.com"],
      scriptSrc: ["'self'", "'unsafe-inline'", "https://www.google-analytics.com",
        "https://www.google.com", "https://maps.googleapis.com", "https://www.gstatic.com/"],
      connectSrc: ['https:'],
      styleSrc: ["'self'", "'unsafe-inline'", "https://fonts.googleapis.com"],
      fontSrc: ["'self'", 'data:', "https://fonts.gstatic.com"],
      imgSrc: ["'self'", 'data:', 'https:'],
      sandbox: ['allow-forms', 'allow-scripts', 'allow-popups', 'allow-same-origin'],
      reportUri: '/report-violation',
      objectSrc: ["'none'"],
      upgradeInsecureRequests: true,
      workerSrc: false
    },
    loose: true,
    reportOnly: false,
    setAllHeaders: false,
    browserSniff: true
  };

  const devProdImpactedConfigs = ['connectSrc', 'scriptSrc', 'styleSrc', 'fontSrc'];

  if (!isProduction) {
    for (let config in configuration.directives) {
      if (configuration.directives.hasOwnProperty(config)) {
        devProdImpactedConfigs.includes(config) && configuration.directives[config].push('http:')
      }
    }
  }

  server.use(csp(configuration));
}

function setServerToUseCorrelationIds(namespace) {
  if (namespace) {
    server.use((req, res, next) => {
      namespace.bindEmitter(req);
      namespace.bindEmitter(res);
      const correlationId = req.get('X-Correlation-Id');
      // run following middleware in the scope of the namespace we created
      namespace.run(() => {
        if (correlationId) {
          cls.getNamespace(GLOBAL_NAMESPACE).set('correlationId', correlationId);
        } else {
          const uuid = uuidV4();
          cls.getNamespace(GLOBAL_NAMESPACE).set('correlationId', uuid);
        }
        next();
      });
    });
  }
}

function setServerToUseCSRF(shouldEnableCSRF = false) {
  if (shouldEnableCSRF && isProductionMode()) {
    server.use(csurf({
      cookie: false,
      value: (req) => req.headers['x-csrf-token']
    }));

    server.use((err, req, res, next) => {
      if (err.code === BAD_CSRF_ERROR_CODE) {
        // handle CSRF token errors here
        logger.error('Bad CSRF token detected', err);
        res.status(403);
        res.send('Invalid request');
      } else {
        next(err);
      }
    });
  }
}

function setServerToUseCORS() {
  if (!isProductionMode()) {
    const corsOptions = {
      origin: 'http://localhost:3000',
      credentials: true
    };
    server.use(cors(corsOptions));
  }
}


const sendHomePage = (req, res, indexPage, shouldEnableCSRF = false) => {
  if (shouldEnableCSRF && isProductionMode()) {
    const updatedIndexPageWithCSRFToken = indexPage.replace(/\{\{csrfToken\}\}/, req.csrfToken());
    res.send(updatedIndexPageWithCSRFToken);
  } else {
    res.send(indexPage);
  }
};

getConfigsFromServer().then(envSpecificConfig => {
  const { setServerConfigs, setFrontendConfigs } = require('./config');
  setServerConfigs(envSpecificConfig);
  setFrontendConfigs(envSpecificConfig);
  const indexPage = fs.readFileSync(path.resolve(buildDir, 'index.html'), 'utf8');
  const config = require('./config').getConfigs();
  const consumerAPIRoutes = require('./api/routes/consumerApiRoutes');
  const menuRoutes = require('./api/routes/menuApiRoutes');
  const userRoutes = require('./api/routes/userRoutes');
  const s3Routes = require('./api/routes/s3Routes');
  const walletRoutes = require('./api/routes/walletRoutes');
  const legacyRedirectionRoutes = require('./api/routes/legacyRedirectionRoutes');
  const { authMiddleWare, httpsMiddleWare } = require('./middleware');
  const jwtValidationUtil = require('../server/api/util/jwtValidationUtil');
  const createRedisClient = require('../server/api/util/commonUtil').createRedisClient;
  const { shouldEnableCSRF } = config;

  // setCSPConfigs(server, csp, isProductionMode);
  setServerToUseCorrelationIds(namespace);

  server.use(bodyParser.json());
  server.use('/olo/health', require('express-healthcheck')({
    healthy: function () {
        return {
          "appId": "olo",
          "status": "application is running"
        };
    }
  }));
  server.use(helmet());
  setServerToUseCORS();
  // serve static files
  server.use(express.static(buildDir, {index: false}));

  const sessionMiddleware = session({
    cookie: {maxAge: config.maxAge, secure: isProductionMode(), httpOnly: true},
    secret: config.secret,
    name: 'olo',
    store: new redisStore({
      prefix: 'olo:',
      client: createRedisClient()
    }),
    resave: false, //Forces the session to be saved back to the session store, even if the session was never modified during the request.
    saveUninitialized: false, //"uninitialized" session to be saved. Session is uninitialized when it is new but not modified.
    proxy: isProductionMode() // Setting whether application is running behind a reverse proxy
    // When app is running behind a proxy server, proxy server should provide X-Forwarded-Proto header in order to secure the cookie
  });

  server.use((req, res, next) => sessionMiddleware.call(this, req, res, namespace.bind(next)));

  server.use(bodyParser.json({
    type: ['json', 'application/csp-report']
  }));

  server.post('/report-violation', (req, res) => {
    if (req.body) {
      logger.info(`CSP Violation block-uri: ${get(req.body, ['csp-report', 'blocked-uri'])} ` +
        `violated-directive: ${get(req.body, ['csp-report', 'violated-directive'])}`);
      logger.debug(`CSP Violation Details: $ ${JSON.stringify(req.body)}`)
    } else {
      logger.info('CSP Violation: No data received!')
    }

    res.status(204).end()
  });

  setServerToUseCSRF(shouldEnableCSRF);

  if (config.isBehindElb === 'true') {
    server.use(httpsMiddleWare);
  }
  server.use(authMiddleWare);

  //Add basic auth header
  server.use('/oauth/token', (req, res, next) => {
    req.headers['Authorization'] = 'Basic ' + config.BASIC_AUTH_TOKEN;
    next();
  });

  server.use('/consumer', consumerAPIRoutes);
  server.use('/auth', userRoutes);
  server.use('/menu', menuRoutes);
  server.use('/s3', s3Routes);
  server.use('/wallet', walletRoutes);
  //legacy redirection
  server.use('/order', legacyRedirectionRoutes);

  /**
   * Redirecting olo v2 to v3
   */
  server.use('/widget', (req, res) => {
    // redirect to menu page
    const url_parts = url.parse(req.url, true);
    let accountId = url_parts.query.company_uid;
    if (!accountId) {
      let temp = req.path.split("/");
      if (temp.length > 1) {
        accountId = temp[1];
      }
    }
    res.redirect(`/${accountId}`);
  });

  /**
   * Redirecting olo v2 to v3 for mobile devices
   */
  server.use('/mobile', (req, res) => {
    // redirect to menu page
    const url_parts = url.parse(req.url, true);
    let accountId = url_parts.query.company_uid;
    if (!accountId) {
      let temp = req.path.split("/");
      if (temp.length > 1) {
        accountId = temp[1];
      }
    }
    res.redirect(`/${accountId}`);
  });


  server.get('/wallet', (req, res) => {
    logger.info("Wallet page loaded");
    // send react app
    sendHomePage(req, res, indexPage, shouldEnableCSRF);
  });

  server.get('/:merchantId/:path(checkout|confirmation|resetPassword)/', (req, res) => {
    // redirect to menu page
    res.redirect(`/${req.params.merchantId}`);
  });

  server.get('/:merchantId', (req, res) => {
    logger.info(`Widget loaded for ${req.params.merchantId}`);
    // send react app
    sendHomePage(req, res, indexPage, shouldEnableCSRF);
  });

  server.get('*', (req, res) => {
    // send react app
    logger.info("Widget loaded with not found route");
    sendHomePage(req, res, indexPage, shouldEnableCSRF);
  });

  jwtValidationUtil.getValidationKeys(createRedisClient()).then(rep => {
    logger.info(`Validation keys received : ${JSON.stringify(rep)}`);
    server.listen(config.serverPort, () => {
      logger.info(`Serving app from directory: ${buildDir}`);
      logger.info(`The server is running at http://localhost:${config.serverPort}/`);
      logger.info(`The server is proxying API calls to ${config.esbUrl}`);
    });
  }).catch(err => {
    logger.error(`Error occurred while receiving validation keys : ${err}`);
  });
});
