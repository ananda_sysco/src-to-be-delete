/**
 * Created by PrabhathP on 06/14/18.
 */
const express = require('express');
const router = express.Router();
const { frameworkLogger } = require('../../logger');
const apiConstants = require('../constant/apiConstants');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const urlParamUtil = require('../util/urlParamUtil');
const request = require('request');

router.get('/getLogo', (req, res) => {
  logger.info(`${FILE} : /getLogo route called`);
  const url = urlParamUtil.getQueryParamFromRequest(FILE, req, apiConstants.PARAM_IMG_URL);
  if (url) {
    request(url).pipe(res);
  } else {
    logger.error(`${FILE} : Invalid url: ${url}`);
    res.status(httpConstants.HTTP_BAD_REQUEST).send('Invalid url.');
  }
});


module.exports = router;
