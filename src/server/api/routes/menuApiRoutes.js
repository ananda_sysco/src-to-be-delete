/**
 * Created by Salinda on 12/5/17.
 */
const express = require('express');
const router = express.Router();
const menuHandler = require('../handler/menuHandler');
const { frameworkLogger } = require('../../logger');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);


/**
 * Handles the response, set status code, response body and ends the response
 * @param response
 * @param responseData
 */
const handleResponse = (response, responseData) => {
  //TODO need to log this, very important
  let responseStatus = null;
  if (responseData instanceof HttpResponse) {
    responseStatus = responseData.getStatusCode();
  } else {
    responseStatus = httpConstants.HTTP_INTERNAL_SERVER_ERROR;
  }
  if (responseStatus !== httpConstants.HTTP_REDIRECT) {
    response.status(responseStatus).send(responseData);
  } else {
    response.redirect(responseData.data);
  }
};


router.get('/getCatalog', (req, res) => {
  logger.info(`${FILE} : /getCatalog route called`);
  return menuHandler.handleGetCatalog(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/preview', (req, res) => {
  logger.info(`${FILE} : /preview route called`);
  return menuHandler.getPreview(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

module.exports = router;
