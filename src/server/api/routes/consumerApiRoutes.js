/**
 * Created by Salinda on 12/5/17.
 */
const express = require('express');
const router = express.Router();
const consumerAPIHandler = require('../handler/consumerApiHandler');
const { frameworkLogger } = require('../../logger');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);

/**
 * Handles the response, set status code, response body and ends the response
 * @param response
 * @param responseData
 */
const handleResponse = (response, responseData) => {
  //TODO need to log this, very important
  let responseStatus = null;
  if (responseData instanceof HttpResponse) {
    responseStatus = responseData.getStatusCode();
  } else {
    responseStatus = httpConstants.HTTP_INTERNAL_SERVER_ERROR;
  }
  response.status(responseStatus).send(responseData);

};

router.get('/getRestaurantInfo', (req, res) => {
  logger.info(FILE + ': /getRestaurantInfo route called');
  return consumerAPIHandler.handleGetRestaurantInfo(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });

});

router.get('/getReceiptInfo', (req, res) => {
  logger.info(FILE + ': /getReceiptInfo route called');
  return consumerAPIHandler.handleGetReceiptInfo(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });

});

router.post('/calculateTax', (req, res) => {
  logger.info(FILE + ': /calculateTax route called');
  return consumerAPIHandler.handleCalculateTax(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });

});

router.post('/createOrder', (req, res) => {
  logger.info(FILE + ': /createOrder route called');
  return consumerAPIHandler.handleCreateOrder(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/checkUser', (req, res) => {
  logger.debug(FILE + ': /checkUser route called');
  return consumerAPIHandler.handleCheckUser(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/checkPhone', (req, res) => {
  logger.debug(FILE + ': /checkPhone route called');
  return consumerAPIHandler.handleCheckPhone(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/orderHistory', (req, res) => {
  logger.debug(FILE + ': /orderHistory route called');
  return consumerAPIHandler.handleGetOrderHistory(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.post('/sendResetPasswordEmail', (req, res) => {
  logger.info(FILE + ': /sendResetPasswordEmail route called');
  return consumerAPIHandler.handleSendingResetPasswordEmail(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.post('/resetPassword', (req, res) => {
  logger.info(FILE + ': /resetPassword route called');
  return consumerAPIHandler.handleResetPassword(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.put('/updateUser', (req, res) => {
  logger.info(FILE + ': /updateUser route called');
  return consumerAPIHandler.handleUpdateUser(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/getOperators', (req, res) => {
  logger.info(FILE + ': /getOperators route called');
  return consumerAPIHandler.handleGetOperatorsList(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  })
});


module.exports = router;
