/**
 * Created by PrabhathP on 12/28/17.
 */
const express = require('express');
const router = express.Router();
const userHandler = require('../handler/userHandler');
const { frameworkLogger } = require('../../logger');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);

/**
 * Handles the response, set status code, response body and ends the response
 * @param response
 * @param responseData
 */
const handleResponse = (response, responseData) => {
  //TODO need to log this, very important
  let responseStatus = null;
  if (responseData instanceof HttpResponse) {
    responseStatus = responseData.getStatusCode();
  } else {
    responseStatus = httpConstants.HTTP_INTERNAL_SERVER_ERROR;
  }
  response.status(responseStatus).send(responseData);

};


router.post('/login', (req, res) => {
  logger.info(`${FILE} : /login route called`);
  return userHandler.handleLogin(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });

});

router.get('/logout', (req, res) => {
  logger.info(`${FILE} : /logout route called`);
  userHandler.handleLogout(req, res);
});

router.post('/signup', (req, res) => {
  logger.info(`${FILE} : /signup route called`);
  return userHandler.handleSignUpUser(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/checkPasswordResetToken', (req, res) => {
  logger.debug(`${FILE} : /checkPasswordResetToken route called`);
  return userHandler.checkPasswordResetToken(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/getLoggedinUser', (req, res) => {
  logger.info(`${FILE} : /getloggedinuser route called`);
  return userHandler.handleLoggedInUser(req).then((response) => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  })
});


module.exports = router;
