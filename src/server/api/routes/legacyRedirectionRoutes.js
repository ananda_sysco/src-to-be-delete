/**
 * @author Tharuka Jayalath
 * (C) 2019, Sysco Labs
 */

const express = require('express');
const router = express.Router();
const consumerAPIHandler = require('../handler/consumerApiHandler');
const legacyHandler = require('../handler/legacyHandler');
const { frameworkLogger } = require('../../logger');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);

/**
 * Handles the response, set status code, response body and ends the response
 * @param response
 * @param responseData
 */
const handleResponse = (response, responseData) => {
  if (responseData instanceof HttpResponse &&
    responseData.getStatusCode() === httpConstants.HTTP_REDIRECT) {
    logger.info(`${FILE} : Redirecting request to ${responseData.data}`);
    response.redirect(responseData.data);
  } else {
    response.redirect('/');
  }
};

router.get('/restaurant/:merchantId', async (req, res) => {
  logger.info(`${FILE} : /restaurant/:merchantId route called`);
  return consumerAPIHandler.handleGetAccountInfo(req).then(response => {
    handleResponse(res, response);
  }).catch(err => {
    handleResponse(res, err);
  });
});

router.get('/r/:receiptId', async (req, res) => {
  logger.info(`${FILE} : /r/:receiptId route called`);
  return handleResponse(res, legacyHandler.handleReceipt(req));
});

module.exports = router;
