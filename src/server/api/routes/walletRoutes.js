/**
 * Created by PrabhathP on 06/14/18.
 */
const express = require('express');
const router = express.Router();
const { frameworkLogger } = require('../../logger');
const httpConstants = require('../constant/httpConstants');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const urlParamUtil = require('../util/urlParamUtil');
const captchaUtil = require('../util/captchaUtil');
const consumerAPIHandler = require('../handler/consumerApiHandler');
const HttpResponse = require('../util/httpResponse');
const nodeRuntimeUtil = require('util');
const config = require("../../../server/config").getConfigs();

/**
 * Handles the response, set status code, response body and ends the response
 * @param response
 * @param responseData
 */
const handleResponse = (response, responseData) => {
  let responseStatus = null;
  if (responseData instanceof HttpResponse) {
    responseStatus = responseData.getStatusCode();
  } else {
    responseStatus = httpConstants.HTTP_INTERNAL_SERVER_ERROR;
  }
  response.status(responseStatus).send(responseData);

};

router.post('/getWalletDetails', async (req, res) => {

  logger.info(`${FILE} : /getWalletInfo route called`);
  const cardNo = urlParamUtil.getDataFieldsFromRequest(FILE, req, "CARD_NO", true);
  const pin = urlParamUtil.getDataFieldsFromRequest(FILE, req, "PIN", true);
  const captchaValue = urlParamUtil.getDataFieldsFromRequest(FILE, req, "CAPTCHA_VALUE");

  if (config.captchaEnabled) {
    let captchaResponse = await captchaUtil.validateCaptcha(captchaValue);

    if (!captchaResponse) {
      return res.status(httpConstants.HTTP_BAD_REQUEST).send(captchaResponse);
    }
  }

  consumerAPIHandler.handleGetWalletData(cardNo, pin, req).then(walletData => {
    let response = [];
    for (let cluster of walletData.clusters) {
      let tempClusterInfo = {};
      tempClusterInfo.value = cluster.balance;
      tempClusterInfo.name = cluster.clusterName;
      if (cluster.cardTransactions && cluster.cardTransactions.length > 0) {
        const temp = cluster.cardTransactions[0];
        tempClusterInfo.lastTransaction = temp.cardTransactionType;
        tempClusterInfo.lastTransactionTS = Math.floor(temp.createdTime / 1000);
      }
      tempClusterInfo.locations = [];
      if (cluster.accounts) {
        for (let account of cluster.accounts) {
          let tempLocation = {};
          tempLocation.name = account.accountName;
          if (account.address) {
            tempLocation.address1 = account.address.address1;
            tempLocation.address2 = account.address.address2;
            tempLocation.city = account.address.city + "," + account.address.state;
          }
          tempClusterInfo.locations.push(tempLocation);
        }
      }
      response.push(tempClusterInfo);
    }
    res.status(httpConstants.HTTP_OK).send(response);
  }).catch(err => {
    logger.error(`${FILE} : error while getting wallet details ${nodeRuntimeUtil.inspect(err)}`);
    handleResponse(res, err);
  });

});


module.exports = router;
