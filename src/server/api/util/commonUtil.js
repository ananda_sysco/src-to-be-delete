/*
 * Created by Tharuka Jayalath 12/19/17
 */
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const urlUtils = require('../util/urlUtils');
const HttpParams = require('../util/httpParams');
const redis = require('redis');
const { isEqual, isUndefined, isNil } = require('lodash');
const config = require('../../config').getConfigs();
const FILE = require('path').basename(__filename);
const uuidv5 = require('uuid/v5');
const {
  KEY_IDEMPOTENCY_HEADER,
  KEY_IDEMPOTENCY_DETAILS
} = require('../constant/miscConstants');
const os = require('os');

const netInterfaces = Object.entries(os.networkInterfaces())
  .filter(entry => entry[0] !== 'lo');
const [ eth ] = netInterfaces;
const { address } = eth[1][0];
const nameSpace = address.split('.').reduce((acc, val) => `${acc}-${val}`, '');
const NAME_SPACE = `${os.uptime()}${nameSpace}.cake.net`;
const NAME_SPACE_UUID = uuidv5(NAME_SPACE, uuidv5.DNS);
logger.info(`${FILE} NAME_SPACE: ${NAME_SPACE}`);
logger.info(`${FILE} NAME_SPACE_UUID: ${NAME_SPACE_UUID}`);

module.exports = {
  /**
   * Constructs HttpParams object for the consumer api endpoint related rest calls
   * @param endPoint
   * @param paramMap
   * @param authToken
   * @param data
   * @param queryParamMap
   * @param optionalHeaders
   * @returns {HttpParams}
   */
  constructConsumerEndpointParams: (endPoint, paramMap, authToken, data, queryParamMap, optionalHeaders = null) => {
    const updatedGetInfoEndpoint = urlUtils.updateEndpointPathVariables(endPoint, paramMap);
    const getRestaurantInfoEndPointUrl = urlUtils.constructEndPointUrl(updatedGetInfoEndpoint);
    const getRestaurantInfoEndPointUrlWithQueryParams = urlUtils.updateEndpointQueryParams(getRestaurantInfoEndPointUrl, queryParamMap);
    let headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'CLIENT': 'olo_v3',
      Authorization: 'Bearer ' + authToken
    };

    if (optionalHeaders) {
      headers = { ...headers, ...optionalHeaders };
    }

    return new HttpParams(getRestaurantInfoEndPointUrlWithQueryParams, headers, data);
  },

  createRedisClient: () => {
    // const client = redis.createClient(config.redisPort, config.redisHost);
    const client = redis.createClient("6379", "127.0.0.1");
    client.on("error", (err) => {
      logger.error('Redis Failed to connect:', err);
    });
    return client;
  },

  generateIdempotencyHeaderValue: () => {
    return uuidv5(`${new Date().getTime()}`, NAME_SPACE_UUID);
  },

  updateSession: (request, key, value, forceUpdate = false) => {
    const { session } = request;
    if (session) {
      const currentValue = session[key];
      if (forceUpdate || currentValue === undefined || currentValue === null) {
        if (typeof value === 'function') {
          request.session[key] = value();
        } else {
          request.session[key] = value;
        }
      }
    }
  },

  getValueByKeyFromSession: (request, key) => {
    const { session } = request;
    if (session) {
      return session[key];
    }
    return null;
  },

  updateSessionWithIdempotencyDetails: (request, order, idempotencyHeaderValueGenerator) => {
    const idempotencyDetails = request.session[KEY_IDEMPOTENCY_DETAILS];
    const { accountId } = order;
    if (isUndefined(idempotencyDetails) || isNil(idempotencyDetails[accountId])) {
      const idempotencyHeaderValue = idempotencyHeaderValueGenerator();
      if (isUndefined(idempotencyDetails)) {
        request.session[KEY_IDEMPOTENCY_DETAILS] = {
          [accountId]: {
            order,
            [KEY_IDEMPOTENCY_HEADER]: idempotencyHeaderValue
          }
        };
      } else {
        request.session[KEY_IDEMPOTENCY_DETAILS] = {
          ...idempotencyDetails,
          [accountId]: {
            order,
            [KEY_IDEMPOTENCY_HEADER]: idempotencyHeaderValue
          }
        };
      }
      logger.info(`${FILE} generated idempotencyHeaderValue: ${idempotencyHeaderValue} for accountId: ${accountId} and updated the session`);
    } else {
      const {
        order: prevOrder,
        [KEY_IDEMPOTENCY_HEADER]: prevIdempotencyHeaderValue
      } = idempotencyDetails[accountId];

      const { orderReadyTime: prevOrderTime, ...prevOrderSubset } = prevOrder;
      const { orderReadyTime: newOrderTime, ...newOrderSubset } = order;

      if (!isEqual(prevOrderSubset, newOrderSubset)) {
        const newIdempotencyHeaderValue = idempotencyHeaderValueGenerator();
        request.session[KEY_IDEMPOTENCY_DETAILS] = {
          ...idempotencyDetails,
          [accountId]: {
            order,
            [KEY_IDEMPOTENCY_HEADER]: newIdempotencyHeaderValue
          }
        };
        logger.info(`${FILE} replaced idempotencyHeaderValue: ${prevIdempotencyHeaderValue} with \
          newIdempotencyHeaderValue: ${newIdempotencyHeaderValue} for accountId: ${accountId}`);
      } else {
        logger.info(`${FILE} previous order created at ${prevOrderTime} and current order created at ${newOrderTime} for accountId: ${accountId} are identical.`);
        logger.info(`${FILE} reusing idempotencyHeaderValue: ${prevIdempotencyHeaderValue} for accountId: ${accountId} since order payloads are identical`);
      }
    }
  },
  extractHeaders: (request, headerMetadata) => {
    const headers = request.headers || {};
    return headerMetadata.map(({ name, modifiedName }) => ({
      [modifiedName]: headers[name] || ''
    })).reduce((acc, val) => ({ ...acc, ...val }), {});
  }

};
