const { frameworkLogger } = require('../../../logger');
const logger = frameworkLogger.getDefaultLogger();

class PerformanceLogger {
  constructor(name) {
    this.startTime = new Date().getTime();
    this.name = name;
  }

  log() {
    this.endTime = new Date().getTime();
    logger.debug('PERFORMANCE CHECK: Time taken for ' + this.name + ' :' + (this.endTime - this.startTime) + ' ms');
  }
}

module.exports = PerformanceLogger;
