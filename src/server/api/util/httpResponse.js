/**
 * Created by Salinda on 1/26/17.
 */
/**
 * Wraps response data of an API call through
 * 'node-rest-client'.
 *
 */

class HttpResponse {
  /**
   * Constructs HttpResponse object with data, status code and error message
   * wrapped. If this object is returned as a result of success API call, it has
   * data, status code and a null message. On the other hand if this is returned due
   * to an error, it has null data, status code and an error content.
   *
   * @param data
   * @param statusCode
   * @param message
   */
    constructor(data, statusCode, message) {
        this.data = data;
        this.statusCode = statusCode;
        this.message = message;
    }

    getData() {
        return this.data;
    }

    getStatusCode() {
        return this.statusCode;
    }

    getMessage(){
        return this.message;
    }
}
module.exports = HttpResponse;
