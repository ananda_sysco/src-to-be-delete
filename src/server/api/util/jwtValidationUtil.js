const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const jwt = require('jsonwebtoken');
const nodeRuntimeUtil = require('util');
const authRestUtil = require('../util/restUtil/authRestUtil');
const config = require('../../config').getConfigs();
const createRedisClient = require('./commonUtil').createRedisClient;

const prepadSigned = (hexStr) => {
  const msb = hexStr[0];
  if (msb < '0' || msb > '7') {
    return `00${hexStr}`;
  }
  return hexStr;
};

const toHex = (number) => {
  const nstr = number.toString(16);
  if (nstr.length % 2) {
    return `0${nstr}`;
  }
  return nstr;
};

const encodeLengthHex = (n) => {
  if (n <= 127) {
    return toHex(n);
  }
  const nHex = toHex(n);
  const lengthOfLengthByte = 128 + nHex.length / 2;
  return toHex(lengthOfLengthByte) + nHex;
};

const rsaPublicKeyToPEM = (modulusB64, exponentB64) => {
  const modulus = new Buffer(modulusB64, 'base64');
  const exponent = new Buffer(exponentB64, 'base64');
  const modulusHex = prepadSigned(modulus.toString('hex'));
  const exponentHex = prepadSigned(exponent.toString('hex'));
  const modlen = modulusHex.length / 2;
  const explen = exponentHex.length / 2;

  const encodedModlen = encodeLengthHex(modlen);
  const encodedExplen = encodeLengthHex(explen);
  const encodedPubkey = '30' +
    encodeLengthHex(modlen + explen + encodedModlen.length / 2 + encodedExplen.length / 2 + 2) +
    '02' + encodedModlen + modulusHex +
    '02' + encodedExplen + exponentHex;

  const der = new Buffer(encodedPubkey, 'hex')
    .toString('base64');

  let pem = `-----BEGIN RSA PUBLIC KEY-----\n`;
  pem += `${der.match(/.{1,64}/g).join('\n')}`;
  pem += `\n-----END RSA PUBLIC KEY-----\n`;
  return pem;
};


let client = createRedisClient();

const getSecretFromStore = () => {
  return new Promise((resolve, reject) => {

    if (!client) {
      client = createRedisClient();
    }
    client.get(config.redisPrefix + 'olo:validationSecret', (err, reply) => {
      if (err) {
        reject(err);
      } else {
        resolve(reply);
      }
    });
  });
};


module.exports = {

  validateToken: (token) => {
    if (!token)
      return Promise.resolve(false);
    const decodedToken = jwt.decode(token, {complete: true});
    if (!decodedToken) {//received token is an invalid(improper) token
      return Promise.resolve(false);
    }
    return getSecretFromStore().then(secret => {
      try {
        jwt.verify(token, secret, {
          issuer: `${config.issuer}`,
          audience: `${config.audience}`,
          algorithms: ['RS256']
        });
        return true;
      } catch (err) {
        logger.error(`${FILE} : JWT verification failed`);
        return false;
      }
    }).catch(err => {
      logger.error(`${FILE} : error : ${nodeRuntimeUtil.inspect(err)}`);
      return false;
    });
  },

  getValidationKeys: async (client) => {
    const keySet = await authRestUtil.getJWTValidationKeys();
    let key;
    if (keySet.keys && keySet.keys.length > 0) {
      key = keySet.keys[0];
    } else {
      return Promise.reject("Invalid keyset");
    }

    const validationSecret = rsaPublicKeyToPEM(key['n'], key['e']);

    return new Promise((resolve, reject) => {
      client.set(config.redisPrefix + 'olo:validationSecret', validationSecret, (err, reply) => {
        if (err) {
          logger.error(`${FILE} : ${nodeRuntimeUtil.inspect(err)}`);
          reject(err);
        } else {
          logger.info(`${FILE} reply`);
          logger.info(`${FILE} : Validation secret successfully saved to cache`);
          resolve(reply);
        }
      });
    });
  }

};
