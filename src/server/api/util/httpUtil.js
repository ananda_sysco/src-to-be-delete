/**
 * Created by Salinda on 12/5/17.
 */
const HttpParams = require('./httpParams');
const HttpResponse = require('./httpResponse');
const constants = require('../constant/httpConstants');
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const request = require('request-promise-native');
const FILE = require('path').basename(__filename);

/**
 * Construct input object for the request with headers and input data.
 * @param httpParams
 * @returns {{}}
 */

const constructRequestOption = (httpParams) => {
  const returnData = {};
  if (httpParams.getUrl() != null) {
    returnData['uri'] = httpParams.getUrl();
  }
  if (httpParams.getHeaders() != null) {
    returnData['headers'] = httpParams.getHeaders();
  }
  if (httpParams.getData() != null) {
    returnData['body'] = httpParams.getData();
  } else {
    returnData['body'] = {};
  }

  if (process.env !== 'production') {
    returnData['time'] = true;
  }

  returnData['json'] = true;
  returnData['gzip'] = true;
  returnData['simple'] = false;  //Get a rejection only if the request failed for technical reasons
  returnData['resolveWithFullResponse'] = true;
  return returnData;
};

module.exports = {

  /**
   * Handles GET requests,expects a HttpParams as a parameter object or returns a rejected
   * promise. API url, headers need to be sent in HttpParams object
   * @param httpParams
   * @returns {Promise}
   */
  doGet: (httpParams) => {
    return new Promise((resolve, reject) => {
      if (httpParams instanceof HttpParams) {
        logger.debug(`${FILE} : Initializing GET API call URL: ${httpParams.getUrl()} HEADERS: ${httpParams.getHeaders()}`);
        request.get(constructRequestOption(httpParams))
          .then(function (parsedBody) {
            logger.debug(`PERFORMANCE CHECK: Time taken for  ${httpParams.getUrl()}  request : ${parsedBody.elapsedTime}ms`);
            return resolve(new HttpResponse(parsedBody.body, parsedBody.statusCode));
          })
          .catch(function (err) {
            return reject(new HttpResponse(err, constants.HTTP_INTERNAL_SERVER_ERROR));
          });
      } else {
        logger.error(`${FILE} : doGet: This requires HttpParams as the input.`);
        reject(new HttpResponse(null, constants.HTTP_BAD_REQUEST, 'This requires a HttpParams as an input parameter'));
      }
    });

  },

  /**
   * Handles POST requests,expects a HttpParams as a parameter object or returns a rejected
   * promise. API url, headers and POST data need to be sent in HttpParams object
   * @param httpParams
   * @returns {Promise}
   */
  doPost: (httpParams) => {
    return new Promise((resolve, reject) => {
      if (httpParams instanceof HttpParams) {
        logger.debug(`${FILE} : Initializing POST API call URL: ${httpParams.getUrl()} HEADERS: ${httpParams.getHeaders()}`);

        request.post(constructRequestOption(httpParams))
          .then(function (parsedBody) {
            logger.debug(`PERFORMANCE CHECK: Time taken for ${httpParams.getUrl()} request : ${parsedBody.elapsedTime}ms`);
            return resolve(new HttpResponse(parsedBody.body, parsedBody.statusCode));
          })
          .catch(function (err) {
            return reject(new HttpResponse(err, constants.HTTP_INTERNAL_SERVER_ERROR));
          });

      } else {
        logger.error(`${FILE} : doPost: This requires HttpParams as the input.`);
        reject(new HttpResponse(null, constants.HTTP_BAD_REQUEST));
      }
    });

  },

  /**
   * Handles PUT requests,expects an HttpParams as a parameter object or returns a rejected
   * promise. API url, headers and PUT data need to be sent in the HttpParams object
   * @param httpParams
   * @returns {Promise}
   */
  doPut: (httpParams) => {
    return new Promise((resolve, reject) => {
      if (httpParams instanceof HttpParams) {
        logger.debug(`${FILE} : Initializing PUT API call URL: ${httpParams.getUrl()} HEADERS: ${httpParams.getHeaders()}`);

        request.put(constructRequestOption(httpParams))
          .then(function (parsedBody) {
            logger.debug(`PERFORMANCE CHECK: Time taken for ${httpParams.getUrl()} request : ${parsedBody.elapsedTime}ms`);
            return resolve(new HttpResponse(parsedBody.body, parsedBody.statusCode));
          })
          .catch(function (err) {
            return reject(new HttpResponse(err, constants.HTTP_INTERNAL_SERVER_ERROR));
          });

      } else {
        logger.error(`${FILE} : doPut: This requires HttpParams as the input.`);
        reject(new HttpResponse(null, constants.HTTP_BAD_REQUEST));
      }
    });

  }
};
