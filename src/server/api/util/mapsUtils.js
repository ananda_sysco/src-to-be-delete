/**
 * @author Tharuka Jayalath
 * (C) 2019, Sysco Labs
 * Created: 3/18/20. Wed 2020 17:16
 */

const { sign } = require('./cryptoUtils');
const { mapsApiUrl, mapsApiKey, mapsApiSecret } = require('../../config').getConfigs();

const configs = {
  apiUrl: mapsApiUrl,
  apiKey: mapsApiKey,
  secret: mapsApiSecret
};

const HEADER_MAP_SIZE = '1232x158';
const SIDE_MAP_SIZE = '351x164';
const mapType = 'roadmap';
const centerMarker = 'color:0xF2700F|';
const mapScale = 2;
const mapZoomLevel = 15;

const constantMapProperties = {
  mapType,
  mapScale,
  mapZoomLevel,
  centerMarker
};

const mapUriMetaData = [
  {uriName: 'sideMapURI', size: SIDE_MAP_SIZE},
  {uriName: 'headerMapURI', size: HEADER_MAP_SIZE},
  {uriName: 'mobileHeaderMapURI'}
];

/**
 * Generates a signed static map URI
 *
 * @param apiUrl  Google static map API URL
 * @param apiKey  Google static map API Key
 * @param secret  Google static map API secret
 * @param mapType Type of the map (Ex: road)
 * @param mapZoomLevel  Zoom Level of the map
 * @param mapScale  Scale of the map
 * @param centerMarker  Center marker of the map (Ex: color of the center marker)
 * @param mapSize Size of the map
 * @param location  lat and lng values included object
 * @returns {string}
 */
function generateSignedMapURI({ apiUrl, apiKey, secret },
                              { mapType, mapZoomLevel, mapScale, centerMarker },
                              mapSize, location) {
  const mapURI = `${apiUrl}/staticmap?size=${mapSize}&maptype=${mapType}
    &center=${location.lat},${location.lng}&scale=${mapScale}
    &zoom=${mapZoomLevel}&key=${encodeURIComponent(apiKey)}
    &markers=${encodeURIComponent(`${centerMarker}${location.lat},${location.lng}`)}`;

  return sign(mapURI, secret);
}

/**
 * Generates map URIs required for OLO FE loading
 *
 * @param mapSize Size of the map (Ex: 1232x158)
 * @param lat latitude of the location
 * @param lng longitude of the location
 * @returns {*}
 */
function generateMapURIs(mapSize, { lat, lng }) {
  mapUriMetaData[2].size = mapSize;
  const location = { lat: parseFloat(lat), lng: parseFloat(lng) };
  return mapUriMetaData.map(({ uriName, size }) => ({
   [uriName]: generateSignedMapURI(configs, constantMapProperties, size, location)
  })).reduce((acc, val) => ({ ...acc, ...val }), {});
}

module.exports = {
  generateMapURIs
};
