/**
 * Created by Salinda on 1/26/17.
 */
/**
 * Wraps all the data related to make an API call through
 * 'node-rest-client'.
 * API url, headers, and data wrap in this object  in order to make an API call.
 */

class HttpParams {
    constructor(url, headers, data) {
        this.url = url;
        this.headers = headers;
        this.data = data;
    }

    getUrl() {
        return this.url;
    }

    getHeaders() {
        return this.headers;
    }

    addHeaders(key, value) {
        if (this.headers) {
          this.headers[key] = value;
        } else {
          this.headers = { key: value };
        }
    }

    getData() {
        return this.data;
    }

}
module.exports = HttpParams;
