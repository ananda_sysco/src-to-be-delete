const config = require('../../config').getConfigs();
const request = require('request-promise-native'); //Cannot use http util since google API is not compatible with one of the headers
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();

module.exports = {

  validateCaptcha: function (captchaValue) {
    const endpoint = config.captchaEndpoint + '?secret=' + config.captchaSecret + '&response=' + captchaValue;
    const requestParam = {
      uri: endpoint,
      json: true
    };

    return new Promise((resolve, reject) => {
      request.get(requestParam).then(response => {
        if (!response.success) {
          logger.info(`Captcha Value' ${captchaValue}`);
          logger.error(`Error while validating captcha ${JSON.stringify(response)}`);
        }
        resolve(response.success);
      }).catch(err => {
        reject(err);
      });
    });

  }
};
