const setOrderAlreadyProcessed = (order) => {
  order.isAlreadyProcessed = true;
  return order;
}

module.exports = {
  setOrderAlreadyProcessed
}
