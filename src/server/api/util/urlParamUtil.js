/**
 * Created by Salinda on 12/5/17.
 */

const url = require("url");
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();


module.exports = {
  /**
   * Extracts data which sent as query params.
   * (Can be generic if this uses in other handlers)
   * @param request
   * @param paramName
   * @returns {*}
   */
  getQueryParamFromRequest: (file, request, paramName) => {
    let url_parts = url.parse(request.url, true);
    let query = url_parts.query;
    let param = query[paramName];
    logger.info(`${file} : getQueryParamFromRequest Parameter Name: ${paramName} Parameter Value: ${param}`);
    return param;
  },

  /**
   * Extracts data which sent through the request body.
   * (Can be generic if this uses in other handlers)
   * @param file
   * @param request
   * @param paramName
   * @param secure
   * @returns {*}
   */
  getDataFieldsFromRequest: (file, request, paramName, secure = false) => {

    let data = null;
    if (paramName) {
      data = request.body[paramName];
    } else {
      data = request.body;
    }
    let convertedToLog = null;
    //Hide the password from log
    if ("password" === paramName) {
      convertedToLog = '****';
    } else {
      convertedToLog = data;
    }
    if (!secure) {
      logger.info(file + ': getDataFieldsFromRequest Parameter Name: ', paramName, ' Parameter Value: ', convertedToLog);
    }
    return data;
  }
};
