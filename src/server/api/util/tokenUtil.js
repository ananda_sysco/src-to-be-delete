/**
 * Created by Salinda on 01/9/18.
 * Token related utility functionality reside here
 */
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const authRestUtil = require('../util/restUtil/authRestUtil');
const moment = require('moment');
const FILE = require('path').basename(__filename);
const jwtValidationUtil = require('./jwtValidationUtil');

const setBearerTokenToSession = (session, tokenData) => {
  session.token = tokenData.access_token;
  session.tokenExpiration = moment().add(tokenData.expires_in, 'seconds');
};

const setJwtTokenToSession = (session, jwtTokenData) => {
  logger.info(`${FILE} :  setJwtTokenToSession`);
  session.jwtToken = jwtTokenData.id_token;
  session.jwtTokenExpiration = moment().add(jwtTokenData.expires_in, 'seconds');
};

module.exports = {
  /**
   * Returns the bearer token stored in the session.
   * This could be a logged in user specific or guest user specific bearer token
   * @param session
   */

  getBearerTokenFromSession: (session) => {
    return session.token
  },

  requestAndSetBearerTokenToSession: async (session, jwtToken) => {
    logger.info(`${FILE} :  requestAndSetLoggedUserBearerTokenToSession`);
    const bearerTokenResponse = await authRestUtil.getUserSpecificBearerToken(jwtToken);

    // const bearerTokenResponse = await authRestUtil.getGuestUserBearerToken(); Get token with client credentials
    setBearerTokenToSession(session, bearerTokenResponse.data);
  },

  requestAndSetJwtAndBearerTokensToSession: async (session, username, password) => {
    const loginResponse = await authRestUtil.login(username, password);
    let jwtData = loginResponse.getData();

    if(!await jwtValidationUtil.validateToken(jwtData.id_token)){
      logger.error(`${FILE} : JWT validation failed. ${JSON.stringify(jwtData)}`);
      throw new Error('JWT validation failed.');
    }else{
      logger.info(`${FILE} : JWT validation success.`);
      logger.info(`${FILE} :  requestAndSetLoggedUserBearerTokenToSession requesting bearer token for user: ${username}`);
      const bearerTokenResponse = await authRestUtil.getUserSpecificBearerToken(jwtData.id_token);
      // TODO uncomment above line and remove the below line once JWT based bearer token is usable

      //const bearerTokenResponse = await authRestUtil.getGuestUserBearerToken();
      let bearerTokenData = bearerTokenResponse.data;

      logger.info(`${FILE} :  requestAndSetLoggedUserBearerTokenToSession storing jwt and bearer token for user: ${username}`);
      setJwtTokenToSession(session, jwtData);
      setBearerTokenToSession(session, bearerTokenData);
    }

  },

  requestBearerToken: async (username, password) => {
    const loginResponse = await authRestUtil.login(username, password);
    let jwtData = loginResponse.getData();
    logger.info(`${FILE} :  requestBearerToken requesting bearer token for user: ${username}`);
    const bearerTokenResponse = await authRestUtil.getUserSpecificBearerToken(jwtData.id_token);
    let bearerTokenData = bearerTokenResponse.data;
    logger.info(`${FILE} :  requestBearerToken success get bearer token for user: ${username}`);
    return bearerTokenData;
  },

  setBearerTokenToSession,

  setJwtTokenToSession,

  isTokenExpired: (token, tokenExpiration) => {
    let updateNeeded = false;
    const now = moment();
    if (!token || now.isAfter(tokenExpiration)) {
      updateNeeded = true;
    }
    return updateNeeded;
  }
};
