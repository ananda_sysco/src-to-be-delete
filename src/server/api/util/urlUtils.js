/**
 * Created by Salinda on 12/5/17.

 Utility class for url manipulation.
 */
const config = require('../../config').getConfigs();
/**
 * Check whether the given map is empty
 * @param inputMap
 * @returns {boolean}
 */
let isMapEmpty = (inputMap) => {
    let isEmpty = false;
  if (inputMap == null || inputMap.size === 0) {
        isEmpty = true;
    }
    return isEmpty;
};

/**
 * Replaces matching content with given content
 * @param inputString
 * @param toFind
 * @param toReplace
 * @returns {*}
 */
let stringReplaceMatchingContent = (inputString, toFind, toReplace)=> {
    let returnString = inputString;
    if (returnString != null && toFind != null && toReplace != null) {
        returnString = returnString.replace(new RegExp(toFind, 'g'), toReplace)
    }
    return returnString;
};

/**
 * Replaces given string's occurrences of keys with values of provided keyValueMap
 * @param inputString
 * @param keyValueMap
 * @returns {*}
 */
let replaceKeysWithValues = (inputString, keyValueMap)=> {
    let updatedString = inputString;
    if (!isMapEmpty(keyValueMap) && inputString != null) {
        for (let [key, value] of keyValueMap.entries()) {
            if (updatedString.includes(key)) {
                /*Has a matching path variable*/
                updatedString = stringReplaceMatchingContent(updatedString, key, value);
            }

        }
    }
    return updatedString;
};

/**
 * Adds key values of keyValueMap to the input string as a url query string
 * @param inputString
 * @param keyValueMap
 * @returns {*}
 */
let addQueryParams = (inputString, keyValueMap)=> {
    let updatedString = inputString;
    if (!isMapEmpty(keyValueMap) && inputString != null) {
        let iterationCount = 1;
        updatedString += '?';
        for (let [key, value] of keyValueMap.entries()) {
            updatedString += key + '=' + value;
          if (keyValueMap.size !== iterationCount) {
                updatedString += '&';
            }
            iterationCount++;
        }
    }
    return updatedString;
};

module.exports = {
    /**
     * Construct complete url with given endpoint url
     * @param endPoint
     * @returns {*}
     */
    constructEndPointUrl: (endPoint)=> {
        return  config.esbUrl + endPoint;
    },

    /**
     * Constructs an endpoint url by replacing path variables with given values as pathVariableMap.
     * This method is able to provide this functionality for complete url or endpoint on its own
     * @param endPoint
     * @param pathVariableMap
     * @returns {*}
     */
    updateEndpointPathVariables: (endPoint, pathVariableMap)=> {
        let updatedEndpoint = endPoint;
        if (!isMapEmpty(pathVariableMap)) {
            updatedEndpoint = replaceKeysWithValues(endPoint, pathVariableMap);
        }
        return updatedEndpoint;
    },

    /**
     * Constructs an endpoint url by adding query params given as pathVariableMap.
     * This method is able to provide this functionality for complete url or endpoint on its own
     * @param endPoint
     * @param queryParamMap
     * @returns {*}
     */
    updateEndpointQueryParams: (endPoint, queryParamMap)=> {
        let updatedEndpoint = endPoint;
        if (queryParamMap !==null && queryParamMap !== undefined && !isMapEmpty(endPoint)) {
            updatedEndpoint = addQueryParams(endPoint, queryParamMap)
        }
        return updatedEndpoint;
    }
};
