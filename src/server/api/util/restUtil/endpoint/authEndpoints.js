/**
 * Created by PrabhhathP on 12/5/17.
 */
const GET_TOKEN = '/oauth/token';
const LOGIN = '/api-v3/service/customers/login';
const RESET_PWD_TOKEN = '/idm/admin/realms/consumers/users/token/token-param';
const GET_AUTH_KEYS = '/idm/realms/consumers/protocol/openid-connect/certs';


module.exports = {
  GET_TOKEN,
  LOGIN,
  RESET_PWD_TOKEN,
  GET_AUTH_KEYS
};
