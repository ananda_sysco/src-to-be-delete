/**
 * Created by Salinda on 12/5/17.
 */
const MERCHANT_INFO_END_POINT = '/api-v3/service/accounts/account-id/profiles';
const RECEIPT_INFO_END_POINT = '/api-v3/service/customer/receipt/receipt-id';
const CALCULATE_TAX_END_POINT = '/api-v3/service/tax';
const CREATE_ORDER_END_POINT = '/api-v3/service/accounts/account-id/orders';
const GET_USER_FROM_EMAIL = '/api-v3/service/customers/emails/email-param';
const RESET_PASSWORD_EMAIL = '/api-v3/service/customers/actions/reset-password';
const CREATE_USER_END_POINT = '/api-v3/service/customers';
const UPDATE_CUSTOMER_END_POINT = '/api-v3/service/customers/customer-id';
const GET_USER_FROM_PHONE = '/api-v3/service/customers/phones/phoneNumber';
const RESET_PASSWORD = '/api-v3/service/customers/actions/confirm-reset-password';
const GET_WALLET_DATA = '/api-v3/service/cards/balance';
const GET_ORDER_HISTORY = '/api-v3/service/customers/user-id-param/orders';
const GET_OPERATORS = '/api-v3/service/accounts/search';
const GET_ACCOUNT_INFO = '/api-v3/service/merchants/merchant-id/profiles';

module.exports = {
  MERCHANT_INFO_END_POINT,
  RECEIPT_INFO_END_POINT,
  CALCULATE_TAX_END_POINT,
  CREATE_ORDER_END_POINT,
  GET_USER_FROM_EMAIL,
  RESET_PASSWORD_EMAIL,
  CREATE_USER_END_POINT,
  UPDATE_CUSTOMER_END_POINT,
  GET_USER_FROM_PHONE,
  RESET_PASSWORD,
  GET_WALLET_DATA,
  GET_ORDER_HISTORY,
  GET_OPERATORS,
  GET_ACCOUNT_INFO
};
