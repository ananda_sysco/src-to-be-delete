/**
 * Created by PrabhathP on 12/5/17.
 */
const SESSION_END_POINT = '/api-v3/service/accounts/account-id/sessions';
const CATALOG_END_POINT = '/api-v3/service/accounts/account-id/catalog';
const CATALOG_SESSION_END_POINT = '/api-v3/service/accounts/account-id/sessions/session-id/catalog';

module.exports = {
  SESSION_END_POINT,
  CATALOG_END_POINT,
  CATALOG_SESSION_END_POINT
};
