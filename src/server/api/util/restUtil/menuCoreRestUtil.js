/**
 * Created by PrabhathP on 12/5/17.
 */
const menuEndPoints = require('./endpoint/menuApiEndpoints');
const apiConstants = require('../../constant/apiConstants');
const httpConstants = require('../../constant/httpConstants');
const HttpResponse = require('../httpResponse');
const httpUtil = require('../httpUtil');
const nodeRuntimeUtil = require('util');
const { frameworkLogger } = require('../../../logger');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const commonUtils = require('../commonUtil');


module.exports = {

  /**
   * Returns restaurant info.
   * @param merchantId
   * @param accessToken
   * @returns {Promise}
   */

  getSessions: (accountID, accessToken) => {
    logger.info(`${FILE} : getSessions called for ${accountID}`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_ACCOUNT_ID, accountID]]);
    const restaurantInfoHttpParams = commonUtils.constructConsumerEndpointParams(menuEndPoints.SESSION_END_POINT, commonPathParamMap, accessToken);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(restaurantInfoHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getSessions Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getSessions ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  getSessions: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  /**
   * Returns catalog info.
   * @param accountId
   * @param sessionId
   * @param accessToken
   * @returns {Promise}
   */
  getCatalog: (accountId, sessionId, accessToken) => {
    logger.info(`${FILE} : getCatalog called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_ACCOUNT_ID, accountId], [apiConstants.PARAM_SESSION_ID, sessionId]]);
    const catalogHttpParams = commonUtils.constructConsumerEndpointParams(menuEndPoints.CATALOG_SESSION_END_POINT, commonPathParamMap, accessToken, undefined);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(catalogHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getCatalog Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getCatalog' ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  getCatalog: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  }
};
