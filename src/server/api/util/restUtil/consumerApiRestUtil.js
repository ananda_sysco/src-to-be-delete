/**
 * Created by Salinda on 12/5/17.
 */
const consumerApiEndPoints = require('./endpoint/consumerApiEndpoints');
const apiConstants = require('../../constant/apiConstants');
const httpConstants = require('../../constant/httpConstants');
const HttpResponse = require('../httpResponse');
const httpUtil = require('../httpUtil');
const nodeRuntimeUtil = require('util');
const { frameworkLogger } = require('../../../logger');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const commonUtils = require('../commonUtil');
const { NAME_IDEMPOTENCY_HEADER } = require('../../constant/miscConstants');
const config = require('../../../../server/config').getConfigs();


module.exports = {
  /**
   * Returns restaurant info.
   * @param accountId
   * @param accessToken
   * @returns {Promise}
   */

  getRestaurantInfo: (accountId, accessToken) => {
    logger.info(`${FILE} : getRestaurantInfo called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_ACCOUNT_ID, accountId]]);
    const restaurantInfoHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.MERCHANT_INFO_END_POINT, commonPathParamMap, accessToken, undefined);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(restaurantInfoHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getRestaurantInfo Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getRestaurantInfo ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  getRestaurantInfo: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  /**
   * Returns receipt info.
   * @param receiptId
   * @param accessToken
   * @returns {Promise}
   */

  getReceiptInfo: (receiptId, accessToken) => {
    logger.info(`${FILE} : getReceiptInfo called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_RECEIPT_ID, receiptId]]);
    const receiptInfoHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.RECEIPT_INFO_END_POINT, commonPathParamMap, accessToken, undefined);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(receiptInfoHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getReceiptInfo Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getReceiptInfo ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  getReceiptInfo: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  /**
   * Calculate Tax
   * @param data
   * @param accessToken
   * @returns {Promise}
   */
  calculateTax: (data, accessToken) => {
    logger.info(`${FILE} : calculateTax called`);
    const productHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.CALCULATE_TAX_END_POINT, null, accessToken, data);
    return new Promise((resolve, reject) => {
      return httpUtil.doPost(productHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : calculateTax Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in calculateTax ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  calculateTax: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  /**
   * Create Order
   * @param accountId
   * @param data
   * @param accessToken
   * @param clientHeaders
   * @param idempotencyHeaderValue
   * @returns {Promise}
   */
  createOrder: (accountId, data, accessToken, clientHeaders, idempotencyHeaderValue) => {
    logger.info(`${FILE} : createOrder called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_ACCOUNT_ID, accountId]]);
    const orderHttpParams = commonUtils
      .constructConsumerEndpointParams(
        consumerApiEndPoints.CREATE_ORDER_END_POINT,
        commonPathParamMap,
        accessToken,
        data,
        null,
        clientHeaders
      );

    if (config.shouldEnableIdempotencyHeader && idempotencyHeaderValue) {
      orderHttpParams.addHeaders(NAME_IDEMPOTENCY_HEADER, idempotencyHeaderValue);
    }

    return new Promise((resolve, reject) => {
      return httpUtil.doPost(orderHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : createOrder Success`);
          return resolve(response);
        } else if (response.statusCode === httpConstants.HTTP_LOCKED) {
          logger.info(`${FILE} : createOrder conflict`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in createOrder ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  createOrder: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  getUser: (email, accessToken) => {
    logger.debug(`${FILE} : getUser called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_USER_EMAIL, email]]);
    const emailHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_USER_FROM_EMAIL, commonPathParamMap, accessToken, null);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(emailHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  getUser: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getPhone: (phone, accessToken) => {
    logger.debug(`${FILE} : getPhone called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_PHONE, phone]]);
    const emailHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_USER_FROM_PHONE, commonPathParamMap, accessToken, null);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(emailHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  getPhone: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  sendResetPasswordEmail: (email, redirectUrl, accessToken) => {
    logger.info(`${FILE} : send reset password email called`);
    const data = {
      email,
      redirectUrl
    };
    const emailHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.RESET_PASSWORD_EMAIL, null, accessToken, data);
    return new Promise((resolve, reject) => {
      return httpUtil.doPost(emailHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  sending reset password email: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  createUser: (user, accessToken) => {
    logger.info(`${FILE} : create user called`);
    const userHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.CREATE_USER_END_POINT, null, accessToken, user);
    return new Promise((resolve, reject) => {
      return httpUtil.doPost(userHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in creating user`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      })
    });
  },

  updateUser: (customer, accessToken) => {
    logger.info(`${FILE} : update user called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_CUSTOMER_ID, customer.id]]);
    const updateUserHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.UPDATE_CUSTOMER_END_POINT, commonPathParamMap, accessToken, customer);
    return new Promise((resolve, reject) => {
      return httpUtil.doPut(updateUserHttpParams).then((response) => {
        return resolve(response);

      }).catch((err) => {
        logger.error(`${FILE} : error in  updateUser: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  resetPassword: (email, password, accessToken, token) => {
    logger.info(`${FILE} : reset password called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_USER_EMAIL, email]]);
    const data = {
      value: password,
      email,
      token
    };
    const pwdHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.RESET_PASSWORD, commonPathParamMap, accessToken, data);
    return new Promise((resolve, reject) => {
      return httpUtil.doPost(pwdHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  resetPassword: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getWalletData: (cardNo, pin, accessToken) => {
    let printedNumber;
    let maskedValue = '';
    if (cardNo) {
      printedNumber = cardNo.replace(/\s/g, ''); //Remove whitespace
    }

    for (let i = 0; i < printedNumber.length; i++) {
      if (i < 4 || i >= printedNumber.length - 4) {
        maskedValue += printedNumber.charAt(i);
      } else {
        maskedValue += 'x';
      }
    }

    const data = {
      pin,
      printedNumber
    };

    const pwdHttpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_WALLET_DATA, null, accessToken, data);
    logger.info(`${FILE} : getWalletData called for ${maskedValue}`);
    return new Promise((resolve, reject) => {
      return httpUtil.doPost(pwdHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  getWalletData: ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getOrderHistory: (userId, accessToken) => {
    logger.debug(`${FILE} : getOrderHistory called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_USER_ID, userId]]);
    const httpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_ORDER_HISTORY, commonPathParamMap, accessToken, null);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(httpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  getOrderHistory:  ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getOperatorsList: (source, accessToken) => {
    logger.info(`${FILE} : getOperatorsList called`);
    const commonPathParamMap = new Map([[]]);
    const queyParamMap = new Map([[apiConstants.PARAM_SEARCH_SOURCE, source]]);
    const getOperatorsListParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_OPERATORS, commonPathParamMap, accessToken, undefined, queyParamMap);

    return new Promise((resolve, reject) => {
      return httpUtil.doGet(getOperatorsListParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getRestaurantInfo Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getOperatorsList ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);
      }).catch(err => {
        logger.error(`${FILE} : error in  getOperatorsList: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  getAccountInfo: (merchantId, accessToken) => {
    logger.info(`${FILE} : getAccountInfo called for ${merchantId}`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_MERCHANT_ID, merchantId]]);
    const httpParams = commonUtils.constructConsumerEndpointParams(consumerApiEndPoints.GET_ACCOUNT_INFO,
      commonPathParamMap, accessToken, null, null);

    return new Promise((resolve, reject) => {
      return httpUtil.doGet(httpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getAccountInfo Success`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getAccountInfo ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);
      }).catch(err => {
        logger.error(`${FILE} : error in  getAccountInfo: ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  }

};
