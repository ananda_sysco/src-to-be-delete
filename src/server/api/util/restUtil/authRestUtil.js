/**
 * Created by PrabhathP on 12/5/17.
 */
const { get } = require('lodash');
const authEndPoints = require('./endpoint/authEndpoints');
const httpConstants = require('../../constant/httpConstants');
const apiConstants = require('../../constant/apiConstants');
const HttpParams = require('../httpParams');
const HttpResponse = require('../httpResponse');
const httpUtil = require('../httpUtil');
const nodeRuntimeUtil = require('util');
const { frameworkLogger } = require('../../../logger');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const config = require('../../../config').getConfigs();
const urlUtils = require('../urlUtils');
const commonUtils = require('../commonUtil');

const constructBasicAuthHeader = (endPoint, data) => {
  const endpointUrl = urlUtils.constructEndPointUrl(endPoint);
  //TODO Update the 'User-Agent' with appropriate value
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: 'Basic ' + config.BASIC_AUTH_TOKEN
  };

  return new HttpParams(endpointUrl, headers, data);
};

const getBearerToken = (requestBody) => {
  const httpParams = constructBasicAuthHeader(authEndPoints.GET_TOKEN, requestBody);

  return new Promise((resolve, reject) => {
    return httpUtil.doPost(httpParams).then(response => {
      if (response.statusCode === httpConstants.HTTP_OK) {
        logger.info(FILE + ': getBearerToken Success');
        return resolve(get(response, 'data.access_token', null));
      }
      logger.error(FILE + ': error in getBearerToken:' + nodeRuntimeUtil.inspect(response));
      return reject(response);

    }).catch(err => {
      logger.error(FILE + ': error in  getUserSpecificBearerToken for user: ' + nodeRuntimeUtil.inspect(err));
      return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  });
};

module.exports = {
  /**
   *Fetches user specific bearer token by using id token which received upon the login.
   * This bearer token will be used for access platform APIS
   * @param userIdToken
   */
  getUserSpecificBearerToken: (userIdToken) => {

    logger.info(`${FILE} : getUserSpecificBearerToken called for user`);
    //TODO change the client id client_id once client id is defined
    const requestBody = 'grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer&client_id=consumer-app&assertion=' +
      userIdToken + '&scope=trust';

    let headers = {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + config.BASIC_AUTH_TOKEN
    };

    let httpParams = new HttpParams(urlUtils.constructEndPointUrl(authEndPoints.GET_TOKEN), headers, requestBody);

    return new Promise((resolve, reject) => {
      return httpUtil.doPost(httpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : getUserSpecificBearerToken token fetch success`);
          logger.debug(`getUserSpecificBearerToken token : ${JSON.stringify(response)}`);
          return resolve(response);
        }
        logger.error(`${FILE} : error in getUserSpecificBearerToken token fetch ${nodeRuntimeUtil.inspect(response)}`);
        return reject(response);

      }).catch(err => {
        logger.error(`${FILE} : error in  getUserSpecificBearerToken token fetch ${nodeRuntimeUtil.inspect(err)}`);
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
      });
    });
  },

  /**
   *
   * @param username
   * @param password
   */
  login: async (username, password) => {
    logger.info(`${FILE} : login called`);
    const urlEncodedRequestBody = `client_id=${config.audience}&scope=trust&grant_type=client_credentials`;
    const accessToken= await getBearerToken(urlEncodedRequestBody);
    const data = {
      userName: username,
      password
    };
    const loginHttpParams = commonUtils.constructConsumerEndpointParams(authEndPoints.LOGIN, new Map(), accessToken, data, null);

    return new Promise((resolve, reject) => {
      return httpUtil.doPost(loginHttpParams).then(response => {
        if (response.statusCode === httpConstants.HTTP_OK) {
          logger.info(`${FILE} : login Success`);
          return resolve(response);
        } else {
          logger.error(`${FILE} : login Failed for user : ${username}`);
          return reject(response);
        }
      }).catch(err => {
        logger.error(`${FILE} : error in login user: ${username} ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getUserByToken: (token, accessToken) => {
    logger.debug(`${FILE} : getUserByToken called`);
    const commonPathParamMap = new Map([[apiConstants.PARAM_RESET_PWD_TOKEN, token]]);
    const tokenHttpParams = commonUtils.constructConsumerEndpointParams(authEndPoints.RESET_PWD_TOKEN, commonPathParamMap, accessToken, null);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(tokenHttpParams).then((response) => {
        return resolve(response);
      }).catch((err) => {
        logger.error(`${FILE} : error in  getUser by token:  ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  },

  getJWTValidationKeys: () => {
    logger.info(`${FILE} : getJWTValidationKeys called`);

    const headers = {
      'Accept': 'application/json'
    };
    const httpParams = new HttpParams(urlUtils.constructEndPointUrl(authEndPoints.GET_AUTH_KEYS), headers);
    return new Promise((resolve, reject) => {
      return httpUtil.doGet(httpParams).then(response => {
        return resolve(response.data);
      }).catch(err => {
        logger.error(`${FILE} : error in getJWTValidationKeys : ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return reject(err);
        }
        return reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    });
  }
};
