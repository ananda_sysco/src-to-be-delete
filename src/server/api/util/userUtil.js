/**
 * Created by Umesh on 01/18/18.
 * User related utility functionality reside here
 */
const setLoggedUserStatusToSession = (session, isLoggedUser) => {
  session.isLoggedUser = isLoggedUser;
}

module.exports = {
  setLoggedUserStatusToSession
}
