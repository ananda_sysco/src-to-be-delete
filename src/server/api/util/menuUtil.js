/**
 * Created by Umesh on 12/22/17.
 */

const {map, keyBy, forEach, filter} = require('lodash');
const uuid = require('uuid');
const PerformanceLogger = require('../util/devUtil/performanceLogger');
/*
  **  sessionObject = {sessions, ...}
  **  return array of session objects in the form
  **  [{id, name, sessionhours: {ranges: []}}, ...]
*/
const sessionMapper = (sessionObject) => {

  const performanceLogger = new PerformanceLogger('Session Mapping');

  const mappedResponse = map(sessionObject.sessions, (session) => {

    const mappedSession = {
      id: session.id,
      name: session.name,
      sessionhours: {ranges: []}
    };

    mappedSession.sessionhours.ranges = map(session.slots, (slot) => {
      const start = slot.startTime;
      const end = slot.endTime;
      const day = String(slot.dayOfWeek);
      const isavailable = slot.isActive;
      return {day, start, end, isavailable};
    });

    return mappedSession;
  });

  performanceLogger.log();
  return mappedResponse;
};

/*
  ** private method to create the product object from a given product
 */
const createProduct = (item, merchantId, modifierGroups, modifierOptions, variations, isSpecialInstruction) => {
  let attributeSet = null;
  if (modifierGroups && modifierOptions) {
    const attributeArray = [];
    forEach(item.modifierGroups, (modifier) => {
      const attribute = {};
      const modifierGroup = modifierGroups[modifier.id];
      attribute["id"] = modifierGroup["id"];
      attribute["name"] = modifierGroup["name"];
      attribute["minSelections"] = (modifier.min || modifier.min === 0) ? modifier.min : modifierGroup["min"];
      attribute["maxSelections"] = (modifier.max || modifier.max === 0) ? modifier.max : modifierGroup["max"];

      const values = map(modifierGroup.modifierOptions, (option) => {
        const modifierOption = modifierOptions[option.id];
        const resultValue = {attributeValueId: option.id, value: modifierOption ? modifierOption.name : ""};
        if (modifierOption && modifierOption.price) {
          resultValue["price"] = modifierOption.price;
        }
        return resultValue;
      });

      attribute["values"] = {value: values};

      attributeArray.push(attribute);
    });

    if (isSpecialInstruction) {
      // add the special instruction
      attributeArray.push({
        id: "1",
        name: "Special Instructions",
        maxSelections: 0,
        minSelections: 0,
        values: {value: []}
      });
    }

    attributeSet = {
      id: uuid.v1(), // NOT FOUND, so generate a one
      name: "Default", // NOT FOUND
      attributes: {
        attribute: attributeArray
      }
    };
  }


  let price = null;
  let variationId = null;
  if (item.variations && item.variations.length > 0) {
    // iterate through all the variations and find the default variation
    const defaultVariations = filter(item.variations, (variation) => {
      const currentVariation = variations[variation.id];
      return currentVariation && currentVariation.isDefault
    });
    price = defaultVariations.length > 0 ? defaultVariations[0].price : null;
    variationId = defaultVariations[0].id;
  }

  const tempName = item.receiptName ? item.receiptName : item.name;

  const resultProduct = {
    id: item.id,
    name: tempName || "",
    description: item.description || "",
    merchantId: merchantId,
    reference: item.barcode || "",
    price: price,
    kitchenName: item.kitchenName || "",
    receiptName: item.receiptName || "",
    taxCategoryId: item.taxId || "",
    variationId: variationId,
    isAuxiliary: false, // THIS IS MISSING
  };

  if (attributeSet) {
    resultProduct["attributeSet"] = attributeSet;
  }

  return resultProduct;
};

/*
  **  catalogResponse = {items, categories, ...}
  **  return categories in the form
  **  {categories: {category:[]}}
*/
const catalogMapper = (catalogResponse, merchantId) => {
  const performanceLogger = new PerformanceLogger('Catalog Mapping');

  const result = {
    data: {
      categories: {
        category: []
      }
    },
    statusCode: 200
  };

  if (catalogResponse.sessions && catalogResponse.sessions.length > 0) {
    // calculating only for the 1st session
    const session = catalogResponse.sessions[0];

    // create object mapping to categories
    const categoryCache = keyBy(catalogResponse.categories, 'id');
    const productCache = keyBy(catalogResponse.items, 'id');
    const modifierGroups = keyBy(catalogResponse.modifierGroups, 'id');
    const modifierOptions = keyBy(catalogResponse.modifierOptions, 'id');
    const variations = keyBy(catalogResponse.variations, 'id');

    const getProductsById = (productId) => {
      const product = productCache[productId];
      if (product) {
        return createProduct(product, merchantId, modifierGroups, modifierOptions, variations, true);
      }
    };

    const resultCategories = [];
    forEach(session.categoryIds, (id) => {
      const categoryFromCache = categoryCache[id];
      if (categoryFromCache) {
        // category is in cache (it should be, otherwise id is wrong)
        const resultCategory = {
          id,
          name: categoryFromCache.name || "",
          description: categoryFromCache.description || "",
          merchantId: merchantId,
          sessionId: session.id,
          isActive: categoryFromCache.isActive,
          lineNo: null, // THIS IS MISSING
          subCategories: {
            category: map(categoryFromCache.childrenIds, (childId) => {
              const childCategory = categoryCache[childId];

              // find products
              const products = map(childCategory.itemIds, getProductsById);

              // create the resulting category
              const category = {
                id: childId,
                name: childCategory.name || "",
                description: childCategory.description || "",
                merchantId: merchantId,
                sessionId: session.id,
                isActive: childCategory.isActive,
                lineNo: null, //THIS IS MISSING
                parentId: id
              };

              if (products.length > 0) {
                category['products'] = {
                  product: products
                };
              }

              return category;
            })
          },
        };

        const products = map(categoryFromCache.itemIds, getProductsById);
        if (products.length > 0) {
          resultCategory["products"] = {
            product: products
          };
        }

        resultCategories.push(resultCategory);

      }
    });
    // append to the returning object
    result.data.categories.category = resultCategories;
  }

  performanceLogger.log();
  return result.data;
};

module.exports = {catalogMapper, sessionMapper};
