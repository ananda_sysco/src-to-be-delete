/*
 * Created by Tharuka Jayalth 12/14/2017
 */
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const urlParamUtil = require('../util/urlParamUtil');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const menuCoreRestUtil = require('../util/restUtil/menuCoreRestUtil');
const nodeRuntimeUtil = require('util');
const FILE = require('path').basename(__filename);
const apiConstants = require('../constant/apiConstants');
const menuUtil = require('../util/menuUtil');
const tokenUtil = require('../util/tokenUtil');
const consumerAPIRestUtil = require('../util/restUtil/consumerApiRestUtil');
const config = require('../../config').getConfigs();


const getSessions = async (session, accountId) => {
  const authToken = await tokenUtil.getBearerTokenFromSession(session);
  return menuCoreRestUtil.getSessions(accountId, authToken).then((response) => {
    logger.info(`${FILE} : Success  getSessions: ${accountId}`);
    if (response.statusCode === httpConstants.HTTP_OK) {
      return Promise.resolve(menuUtil.sessionMapper(response.data));
    } else {
      return Promise.reject(response);
    }
  }).catch(err => {
    logger.error(`${FILE} : error in  getSessions: ${accountId} ${nodeRuntimeUtil.inspect(err)}`);
    if (err instanceof HttpResponse) {
      return Promise.reject(err);
    }
    return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
  });
};

const handleGetCatalog = async (request) => {
  const accountID = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_ACCOUNT_ID);
  const sessionId = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_SESSION_ID);
  const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
  return menuCoreRestUtil.getCatalog(accountID, sessionId, authToken).then((response) => {
    logger.info(`${FILE} : Success  handleGetCatalog: ${accountID}`);
    return Promise.resolve(new HttpResponse(menuUtil.catalogMapper(response.data, accountID), response.statusCode, null));
  }).catch(err => {
    logger.error(`${FILE} : error in  handleGetCatalog: ${accountID} ${nodeRuntimeUtil.inspect(err)}`);
    if (err instanceof HttpResponse) {
      return Promise.reject(err);
    }
    return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
  });
};

const getPreview = async (request) => {
  const accountId = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_ACCOUNT_ID);
  const authToken = await tokenUtil.requestBearerToken(config.proxyUser, config.proxyUserPassword);

  return consumerAPIRestUtil.getRestaurantInfo(accountId, authToken.access_token).then(response => {
    if (response.statusCode !== httpConstants.HTTP_OK) {
      return Promise.reject(response);
    }
    const restaurantInfo = response.data;
    logger.info(`${FILE} : success getPreview: ${accountId}`);
    if (restaurantInfo.isCakeOloAvailable || (!restaurantInfo.isCakeOloAvailable && !restaurantInfo.isOnlineOrderAvailable)) {
      return Promise.resolve(new HttpResponse(`/${accountId}`, httpConstants.HTTP_REDIRECT, null));
    }
    return Promise.resolve(new HttpResponse(config.legacyOLOUrl + restaurantInfo.id, httpConstants.HTTP_REDIRECT, null));
  }).catch(err => {
    logger.error(`${FILE} : error in getPreview: ${accountId} ${nodeRuntimeUtil.inspect(err)}`);
    if (err instanceof HttpResponse) {
      return Promise.reject(err);
    }
    return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
  });
};

module.exports = {getSessions, handleGetCatalog, getPreview};
