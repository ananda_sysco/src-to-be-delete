/**
 * Created by Salinda on 12/5/17.
 */
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const urlParamUtil = require('../util/urlParamUtil');
const FILE = require('path').basename(__filename);
const authRestUtil = require('../util/restUtil/authRestUtil');
const tokenUtil = require('../util/tokenUtil');
const nodeRuntimeUtil = require('util');
const userUtil = require('../util/userUtil');
const apiConstants = require('../constant/apiConstants');
const httpConstants = require('../constant/httpConstants');
const HttpResponse = require('../util/httpResponse');
const consumerApiRestUtil = require('../util/restUtil/consumerApiRestUtil');
const jwt = require('jsonwebtoken');

module.exports = {

  handleLogin: async (request) => {
    const username = urlParamUtil.getDataFieldsFromRequest(FILE, request, apiConstants.DATA_PARAM_USERNAME);
    const password = urlParamUtil.getDataFieldsFromRequest(FILE, request, apiConstants.DATA_PARAM_PASSWORD);
    const session = request.session;

    await tokenUtil.requestAndSetJwtAndBearerTokensToSession(session, username, password);
    logger.debug(`${FILE} : session set ${session}`);

    userUtil.setLoggedUserStatusToSession(session, true);
    const decodedJwt = jwt.decode(session.jwtToken);

    if (decodedJwt) {
      const email = decodedJwt.email;
      const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
      return consumerApiRestUtil.getUser(email, authToken).then((response) => {
        logger.info(`${FILE} :  Success fetching user by email ( ${email} ) handleLogin:`);
        if (httpConstants.HTTP_OK === response.statusCode) {
          request.session.user = response.data;
          return Promise.resolve(response);
        } else {
          return Promise.reject(response);
        }
      }).catch(err => {
        logger.error(`${FILE} : Error fetching user by email ( ${email} )  handleLogin: ${email} ${nodeRuntimeUtil.inspect(err)}`);
        if (err instanceof HttpResponse) {
          return Promise.reject(err);
        }
        return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
      });
    }

  },


  handleSignUpUser: async (request) => {
    const user = urlParamUtil.getDataFieldsFromRequest(FILE, request, null);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return consumerApiRestUtil.createUser(user, authToken).then((response) => {
      if (httpConstants.HTTP_OK === response.statusCode) {
        return Promise.resolve(response);
      } else {
        return Promise.reject(response);
      }
    }).catch((err) => {
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  },

  handleLoggedInUser: (request) => {
    try {
      const loggedInUser = request.session.user;
      if (loggedInUser === undefined) {
        return Promise.resolve(new HttpResponse({user: null}, httpConstants.HTTP_OK, null));
      }
      return Promise.resolve(new HttpResponse({user: loggedInUser}, httpConstants.HTTP_OK, null));
    } catch (e) {
      return Promise.resolve(new HttpResponse({user: null}, httpConstants.HTTP_OK, null));
    }
  },


  /**
   * Check whether a user exist in the system
   * @param request
   * @param tokenParam
   * @returns {Promise<void>}
   */
  checkPasswordResetToken: async (request, tokenParam) => {
    let token = tokenParam;
    if (!tokenParam) {
      token = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_RESET_PWD_TOKEN);
    }
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return authRestUtil.getUserByToken(token, authToken).then((response) => {
      logger.debug(`${FILE} : Success  checkPasswordResetToken:`);
      if (response.statusCode !== 200) {
        if (response.data && response.data.code === "40200") {
          return Promise.resolve(new HttpResponse({
            'isValid': false,
            'message': "Password reset token expired"
          }, 200, null));
        } else if (response.data && response.data.code === "40060") {
          return Promise.resolve(new HttpResponse({
            'isValid': false,
            'message': "Password reset requested user not found"
          }, 200, null));
        }
        else {
          logger.error(`${FILE} : error in checkPasswordResetToken for given token with the response: ${response}`);
          return Promise.reject(response);
        }
      }
      return Promise.resolve(new HttpResponse({
        'isValid': true,
        'data': (response.data ? response.data : null)
      }, 200, null));
    }).catch(err => {
      logger.error(`${FILE} : error in checkPasswordResetToken for given token with the error: ${nodeRuntimeUtil.inspect(err)}`);
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  },

  handleLogout: (request, response) => {
    request.session.destroy(function (err) {
      // will have a new session here
      logger.info('Logged out successfully');
      if (err) {
        response.sendStatus(httpConstants.HTTP_INTERNAL_SERVER_ERROR);
      } else {
        response.sendStatus(httpConstants.HTTP_OK);
      }
    })
  }

};
