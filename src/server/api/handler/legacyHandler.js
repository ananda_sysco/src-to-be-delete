/**
 * Created by umeshj on 19/08/19.
 */
const { get } = require('lodash');
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const FILE = require('path').basename(__filename);
const httpConstants = require('../constant/httpConstants');
const HttpResponse = require('../util/httpResponse');
const apiConstants = require('../constant/apiConstants');

module.exports = {

  handleReceipt: (request) => {
    const receiptId = get(request.params, apiConstants.PATH_PARAM_RECEIPT_ID, null);
    if (receiptId !== null) {
      logger.info(`${FILE} : Receipt id found in the url path to redirect : ${receiptId}`);
      return new HttpResponse(`/receipt/${receiptId}`, httpConstants.HTTP_REDIRECT, null);
    }
    return new HttpResponse('/', httpConstants.HTTP_REDIRECT, null);
  }

};
