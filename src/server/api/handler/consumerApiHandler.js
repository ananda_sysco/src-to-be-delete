/**
 * Created by Salinda on 12/5/17.
 */
const { frameworkLogger } = require('../../logger');
const logger = frameworkLogger.getDefaultLogger();
const urlParamUtil = require('../util/urlParamUtil');
const HttpResponse = require('../util/httpResponse');
const httpConstants = require('../constant/httpConstants');
const { headerMetadata } = require('../constant/miscConstants');
const { extractHeaders } = require('../util/commonUtil');
const consumerAPIRestUtil = require('../util/restUtil/consumerApiRestUtil');
const tokenUtil = require('../util/tokenUtil');
const {
  updateSession,
  generateIdempotencyHeaderValue,
  getValueByKeyFromSession,
  updateSessionWithIdempotencyDetails
} = require('../util/commonUtil');
const apiConstants = require('../constant/apiConstants');
const nodeRuntimeUtil = require('util');
const FILE = require('path').basename(__filename);
const menuHandler = require('./menuHandler');
const menuUtil = require('../util/menuUtil');
const mapsUtils = require('../util/mapsUtils');
const orderUtil = require('../util/orderUtil');
const config = require('../../config').getConfigs();
const {
  KEY_IDEMPOTENCY_DETAILS,
  KEY_IDEMPOTENCY_HEADER
} = require('../constant/miscConstants');

module.exports = {

  handleGetRestaurantInfo: async (request) => {

    const accountId = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_ACCOUNT_ID);
    const mobileMapSize = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_MOBILE_MAP_SIZE);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    const session = await menuHandler.getSessions(request.session, accountId);

    return consumerAPIRestUtil.getRestaurantInfo(accountId, authToken).then((response) => {
      logger.info(FILE + ': Success  handleGetRestaurantInfo:' + accountId);
      response.data.sessions = {session};
      if (mobileMapSize && response.data.location) {
        response.data.mapInfo = mapsUtils.generateMapURIs(mobileMapSize, response.data.location);
      }
      return Promise.resolve(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleGetRestaurantInfo: ' + accountId, nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  handleGetReceiptInfo: async (request) => {

    const receiptId = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_RECEIPT_ID);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);

    return consumerAPIRestUtil.getReceiptInfo(receiptId, authToken).then((response) => {
      logger.info(FILE + ': Success  handleGetReceiptInfo:' + receiptId);
      return Promise.resolve(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleGetReceiptInfo: ' + receiptId, nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  handleCalculateTax: async (request) => {
    const taxData = urlParamUtil.getDataFieldsFromRequest(FILE, request, null);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return consumerAPIRestUtil.calculateTax(taxData, authToken).then((response) => {
      logger.info(FILE + ': Success  handleCalculateTax:');
      return Promise.resolve(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleCalculateTax: ', nodeRuntimeUtil.inspect(taxData), nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  handleCreateOrder: async (request) => {
    const orderData = urlParamUtil.getDataFieldsFromRequest(FILE, request, null);
    const accountId = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_ACCOUNT_ID);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    let idempotencyKey = null;
    if (config.shouldEnableIdempotencyHeader) {
      updateSessionWithIdempotencyDetails(request, orderData, generateIdempotencyHeaderValue);
      idempotencyKey = getValueByKeyFromSession(request, KEY_IDEMPOTENCY_DETAILS)[accountId][KEY_IDEMPOTENCY_HEADER];
    }
    logger.debug(FILE + ': Create order called' + nodeRuntimeUtil.inspect(orderData));
    return consumerAPIRestUtil
      .createOrder(accountId, orderData, authToken, extractHeaders(request, headerMetadata), idempotencyKey)
      .then((response) => {
      logger.info(FILE + ': Success  handleCreateOrder, accountID:' + accountId);
      if (config.shouldEnableIdempotencyHeader) {
        const idempotencyDetails = request.session[KEY_IDEMPOTENCY_DETAILS];
        logger.info(`${FILE} removing idempotencyHeaderValue: ${idempotencyDetails[accountId][KEY_IDEMPOTENCY_HEADER]} \
          of accountId: ${accountId} after a successful order placement`);
        idempotencyDetails[accountId] = null;
        updateSession(request, KEY_IDEMPOTENCY_DETAILS, idempotencyDetails, true);
      }
      if (response.statusCode === httpConstants.HTTP_LOCKED) {
        logger.info(`${FILE}: Order has been processed in a prior createOrder call. ${response.data.additionalInfo}`);
        return Promise.resolve(new HttpResponse(orderUtil.setOrderAlreadyProcessed(orderData), httpConstants.HTTP_OK, response.data.additionalInfo));
      }
      return Promise.resolve(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleCreateOrder: ', nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  /**
   * Check whether a user exist in the system
   * @param request
   * @param emailParam This is optional
   * @returns {Promise<void>}
   */
  handleCheckUser: async (request, emailParam) => {
    let email = emailParam;
    if (!emailParam) {
      email = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_USER_EMAIL);
    }
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return consumerAPIRestUtil.getUser(email, authToken).then((response) => {
      logger.info(FILE + ': Success  handleCheckUser:' + email);
      if (response.statusCode !== 200) {
        if (response.data && response.data.code === "10210") {
          return Promise.resolve(new HttpResponse({'isAvailable': false}, 200, null));
        } else {
          logger.error(FILE + ': error in  handleCheckUser: ' + email, nodeRuntimeUtil.inspect(response));
          return Promise.reject(response);
        }
      }
      return Promise.resolve(new HttpResponse({'isAvailable': true}, 200, null));
    }).catch(err => {
      logger.error(FILE + ': error in  handleCheckUser: ' + email, nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  /**
   * Check whether a user exist in the system
   * @param request
   * @param emailParam This is optional
   * @returns {Promise<void>}
   */
  handleCheckPhone: async (request) => {
    const phone = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_PHONE);
    const email = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_EMAIL);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return consumerAPIRestUtil.getPhone(phone, authToken).then((response) => {
      logger.debug(FILE + ': Success  handleCheckPhone:' + phone);
      if (response.statusCode !== httpConstants.HTTP_OK) {
        if (response.data && response.data.code === "40850") {
          return Promise.resolve(new HttpResponse({'isAvailable': false}, httpConstants.HTTP_OK, null));
        } else {
          logger.error(FILE + ': error in  handleCheckPhone:' + phone, nodeRuntimeUtil.inspect(response));
          return Promise.reject(response);
        }
      }
      else {
        if (response.data && response.data.email === email) {
          return Promise.resolve(new HttpResponse({'isAvailable': false}, httpConstants.HTTP_OK, null));
        }
        return Promise.resolve(new HttpResponse({'isAvailable': true}, httpConstants.HTTP_OK, null));
      }
    }).catch(err => {
      logger.error(FILE + ': error in  handleCheckPhone:' + phone, nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  /**
   * Handles sending the forgot password email to the
   * given email addresses, once that email checks out for an
   * already existing user
   * @param request
   * @returns {Promise<void>}
   */
  handleSendingResetPasswordEmail: async (request) => {

    const email = urlParamUtil.getDataFieldsFromRequest(FILE, request, apiConstants.PARAM_EMAIL);
    const redirectUrl = urlParamUtil.getDataFieldsFromRequest(FILE, request, apiConstants.PARAM_REDIRECTION_URL);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    const userStatus = await module.exports.handleCheckUser(request, email);

    if (!(userStatus && userStatus.data && userStatus.data.isAvailable)) {
      return Promise.reject(new HttpResponse({
        'errorCode': 404,
        'message': 'User not found'
      }, httpConstants.HTTP_NOT_FOUND, null));
    }

    return consumerAPIRestUtil.sendResetPasswordEmail(email, redirectUrl, authToken).then((response) => {
      if (response.statusCode === httpConstants.HTTP_NO_CONTENT) {
        logger.info(FILE + ': Success  handleSendingResetPasswordEmail:' + email);
        return Promise.resolve(response);
      }
      logger.error(FILE + ': error in  handleSendingResetPasswordEmail: ' + email, nodeRuntimeUtil.inspect(response));
      return Promise.reject(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleSendingResetPasswordEmail: ' + email, nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  /**
   * Handles updating the user information of a registered user
   * @param request
   * @returns {Promise<void>}
   */
  handleUpdateUser: async (request) => {

    const customerObject = urlParamUtil.getDataFieldsFromRequest(FILE, request, null);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);

    return consumerAPIRestUtil.updateUser(customerObject, authToken).then((response) => {
      if (response.statusCode === httpConstants.HTTP_OK) {
        logger.info(FILE + ': Success  handleUpdateUser for customer');
        return Promise.resolve(new HttpResponse({
          'isUpdated': true,
          'user': response.data
        }, httpConstants.HTTP_OK, null));
      }
      logger.error(FILE + ': error in  handleUpdateUser for customer: ' + nodeRuntimeUtil.inspect(customerObject), nodeRuntimeUtil.inspect(response));
      return Promise.reject(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleUpdateUser: ' + nodeRuntimeUtil.inspect(customerObject), nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, err));
    });
  },

  /**
   * Handles resetting password for a given user
   * @param request
   * @returns {Promise<void>}
   */
  handleResetPassword: async (request) => {

    const password = urlParamUtil.getDataFieldsFromRequest(FILE, request, apiConstants.DATA_PARAM_PASSWORD);
    const email = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_USER_EMAIL);
    const token = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_RESET_PWD_TOKEN);
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);

    return consumerAPIRestUtil.resetPassword(email, password, authToken, token).then((response) => {
      logger.info(FILE + ': Success  in handleResetPassword for email:' + email);
      if (response.statusCode !== httpConstants.HTTP_NO_CONTENT) {
        return Promise.reject(response);
      }
      return Promise.resolve(response);
    }).catch(err => {
      logger.error(FILE + ': error in  handleResetPassword for email: ' + email, ' ', nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  },

  /**
   * Returns data about a particular gift card
   * @param cardNo
   * @param pin
   * @param request
   * @returns {Promise<WalletDataResponse>}
   */
  handleGetWalletData: async (cardNo, pin, request) => {
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    return consumerAPIRestUtil.getWalletData(cardNo, pin, authToken).then((response) => {
      logger.info(FILE + ': Success  in handleGetWalletData');
      if (response.statusCode !== httpConstants.HTTP_OK) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.data);
    }).catch(err => {
      logger.error(FILE + ': error in handleGetWalletData', nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  },


  handleGetOrderHistory: async (request) => {
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    const userId = request.session.user.id;
    return consumerAPIRestUtil.getOrderHistory(userId, authToken).then((response) => {
      logger.info(FILE + ': Success  in handleGetWalletData');
      if (response.statusCode !== httpConstants.HTTP_OK) {
        return Promise.reject(response);
      }
      return Promise.resolve(response.data);
    }).catch(err => {
      logger.error(FILE + ': error in handleGetWalletData', nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  },

  handleGetOperatorsList: async (request) => {
    const authToken = await tokenUtil.getBearerTokenFromSession(request.session);
    const source = urlParamUtil.getQueryParamFromRequest(FILE, request, apiConstants.PARAM_SEARCH_SOURCE);

    return consumerAPIRestUtil.getOperatorsList(source, authToken).then((response) => {
      logger.info(FILE + ': Success in handleGetOperatorsList');
      if (response.statusCode !== httpConstants.HTTP_OK) {
        return Promise.reject(response);
      }
      let hits = response.data ? response.data.searchResultList : [];
      let responseArray = [];
      for (let restaurant of hits) {
        if(!restaurant.isOnline){
          continue;
        }
        if (restaurant.sessions && restaurant.sessions.sessions && restaurant.sessions.sessions.length > 0) {
          restaurant.sessions = menuUtil.sessionMapper(restaurant.sessions);
        } else {
          continue;
        }
        responseArray.push(restaurant);
      }
      response.data = responseArray;
      return Promise.resolve(response);
    }).catch(err => {
      logger.info(FILE + ': Error in handleGetOperatorsList', nodeRuntimeUtil.inspect(err));
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });

  },

  handleGetAccountInfo: async (request) => {
    const merchantId = request.params.merchantId;
    const authToken = await tokenUtil.requestBearerToken(config.proxyUser, config.proxyUserPassword);

    return consumerAPIRestUtil.getAccountInfo(merchantId, authToken.access_token).then(response => {
      if (response.statusCode !== httpConstants.HTTP_OK) {
        return Promise.reject(response);
      }
      logger.info(`${FILE} : Success in handleGetAccountInfo for ${merchantId}`);
      return Promise.resolve(new HttpResponse(`/${response.data.accountId}`, httpConstants.HTTP_REDIRECT, null));
    }).catch(err => {
      logger.info(`${FILE} : Error in handleGetAccountInfo for ${merchantId}, ${nodeRuntimeUtil.inspect(err)}`);
      if (err instanceof HttpResponse) {
        return Promise.reject(err);
      }
      return Promise.reject(new HttpResponse(null, httpConstants.HTTP_INTERNAL_SERVER_ERROR, null));
    });
  }

};
