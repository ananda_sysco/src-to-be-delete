/**
 * Created by Salinda on 12/5/17.
 */
module.exports = {
  PARAM_MERCHANT_ID: 'merchant-id',
  PARAM_SESSION_ID: 'session-id',
  PARAM_ACCOUNT_ID: 'account-id',
  PARAM_RECEIPT_ID: 'receipt-id',
  PATH_PARAM_RECEIPT_ID: 'receiptId',
  DATA_PARAM_USERNAME: 'username',
  DATA_PARAM_PASSWORD: 'password',
  PARAM_USER_EMAIL: 'email-param',
  PARAM_EMAIL: 'email',
  PARAM_PHONE: 'phoneNumber',
  PARAM_CUSTOMER_ID: 'customer-id',
  PARAM_RESET_PWD_TOKEN: 'token-param',
  PARAM_IMG_URL: 'img-url',
  PARAM_USER_ID: 'user-id-param',
  PARAM_REDIRECTION_URL: 'redirectUrl',
  PARAM_USER_PASSWORD: 'password',
  PARAM_USER_FIRST_NAME: 'firstName',
  PARAM_USER_LAST_NAME: 'lastName',
  PARAM_USER_CONTACTS: 'contacts',
  PARAM_CARD_NO: 'card-no-param',
  PARAM_SEARCH_SOURCE: 'source',
  PARAM_MOBILE_MAP_SIZE: 'mobile-map-size'
};
