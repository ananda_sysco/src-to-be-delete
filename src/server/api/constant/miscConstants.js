/**
 * @author Tharuka Jayalath
 * (C) 2019, Sysco Labs
 * Created: 4/2/20. Thu 2020 20:15
 */

module.exports = {
  headerMetadata: [
    { name: 'x-forwarded-for', modifiedName: 'X-Origin' },
    { name: 'user-agent', modifiedName: 'X-Origin-User-Agent' }
  ],
  KEY_IDEMPOTENCY_DETAILS: 'idempotencyDetails',
  KEY_IDEMPOTENCY_HEADER: 'idempotencyHeader',
  NAME_IDEMPOTENCY_HEADER: 'Idempotency-Key'
};
