const { frameworkLogger } = require('../server/logger');
const logger = frameworkLogger.getDefaultLogger();
const config = require('../server/config').getConfigs();
const tokenUtil = require('../../src/server/api/util/tokenUtil');
const userUtil = require('../../src/server/api/util/userUtil');
const FILE = require('path').basename(__filename);
const HTTP_CONSTANTS = require('../../src/server/api/constant/httpConstants');
const nodeRuntimeUtil = require('util');

const constructInternalServerErrorAndSend = (response, error) => {
  response.status(HTTP_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR).send(error);
};

//TODO: change the front to handle this error, currently it displays Oops page
/**
 * Handles the authenticity of the request.
 * Scenarios
 * 01) Request from guest user
 * 02) Request from logged in user
 *
 * @param req
 * @param res
 * @param next
 */
const authMiddleWare = async (req, res, next) => {
  if ('application/vnd.platform.api+json' !== req.headers['request-type'] || req.originalUrl === '/auth/logout') {
    next();  //Bypass auth middleware for static page loading
  } else {
    const session = req.session;
    try {
      if (session) {
        let jwtToken = session.jwtToken;
        let jwtTokenExpiration = session.jwtTokenExpiration;

        const proxyUserName = config.proxyUser;
        const proxyUserPassword = config.proxyUserPassword;

        if (jwtToken) {
          if (tokenUtil.isTokenExpired(jwtToken, jwtTokenExpiration)) {
            logger.info(FILE + 'authMiddleWare jwt token expired.');
            // user can be either a logged user of guest user.
            if (session.isLoggedUser) {
              logger.info(FILE + 'authMiddleWare jwt token redirect to restaurant details page.');
              userUtil.setLoggedUserStatusToSession(session, false);
              req.session.destroy(function (err) {
                if (err) {
                  throw new Error('Error clearing session');
                }
                res.set({'Access-Control-Expose-Headers': 'X-olo-v3-unauthorized', 'X-olo-v3-unauthorized': 'true'});
                res.status(HTTP_CONSTANTS.HTTP_UNAUTHORIZED).send({code: 4005});
              });
            } else {
              logger.info(FILE + 'authMiddleWare jwt token expired fetching new tokens for proxy user');
              await tokenUtil.requestAndSetJwtAndBearerTokensToSession(session, proxyUserName, proxyUserPassword);
              logger.info(FILE + 'authMiddleWare fetchikng jwt token success');
              next();
            }
          } else if (tokenUtil.isTokenExpired(session.token, session.tokenExpiration)) {
            // user has a valid jwt token, but bearer token is expired
            await tokenUtil.requestAndSetBearerTokenToSession(session, jwtToken);
            next();
          } else {
            // has a jwt token, jwt is not expired, access token is not expired. So use those tokens
            next();
          }
        } else {
          logger.info(FILE + 'authMiddleWare jwt token not available, fetching new tokens for proxy user');
          await tokenUtil.requestAndSetJwtAndBearerTokensToSession(session, proxyUserName, proxyUserPassword);
          next();
        }
      } else {
        logger.error(FILE + 'authMiddleWare Error session is not available');
        constructInternalServerErrorAndSend(res, {error: "No session available"});
      }
    } catch (error) {
      logger.error(FILE + 'authMiddleWare Error in handling session', nodeRuntimeUtil.inspect(error));
      constructInternalServerErrorAndSend(res, error);
    }
  }
};

const httpsMiddleWare = (req, res, next) => {
//http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/TerminologyandKeyConcepts.html#x-forwarded-proto
  const xForwardedProto = 'x-forwarded-proto';
  const https = "https";
  if (req.get(xForwardedProto) != https) {
    res.set(xForwardedProto, https);
    res.redirect('https://' + req.get('host') + req.url);
  } else {
    next();
  }
};

module.exports = {
  authMiddleWare,
  httpsMiddleWare
};
