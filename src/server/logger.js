/**
 * Created by achintha on 6/27/19.
 */

const cls = require('cls-hooked');
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf, splat, colorize} = format;
const fs = require('fs');

const logDirectoryPath = '/var/log/olo-v3/';
const logFileName = 'olo-v3.log';
const filePath = logDirectoryPath + logFileName;

if (!fs.existsSync(logDirectoryPath)) {
  fs.mkdirSync(logDirectoryPath);
}

const level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info';

const customFormatter = printf(({timestamp, level, message, meta}) => {
  const ns = cls.getNamespace('net.cake.global');
  const correlationId = ns ? ns.get('correlationId') : null;
  return timestamp + ' ' + correlationId + ' ' + level.toUpperCase() + ' ' + (undefined !== message ? message : '') +
    (meta && Object.keys(meta).length ? '\n\t' + JSON.stringify(meta) : '');
});

const winstonTransporter = new (require('winston-daily-rotate-file'))({
  level,
  filename: filePath,
  handleExceptions: true,
  datePattern: 'YYYY-MM-DD',
  prepend: true,
  json: false
});

winstonTransporter.on('rotate', () => {
  try {
    // empty the log file
    fs.truncateSync(filePath, 0);
  } catch (err) {
    console.error(`Error in truncating file: ${err.message}`);
  }
});


const defaultLogger = createLogger({
  format: combine(
    colorize({message: true}),
    timestamp(),
    splat(),
    customFormatter
  ),
  transports: [
    new transports.File({filename: filePath, level: 'debug'}),
    winstonTransporter,
    new transports.Console({
      level,
      handleExceptions: true,
      json: false
    })
  ],
  exitOnError: false
});

const createCustomLogger = (loggerConfig) => {
  return createLogger(loggerConfig);
};

const frameworkLogger = {
  createCustomLogger,
  getDefaultLogger: () => {
    return defaultLogger;
  },

  level: {
    ALL: 'ALL',
    TRACE: 'TRACE',
    DEBUG: 'DEBUG',
    INFO: 'INFO',
    WARN: 'WARN',
    ERROR: 'ERROR',
    FATAL: 'FATAL',
    OFF: 'OFF',
  },
};

module.exports = {createLogger, frameworkLogger};
