const request = require('request-promise');
const minimist = require('minimist');
const { getLogger } = require('log4js');
const logger = getLogger('app');
const MAX_RETRY_COUNT_FOR_INJECT_CONFIGS = 3;
const { getConfigs } = require('./config');

let appConfigs = null;
/**
 * Initialize the Configuration from the Config server and inject to the app
 */
function getConfigsFromServer(tryCount = 0) {
    logger.info(`Loading Configs from config-api. Try Count : ${tryCount}`);
    const argc = minimist(process.argv.slice(2));

    const CONFIG_API_HOST = argc['conf-api'];
    const CONFIG_API_KEY = argc['conf-api-key'];
    const APP_ID = argc['app-id'];
    const ENV = argc['env'].toLocaleString();

    const options = {
        method: 'GET',
        uri: `${CONFIG_API_HOST}/${APP_ID}/${ENV}`,
        headers: { Authorization: `Basic ${CONFIG_API_KEY}` },
    };

    return request(options)
        .then(response => {
            appConfigs = JSON.parse(response).propertySources[0].source;
            appConfigs["app.ENV"] = ENV;
            return appConfigs;
        })
        .catch((err) => {
            logger.error('Error while fetching configurations from CONFIG API', err);
            if (tryCount >= MAX_RETRY_COUNT_FOR_INJECT_CONFIGS) {
                throw err;
            }

            return getConfigs(tryCount + 1);
        });

}

module.exports = getConfigsFromServer;
