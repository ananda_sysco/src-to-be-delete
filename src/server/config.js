const fs = require('fs');
const path = require('path');
const buildDir = path.resolve(__dirname, '../../build');
const { frameworkLogger } = require('./logger');
const logger = frameworkLogger.getDefaultLogger();

const parseStringToNumber = (stringValue) => {
  if (!isNaN(stringValue)) {
    return parseInt(stringValue, 10);
  } else {
    throw new Error('Parameter is not a number!');
  }
};

let serverConfigs = {};
const setServerConfigs = (serverConfigs_) => {
  serverConfigs = serverConfigs_;
};

exports.setServerConfigs = setServerConfigs;

const getCaptchaConfigs = () => {
  return {
    captchaEnabled: process.env.CAPTCHA_ENABLED || serverConfigs.captchaEnabled,
    captchaEndpoint: process.env.CAPTCHA_ENDPOINT || serverConfigs.captchaEndpoint,
    captchaSecret: process.env.CAPTCHA_SECRET || serverConfigs.captchaSecret
  }
};

const getRedisConfigs = () => {
  return {
    redisHost: process.env.REDIS_HOST || serverConfigs.redisHost,
    redisPort: process.env.REDIS_PORT || serverConfigs.redisPort,
    redisPrefix: process.env.REDIS_PREFIX || serverConfigs.redisPrefix
  }
};

const getMapsConfigs = () => {
  return {
    mapsApiUrl: process.env.MAPS_API_URL || serverConfigs.mapsApiUrl,
    mapsApiKey: process.env.MAPS_API_KEY || serverConfigs.mapsApiKey,
    mapsApiSecret: process.env.MAPS_API_SECRET || serverConfigs.mapsApiSecret
  }
};

exports.getConfigs = () => {
  return {
    serverPort: process.env.PORT || serverConfigs.serverPort,
    proxyUser: process.env.PROXY_USER || serverConfigs.proxyUser,
    proxyUserPassword: process.env.PROXY_USER_PASSWORD || serverConfigs.proxyUserPassword,
    esbUrl: process.env.ESB_URL || serverConfigs.esbUrl,
    BASIC_AUTH_TOKEN: process.env.AUTH_TOKEN || serverConfigs.BASIC_AUTH_TOKEN,
    maxAge: parseStringToNumber(process.env.MAX_AGE || serverConfigs.maxAge || 43200000),
    ...getRedisConfigs(),
    isBehindElb: process.env.IS_BEHIND_ELB || serverConfigs.isBehindElb,
    secret: process.env.SESSION_SECRET || serverConfigs.secret,
    legacyOLOUrl: process.env.LEGACY_OLO_URL || serverConfigs.legacyOLOUrl,
    audience: process.env.AUDIENCE || serverConfigs.audience,
    issuer: process.env.ISSUER || serverConfigs.issuer,
    shouldEnableCSRF: process.env.SHOULD_ENABLE_CSRF || serverConfigs.shouldEnableCSRF,
    shouldEnableIdempotencyHeader: process.env.SHOULD_ENABLE_IDEMPOTENCY_HEADER || serverConfigs.shouldEnableIdempotencyHeader,
    ...getCaptchaConfigs(),
    ...getMapsConfigs()
  };
};

const updateFileContent = (filePath, content) => {
  fs.writeFileSync(filePath, content);
  logger.info(`File updated successfully : ${filePath}`);
};

const updateIndexHtml = ({REACT_APP_GOOGLE_API_KEY}) => {
  const indexFilePath = path.resolve(buildDir, 'index.html');
  const content = fs.readFileSync(indexFilePath, 'utf8');
  const updatedIndexPage = content.replace(/REACT_APP_GOOGLE_API_KEY/,
    REACT_APP_GOOGLE_API_KEY);
  updateFileContent(indexFilePath, updatedIndexPage);
};

exports.setFrontendConfigs = (configs) => {
  const frontendConfigs = Object.entries(configs)
    .filter(([key]) => key.match(/frontend\./) !== null)
    .reduce((acc, [key, val]) => {
      const modifiedKey = key.split('frontend.')[1];
      acc[modifiedKey] = val;
      return acc;
    }, {});

  const frontendConfigsFilePath = path.resolve(buildDir, 'env-config.js');
  const frontendConfigsFileContent = `window._env_= ${JSON.stringify(frontendConfigs)}`;
  updateFileContent(frontendConfigsFilePath, frontendConfigsFileContent);
  updateIndexHtml(frontendConfigs);
};
