import React from 'react';
import ReactDOM from 'react-dom';
import "babel-polyfill";
import App from './components/app/App';
import store from './store';


const render = (Component) => {
  return ReactDOM.render(
    <Component store={store} />,
    document.getElementById('root')
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./components/app/App', () => {
    const NextApp = require('./components/app/App').default;
    render(NextApp);
  });
}
