import env from './utils/env';

function getDefaultProxy() {
  if (!(env('NODE_ENV') === 'production')) {
    if (typeof window !== 'undefined') {
      return window.location.protocol + '//localhost:3003'; //Dev mode
    }
  } else {
    return '';
  }
}

function getConfig(configKey) {
  if (window !== undefined && window._env_) {
    return `${window._env_[configKey]}`;
  }
  return null;
}

const REACT_APP_GOOGLE_API_KEY = 'REACT_APP_GOOGLE_API_KEY';
const REACT_APP_MIXPANEL_TOKEN = 'REACT_APP_MIXPANEL_TOKEN';
const REACT_APP_GA_TOKEN = 'REACT_APP_GA_TOKEN';
const REACT_APP_RESTAURANT_REDIRECT_URL = 'REACT_APP_RESTAURANT_REDIRECT_URL';
const GIFT_CARD_URL = 'GIFT_CARD_URL';
const CAPTCHA_SITE_KEY = 'CAPTCHA_SITE_KEY';
const DONATION_VIEW_ENABLED = 'DONATION_VIEW_ENABLED';

export default {
  appPort: env('PORT', 3000),
  backendUrl: getDefaultProxy(),
  esbUrl: env('ESB_URL', 'https://tst3-api.leapset.com:8443'), //This will be not used unless front end is not configured to use a proxy
  googleApiKey: getConfig(REACT_APP_GOOGLE_API_KEY) || env(REACT_APP_GOOGLE_API_KEY, 'AIzaSyDCaoIlnbR9BsjLczRyqByMPEOGv752BV0'),
  mixpanelToken: getConfig(REACT_APP_MIXPANEL_TOKEN) || env(REACT_APP_MIXPANEL_TOKEN, '05ce2b43fdacd5495147bcbec3fd255c'),
  gaToken: getConfig(REACT_APP_GA_TOKEN) || env(REACT_APP_GA_TOKEN, 'UA-109724690-3'),
  applicationBaseUrl: getConfig(REACT_APP_RESTAURANT_REDIRECT_URL) || env(REACT_APP_RESTAURANT_REDIRECT_URL, 'https://tst1-orders.cake.net'),
  countryList: ["us"],//This will be used by the address auto population component to restrict the address auto population range,
  giftCardUrl: getConfig(GIFT_CARD_URL) || env(GIFT_CARD_URL, 'tst1-giftcard.cake.net'),
  captchaEnabled: env('CAPTCHA_ENABLED', true),
  captchaSiteKey: getConfig(CAPTCHA_SITE_KEY) || env(CAPTCHA_SITE_KEY, '6Lfbx2EUAAAAAEkCqp2rM0_Dgl67qAWlJlNJbJ5N'),
  mapsApiUrl: 'https://maps.googleapis.com/maps/api',
  searchRadius: env('SEARCH_RADIUS', '50km'),
  searchResultsPageSize: env('SEARCH_RESULTS_PAGE_SIZE', 30),
  searchDisplayPageSize: env('SEARCH_DISPLAY_PAGE_SIZE', 10),
  searchResultsMaxOperatorCount: env('MAX_OPERATOR_COUNT', 200),
  donationViewEnabled: getConfig(DONATION_VIEW_ENABLED) || env(DONATION_VIEW_ENABLED, true)
};
