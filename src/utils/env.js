export default function env(key, defaultValue) {
  const keys = Array.isArray(key) ? key : [key];
  const match = keys.find(search => search in process.env);

  return match ? process.env[match] : defaultValue;
}
