import numeral from 'numeral';

const is24Hours = (slot) => slot && slot.start === '00:00:00' && (slot.end === '00:00:00' || slot.end === '23:59:59');

export function contains24Hours(slots) {
  if (Array.isArray(slots)) {
    return slots.some((slot) => is24Hours(slot));
  }

  return is24Hours(slots);
}

export function all24Hours(slots) {
  if (Array.isArray(slots) && slots.length === 7) {
    return slots.every((slot) => is24Hours(slot));
  }

  return is24Hours(slots);
}

export function formatPhoneNumber(phone) {
  return phone ? `(${phone.areaCode}) ${phone.exchangeCode}-${phone.subscriberNumber}` : '';
}

export function formatPrice(num, currencyCode) {
  const number = numeral(num);

  if (number.value() === null) {
    return null;
  }

  return currencyCode ? number.format(`${currencyCode}0,0.00`) : number.format('0,0.00');
}
