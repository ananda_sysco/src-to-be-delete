import { creditCardNumber, emailValidation, phoneNumberValidation, addressStateValidation, addressZipCodeValidation } from '../validators';
import moment from 'moment';

describe('utils/toolbox/validators/creditCardNumber', () => {
  const validNumbers = {
    AmericanExpress:371449635398431,
    DinersClub	:30569309025904,
    Discover	:6011111111111117,
    JCB:3530111333300000,
    MasterCard:5555555555554444,
    Visa:4111111111111111
  };

  it('should accept invalid credit card numbers', (done) => {
    let isError = false;
    let count = 0;
    for (let key in validNumbers) {
      count++;
      if (validNumbers.hasOwnProperty(key)) {
        creditCardNumber(null,validNumbers[key], (errors)=>{
          if(errors.length > 0){
            isError = true;
          }
          if(count===6) {
            expect(isError).toBe(false);
            done();
          }
        });
      }
    }
  });

  it('should not accept credit card numbers', (done) => {
    creditCardNumber(null, 4111111111111112, (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
});


/*describe('utils/toolbox/validators/creditCardExpiration', () => {

  it('should accept invalid formats', (done) => {
    creditCardExpiration(null, '2020/02/30',(errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });

  it('should accept invalid formats', (done) => {
    creditCardExpiration(null, 'invalid',(errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });

  it('should accept before dates', (done) => {
    creditCardExpiration(null, moment().subtract(1,'year'),(errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });

  it('should accept valid future date', (done) => {
    creditCardExpiration(null, moment().add(1,'year'),(errors)=>{
      expect(errors.length).toBe(0);
      done();
    });
  });
});*/

describe('utils/toolbox/validators/emailValidation', () => {
  /*
    Following cases are tested:
        1. empty value
        2. without @
        3. without host
        4. without a domain
        5. email with special characters
        6. email without special characters
    Following cases are not tested
        7. cake.net or trycake.com host email
    */

  test('empty value', (done) => {
    emailValidation(null, "", (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
  test('without @', (done) => {
    emailValidation(null, "umesh", (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
  test('without host', (done) => {
    emailValidation(null, "umesh@", (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
  test('without a domain', (done) => {
    emailValidation(null, "umesh@cake", (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
  test('email with special characters', (done) => {
    emailValidation(null, "#@umesh@cake.com", (errors)=>{
      expect(errors.length).toBe(1);
      done();
    });
  });
  test('email without special characters', (done) => {
    emailValidation(null, "umesh@cake.com", (errors)=>{
      expect(errors.length).toBe(0);
      done();
    });
  });
});


describe('utils/toolbox/validators/phoneNumberValidation', () => {

  test('empty number', (done) => {
    phoneNumberValidation(null, "", (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('integer number', (done) => {
    phoneNumberValidation(null, 4155552671, (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('non US number', (done) => {
    phoneNumberValidation(null, '441632960646', (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('US number', (done) => {
    phoneNumberValidation(null, '415 555 2671', (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });
});


describe('utils/toolbox/validators/addressStateValidation', () => {

  test('empty number', (done) => {
    addressStateValidation(null, "", (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('invalid type', (done) => {
    addressStateValidation(null, 10, (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('full state name', (done) => {
    addressStateValidation(null, 'california', (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('valid short name simple', (done) => {
    addressStateValidation(null, 'ca', (errors) => {
      expect(errors.length).toBe(0);
      done();
    });
  });

  test('valid short name capital', (done) => {
    addressStateValidation(null, 'CA', (errors) => {
      expect(errors.length).toBe(0);
      done();
    });
  });

  test('valid short name mixed', (done) => {
    addressStateValidation(null, 'cA', (errors) => {
      expect(errors.length).toBe(0);
      done();
    });
  });
});


describe('utils/toolbox/validators/addressZipCodeValidation', () => {

  test('empty number', (done) => {
    addressZipCodeValidation(null, "", (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('invalid type', (done) => {
    addressZipCodeValidation(null, 10001, (errors) => {
      expect(errors.length).toBe(1);
      done();
    });
  });

  test('valid code', (done) => {
    addressZipCodeValidation(null, '12345-1234', (errors) => {
      expect(errors.length).toBe(0);
      done();
    });
  });
});
