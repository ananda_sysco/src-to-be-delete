import {
  checkNullOrUndefinedOrEmptyString,
  checkNullOrUndefined,
  isExpired,
  getFormattedCardNumber,
  checkNullOrUndefinedOrNotANumber,
  isValidExpiryMonth,
  isValidExpiryYear,
  removeEmptyElementsFromObject,
  isEven
} from '../commonUtil';

describe('utils/commonUtil/checkNullOrUndefined',()=>{
  it('should return true for input null',()=>{
    expect(checkNullOrUndefined(null)).toEqual(true);
  });

  it('should return true for input undefined',()=>{
    expect(checkNullOrUndefined(undefined)).toEqual(true);
  });

  it('should return false for input {}',()=>{
    expect(checkNullOrUndefined({})).toEqual(false);
  });

  it('should return false for empty string',()=>{
    expect(checkNullOrUndefined('')).toEqual(false);
  });

});

describe('utils/commonUtil/checkNullUndefinedOrEmptyString',()=>{
  it('should return true for empty string',()=>{
    expect(checkNullOrUndefinedOrEmptyString('')).toEqual(true);
  })
});

describe('utils/commmonUtil/checkNullOrUndefinedOrNotANumber', ()=>{
  it('should return for true for NaN', ()=>{
    expect(checkNullOrUndefinedOrNotANumber(NaN)).toEqual(true);
  });
});

describe('utils/commonUtil/isExpired',()=>{
  it('should return true for 2000/10',()=>{
    expect(isExpired(10,2000,"America/Los_Angeles")).toEqual(true);
  });

  it('should return false for 2050/10',()=>{
    expect(isExpired(10,2050,"America/Los_Angeles")).toEqual(false);
  });
});

describe('utils/commonUtil/isValidExpiryMonth',()=>{
  it('should return true for 1',()=>{
    expect(isValidExpiryMonth(1)).toEqual(true);
  });

  it('should return true for 12',()=>{
    expect(isValidExpiryMonth(12)).toEqual(true);
  });

  it('should return false for 0',()=>{
    expect(isValidExpiryMonth(0)).toEqual(false);
  });

  it('should return false for 13',()=>{
    expect(isValidExpiryMonth(13)).toEqual(false);
  });

  it('should return false for strings',()=>{
    expect(isValidExpiryMonth('temp')).toEqual(false);
  });
});

describe('utils/commonUtil/isValidExpiryYear',()=>{
  it('should return true for 0',()=>{
    expect(isValidExpiryYear(0)).toEqual(true);
  });

  it('should return true for 99',()=>{
    expect(isValidExpiryYear(99)).toEqual(true);
  });

  it('should return false for -1',()=>{
    expect(isValidExpiryYear(-1)).toEqual(false);
  });

  it('should return false for 100',()=>{
    expect(isValidExpiryYear(100)).toEqual(false);
  });
});

describe('utils/commonUtil/getFormattedCardNumber', ()=>{
  it('should return Add a new payment option',()=>{
    expect(getFormattedCardNumber('+ Add a new card')).toEqual('Add a new payment option');
  });

  it('should return Card ending in 1111',()=>{
    expect(getFormattedCardNumber('1111')).toEqual('Card ending in 1111');
  });
});

describe('utils/commonUtil/removeEmptyElementsFromObject',()=>{
  it('should return object with non empty fields',()=>{
    expect(removeEmptyElementsFromObject({
      firstName: 'customerFirstName',
      lastName:'',
      email: null
    })).toEqual({
      firstName: 'customerFirstName',
      email: null});
  });
});

describe('utils/commonUtil/isEven', ()=>{
  expect(isEven(0)).toEqual(true);
  expect(isEven(1)).toEqual(false);
});
