import moment from 'moment-timezone';
import {
  getEndTimeMoment,
  getStartTimeMoment,
  withAvailability,
  checkSlotForToday,
  isSessionClosed,
} from '../sessionUtil';

describe('utils/sessionUtil/getStartTimeMoment', () => {
  it('should get start time moment for the given time', () => {
    const startTimeMoment1 = getStartTimeMoment('10:12:20');
    const startTimeMoment2 = getStartTimeMoment('10:12:19');
    expect(startTimeMoment1 > startTimeMoment2).toEqual(true);
  });
});

describe('utils/sessionUtil/getEndTimeMoment', () => {
  it('should get end time moment for the given time', () => {
    const endTimeMoment1 = getStartTimeMoment('10:12:20');
    const endTimeMoment2 = getStartTimeMoment('10:12:19');
    expect(endTimeMoment1 > endTimeMoment2).toEqual(true);
  });

  it('should get end time 1 second early at 12 am', () => {
    const endTimeMoment1 = getEndTimeMoment('00:00:00').format('h:mm A');
    expect(endTimeMoment1).toEqual('11:59 PM');
  });
});


describe('utils/sessionUtil/withAvailability', () => {
  it('should give unavailable for time not in between start and end', () => {
    const sessions = withAvailability([{slots:[
      {
        isavailable: true,
        start:moment(moment().tz("America/Los_Angeles").subtract(2,'seconds')).tz("America/Los_Angeles").format('HH:mm:ss'),
        end:moment(moment().tz("America/Los_Angeles").subtract(1,'seconds')).tz("America/Los_Angeles").format('HH:mm:ss'),
        day:moment().tz("America/Los_Angeles").day()+1}
      ]}],"America/Los_Angeles", false);
    expect(sessions["0"].isAvailable).toEqual(false);
  });

  it('should give available for time in between start and end', () => {
    const sessions = withAvailability([{slots:[
      {
        isavailable: true,
        start:moment(moment().tz("America/Los_Angeles").subtract(4,'seconds')).tz("America/Los_Angeles").format('HH:mm:ss'),
        end:moment(moment().tz("America/Los_Angeles").add(4,'seconds')).tz("America/Los_Angeles").format('HH:mm:ss'),
        day:moment().tz("America/Los_Angeles").day()+1}
    ]}],"America/Los_Angeles", false);
    expect(sessions["0"].isAvailable).toEqual(true);
  });

});

describe('utils/sessionUtil/checkSlotForToday',()=>{
  it('should return false',()=>{
    expect(checkSlotForToday(moment("19930617","YYYYMMDD"), {day: 5})).toEqual(false);
  });

  it('should return true',()=>{
    expect(checkSlotForToday(moment("19930617","YYYYMMDD"), {day: 1})).toEqual(true);
  });
});
