import { formatPrice, formatPhoneNumber, contains24Hours } from '../toolbox';

describe('utils/toolbox/formatPrice', () => {
  it('should not accept non-integers', () => {
    const formattedPrice = formatPrice('abc');
    expect(formattedPrice).toBeNull();
  });

  it('should format integers', () => {
    const formattedPrice = formatPrice('1');
    expect(formattedPrice).toBe('1.00');
  });

  it('should accept a currency symbol', () => {
    let formattedPrice = formatPrice('1.00', '$');
    const priceWithSign = '$1.00';
    expect(formattedPrice).toBe(priceWithSign);

    formattedPrice = formatPrice('1.00');
    expect(formattedPrice).not.toBe(priceWithSign);
  });
});


describe('utils/toolbox/formatPrice', () => {
  it('should not accept non-integers', () => {
    const formattedPrice = formatPrice('abc');
    expect(formattedPrice).toBeNull();
  });

  it('should format integers', () => {
    const formattedPrice = formatPrice('1');
    expect(formattedPrice).toBe('1.00');
  });

  it('should accept a currency symbol', () => {
    let formattedPrice = formatPrice('1.00', '$');
    const priceWithSign = '$1.00';
    expect(formattedPrice).toBe(priceWithSign);

    formattedPrice = formatPrice('1.00');
    expect(formattedPrice).not.toBe(priceWithSign);
  });
});


describe('utils/toolbox/formatPhoneNumber', () => {
  it('should give a phone number in the format (areaCode) exchangeCode-subscriberNumber', () => {
    const formattedPhoneNumber = formatPhoneNumber({areaCode:123 , exchangeCode:4567 , subscriberNumber:12345});
    expect(formattedPhoneNumber).toBe('(123) 4567-12345');
  });
});

describe('utils/toolbox/contains24Hours', () => {
  it('should mark true for 24 hour single slot', () => {
    expect(contains24Hours({start:'00:00:00', end:'00:00:00'})).toBe(true);
  });

  it('should mark true for 24 hours containing slot array', () => {
    expect(contains24Hours([{start:'00:00:00', end:'00:00:00'}, {start:'08:00:00', end:'23:00:00'}])).toBe(true);
  });

  it('should mark false for 24 hours not containing slot array', () => {
    expect(contains24Hours([{start:'09:00:00', end:'00:00:00'}, {start:'08:00:00', end:'23:00:00'}])).toBe(false);
  });
});

