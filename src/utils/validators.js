import luhn from 'luhn';
import validator from 'validator';

const isValidState = (state) => {
  let states = "wa|or|ca|ak|nv|id|ut|az|hi|mt|wy|co|nm|nd|sd|ne|ks|ok|tx|mn|ia|mo|ar|la|wi|il|ms|mi|in|ky|tn|al|fl|ga|sc|nc|oh|wv|va|pa|ny|vt|me|nh|ma|ri|ct|nj|de|md|dc|pr|vi|mp|pw|fm|mh|as|gu|";
  return typeof state === 'string' && state.length === 2 && states.indexOf(state.toLowerCase() + "|") > -1;
};

const isValidPhoneNumber = (number) => {
  if (typeof number === 'string') {
    return validator.isMobilePhone(number, 'en-US');
  } else {
    return false;
  }
};

const isValidPostalCode = (code) => {
  if (typeof code === 'string') {
    return validator.isPostalCode(code, 'US');
  } else {
    return false;
  }
};

export const creditCardNumber = (rule, value, callback) => {
  const errors = [];

  if (!luhn.validate(value)) {
    errors.push('Invalid card number');
  }

  callback(errors);
};

const validateEmail = (email) => {
  const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(email);
};


export const emailValidation = (rule, value, callback) => {
  const errors = [];

  if (!validateEmail(value)) {
    errors.push('Invalid email');
  }
  callback(errors);
};

export const phoneNumberValidation = (rule, value, callback) => {
  const errors = [];

  if (!isValidPhoneNumber(value)) {
    errors.push('Invalid phone number');
  }
  callback(errors);
};

export const addressStateValidation = (rule, value, callback) => {
  const errors = [];

  if (!isValidState(value)) {
    errors.push('Invalid state');
  }
  callback(errors);
};

export const addressZipCodeValidation = (rule, value, callback) => {
  const errors = [];

  if (!isValidPostalCode(value)) {
    errors.push('Invalid zip code');
  }
  callback(errors);
};



