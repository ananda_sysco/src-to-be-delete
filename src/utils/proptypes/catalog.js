import PropTypes from 'prop-types';

export default PropTypes.shape({
  categories: PropTypes.shape({
    category: PropTypes.arrayOf(PropTypes.object),
  }),
});
