import PropTypes from 'prop-types';
import attribute from './attribute';

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  price: PropTypes.number.isRequired,
  attributeSet: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    attributes: PropTypes.arrayOf(attribute),
  }),
});
