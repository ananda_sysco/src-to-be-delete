import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  deliver: PropTypes.bool,
  pickup: PropTypes.bool,
  location: PropTypes.shape({
    lat: PropTypes.string,
    lng: PropTypes.string,
  }),
  phone: PropTypes.shape({
    countryCode: PropTypes.string,
    areaCode: PropTypes.string,
    exchangeCode: PropTypes.string,
    subscriberNumber: PropTypes.string,
  }),
  address: PropTypes.shape({
    address1: PropTypes.string,
    address2: PropTypes.string,
    state: PropTypes.string,
    city: PropTypes.string,
    zipCode: PropTypes.string,
    country: PropTypes.string,
    countryCode: PropTypes.string,
  }),
});
