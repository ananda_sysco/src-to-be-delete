import PropTypes from 'prop-types';

export default PropTypes.shape({
  lineItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    totalCost: PropTypes.number.isRequired,
  })),
  orderNumber: PropTypes.string,
  accountId: PropTypes.string,
  tax: PropTypes.number,
  discount: PropTypes.number,
  total: PropTypes.number,
  subTotal: PropTypes.number,
  tipAmount: PropTypes.number,
  createdTime: PropTypes.number
});
