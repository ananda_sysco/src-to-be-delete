import PropTypes from 'prop-types';

export default PropTypes.shape({
  items: PropTypes.arrayOf(PropTypes.shape({
    lineItemId: PropTypes.string.isRequired,
    productId: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    totalCost: PropTypes.number.isRequired,
  })),
  orderType: PropTypes.oneOf(['PICKUP', 'DELIVERY']),
  totalCost: PropTypes.number,
  tax: PropTypes.number,
  discountType: PropTypes.oneOf(['PERCENT', 'AMOUNT']),
  discountCode: PropTypes.string,
  discountAmout: PropTypes.number,
  discount: PropTypes.number,
  grandTotal: PropTypes.number,
});
