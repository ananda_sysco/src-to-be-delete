import PropTypes from 'prop-types';

export default PropTypes.shape({
  minSelections: PropTypes.number.isRequired,
  maxSelections: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  values: PropTypes.arrayOf(PropTypes.shape({
    attributeValueId: PropTypes.string,
    value: PropTypes.string,
    price: PropTypes.number,
  })).isRequired,
});
