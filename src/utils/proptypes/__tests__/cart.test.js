import React  from 'react';
import sinon from 'sinon';

import cart from '../cart';

describe('util/proptypes/cart', () => {
  beforeEach(() => {
    sinon.stub(console, 'error');
  });
  afterEach(() => {
    console.error.restore();
  });

  const TestComponent = () => (
    <h1>Test Component</h1>
  );
  TestComponent.propTypes = {cart:cart};

  it('should not give error on valid props types', () => {
    const test = (<TestComponent cart={{
      items: [{
        lineItemId: 'xxx',
        productId: 'xxx',
        quantity: 10,
        name: 'xxx',
        totalCost: 100,
      }],
      orderType: 'PICKUP',
      totalCost: 1000,
      tax: 10,
      discountType: 'PERCENT',
      discountCode: 'xxx',
      discountAmout: 10,
      discount: 10,
      grandTotal: 2100,
    }} />);
    sinon.assert.notCalled(console.error);
  });

  it('should not give error on valid props types without required props', () => {
    const test = (<TestComponent cart={{
      items: [{
        lineItemId: 'xxx',
        productId: 'xxx',
        quantity: 10,
        name: 'xxx',
        totalCost: 100,
      }],
    }} />);
    sinon.assert.notCalled(console.error);
  });

  it('should give a console error when missing a required prop', () => {
    const test = (<TestComponent cart={{
      items: [{
        productId: 'xxx',
        quantity: 10,
        name: 'xxx',
        totalCost: 100,
      }],
    }} />);
    sinon.assert.callCount(console.error,1);
  });

  it('should give a console error when type is wrong', () => {
    const test = (<TestComponent cart={{
      items: [{
        lineItemId: 100, //wrong type
        productId: 'xxx',
        quantity: 10,
        name: 'xxx',
        totalCost: 100,
      }],
    }} />);
    sinon.assert.callCount(console.error,1);
  });

});
