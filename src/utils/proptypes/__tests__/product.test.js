import React  from 'react';
import sinon from 'sinon';

import product from '../product';

describe('util/proptypes/product', () => {
  beforeEach(() => {
    sinon.stub(console, 'error');
  });
  afterEach(() => {
    console.error.restore();
  });

  const TestComponent = () => (
    <h1>Test Component</h1>
  );
  TestComponent.propTypes = {product: product};

  it('should not give error on valid props types', () => {
    const test = (<TestComponent product={{
      id: 'xxx',
      name: 'xxx',
      description: 'xxx',
      price: 100,
      attributeSet: {
        id: 'xxx',
        name: 'xxx',
        attributes: [{
          minSelections: 1,
          maxSelections: 10,
          id: 'xxx',
          name: 'xxx',
          values: [],
        }],
      },
    }} />);

    sinon.assert.notCalled(console.error);
  });

  it('should not give error on valid props types without required props', () => {
    const test = (<TestComponent product={{
      id: 'xxx',
      name: 'xxx',
      price: 100,
    }} />);

    sinon.assert.notCalled(console.error);
  });

  it('should give a console error when type is wrong', () => {
    const test = (<TestComponent product={{
      id: 'xxx',
      name: 'xxx',
      price: 'xxx',//Wrong type
    }} />);
    sinon.assert.callCount(console.error,1);
  });
});
