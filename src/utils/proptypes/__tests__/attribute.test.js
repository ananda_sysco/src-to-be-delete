import React  from 'react';
import sinon from 'sinon';

import attribute from '../attribute';

describe('util/proptypes/attribute', () => {
  beforeEach(() => {
    sinon.stub(console, 'error');
  });
  afterEach(() => {
    console.error.restore();
  });

  const TestComponent = () => (
    <h1>Test Component</h1>
  );
  TestComponent.propTypes = {attribute:attribute};

  it('should not give error on valid props types', () => {
    const test1 = (<TestComponent attribute={{
      minSelections: 1,
      maxSelections: 10,
      onChange: ()=>{},
      id: 'xxx',
      name: 'xxx',
      values: [{
        attributeValueId: 'xxx',
        value: 'xxx',
        price: 100,
      }],
    }} />);
    sinon.assert.notCalled(console.error);
  });


  it('should not give error on valid props types without required props', () => {
    const test1 = (<TestComponent attribute={{
      minSelections: 1,
      maxSelections: 10,
      id: 'xxx',
      name: 'xxx',
      values: [],
    }} />);
    sinon.assert.notCalled(console.error);
  });

  it('should give a console error when missing a required prop', () => {
    const test1 = (<TestComponent attribute={{
      maxSelections: 10,
      id: 'xxx',
      name: 'xxx',
      values: [],
    }} />);
    sinon.assert.callCount(console.error,1);
  });

  it('should give a console error when type is wrong', () => {
    const test1 = (<TestComponent attribute={{
      minSelections: 'xxx',
      maxSelections: 10,
      id: 'xxx',
      name: 'xxx',
      values: [],
    }} />);
    sinon.assert.callCount(console.error,1);
  });

});
