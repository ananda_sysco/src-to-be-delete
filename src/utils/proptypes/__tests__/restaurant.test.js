import React  from 'react';
import sinon from 'sinon';

import restaurant from '../restaurant';

describe('util/proptypes/restaurant', () => {
  beforeEach(() => {
    sinon.stub(console, 'error');
  });
  afterEach(() => {
    console.error.restore();
  });

  const TestComponent = () => (
    <h1>Test Component</h1>
  );
  TestComponent.propTypes = {restaurant: restaurant};

  it('should not give error on valid props types', () => {
    const test = (<TestComponent restaurant={{
      id: 'xxx',
      name: 'xxx',
      description: 'xxx',
      deliver: false,
      pickup: true,
      location: {
        lat: 'xxx',
        lng: 'xxx',
      },
      phone: {
        countryCode: 'xxx',
        areaCode: 'xxx',
        exchangeCode: 'xxx',
        subscriberNumber: 'xxx',
      },
      address: {
        address1: 'xxx',
        address2: 'xxx',
        state: 'xxx',
        city: 'xxx',
        zipCode: 'xxx',
        country: 'xxx',
        countryCode: 'xxx',
      },
    }} />);

    sinon.assert.notCalled(console.error);
  });

  it('should not give error on valid props types without required props', () => {
    const test = (<TestComponent restaurant={{
      id: 'xxx',
      name: 'xxx',
    }} />);

    sinon.assert.notCalled(console.error);
  });

  it('should give a console error when type is wrong', () => {
    const test = (<TestComponent restaurant={{
      id: 'xxx',
      name: 123, //Wrong type
    }} />);
    sinon.assert.callCount(console.error,1);
  });
});
