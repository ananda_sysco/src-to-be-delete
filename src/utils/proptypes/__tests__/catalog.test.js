import React  from 'react';
import sinon from 'sinon';

import catalog from '../catalog';

describe('util/proptypes/cart', () => {
  beforeEach(() => {
    sinon.stub(console, 'error');
  });
  afterEach(() => {
    console.error.restore();
  });

  const TestComponent = () => (
    <h1>Test Component</h1>
  );
  TestComponent.propTypes = {catalog:catalog};

  it('should not give error on valid props types', () => {
    const test = (<TestComponent catalog={{
      categories: {
        category: [{},{}],
      },
    }} />);

    sinon.assert.notCalled(console.error);
  });

  it('should give a console error when type is wrong', () => {
    const test = (<TestComponent catalog={{
      categories: {
        category: {},
      },
    }} />);
    sinon.assert.callCount(console.error,1);
  });
});
