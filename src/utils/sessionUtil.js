import moment from 'moment-timezone';
import {all24Hours} from './toolbox';
import {mapValues} from 'lodash';

export const DATE_TIME_FORMAT = 'HH:mm:ss';

/**
 * Get moment for the given time
 * @param time in 'HH:mm:ss' format
 * @returns {*}
 */
export function getStartTimeMoment(time) {
  return moment(time, DATE_TIME_FORMAT);
}

/**
 * Get moment for the given time. Handles scenario when end time is 00:00:00.
 * @param time in 'HH:mm:ss' format
 * @returns {*}
 */
export function getEndTimeMoment(time) {
  const end = moment(time, DATE_TIME_FORMAT);
  if (time === "00:00:00") {
    end.add(1, 'day').subtract(1, 'second');
  }
  return end;
}

/**
 * Check whether a particular slot matches today
 * @param nowTime
 * @param slot
 * @returns {boolean}
 */
export function checkSlotForToday(nowTime, slot) {
  return slot.day.toString() !== (nowTime.day() + 1).toString();
}

function getSlotNumberForToday(nowTime) {
  return nowTime.day() + 1;
}

export const isSessionClosed = (session, timezone) => {
  const timeNow = moment().tz(timezone);
  for (let slot of session.slots) {
    if (slot.day.toString() === (timeNow.day() + 1).toString()) {
      return false;
    }
  }
  return true;
};

export function withAvailability(sessions, timezone, reset = true) {
  const now = moment().tz(timezone);
  const momentTimeNow = moment(now.format(DATE_TIME_FORMAT), DATE_TIME_FORMAT);

  const mapped = mapValues(sessions, (session) => {
    // find a time slot for today

    if (reset) {
      delete session.isAvailable;
      delete session.isPending;
    }

    const {slots} = session;

    let isAvailable;
    let isPending;

    if (isSessionClosed(session, timezone)) {
      isAvailable = false;
      isPending = false;
    } else {
      isAvailable = all24Hours(slots) || slots.some((slot) => {
        // check for isavailable and same day
        if (checkSlotForToday(now, slot)) {
          return false;
        }

        // check time
        const start = getStartTimeMoment(slot.start);
        const end = getEndTimeMoment(slot.end);

        if (end.isBefore(start)) {
          return false;
        }

        return momentTimeNow.isBetween(start, end);
      });

      isPending = !all24Hours(slots) && slots.some((slot) => {
        // check for isavailable and same day
        if (checkSlotForToday(now, slot)) {
          return false;
        }

        // check time
        const start = getStartTimeMoment(slot.start);
        const end = getEndTimeMoment(slot.end);

        if (end.isBefore(start)) {
          return false;
        }

        return momentTimeNow.isBefore(start);
      });
    }

    return {
      ...session,
      isAvailable,
      isPending
    };
  });

  return mapped;
}

function getHumanizedDateTime(dayOfWeek, startTime, nowTime) {
  return (moment().day(dayOfWeek - 1)).calendar(null, {
    sameDay: () => {
      const isStartTimeMidNight = startTime.format('hh:mm:ss A') === "12:00:00 AM";
      if (startTime && nowTime) {
        if (startTime.isAfter(nowTime) && !isStartTimeMidNight) {
          return '[Today]'
        } else if (startTime.isAfter(nowTime) && isStartTimeMidNight) {
          return '[Tomorrow]'
        } else {
          return 'dddd'
        }
      }
    },
    nextDay: '[Tomorrow]',
    nextWeek: 'dddd',
    lastDay: 'dddd',
    lastWeek: 'dddd',
    sameElse: 'DD/MM/YYYY'
  }) + ' at ' + startTime.format('hh:mm a');
}

function adjustPrepDeliveryTime(sessions, prepTime, deliveryTime) {
  if (prepTime || deliveryTime) {
    let prepTimeToDeduct = prepTime || 0;
    let deliveryTimeToDeduct = deliveryTime || 0;
    for (let session of sessions) {
      for (let slot of session.slots) {
        let endTimeMoment = getEndTimeMoment(slot.end)
          .subtract(prepTimeToDeduct, 'minutes')
          .subtract(deliveryTimeToDeduct, 'minutes');
        slot.end = endTimeMoment.format(DATE_TIME_FORMAT);
      }
    }
  }
}

/**
 * Returns whether available now and next available time
 * @param sessions
 * @param operator
 */
export function getOpeningTime(sessions, operator) {
  const timezone = operator.timeZone;
  const prepTime = operator.orderPrepTime;
  let deliveryTime = operator.deliveryTime;
  let isDeliveryOnly = operator.isDeliveryAvailable && !operator.isPickupAvailable;
  const toReturn = {
    isAvailable: false,
    nextAvailableTime: null
  };

  if (!sessions || sessions.length === 0) {
    return toReturn;
  }

  adjustPrepDeliveryTime(sessions, prepTime, isDeliveryOnly ? deliveryTime : null);

  sessions = withAvailability(sessions, timezone);

  let temp = [];
  for (let s in sessions) {
    if (sessions.hasOwnProperty(s)) {
      temp.push(sessions[s]);
    }
  }

  sessions = temp;

  const availableSessions = sessions.filter(session => session.isAvailable);
  const pendingSessions = sessions.filter(session => session.isPending);

  if (availableSessions.length > 0 || pendingSessions.length > 0) {
    toReturn.isAvailable = true;
    return toReturn;
  }

  const nowTime = moment().tz(timezone);
  const slotNumberToday = getSlotNumberForToday(nowTime);

  let slotMins = new Map();

  //region calculate start time for each day of the week
  for (let i = 1; i <= 7; i++) {
    let min = null;

    for (let session of sessions) {
      let sessionMin;

      const ranges = session.slots;
      if (!ranges || ranges.length === 0) {
        continue;
      }

      let filteredSlots = ranges.filter(slot => slot.day === String(i));
      if (filteredSlots && filteredSlots.length > 0) {
        sessionMin = moment.tz(filteredSlots[0].start, DATE_TIME_FORMAT, timezone);
        if (!min || sessionMin.isBefore(min)) {
          min = sessionMin;
        }
      }
    }
    slotMins.set(i, min);
  }
  //endregion

  //region find next available day of week
  for (let i = slotNumberToday + 1; i <= 7; i++) {
    if (slotMins.get(i)) {
      toReturn.nextAvailableTime = getHumanizedDateTime(i, slotMins.get(i), nowTime);
      break;
    }
  }
  if (!toReturn.nextAvailableTime) {
    for (let i = 1; i <= slotNumberToday; i++) {
      if (slotMins.get(i)) {
        toReturn.nextAvailableTime = getHumanizedDateTime(i, slotMins.get(i), nowTime);
        break;
      }
    }
  }
  //endregion

  return toReturn;
}








