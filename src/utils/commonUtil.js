/*
 * Created by Tharuka Jayalath 01/10/2018
 */
import {
  ADD_CARD_LABEL, ADD_PAYMENT_OPTION, DATE_FORMAT_STRINGS, COUNTRY_CURRENCY_MAP
} from '../lib/constants';

import { omitBy } from 'lodash';
import moment from 'moment-timezone';

//if value is null or undefined function will return true
//Else return false
export function checkNullOrUndefined(value) {
  return value === null || value === undefined;
}

//if value is null or undefined of empty string function will return true
//Else return false
export function checkNullOrUndefinedOrEmptyString(value) {
  return checkNullOrUndefined(value) || value === '';
}

//if value is null or undefined or Not a valid number function will return true
//Else return false
export function checkNullOrUndefinedOrNotANumber(value) {
  return checkNullOrUndefined(value) || isNaN(value);
}

// returns a copy of the given object after
// removing the attributes with empty values
export function removeEmptyElementsFromObject(object) {
  if (!object) {
    return null
  }
  let manipulatingObject = Object.assign({}, object);
  return Object.assign({}, omitBy(manipulatingObject, (value, key) => value === ""));
}

// For checking whether the given number is even
export const isEven = number => number % 2 === 0;

/**
 * This validates a given address is a LatLng
 * @param address
 * @returns {{isLatLng: boolean, latLng: {}}}
 */
export function validateIsLatLng(address) {
  const output = {isLatLng: false, latLng: {}};
  if (address && typeof address === 'string') {
    const result = address.split(',');
    if (result.length === 2 && !isNaN(parseFloat(result[0])) && !isNaN(parseFloat(result[1]))) {
      output.isLatLng = true;
      output.latLng = {lat: result[0], lng: result[1]}
    }
  }
  return output;
}

/**
 * Returns the formatted card number by removing the mask and adding prefix.
 * @param cardNumber
 * @returns {string}
 */
export function getFormattedCardNumber(cardNumber) {
  let formattedString = null;
  if (cardNumber) {
    if (ADD_CARD_LABEL === cardNumber) {
      formattedString = ADD_PAYMENT_OPTION;
    } else {
      formattedString = 'Card ending in ' + cardNumber.replace(new RegExp('X', 'g'), '');
    }
  }
  return formattedString;
}

/**
 * Check weather the given month is valid or not
 * @param month
 * @returns {boolean}
 */
export function isValidExpiryMonth(month) {
  const monthNumber = parseInt(month, 10);
  return !isNaN(monthNumber) && monthNumber >= 1 && monthNumber <= 12;
}

/**
 * Check wheather the given year of CC is valid
 * @param year
 * @returns {boolean}
 */
export function isValidExpiryYear(year) { //pass 2 digit year value
  const yearNumber = parseInt(year, 10);
  return !isNaN(yearNumber) && yearNumber >= 0 && yearNumber <= 99;
}

export function isMobile() {
  const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  return w <= 767;
}

export function scrollToElement(selector) {
  const alertElement = document.querySelector(selector);
  if (alertElement && alertElement.scrollIntoView) {
    alertElement.scrollIntoView();
  }
}

/**
 * Check whether the given CC is expired
 * @param month
 * @param year
 * @param timezone
 * @returns {boolean}
 */
export function isExpired(month, year, timezone) {//Pass 4 digit year value
  const FINAL_DATE_FORMAT = 'YYYY-MM-DD';
  const TEMP_DATE_FORMAT = 'YYYY-MM';
  const yearAndDate = year + '-' + month;
  let now;
  let cardExpiryDate;

  if (!checkNullOrUndefined(timezone)) {
    now = moment().tz(timezone);
    cardExpiryDate = moment(yearAndDate + '-' + moment(yearAndDate, TEMP_DATE_FORMAT).daysInMonth(), FINAL_DATE_FORMAT).tz(timezone);
  } else {
    now = moment();
    cardExpiryDate = moment(yearAndDate + '-' + moment(yearAndDate, TEMP_DATE_FORMAT).daysInMonth(), FINAL_DATE_FORMAT);
  }
  return cardExpiryDate.isBefore(moment(now.format(FINAL_DATE_FORMAT), FINAL_DATE_FORMAT));
}

export function getReceiptFormattedTime(timestamp, timezone) {
  return moment(timestamp).tz(timezone).format(DATE_FORMAT_STRINGS.RECEIPT);
}

export function getCurrencySymbol(countryCode) {
  return COUNTRY_CURRENCY_MAP[countryCode] || '';
}

export function updateUrlWithQueryParams(url, params = []) {
  return params.reduce((acc, { paramName, paramValue }) => `${acc}&${paramName}=${paramValue}`, url);
}
