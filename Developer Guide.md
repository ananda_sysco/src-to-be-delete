# Developer Goide
This document explains how to start development with OLO widget. This document will go through an example where you will add a new screen to the OLO App.

### Adding a component
This should go the relavant directory in scr/components
 -  Create a directory called test and add a react component TestComponent.js
 -  In the render method render an element with a  passed prop
 -  To demnonstrate how to bind the above prop to a value in the store, it will be bound to a filed called testValue in the store
 -  Following is an example for the test component
 

```

import React, {Component} from 'react';
import {connect} from 'react-redux';


export class TestComponent extends Component {
  render(){
    return <h1>{this.props.testProperty}</h1>;
  }
}

function mapState(state) {
  return {
    testProperty: state.app.testValue,
  };
}

const actions = {}; //Add actions here

export default connect(mapState, actions)(TestComponent);

```
 
 ##Using the component
 To demonstrate this we'll add this to Restaurant Page
 ```
 import TestComponent from "components/test/TestComponent";
 ```
 Render method will be changed like this
 ```
 return (
      <DocumentTitle title={title}>
        <Fragment>
          <LoginModal/>
          <ForgotPasswordModal/>
          <PasswordResetModal/>
          <InvalidProductModal/>
          <UserDetails></UserDetails>
          <TestComponent/>
          {children}
        </Fragment>
      </DocumentTitle>
 ```

Start both dev server and backend
Go to http://localhost:3000/{account_id}
You will be able to see the new component rendered





