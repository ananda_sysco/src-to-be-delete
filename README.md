# Cake Serve Front End
This repo consists the code base for Serve front end application for CAKE POS. This is a consumer facing application and contains a ReactJS frontend
 and a NodeJS backend.
This project is maintained by Falcon team. falcons_team@sysco.com

## Demo
Use `https://{env}-serve.cake.net/{accountId}` to view a demo of serve application.
- env: `tst1, tst3, plat1`
- accountId: Account ID of the merchant/operator 

## Architecture
Refer [Serve Architecture](https://syscolabs-jira.atlassian.net/wiki/spaces/CKB/pages/624034557/03+Cake+Serve+-+Phase+01+-+System+Analysis+and+Design
) document.
## Overview and Requirements

- Built with [Create React App](https://github.com/facebookincubator/create-react-app)
- node 8.11.1
- React 16
- Redux
- Styled Components
- Requires Node 8.x.x

## Env Vars

- For Env Vars used by the front end see `src/config.js`. These will be used at build time only
- For Env Vars used by the backend see `src/server/config.js`

## How to Run

- **Clone the repository from stash** [Serve](https://ananda_sysco@bitbucket.org/src-serve-fe.git)
- **Checkout the appropriate branch or create new branch from an existing branch** (Ex: `develop`)
- **Run:** 
    ```sh
    $ npm install
    ```
- **Change the configurations according to the environment**


- **Dev**:
  ```sh
  $ npm run server (Start API proxy)
  $ npm start (Start dev server)
  ```

- **Tests:** 
  ```sh
  $ npm test (Interactive)
  ```
 
  ```sh
  $ CI=true npm test (Non-interactive)
  ```
- **Story Book:** 
  ```sh 
  $ npm run storybook
  ```

- **Prod Build**: 
  ```sh
  $ npm run build (Make the optimized production build)
  $ npm run server (Start application)
  ```

  
#### Special Notes:
- During the **Dev** mode, both frontend and backend need to be started separately by issuing the commands specified under the **Dev** section.
(This is due to the use of `webpack-dev-server` for live reloading the frontend changes)
- During the **Prod build**, bundled frontend will be served by the backend itself at the `*` route. 

## Developer Guide
- Development branch: `develop`
- Create a new branch from `develop` (Ex: `feature/new-feature-branch`)
- Implement the required functionality
- Make a pull request from working branch (Ex: `feature/new-feature-branch`) to `develop`
- Merge the pull request after approving by all the persons included in the pull request
- Need to merge `develop` branch to `release` branch before going to the staging   and production 
- For an example refer [this](./Developer%20Guide.md)

## Code Organization

### `lib/ui`

Simple, themeable UI library written with Styled Components

### `src/server`

API proxy server written on Express. **This code is not transpiled.** It needs to be written using features available in Node 6.10.1 (until an upgrade).

### `src/store`

All code related to the redux store is here. It's organized in a modular fashion loosely based on the [Ducks](https://github.com/erikras/ducks-modular-redux) organization.

### `src/components`

This directory contains all of the React components for the app, organized by module. 

### `src/managers`

This directory contains the helper methods used for user related tasks. (Ex: mapping payment types for Update User API call)

### `test`

This directory contains test fixtures and utilities. Actual tests live alongside their target counterparts in `__tests__` directories.

## Deployment Procedure

Refer [Deploying Serve](https://syscolabs-jira.atlassian.net/wiki/spaces/CKB/pages/618696475/07+Cake+Serve+-+Phase+01+-+Deployment) for understanding the deployment procedure.
