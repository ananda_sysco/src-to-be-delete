// helpers
const initialState = {
  attributes: {},
  attributeSets: {},
  attributeValues: {},
  categories: {},
  currentSession: null, // id of valid session if applicable
  currentDay: null, // index of current day (1-7)
  notifications: [],
  products: {},
  restaurantInfo: null,
  restaurantError: null,
  selectedSession: null, // id
  sessions: {},
  sessionCategories: [],
  catalogIsLoading: false,
  disableCheckout: false
};

// reducer
export default function appReducer(state = initialState, action = {}) {
  return state;
}

