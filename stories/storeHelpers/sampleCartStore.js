export const orderTypes = {
  pickup: 'PICKUP',
  delivery: 'DELIVERY',
};

export const tipTypes = {
  percentage: 'PERCENTAGE',
  value: 'VALUE',
};

// helpers
const initialState = {
  items: [],
  empty: true,
  totalCost: 0,
  totalItems: 0,
  orderType: orderTypes.pickup,
  tax: 0,
  tip: null,
  tipPercentage: null,
  discountCode: null,
  discountType: null,
  discount: 0,
  grandTotal: 0,
  orderTime: null,
  deliveryAddress: null,
  deliveryFee: 0,
  totalIsLoading: false,
  orderIsSending: false,
  readyByOptions: [],
  invalidCartItems: [],
  isInvalidItemsModalShown: true,
  erroneousSession: null,
  sessionBasedInvalidItemsInCart: false
};

// reducer
export default function cart(state = initialState, action = {}) {
  return state;
}

