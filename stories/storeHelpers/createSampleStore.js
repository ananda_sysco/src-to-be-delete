import {createStore, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {autoRehydrate} from 'redux-persist';

import app from './sampleAppStore';
import cart from './sampleCartStore';
import user from './sampleUserStore';

const rootReducer = combineReducers({app, cart, user});

const store = createStore(rootReducer, composeWithDevTools(
  autoRehydrate(),
));

export default store;
