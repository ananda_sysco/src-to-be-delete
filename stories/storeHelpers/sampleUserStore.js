const initialState = {
  currentUser: null, //Current user is depreciated
  loggedInUser: null,
  isLoginModalShown: false,
  isForgotPasswordModalShown: false,
  isLogoutModalShown: false,
  isFlowWaiting: false,
};

export default function userReducer(state = initialState, action = {}) {
  return state;
}
