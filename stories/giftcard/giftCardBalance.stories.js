import React from 'react';

import {storiesOf} from '@storybook/react';

import styled, {ThemeProvider} from 'styled-components';
import theme from 'lib/ui/themes/cake';
import {FormInput, Section} from 'lib/StyledUI';
import {Button, Col, Grid, Heading, InfoIcon, Label, NotificationBar, Row, themed} from 'lib/ui';
import ReactTooltip from 'react-tooltip';
import {options} from 'lib/ui/utils/theme';
import Loading from 'components/global/Loading';


const NotificationWrapper = styled(Grid)`
  position: fixed;
  z-index: 999;
  left: 0;
  width: 100%;
`;
const HeaderBar = styled(Col)`
  border-bottom: 1px solid #d1ccc7;
  padding: 27px 17px;
  margin-bottom: 72px;
`;
const BalanceHeading = styled(Heading)`
  font-family: BrixSansLight;
  font-size: 2.05rem;
  margin-bottom: 4.1rem;
  margin-top: 0.5rem;
`;
const BalanceButton = styled(Button)`
  margin-top: 70px;
  margin-bottom: 100px;
`;
const FieldIconWrapper = styled.div`
  padding-top: 9px;
  margin-left: -43px;
`;
const BalanceLabel = styled(Heading)`
  font-family: "DuplicateSlabThin";
  font-size: 1.5rem;
  margin-bottom: 35px;
  margin-top: 0.5rem;
`;
const ClusterName = styled(Heading)`
  font-family: "BrixSansMedium";
  font-size: 1.5rem;
  font-weight: bold;
  border-top: ${options({
    Secondary: '7px solid #5f5f5f',
    Primary: '0px solid'}, 'Secondary')};
  display: ${options({
    Secondary: 'inline-block',
    Primary: 'static'}, 'Secondary')};
  padding-top: ${options({
    Secondary: '10px',
    Primary: '0px'}, 'Secondary')};
  margin-top: ${options({
    Secondary: '50px',
    Primary: '0px'}, 'Secondary')};
`;
const MerchantName = styled(Heading)`
  font-family: "BrixSansMedium";
  font-size: 1.125rem;
  font-weight: bold;
`;
const ClusterValue = styled(Label)`
  font-family: "BrixSansMedium";
  font-size: 3.375rem;
  font-weight: bold;
  margin-top: 0.5rem;
  margin-bottom: 1rem;
`;
const LastActivity = styled(Label)`
  font-family: "BrixSansLight";
  font-size: 1rem;
  color: #A8A7A4;
  margin-top: 0.5rem;
  margin-bottom: 1rem;
`;
const LocationSummaryHeading = styled.a `
  color: ${themed('colors.primary')};
  font-family: BrixSansLightItalic;
  font-size: 1.125rem;
  margin-top: 14px;
  display: block;
`;
const MerchantAddress = styled(Label)`
  color: ${themed('colors.primary')};
  font-family: BrixSansLightItalic;
  font-size: 0.875rem;
  color: #5f5f5f;
  display: block;
`;
const HorizontalDevider = styled.span`
  width: 100%;
  height: 1px;
  border-bottom: 1px solid #D1CCC7;
  margin-top: 5rem;
  margin-bottom: 3rem;
`;
const LoaderText = styled(Label)`
  color: ${themed('colors.primary')};
  font-family: BrixSansLight;
  font-size: 1rem;
  color: #A8A7A4;
`;
const LocationContainer = styled(Col)`
  margin-top:50px
`;
const LoaderWrapper = styled(Row)`
  margin-top:70px;
  margin-bottom: 91px;
`;

                storiesOf('Cake Gift Card', module).add('Check Balance', () => (
                    <ThemeProvider theme={theme}>
                        <Grid>
                            <Row>
                                <HeaderBar xs={12}>
                                    <svg width="110" viewBox="0 0 275 68">
                                        <g fill="#F2700F">
                                            <path
                                                d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                                            <polygon
                                                points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                                            <path
                                                d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
                                        </g>
                                    </svg>
                                </HeaderBar>
                            </Row>
                            <Row>
                                <Col xsOffset={1} xs={10} sm={8} md={7}>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceHeading>Check Your CAKE Gift Card Balance</BalanceHeading>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={7} md={6}>
                                            <FormInput
                                                name={'CardNumber'}
                                                placeholder={'Card Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6} sm={3} md={3}>
                                            <FormInput
                                                name={'PinNumber'}
                                                placeholder={'Pin Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper><ReactTooltip place='right' effect='solid' html={true}/></Col>
                                    </Row>
                                    <Section>
                                        <Col>
                                            <div
                                                class="g-recaptcha"
                                                data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
                                        </Col>
                                    </Section>
                                    <Row>
                                        <Col xs={12}>
                                            <Row center="xs" start="sm">
                                                <Col xs={8} sm={6} md={4}>
                                                    <BalanceButton primary full>Check Balance</BalanceButton>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>
                    </ThemeProvider>
                )).add('Check Balance - Invalid Card No', () => (
                    <ThemeProvider theme={theme}>
                        <Grid>
                            <Row>
                                <NotificationWrapper>
                                    <NotificationBar
                                        type={'error'}
                                        message={`Sorry, that card number or PIN isn't valid. Please try again`}
                                        actions={null}/></NotificationWrapper>
                                <HeaderBar xs={12}>
                                    <svg width="110" viewBox="0 0 275 68">
                                        <g fill="#F2700F">
                                            <path
                                                d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                                            <polygon
                                                points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                                            <path
                                                d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
                                        </g>
                                    </svg>
                                </HeaderBar>
                            </Row>
                            <Row>
                                <Col xsOffset={1} xs={10} sm={8} md={7}>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceHeading>Check Your CAKE Gift Card Balance</BalanceHeading>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={7} md={6}>
                                            <FormInput
                                                name={'CardNumber'}
                                                placeholder={'Card Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6} sm={3} md={3}>
                                            <FormInput
                                                name={'PinNumber'}
                                                placeholder={'Pin Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper><ReactTooltip place='right' effect='solid' html={true}/></Col>
                                    </Row>
                                    <Section>
                                        <Col>
                                            <div
                                                class="g-recaptcha"
                                                data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
                                        </Col>
                                    </Section>
                                    <Row>
                                        <Col xs={12}>
                                            <Row center="xs" start="sm">
                                                <Col xs={8} sm={6} md={4}>
                                                    <BalanceButton primary full>Check Balance</BalanceButton>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>
                    </ThemeProvider>
                )).add('Check Balance - Loading', () => (
                    <ThemeProvider theme={theme}>
                        <Grid>
                            <Row>
                                <HeaderBar xs={12}>
                                    <svg width="110" viewBox="0 0 275 68">
                                        <g fill="#F2700F">
                                            <path
                                                d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                                            <polygon
                                                points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                                            <path
                                                d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
                                        </g>
                                    </svg>
                                </HeaderBar>
                            </Row>
                            <Row>
                                <Col xsOffset={1} xs={10} sm={8} md={7}>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceHeading>Check Your CAKE Gift Card Balance</BalanceHeading>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={7} md={6}>
                                            <FormInput
                                                name={'CardNumber'}
                                                placeholder={'Card Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6} sm={3} md={3}>
                                            <FormInput
                                                name={'PinNumber'}
                                                placeholder={'Pin Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper><ReactTooltip place='right' effect='solid' html={true}/></Col>
                                    </Row>
                                    <Section>
                                        <Col>
                                            <div
                                                class="g-recaptcha"
                                                data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
                                        </Col>
                                    </Section>
                                    <Row>
                                        <Col xs={12}>
                                            {/* <Row center="xs" start="sm">
                                                <Col xs={8} sm={6} md={4}>
                                                    <BalanceButton primary full>Check Balance</BalanceButton>
                                                </Col>
                                            </Row> */}
                                            <LoaderWrapper start="xs" middle="xs">
                                                <Col xs={2} sm={2} md={1}>
                                                    <Loading size={45}/>
                                                </Col>
                                                <Col xs={10} sm={10} md={11}>
                                                    <LoaderText>...Checking card balance</LoaderText>
                                                </Col>
                                            </LoaderWrapper>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>
                    </ThemeProvider>
                )).add('View Results - Single Cluster', () => (
                    <ThemeProvider theme={theme}>
                        <Grid>
                            <Row>
                                <HeaderBar xs={12}>
                                    <svg width="110" viewBox="0 0 275 68">
                                        <g fill="#F2700F">
                                            <path
                                                d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                                            <polygon
                                                points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                                            <path
                                                d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
                                        </g>
                                    </svg>
                                </HeaderBar>
                            </Row>
                            <Row>
                                <Col xsOffset={1} xs={10} sm={8} md={7}>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceLabel xxlarge>Your Balance is:</BalanceLabel>
                                            <ClusterName Primary xlarge>Ike's Sandwiches</ClusterName>
                                            <ClusterValue>$45.23</ClusterValue>
                                            <LastActivity>Last use: May 23, 2018 (Redeem)</LastActivity>
                                        </Col>
                                        <Col xs={12}>
                                            <LocationSummaryHeading to="#" onClick={this.handleForgetPassword}>4 Locations</LocationSummaryHeading>
                                        </Col>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>Ike's Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>Ike's Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>Ike's Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                    </Row>
                                    <Row>
                                        <HorizontalDevider/>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceHeading>Check Another Card's Balance</BalanceHeading>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={7} md={6}>
                                            <FormInput
                                                name={'CardNumber'}
                                                placeholder={'Card Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6} sm={3} md={3}>
                                            <FormInput
                                                name={'PinNumber'}
                                                placeholder={'Pin Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper><ReactTooltip place='right' effect='solid' html={true}/></Col>
                                    </Row>
                                    <Section>
                                        <Col>
                                            <div
                                                class="g-recaptcha"
                                                data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
                                        </Col>
                                    </Section>
                                    <Row>
                                        <Col xs={12}>
                                            <Row center="xs" start="sm">
                                                <Col xs={8} sm={6} md={4}>
                                                    <BalanceButton primary full>Check Balance</BalanceButton>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>
                    </ThemeProvider>
                )).add('View Results - Multiple Clusters', () => (
                    <ThemeProvider theme={theme}>
                        <Grid>
                            <Row>
                                <HeaderBar xs={12}>
                                    <svg width="110" viewBox="0 0 275 68">
                                        <g fill="#F2700F">
                                            <path
                                                d="M207.5,64.8 C191.9,65.4 184,62.6 164.1,40.4 L154.9,47.9 L154.9,64.8 L144.5,64.8 L144.5,8.8 L154.9,8.8 L154.9,37.5 L189.5,8.8 L202.9,8.8 L171,34.9 C190.4,56.6 194.3,56 207.5,56 L207.5,64.8 Z"/>
                                            <polygon
                                                points="274.4 17.5 274.4 8.8 229.9 0 216.3 8.8 216.3 64.8 274.4 64.8 274.4 56 226.8 56 226.8 40.4 274.4 40.4 274.4 31.9 226.8 31.9 226.8 17.5"/>
                                            <path
                                                d="M105.2 8.8L95.2 8.8 65.6 64.8 76.7 64.8 83.8 50.9 116.7 50.9 124.1 64.8 135.4 64.8 105.2 8.8zM99.9 19.7L112 42.3 88.2 42.3 99.9 19.7zM34.2 58.3C19.6 58.3 10.6 49 10.6 37 10.6 25 19.5 15.7 33.4 15.7 41.9 15.7 50.6 19.2 56.6 24.4L62.5 17.5C55.9 11 45 6.9 34.2 6.9 13.8 6.9 0 20.4 0 37 0 55.1 14.4 67.1 33.5 67.1 45.6 67.1 56.2 62.9 63 55.9L57.1 49.5C51.5 54.7 42.8 58.3 34.2 58.3z"/>
                                        </g>
                                    </svg>
                                </HeaderBar>
                            </Row>
                            <Row>
                                <Col xsOffset={1} xs={10} sm={8} md={7}>
                                    <Row>
                                    <Col md={12}>
                                        <BalanceLabel xxlarge>Your Balance is:</BalanceLabel>
                                        <ClusterName Primary xlarge>Ike's Sandwiches</ClusterName>
                                        <ClusterValue>$45.23</ClusterValue>
                                        <LastActivity>Last use: May 23, 2018 (Redeem)</LastActivity>
                                    </Col>
                                    <Col xs={12}>
                                        <LocationSummaryHeading to="#" onClick={this.handleForgetPassword}>4 Locations</LocationSummaryHeading>
                                    </Col>
                                    <LocationContainer xs={12} sm={6} md={4}>
                                        <MerchantName large>Ike's Redwood City</MerchantName>
                                        <MerchantAddress>
                                            <div>123 Fourth St.</div>
                                            <div>Redwood City, CA</div>
                                        </MerchantAddress>
                                    </LocationContainer>
                                    <LocationContainer xs={12} sm={6} md={4}>
                                        <MerchantName large>Ike's Redwood City</MerchantName>
                                        <MerchantAddress>
                                            <div>123 Fourth St.</div>
                                            <div>Redwood City, CA</div>
                                        </MerchantAddress>
                                    </LocationContainer>
                                    <LocationContainer xs={12} sm={6} md={4}>
                                        <MerchantName large>Ike's Redwood City</MerchantName>
                                        <MerchantAddress>
                                            <div>123 Fourth St.</div>
                                            <div>Redwood City, CA</div>
                                        </MerchantAddress>
                                    </LocationContainer>
                                      <LocationContainer xs={12} sm={6} md={4}>
                                        <MerchantName large>Ike's Redwood City</MerchantName>
                                        <MerchantAddress>
                                          <div>123 Fourth St.</div>
                                          <div>Redwood City, CA</div>
                                        </MerchantAddress>
                                      </LocationContainer>
                                </Row>
                                    <Row>
                                        <Col md={12}>
                                            {/* <BalanceLabel>Your Balance is:</BalanceLabel> */}
                                            <ClusterName xlarge>McDonalds</ClusterName>
                                            <ClusterValue>$45.23</ClusterValue>
                                            <LastActivity>Last use: May 23, 2018 (Redeem)</LastActivity>
                                        </Col>
                                        <Col xs={12}>
                                            <LocationSummaryHeading to="#" onClick={this.handleForgetPassword}>4 Locations</LocationSummaryHeading>
                                        </Col>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>McDonalds Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>McDonalds Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                        <LocationContainer xs={12} sm={6} md={4}>
                                            <MerchantName large>McDonalds Redwood City</MerchantName>
                                            <MerchantAddress>
                                                <div>123 Fourth St.</div>
                                                <div>Redwood City, CA</div>
                                            </MerchantAddress>
                                        </LocationContainer>
                                    </Row>
                                    <Row>
                                        <HorizontalDevider/>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <BalanceHeading>Check Another Card's Balance</BalanceHeading>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={7} md={6}>
                                            <FormInput
                                                name={'CardNumber'}
                                                placeholder={'Card Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={6} sm={3} md={3}>
                                            <FormInput
                                                name={'PinNumber'}
                                                placeholder={'Pin Number'}
                                                error={this.error}
                                                disabled={this.disabled}
                                                onBlur={this.handleBlur}/>
                                        </Col>
                                        <Col>
                                            <FieldIconWrapper
                                                data-tip={`<img src=${require(`../../src/assets/wallet-scratch-pin.png`)} />`}
                                                data-html={true}><InfoIcon/></FieldIconWrapper><ReactTooltip place='right' effect='solid' html={true}/></Col>
                                    </Row>
                                    <Section>
                                        <Col>
                                            <div
                                                class="g-recaptcha"
                                                data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
                                        </Col>
                                    </Section>
                                    <Row>
                                        <Col xs={12}>
                                            <Row center="xs" start="sm">
                                                <Col xs={8} sm={6} md={4}>
                                                    <BalanceButton primary full>Check Balance</BalanceButton>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Grid>
                    </ThemeProvider>
                ));
