import React from 'react';
import {storiesOf} from '@storybook/react';
import {MemoryRouter} from 'react-router';
import DonationModal from '../../stories/gifts/DonationModal';


storiesOf('OLO Gifts', module)
    .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
    .add('Not Selected', () => (
        <DonationModal isCustomValueSelected={false} />
    ))
    .add('Set your amount selected', () => (
        <DonationModal isCustomValueSelected />
    ))
    .add('Custom amount with value', () => (
        <DonationModal isCustomValueSelected customAmount='125'/>
    ));
