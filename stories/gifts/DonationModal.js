import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, {ThemeProvider} from 'styled-components';
import Modal from 'react-modal';
import theme from 'lib/ui/themes/cake';
import { Button, Col, Row, themed } from 'lib/ui';
import { CloseButton, ExitRow, FormInput, ResponsiveModal, StyledContainer } from 'lib/StyledUI';
import { options } from 'lib/ui/utils/theme';

const CloseWrapper = styled(({ loading, ...props }) => <Col {...props} />)`
  background: ${options({
  loading: 'colors.foregroundLight',
  default: 'colors.background',
}, 'default')};
`;
const foreground = ({ theme }) => theme.colors.foreground;

export const DonationButton = styled(Button)`
  width: 89px;
  margin-top: 20px;
  height: 89px;
  border-radius: 6px;
  background-color: ${options({
    unselected: '#F2700F',
    selected: '#A74B00',
  }, 'unselected')};
  font-size: ${options({
    normal: '2rem',
    custom: '0.95rem',
  }, 'normal')};
`;


const CustomAmountInput = styled(FormInput)`
  padding: 0.5rem;  
  border-right: ${options({
    empty: '1px solid #F2700F !important',
    filled: '0px',
}, 'filled')};
`;



const Title = styled.h1`
  font-family: BrixSansMedium;
  font-size: 2.5rem;
  margin-bottom: 1rem;
  color: ${themed('colors.foreground')};
  margin-top: 1.8rem;
  margin-bottom: 5px;
  text-align: center;
  word-break: break-word;
`;

const DetailWrapper = styled(Col)`
  padding: 2% 12% 0 12%;
  font-family: BrixSansLight; 
  font-size: 1rem;
  line-height: 1.7rem;
  text-align: center;
  color: #5F5F5F;
  margin-bottom: 20px;
`;
const CustomInputWrapper = styled(Col)`
  margin-top: 37px;
  text-align: center;
  align-items: center;
  justify-content: center;
  display: flex;
`;
const SmallTextWrapper = styled(Col)`
  padding: 15px 20px 20px 20px;
  padding-top: ${options({
    empty: '80px',
    full: '15px',
}, 'empty')};
  font-family: BrixSansLight;
  font-size: 0.8rem;
  color:#484542;
`;
const Footer = styled(Row)`
  border-color: ${foreground};
  margin-top: 34px;
  margin-bottom: 30px;
`;

const ActionButton = styled(Button)`
  padding:1rem;
  font-size:1em;1.2em;
`;

const ActionButtonOutline = styled(Button)`
  padding:1rem;
  font-size:1em;1.2em;
  color: #EE7E04;
`;

const donationValues = [1, 5, 10, 20, 50];

class DonationModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      donationAmount: 0,
      isSelected: true,
      isCustomSelected: true
    }
  }
  static propTypes = {
    isValueSelected: PropTypes.bool,
    isCustomValueSelected: PropTypes.bool,
    customAmount: PropTypes.string
  };

  donationButtonsOnPress = (value) => {
    this.setState({ donationAmount: value });
  };

  renderPredefinedDonationValues() {
    return donationValues.map(value => (
      <DonationButton onPress={() => this.donationButtonsOnPress(value)}>
        ${value}
      </DonationButton>
    ));
  }

  customDonationOnChange = ({ target: { value } }) => {
    this.setState({ donationAmount: value });
  };

  handleProceed = (checkout = false) => {
    const { donationAmount } = this.state;
    const { isOnCheckoutPage, addDonation, setDonationOverlayDisplayCondition } = this.props;
    addDonation(donationAmount);
    setDonationOverlayDisplayCondition(false);
    if (checkout && !isOnCheckoutPage) {
      document.querySelector('#cart_checkout_btn').click();
    }
  };



  render() {
    const { setDonationOverlayDisplayCondition, isCustomValueSelected, customAmount  } = this.props;
    return (
    <ThemeProvider theme={theme}>
      <Modal
        isOpen={true}
        className={'DonationModal'}
        contentLabel={'Donation'}
        style={ResponsiveModal}
        shouldCloseOnOverlayClick={true}
        onRequestClose={() => setDonationOverlayDisplayCondition(false)}
      >
        <StyledContainer>
          <CloseWrapper xs={12}>
            <ExitRow end="xs">
              <Col xsOffset={10} xs={2} smOffset={11} sm={1}>
                <CloseButton onClick={() => setDonationOverlayDisplayCondition(false)} />
              </Col>
            </ExitRow>
            <Title>We Can’t Thank You Enough</Title>
            <DetailWrapper xs={12}>
              Your donation means that our reataurant will be able to weather this difficult time.
              <br />We thank you for supposrting us. Hoping we can see you in person soon!
            </DetailWrapper>
            <Row center="xs">
                <Col xs={9}>
                    <Row around="xs"  center="xs">
                    {this.renderPredefinedDonationValues()}
                    {isCustomValueSelected ? <DonationButton selected custom onPress={() => this.donationButtonsOnPress()}>
                        SET YOUR AMOUNT
                    </DonationButton> : <DonationButton custom onPress={() => this.donationButtonsOnPress()}>
                        SET YOUR AMOUNT
                    </DonationButton>}
                    </Row>
                </Col>
            </Row>
            {isCustomValueSelected &&
            <Row center="xs" className="customInput">
                <CustomInputWrapper xs={3}>
                <span className="currencySign">$</span>
                    <CustomAmountInput name={'custom'} placeholder={''}
                    onBlur={this.handleBlur} maxlength={'40'} value={customAmount} />
                <span className="decimalSign">.00</span>
                </CustomInputWrapper>
            </Row>
            }
            {isCustomValueSelected &&
            <Footer center="xs">
                <Col xs={9}>
                    <Row around="xs"  center="xs">
                        <Col xs={6}>
                            <ActionButtonOutline full primary outline onPress={this.handleProceed}>
                                Continue Shopping
                            </ActionButtonOutline>
                        </Col>
                        <Col xs={6}>
                            <ActionButton full primary onPress={() => { }}>
                                Checkout
                            </ActionButton>
                        </Col>
                    </Row>
                </Col>
            </Footer>}
            {isCustomValueSelected ?
            <SmallTextWrapper full xs={12}>
              *Lorem ipsum 100% of your donation will go directly to the restaurant
            </SmallTextWrapper> : <SmallTextWrapper xs={12}>
              *Lorem ipsum 100% of your donation will go directly to the restaurant
            </SmallTextWrapper>}
          </CloseWrapper>
        </StyledContainer>
      </Modal>
      </ThemeProvider>

    )
  }
}
export default DonationModal;
