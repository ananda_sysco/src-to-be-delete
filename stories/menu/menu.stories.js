import React from 'react';
import {storiesOf} from '@storybook/react';
import {MemoryRouter} from 'react-router';


storiesOf('OLO Menu', module)
    .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
    .add('Menu', () => (
        <div>To be added</div>
    ));