import React, {Component} from 'react';
import styled, {ThemeProvider} from 'styled-components';
import PropTypes from 'prop-types';
import theme from 'lib/ui/themes/cake';
import {FormInput} from 'lib/StyledUI';
import {
  Button,
  CakeLogo,
  Col,
  Container,
  Grid,
  Heading,
  Label,
  Row,
  Select,
  themed,
  UserDarkIcon,
  UserIcon
} from 'lib/ui';
import GlobalHeader from '../../stories/search/GlobalHeader';
import Footer from 'components/global/Footer';
import GoogleMap from '../../stories/search/GoogleMap';
import RestaurantListItem from '../../stories/search/SearchItem';

const SearchRow = styled(Row)`
min-height: calc(100vh - 115px);
`;
const RestaurantContainer = styled.div`
display: flex;
flex-direction: row;
padding-left: 0.5rem;
padding-right: 0.5rem;
`;

const RestaurantListWrapper = styled.div`
display: flex;
flex-direction: column;
width: 345px;
@media (max-width: 767px)
{
    width: 100vw;
}
`;
const MapContainer = styled.div`
width: calc(100vw - 345px);
height: calc(100vh - 115px);
display: flex;

@media (max-width: 767px)
{
    width: 100%;
    height: calc(100vh - 239px);
}
`;
const SearchButton = styled(Button)`
display: inline;
margin-left: 5px;
padding-top: 7px;
padding-bottom: 7px;
width: 78px;
flex: 0 1 auto;
font-size: 13px;
font-family: "BrixSansLight"
@media (max-width: 767px)
{
    font-size: 1rem;
    padding: 0.5rem;
}
`;
const AppContainer = styled(Container)`
position: relative;
padding: 0 !important;
background-color: transparent;
@media (max-width: 767px) {
  overflow-x: hidden;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;  
}
`;
const SearchInput = styled(FormInput)`
width: 216px;
font-size: 13px;
margin-bottom: 0px;
display: inline;
padding-top: 2px;
padding-bottom: 2px;
@media (max-width: 767px) {
    width: calc(100vw - 80px);
}
`;
const PlaceholderStateInput = styled.div`
width: 220px;
background-color: #fff;
font-family: BrixSansLight;
color: #363532;
display: inline-block;
height: 24px;
padding: 0.5rem;
font-size: 0.875rem;
border-radius: 5px;
border-color: #d1ccc7;
border-width: 1px;
border-style: solid;
outline: none;
line-height: 24px;
span {
    color: #A8A7A4;
    margin-left: 10px;
}
`;

// Search Results
const RestaurantFilter = styled.div`
background-color: #F0EFEB;
padding: 12px 13px;
`;
const MobileMapContainer = styled.div`
width: 100%;
}
`;
const RestaurantList = styled.div`
height: calc(100vh - 169px);
overflow-y:auto;
`;
export default class SearchResults extends Component {
    //This is a temporary prop for presentaiton purposes only. Will not be required in development.
    static propTypes = {
        isMapView: PropTypes.bool,
      };
      renderFilter() {
          return <RestaurantFilter className="test">
        {/* <PlaceholderStateInput>I'm Near Lorem<span>Street Address, City or Zip Code</span></PlaceholderStateInput> */}
        <SearchInput placeholder={'Street Address, City or Zip Code'} />
        <div className="searchResultButton">
            <SearchButton primary full>Search</SearchButton>
        </div>
        <div className="switchViewIcon">
        {!this.props.isMapView ? <UserDarkIcon /> : <UserIcon /> }
        </div>
       </RestaurantFilter>
      }
  render() {


    return (
        <ThemeProvider theme={theme}>
            <AppContainer fluid className="appContainer">
                <Grid fluid className="searchContainer searchResults">
                    <GlobalHeader />
                    <SearchRow>
                    {!this.props.isMapView ?
                        <RestaurantContainer>
                         <RestaurantListWrapper>
                                {this.renderFilter()}
                                <RestaurantList>
                                    <RestaurantListItem></RestaurantListItem>
                                    <RestaurantListItem isRestaurantClosed ></RestaurantListItem>
                                </RestaurantList>
                            </RestaurantListWrapper>
                            <MapContainer className="hideFromMobile">
                                <GoogleMap>
                                </GoogleMap>
                            </MapContainer>
                        </RestaurantContainer> :
                        <MobileMapContainer>{this.renderFilter()}<MapContainer>
                                <GoogleMap>
                                </GoogleMap>
                            </MapContainer>
                            <RestaurantListItem></RestaurantListItem>
                        </MobileMapContainer>
                             }
                    </SearchRow>
                </Grid>
                <Footer/>
            </AppContainer>
        </ThemeProvider>
    );
  }

}
