import React from 'react';
import {storiesOf} from '@storybook/react';

import { MemoryRouter } from 'react-router';
import SearchResults from '../../stories/search/SearchResults';

storiesOf('OLO Search Results', module).addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
)).add('Search Results - Restaurant List', () => (
    <SearchResults isMapView={false} />
))
.add('Search Results - Map View', () => (
    <SearchResults isMapView={true} />
));