import React from 'react';
import {storiesOf} from '@storybook/react';
import {MemoryRouter} from 'react-router';
import SearchHome from '../../stories/search/SearchHome';


storiesOf('OLO Search', module)
    .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
))
    .add('Search Home', () => (
        <SearchHome />
    ));