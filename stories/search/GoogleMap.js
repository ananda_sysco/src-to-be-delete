import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const ReactComponent = ({ text }) => <div>{text}</div>;

class GoogleMap extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };

  render() {
    return (
      <div style={{ height: 'inherit', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{  }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <ReactComponent
            lat={59.955413}
            lng={30.337844}
            text={'Kreyser Avrora'}
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default GoogleMap;