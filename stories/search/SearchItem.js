import React, { Component } from 'react';
import { render } from 'react-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {options} from 'lib/ui/utils/theme';


import { Heading } from 'lib/ui';

const ListItem = styled.div`
display: flex;
flex-flow: row wrap;
padding-top:20px;
padding-bottom: 20px;
border-bottom: 0.72px solid rgba(209,204,199,0.5);
padding-right: 10px;
span{
    font-family: "BrixSansLight";
    font-size: 10px;
    color: #fff;
    background-color: ${props => props.isRestaurantClosed ? '#363532' : '#84BD00'};    
    border-radius: 4px;
    padding: 1px 10px 2px 10px;
}
  background-color: ${props => props.isRestaurantClosed ? '#F0EFEB' : 'transparent'};
  opacity: ${props => props.isRestaurantClosed ? '0.3' : '1'};  
`;
const CusineType = styled.div`
font-family: "BrixSansLightItalic";
font-size: 12px;
font-weight: bold;
color: #5f5f5f;
margin-top: 8px;
`;
const ItemNo = styled.div`
width: 27px;
text-align:right;
font-family: "BrixSansLight";
font-size: 14px;
color: #5f5f5f;
padding-right: 3px;
padding-top: 4px;
box-sizing: content-box;
`;
const ItemDescription = styled.div`
flex: 1;
`;
const TagContainer = styled.div`
width: 100%;
padding-left:30px;
margin-bottom:17px;
`;
const AddressLine = styled.div`
font-family: "BrixSansLight";
font-size: 12px;
color: #A8A7A4;
margin-top:3px;
`;
const RestaurantName = styled(Heading)`
font-family: "BrixSansLight";
font-size: 20px;
color: #5f5f5f;
margin: 0px;
`;


export default class RestaurantListItem extends Component {
    static propTypes = {
        isRestaurantClosed: PropTypes.bool,
      };
    
  render() {
    return (
        <ListItem isRestaurantClosed={this.props.isRestaurantClosed}>
            <TagContainer><span>{this.props.isRestaurantClosed ? "OPENS TOMORROW @ 10AM" : "DELIVERY AVAILABLE"}</span></TagContainer>
            <ItemNo>1.</ItemNo>
            <ItemDescription>
            <RestaurantName large>Cafe Layers</RestaurantName>
            <AddressLine>101 Redwood Shores Parkway, Redwood City, CA 94065</AddressLine>
            <CusineType>AMERICAN</CusineType>
            </ItemDescription>
        </ListItem>
    );
  }

}
