import React, { Component } from 'react';
import styled, {ThemeProvider} from 'styled-components';
import theme from 'lib/ui/themes/cake';
import {FormInput} from 'lib/StyledUI';
import {
    Button,
    Col,
    Grid,
    Heading,
    UserDarkIcon,
    Label,
    Row,
    CakeLogo
} from 'lib/ui';
import {Container, Select} from 'lib/ui';
import GlobalHeader from '../../stories/search/GlobalHeader';
import Footer from 'components/global/Footer';

const SearchRow = styled(Row)`
background-image: url(${require(`../../src/assets/hero.png`)});
background-size: cover;
background-repeat: no-repeat;
min-height: calc(100vh - 115px);
.cakeLogo{
    margin-top: 18px;
    margin-left: 0px;
    width:138px;
}
@media (max-width: 767px)
{
    background-image: none;
}
`;
const SearchContent = styled(Col)`
`;
const SearchHeader = styled(Heading)`
font-family: "DuplicateSlabThin";
color: #fff;
font-size: 60px;
text-align:center;
margin-top: 140px;
@media (max-width: 767px)
{
    font-size: 32px;
    margin-top: 42px;
    margin-bottom: 62px;
    color: #5F5F5F;
}
`;
const SearchButton = styled(Button)`
display: inline;
margin-left: 5px;
padding-top: 12px;
padding-bottom: 13px;
width: 104px;
flex: 0 1 auto;
font-size: 14px;
font-family: "BrixSansLight";
@media (max-width: 767px)
{
    display: block;
    margin-top: 20px;
    margin-bottom: 50px;
    margin-left: 0px;
    width: 100%;
}
`;
const AppContainer = styled(Container)`
position: relative;
padding: 0 !important;
background-color: transparent;
@media (max-width: 767px) {
  overflow-x: hidden;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;  
}
`;
const SearchDescription = styled(Label)`
font-family: "BrixSansLight";
font-size: 18px;
display: block;
text-align: center;
color: #fff;
padding-top: 35px;
`;
const SearchFieldContainer = styled.div`
 margin: 0 auto;
 max-width: 665px;

`;
const SearchInput = styled(FormInput)`
min-width: 75%;
margin-bottom: 0px;
display: inline;
@media (max-width: 767px)
{
    width: 100%;
    height: 42px;
    box-sizing: border-box;
}
`;
const PlaceholderStateInput = styled.div`
width: 527px;
background-color: #fff;
font-family: BrixSansLight;
color: #363532;
display: inline-block;
height: 24px;
padding: 0.5rem;
font-size: 0.875rem;
border-radius: 5px;
border-color: #d1ccc7;
border-width: 1px;
border-style: solid;
outline: none;
line-height: 24px;
span {
    color: #A8A7A4;
    margin-left: 10px;
}
`;

export default class SearchHome extends Component {
    
  render() {
    const profileoptions = [
        {
            value: 'profile',
            label: 'Profile & Order History'
        }, {
            value: 'logout',
            label: 'Logout'
        }
    ];
    
    return (
        <ThemeProvider theme={theme}>
            <AppContainer fluid className="appContainer">
                <Grid fluid className="searchContainer searchHome">
                <GlobalHeader />
                <SearchRow center="xs">
                        <SearchContent xs={10}>
                        <div className="showOnMobile">
                            <CakeLogo />
                        </div>
                            <SearchHeader>Find the best food around you.</SearchHeader>
                            
                                <SearchFieldContainer>
                                    {/* <PlaceholderStateInput>I'm Near Lorem<span>Street Address, City or Zip Code</span></PlaceholderStateInput> */}
                                    <SearchInput placeholder={'Street Address, City or Zip Code'} />
                                    <SearchButton primary full>Search</SearchButton>
                                </SearchFieldContainer>
                                
                            <SearchDescription className="hideFromMobile" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus</SearchDescription>
                        </SearchContent>
                    </SearchRow>
                </Grid>
                <Footer/>
            </AppContainer>
        </ThemeProvider>
    );
  }

}
