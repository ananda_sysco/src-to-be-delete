import React, {Component} from 'react';
import styled from 'styled-components';
import {CakeLogo, Col, Row, Select, UserDarkIcon, UserOutlineIcon} from 'lib/ui';

import '../../src/lib/ui/global/search.css';
import Link from 'components/app/router/Link';

const GlobalHeader = styled(Row)`
height: 54px;
align-items: center;
`;
const HeaderLinks = styled(Col)`
height: 100%;
`;
const HeaderItem = styled.div`
float: right;
height: 100%;
width: 32px;
padding-right: 20px;
padding-left: 20px;
text-align:center;
svg {
    width: 20px;
    margin-top:14px;
}
&:hover{
    background-color: #A8A7A4;
}
`;
const LoginLink = styled(Link)`
font-family: BrixSansLight;
font-size: 0.9rem;
&:focus {
    outline: none;
}
`;


export default class RestaurantListItem extends Component {

  render() {
    const profileoptions = [
        {
            value: 'profile',
            label: 'Profile & Order History'
        }, {
            value: 'logout',
            label: 'Logout'
        }
    ];
    return (
        <GlobalHeader className="globalHeader">
            <Col xs={2}>
                <CakeLogo />
            </Col>
            <Col xs={6}>
            </Col>
            <HeaderLinks xs={4}>
                <HeaderItem className>{/* <LoginLink to="#">Sign In</LoginLink> */}<UserDarkIcon/></HeaderItem><Select
                    className="profileMenu"
                    clearable={false}
                    searchable={false}
                    value={"selectedSession"}
                    options={profileoptions}
                    placeholder={""}
            />
            </HeaderLinks>
        </GlobalHeader>
    );
  }
}
