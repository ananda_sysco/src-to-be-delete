import React from 'react';
import {merge, each, set} from 'lodash';
import {Provider} from 'react-redux';
import store from './storeHelpers/createSampleStore';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {linkTo} from '@storybook/addon-links';
import styled from 'styled-components';

import {Container} from '../src/lib/ui';
import cakeTheme from '../src/lib/ui/themes/cake';
import {ThemeProvider} from 'styled-components';

import InvalidProductModal from "../src/components/product/InvalidProductModal";
import PaymentCards from "../src/components/card/PaymentCards";

import {themed, Button} from '../src/lib/ui';

const theme = merge({}, cakeTheme);

const EditButton = styled(Button);
`
  padding-left: 0;
  padding-right: ${cakeTheme.layout.gutter.xsmall}rem;
`;
storiesOf('Button', module).add('with text', () => <ThemeProvider theme={theme}><Provider
  store={store}><InvalidProductModal/></Provider></ThemeProvider>);
storiesOf('Button', module).add('with text 2', () => <ThemeProvider theme={theme}><Button small link>
  edit </Button></ThemeProvider>);
storiesOf('Payment Cards', module).add('default', () => <ThemeProvider theme={theme}><PaymentCards/></ThemeProvider>);
