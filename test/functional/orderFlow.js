/* eslint-disable max-len */
import 'test_helper';
import { get, head } from 'lodash';
import config from 'config';
import IDM from 'lib/IDM';
import ConsumerApi from 'lib/ConsumerApi';

let accessToken = '';
const testMerchantId = 'c20-cafeiveta95060';

const consumerApi = new ConsumerApi(config);
const idm = new IDM(config);

describe('order flow', () => {
  beforeAll(async () => {
    const authInfo = await idm.getAccessToken();
    accessToken = get(authInfo, 'access_token');
    expect(accessToken).toBeTruthy();
  });

  it('should get restaurant, catalog & item details', async () => {
    const restaurant = await consumerApi.getRestaurantInfo(accessToken, testMerchantId);
    expect(restaurant).toBeTruthy();
    const session = head(restaurant.sessions.session);
    if (session) {
      expect(session.id).toBeTruthy();
      expect(session.name).toBeTruthy();
    }
  });
});
