# How to run server tests
### `npm run server_test`

# Passing parameters to test
### `npm run server_test -- --merchantId='123456'`

##### Note 1st double dash (--) is important
## Parameters that can be passed
* merchantId

