export default async (reducer, initialState, action, getState) => {
  let reduced = null;

  const dispatch = (a) => {
    reduced = reducer(initialState, a);
  };

  await action(dispatch, getState);

  return reduced;
};
