export default {
  id: '6e6d4aa1-1187-416f-ecaf-82da8e476280',
  name: 'Home-made Granola',
  description: 'A tasty mix of flax, oats, honey, and nuts together with plain yogurt and strawberries.',
  merchantId: 'c20-cafeiveta95060',
  reference: '1',
  price: 6,
  isAuxiliary: false,
  attributeSet: {
    id: 'c09ebe76-7db4-437b-f2bc-c2f850e25f88',
    name: 'Default',
    attributes: {
      attribute: [
        {
          id: 'b2e920c9-b01d-46a4-eff3-e5a6a7483161',
          name: 'Additions',
          maxSelections: 1,
          minSelections: 0,
          values: {
            value: [
              {
                attributeValueId: '56608544-07b2-4b31-9118-a6ccb529c177',
                value: 'Add Banana',
                price: 1,
              },
              {
                attributeValueId: 'af35606b-f7fe-4a0b-f3ea-895efe17c439',
                value: 'Add Strawwberries',
                price: 1.5,
              },
            ],
          },
        },
        {
          id: '1',
          name: 'Special Instructions',
          maxSelections: 0,
          minSelections: 0,
          values: {
            value: [],
          },
        },
      ],
    },
  },
  taxCategoryId: '001',
  kitchenName: 'Home-made Granola 12 oz',
  receiptName: 'Home-made Granola',
};

export const BLT = {
  'id': 'fb4e9a40-df9d-4dce-8005-0f858afbe9a9',
  'name': 'BLT',
  'description': 'Bacon, butter lettuce, and Roma tomatoes on sourdough bread with spicy home-made mayonnaise. Add avocado for $1 and poached egg for $2.',
  'merchantId': 'c20-cafeiveta95060',
  'reference': '25',
  'price': 8,
  'isAuxiliary': false,
  'attributeSet': {
    'id': '91d57046-bb0d-4d0a-f0bc-5df6f7d81314',
    'name': 'Default',
    'attributes': {
      'attribute': [
        {
          'id': 'fae485b2-a322-48a0-df48-b9f0dd39a82c',
          'name': 'Substitute',
          'maxSelections': 1,
          'minSelections': 0,
          'values': {
            'value': [
              {
                'attributeValueId': '62d05857-57a7-47e0-cd13-49856b202ffb',
                'value': 'Substitute Gluten-Free Bread',
                'price': 1.5,
              },
            ],
          },
        },
        {
          'id': 'a96df2f5-02ab-43e6-e1a7-881782cddc50',
          'name': 'Additions',
          'maxSelections': 4,
          'minSelections': 0,
          'values': {
            'value': [
              {
                'attributeValueId': '49874a30-2471-497f-c81b-985760436a21',
                'value': 'Add Avocado',
                'price': 1.5,
              },
              {
                'attributeValueId': '8db05387-633f-483f-e573-2918c3bfc310',
                'value': 'Add Organic Fried Egg',
                'price': 1.5,
              },
              {
                'attributeValueId': '7eb97be9-1d55-4fc2-962c-5a8a9e0cfaf3',
                'value': 'Organic Turkey',
                'price': 3,
              },
              {
                'attributeValueId': '33464aa9-b249-4e43-b25a-a1951b584ae4',
                'value': 'Pesto',
                'price': 1,
              },
              {
                'attributeValueId': '19085ecf-7b07-4ec8-e3f3-ba067592259c',
                'value': 'Add Hot Giardinara',
                'price': 1,
              },
            ],
          },
        },
        {
          'id': 'f1323632-222b-4617-9660-617b913a7304',
          'name': 'Exceptions',
          'maxSelections': 1,
          'minSelections': 0,
          'values': {
            'value': [
              {
                'attributeValueId': '0ba7642c-7e74-4e33-87e9-ea3552603b83',
                'value': 'No Mayonnaise',
              },
              {
                'attributeValueId': '94e59c39-f55d-4f51-eefd-9a029aca7c13',
                'value': 'No Tomato',
              },
              {
                'attributeValueId': '534036c7-5094-4b6f-f9aa-aebea36cf9b3',
                'value': 'No Lettuce',
              },
            ],
          },
        },
        {
          'id': 'c122dd87-161c-4d07-cea6-71085b7d0fba',
          'name': 'Subsitutions',
          'maxSelections': 4,
          'minSelections': 0,
          'values': {
            'value': [
              {
                'attributeValueId': '828fa060-a99b-4852-cbd3-15a1aa3552ac',
                'value': 'Sub Ciabatta',
              },
              {
                'attributeValueId': '77e9e037-6ed0-4f38-efca-28e1f7fa7cd5',
                'value': 'Sub Wheat Bread',
              },
              {
                'attributeValueId': '921c200f-8ce2-4b52-a351-7bde20add0fb',
                'value': 'Sub Regular Mayo',
              },
              {
                'attributeValueId': '9788614e-28d5-4d72-a7cd-3da299608731',
                'value': 'Sub Croissant',
                'price': 1,
              },
              {
                'attributeValueId': 'f2e06f67-5414-4d2d-c3c4-2cd707c4d007',
                'value': 'Wrap',
              },
            ],
          },
        },
        {
          'id': '1',
          'name': 'Special Instructions',
          'maxSelections': 0,
          'minSelections': 0,
          'values': {
            'value': [],
          },
        },
      ],
    },
  },
  'taxCategoryId': '001',
  'kitchenName': 'BLT',
  'receiptName': 'BLT',
};
