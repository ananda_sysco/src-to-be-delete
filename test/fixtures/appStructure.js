export default {
  app: {
    attributes: {
      '1': {
        id: '1',
        name: 'Special Instructions',
        maxSelections: 0,
        minSelections: 0,
        values: []
      },
      'd6f39d19-e593-4ad7-ce77-8e8730b4b9da': {
        id: 'd6f39d19-e593-4ad7-ce77-8e8730b4b9da',
        name: 'Additions',
        maxSelections: 7,
        minSelections: 0,
        values: [
          '46fffebb-7a89-45fd-8ca4-6e07361a01af',
          'a083a089-b4f3-4174-face-7b0b69681cd6',
          'ca549d5f-ac76-4077-d13a-9738020ad498',
          'c0f95e6d-6820-40f1-c735-f15b3fedde0c',
          'a92efeef-6692-41e8-9113-a5d62e91f20e',
          '1e778980-b619-4b4d-80e4-9e2e4c28ee79',
          '29738529-9522-4c18-cb26-0ea9578b845d',
          '0b3b0a7c-5578-4a9c-b31e-f93cfdab3ac6',
          'ccd303c4-d85b-42e9-82f3-e192135018be'
        ]
      },
      'b86c582d-6805-4fd7-f566-d57ca1aabe8c': {
        id: 'b86c582d-6805-4fd7-f566-d57ca1aabe8c',
        name: 'Exceptions',
        maxSelections: 2,
        minSelections: 0,
        values: [
          '24f914e5-97f4-4eab-8831-17f2940156eb',
          '239c880c-c6aa-40b8-b33f-c5ff91f34482'
        ]
      },
      '4bbcfe6e-ca99-425c-c3ce-c4270ffea650': {
        id: '4bbcfe6e-ca99-425c-c3ce-c4270ffea650',
        name: 'Add-Ons',
        maxSelections: 6,
        minSelections: 0,
        values: [
          '4b409c5c-07df-4bca-dff0-d8981815cf06',
          '28a25a62-8cf8-47ec-f05e-5c94e427ea41',
          '6ec65d1f-2875-4ada-fca0-116cb75c8a8f',
          'a409286d-fc3b-4e75-fea5-2950f2fccbc3',
          '4410b15e-c2a0-443b-accd-96f22b32d722',
          '5d2f0d6a-cbe1-400b-ef3e-36cffeabf943'
        ]
      },
      '1128647e-3d94-4bdc-cebf-38f5a513d0ad': {
        id: '1128647e-3d94-4bdc-cebf-38f5a513d0ad',
        name: 'Syrup Flavors',
        maxSelections: 9,
        minSelections: 0,
        values: [
          'a5c47a0a-81e5-4bcc-f32b-27f7dab449af',
          'c08b7ab5-9cc0-4c57-d270-0d1dd6b1ace9',
          'fc5f10ec-7195-4456-a5eb-a7a5d3562cb2',
          '5f7358f3-1f85-47bc-a54a-d2383d62beb3',
          '54670045-96c0-407c-ca20-213e9015a7ea',
          '12f8d690-1829-4cd9-b410-b5f48e02763e',
          'ef5ae7d4-e78d-4fdc-ebf8-a774fdad94d0',
          'bebb091d-4fb7-44d6-e198-31e6bc4ca0fc',
          '7bcc723b-b7c8-4751-fcb9-e6919e8af6d6',
          'c87120d6-93ff-4526-f389-d329256ed9d7'
        ]
      }
    },
    attributeSets: {
      '3d83b2eb-e386-4490-e50a-1a309eb7c46a': {
        id: '3d83b2eb-e386-4490-e50a-1a309eb7c46a',
        name: 'Default',
        attributes: [
          '1'
        ]
      },
      'c4890bd1-e4e5-47ed-d9ab-68edfd6f46e3': {
        id: 'c4890bd1-e4e5-47ed-d9ab-68edfd6f46e3',
        name: 'Default',
        attributes: [
          'd6f39d19-e593-4ad7-ce77-8e8730b4b9da',
          'b86c582d-6805-4fd7-f566-d57ca1aabe8c',
          '1'
        ]
      },
      'd8522630-e52f-4f17-95e0-e6717372004c': {
        id: 'd8522630-e52f-4f17-95e0-e6717372004c',
        name: 'Au Lait',
        attributes: [
          '4bbcfe6e-ca99-425c-c3ce-c4270ffea650',
          '1128647e-3d94-4bdc-cebf-38f5a513d0ad',
          '1'
        ]
      }
    },
    attributeValues: {
      '46fffebb-7a89-45fd-8ca4-6e07361a01af': {
        attributeValueId: '46fffebb-7a89-45fd-8ca4-6e07361a01af',
        value: 'Add Avocado',
        price: 1.5
      },
      'a083a089-b4f3-4174-face-7b0b69681cd6': {
        attributeValueId: 'a083a089-b4f3-4174-face-7b0b69681cd6',
        value: 'Add 1 Slice of Bacon',
        price: 1
      },
      'ca549d5f-ac76-4077-d13a-9738020ad498': {
        attributeValueId: 'ca549d5f-ac76-4077-d13a-9738020ad498',
        value: 'Add 2 Slices of Bacon',
        price: 2
      },
      'c0f95e6d-6820-40f1-c735-f15b3fedde0c': {
        attributeValueId: 'c0f95e6d-6820-40f1-c735-f15b3fedde0c',
        value: 'Add Sourdough Toast',
        price: 1
      },
      'a92efeef-6692-41e8-9113-a5d62e91f20e': {
        attributeValueId: 'a92efeef-6692-41e8-9113-a5d62e91f20e',
        value: 'Add Wheat Toast',
        price: 1
      },
      '1e778980-b619-4b4d-80e4-9e2e4c28ee79': {
        attributeValueId: '1e778980-b619-4b4d-80e4-9e2e4c28ee79',
        value: 'Add 1 Sausage',
        price: 1
      },
      '29738529-9522-4c18-cb26-0ea9578b845d': {
        attributeValueId: '29738529-9522-4c18-cb26-0ea9578b845d',
        value: 'Add 2 Sausages',
        price: 2
      },
      '0b3b0a7c-5578-4a9c-b31e-f93cfdab3ac6': {
        attributeValueId: '0b3b0a7c-5578-4a9c-b31e-f93cfdab3ac6',
        value: 'Add 1 Pork Sausage',
        price: 1
      },
      'ccd303c4-d85b-42e9-82f3-e192135018be': {
        attributeValueId: 'ccd303c4-d85b-42e9-82f3-e192135018be',
        value: 'Add 2 Pork Sausage',
        price: 2
      },
      '24f914e5-97f4-4eab-8831-17f2940156eb': {
        attributeValueId: '24f914e5-97f4-4eab-8831-17f2940156eb',
        value: 'No Champagne Vinaigrette'
      },
      '239c880c-c6aa-40b8-b33f-c5ff91f34482': {
        attributeValueId: '239c880c-c6aa-40b8-b33f-c5ff91f34482',
        value: 'No Parmesan'
      },
      '4b409c5c-07df-4bca-dff0-d8981815cf06': {
        attributeValueId: '4b409c5c-07df-4bca-dff0-d8981815cf06',
        value: 'Add Shot of Espresso',
        price: 1.5
      },
      '28a25a62-8cf8-47ec-f05e-5c94e427ea41': {
        attributeValueId: '28a25a62-8cf8-47ec-f05e-5c94e427ea41',
        value: 'Add Double Shot of Espresso',
        price: 2
      },
      '6ec65d1f-2875-4ada-fca0-116cb75c8a8f': {
        attributeValueId: '6ec65d1f-2875-4ada-fca0-116cb75c8a8f',
        value: 'Add Almond Milk',
        price: 0.75
      },
      'a409286d-fc3b-4e75-fea5-2950f2fccbc3': {
        attributeValueId: 'a409286d-fc3b-4e75-fea5-2950f2fccbc3',
        value: 'Add Soy Milk',
        price: 0.75
      },
      '4410b15e-c2a0-443b-accd-96f22b32d722': {
        attributeValueId: '4410b15e-c2a0-443b-accd-96f22b32d722',
        value: 'Sub Half n Half'
      },
      '5d2f0d6a-cbe1-400b-ef3e-36cffeabf943': {
        attributeValueId: '5d2f0d6a-cbe1-400b-ef3e-36cffeabf943',
        value: 'Sub Non Fat Milk'
      },
      'a5c47a0a-81e5-4bcc-f32b-27f7dab449af': {
        attributeValueId: 'a5c47a0a-81e5-4bcc-f32b-27f7dab449af',
        value: 'Vanilla Syrup',
        price: 0.5
      },
      'c08b7ab5-9cc0-4c57-d270-0d1dd6b1ace9': {
        attributeValueId: 'c08b7ab5-9cc0-4c57-d270-0d1dd6b1ace9',
        value: 'Almond Syrup',
        price: 0.5
      },
      'fc5f10ec-7195-4456-a5eb-a7a5d3562cb2': {
        attributeValueId: 'fc5f10ec-7195-4456-a5eb-a7a5d3562cb2',
        value: 'Hazelnut Syrup',
        price: 0.5
      },
      '5f7358f3-1f85-47bc-a54a-d2383d62beb3': {
        attributeValueId: '5f7358f3-1f85-47bc-a54a-d2383d62beb3',
        value: 'Peppermint Syrup',
        price: 0.5
      },
      '54670045-96c0-407c-ca20-213e9015a7ea': {
        attributeValueId: '54670045-96c0-407c-ca20-213e9015a7ea',
        value: 'Banana Syrup',
        price: 0.5
      },
      '12f8d690-1829-4cd9-b410-b5f48e02763e': {
        attributeValueId: '12f8d690-1829-4cd9-b410-b5f48e02763e',
        value: 'Pumpkin Spice Syrup',
        price: 0.5
      },
      'ef5ae7d4-e78d-4fdc-ebf8-a774fdad94d0': {
        attributeValueId: 'ef5ae7d4-e78d-4fdc-ebf8-a774fdad94d0',
        value: 'Irish Cream Syrup',
        price: 0.5
      },
      'bebb091d-4fb7-44d6-e198-31e6bc4ca0fc': {
        attributeValueId: 'bebb091d-4fb7-44d6-e198-31e6bc4ca0fc',
        value: 'Sugar Free Vanilla',
        price: 0.5
      },
      '7bcc723b-b7c8-4751-fcb9-e6919e8af6d6': {
        attributeValueId: '7bcc723b-b7c8-4751-fcb9-e6919e8af6d6',
        value: 'Sugar Free Almond',
        price: 0.5
      },
      'c87120d6-93ff-4526-f389-d329256ed9d7': {
        attributeValueId: 'c87120d6-93ff-4526-f389-d329256ed9d7',
        value: 'Caramel Syrup',
        price: 0.5
      }
    },
    categories: {
      '0aa403d1-bd20-43af-9176-c64ac06bf13c': {
        id: '0aa403d1-bd20-43af-9176-c64ac06bf13c',
        name: 'Breakfast Meals',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 0,
        subCategories: [],
        products: [
          '6e6d4aa1-1187-416f-ecaf-82da8e476280',
          'a1c738ca-0160-4419-e64d-dc196f81db85',
          'fea84a42-8bc1-4b32-9088-562f34f7b033',
          '5fd1e7e8-5728-4ecd-8423-221caa297981',
          '319a75c0-58bd-41ff-abfe-090726dfa4f3',
          '5404182a-5aff-4668-9724-b0764351069a',
          'd55b1dee-b7e6-4b48-e450-34993ce475e8',
          '73ef5a34-a928-4998-b301-7229893a8df9',
          '80c16a85-b42a-48f8-ef63-7003171efa38'
        ]
      },
      '15f962ed-9530-4f40-c302-933f8d724235': {
        id: '15f962ed-9530-4f40-c302-933f8d724235',
        name: 'Home-made Soup of the Day',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: 'bd10692b-f653-4b1e-cb53-b0deb949ed52',
        lineNo: 2,
        products: [
          'a9235b66-7b16-4d8e-8a4f-42d626644807',
          '9accdece-b58d-468b-fc96-365f19f2d471',
          '676e42fe-94fd-4c01-ffbd-3f23fce2d09c'
        ],
        isSubCategory: true
      },
      'bd10692b-f653-4b1e-cb53-b0deb949ed52': {
        id: 'bd10692b-f653-4b1e-cb53-b0deb949ed52',
        name: 'Soups',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 1,
        subCategories: [
          '15f962ed-9530-4f40-c302-933f8d724235'
        ]
      },
      '8c800dd3-673e-4add-d093-2b3f22c2a29e': {
        id: '8c800dd3-673e-4add-d093-2b3f22c2a29e',
        name: 'Salads',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 3,
        subCategories: [],
        products: [
          'fab4fbb9-29c6-4bcb-9787-1c48629ec19e',
          'a9274c0e-1793-4275-ec4c-f8f965df5896',
          'e7abcd9a-4fd1-409f-eba3-4ba790ba71cc'
        ]
      },
      '206a95c3-6cbf-4687-d9aa-83424e02bb5f': {
        id: '206a95c3-6cbf-4687-d9aa-83424e02bb5f',
        name: 'Toast',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 4,
        subCategories: [],
        products: [
          'dfba41eb-81c1-4b2d-fef7-f089b7badd8d',
          'a9bc1fd5-b57b-496b-a43b-72d5452103d6'
        ]
      },
      '359caf2f-a0f0-419f-c69e-a17712c0e915': {
        id: '359caf2f-a0f0-419f-c69e-a17712c0e915',
        name: 'Pastries and Snacks',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 5,
        subCategories: [],
        products: [
          '286e7c69-38ff-4651-b725-7f9d5350d400',
          '89fbe413-33ef-4e14-9b4f-91318690bbf8',
          'eae9fd31-aadc-40c2-8751-f6383b03be66',
          '66153de8-660e-4324-9067-0d1b12d0ad19',
          '7885b4aa-d723-488b-986a-019ccc9a0d33',
          '62db0b3e-e1a4-4c1b-b92c-a238798f3eea',
          'c42a601b-8b06-4adf-9048-170e366f035c',
          '542430e3-960b-4f05-b15e-986c3fe8221d',
          'e0e59fad-1dfb-411c-df2e-3763e21b90e7',
          '77116fb8-f433-469d-9d63-36e4bae2aea0',
          '9dd6ec3e-bcc9-4e4b-ed66-9132c120dbe8',
          'b0acbe65-eaa6-4f63-bbb3-cb311ede0bd2'
        ]
      },
      '4be9dfe0-c34f-4088-c4e1-f623c171c6d9': {
        id: '4be9dfe0-c34f-4088-c4e1-f623c171c6d9',
        name: 'Sandwiches',
        description: 'All of our sandwiches are available on gluten-free bread for additional $1.',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 6,
        subCategories: [],
        products: [
          'fb4e9a40-df9d-4dce-8005-0f858afbe9a9',
          '6e348d32-2e47-4dbd-e180-e40ee779b285',
          'f787c120-3a05-46cf-ab69-20decc5222d0',
          '5ea7f9b9-b7ab-4bc4-fed4-e4a4cc0f683f',
          'aaf3c8a3-0d2b-4234-98db-5324bb0c2af6',
          'e8dc3919-ef49-484c-e6b6-3f5ff639fdeb',
          'd26a1035-ce1a-4450-d958-40051b18e54c',
          '221fa449-f984-48d9-e24b-f2298897b6f9',
          'e59e4912-b8c6-4c74-cfe2-80bbc8fcb5b8'
        ]
      },
      'a0681e59-a954-436f-80f8-043aa81a5554': {
        id: 'a0681e59-a954-436f-80f8-043aa81a5554',
        name: 'Soup and Sandwich Combos',
        description: 'Combos have half sandwiches only',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 7,
        subCategories: [],
        products: [
          '137fe328-7b7f-491c-d452-2bb1068c3c3e',
          '6cbf66bd-1fb5-4c7c-a1d9-c5b751bc95d7',
          'f8efb9fd-97b3-4bc0-ec6c-238bb381c3cb',
          '6c7b9f48-7e4a-461d-ec56-61e454c885c8',
          'fbf13dc3-2613-4258-fede-36333bc6fb11',
          '3b1b4566-bbe9-416f-be8e-abe0515d2361',
          'f0cd23e1-b373-4e74-910c-0de35da66fa9',
          '46a58e55-5ec5-441a-89cb-410b0c6cf5b3'
        ]
      },
      '96234515-7d8c-480e-d392-921989a5844b': {
        id: '96234515-7d8c-480e-d392-921989a5844b',
        name: 'Salad and Sandwich Combos',
        description: 'Combos have half sandwiches only',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 8,
        subCategories: [],
        products: [
          '6e056c34-a372-44d1-9787-565897d3e8ba',
          '696f81f2-9c98-4a68-f92c-323cee702124',
          'b3b5ea18-12ed-4572-e176-9797ab705ef4',
          '38eb009c-c968-453c-f4e5-eab78915d64b',
          '41bfc758-2d2a-4d20-9df2-a39c2df23f10',
          'b2c5731e-015e-4b5d-90ba-5b595930b68b',
          'e102afec-8950-4486-e097-b376e1cc9d26',
          'e78b5d0c-39be-40e5-a186-48d6be621ad1'
        ]
      },
      'ad8465bf-c71b-4c2d-bf55-509493bb9eeb': {
        id: 'ad8465bf-c71b-4c2d-bf55-509493bb9eeb',
        name: 'Kid\'s Menu',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 9,
        subCategories: [],
        products: [
          'f878ccc8-4f95-4bb9-b498-6ba059e02f5f',
          '79180475-2897-4d22-c544-50bdcd66b1cd'
        ]
      },
      '42e1637e-1494-4cec-d761-8904e9d1a4ec': {
        id: '42e1637e-1494-4cec-d761-8904e9d1a4ec',
        name: 'Sides and Extras',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 10,
        subCategories: [],
        products: [
          '510f7a3f-0787-43e7-81c2-bd9731c38eb3',
          'dcafabb0-aac7-4651-8cf0-fdae246fe653',
          '1eb1da7a-e649-4907-df47-b05143948b6c',
          'e3357205-8799-4c8e-d132-291f06ee288d',
          '498d5c9e-354a-4352-8e9d-605a4e1261b3',
          '602ba9ff-7da7-49df-e49a-461bbc495c74',
          'a06139dd-b64a-4afd-cbf2-6d5a604bdce6',
          '1c019819-7834-4d87-e7bc-c2946add2f56',
          '1631cdc9-a602-401a-8baa-6ef3ac15f071',
          'fcedf46d-f640-449c-ec42-311017243d1d',
          'ba1d338e-db0b-437f-b620-44b0f57c18de',
          '31e7be7e-ab14-4e00-c887-ebdd43799f89',
          '55921914-629f-4bb0-e99c-622de2178310',
          '7e117a3e-5fcd-4b77-9c14-83c88b999b29',
          '9036d28c-3867-4349-ee1c-e483f368e90c'
        ]
      },
      '1e7865dd-3155-4081-ca1e-17ba28ee48bb': {
        id: '1e7865dd-3155-4081-ca1e-17ba28ee48bb',
        name: 'Espresso',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 13,
        products: [
          '2cd82be0-9bd3-4bfd-f1ea-8441f1c1e2e6',
          '7a88f5e3-9e17-44ff-bb7e-91f69df3532c',
          '59ab0144-161d-47f2-b846-c5317065beb7'
        ],
        isSubCategory: true
      },
      'ea8a871b-24ed-477a-8ba4-514bd4ea93e2': {
        id: 'ea8a871b-24ed-477a-8ba4-514bd4ea93e2',
        name: 'Macchiato',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 14,
        products: [
          '765cb713-597f-495d-beca-1d59f57a6193',
          '8dadf491-c087-494f-cca8-77bd025e8648'
        ],
        isSubCategory: true
      },
      '01b91a56-60c9-4e2f-af06-8a3caf263071': {
        id: '01b91a56-60c9-4e2f-af06-8a3caf263071',
        name: 'Americano',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 15,
        products: [
          'c8b2b590-c0a0-4903-fa00-a5f2deb58d74',
          'a262e785-57fc-40a5-9a81-986dcc7d1c8a',
          'b0cd209d-f59e-4ead-ef51-fbb62c0c8685'
        ],
        isSubCategory: true
      },
      'db82b010-8dcb-496d-91a3-47ce37b35c1e': {
        id: 'db82b010-8dcb-496d-91a3-47ce37b35c1e',
        name: 'Cafe Latte',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 16,
        products: [
          'fdaea2ae-36e6-4b16-dd24-835466810553',
          '59c5d219-ef6c-401e-b3e1-3e2691842ab7',
          'b2a437c9-28aa-413e-e272-f33cd9436350'
        ],
        isSubCategory: true
      },
      'd0360cc5-22d1-4641-961f-a47efbb6f6a2': {
        id: 'd0360cc5-22d1-4641-961f-a47efbb6f6a2',
        name: 'Cafe Mocha',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 17,
        products: [
          '2d993f1b-7f6d-4197-9f30-7e9c428710a2',
          '55264403-dc5d-464d-8577-0ba773d8e8c9',
          '7379e6c2-915b-4fc4-8390-df5a2f11cfdb'
        ],
        isSubCategory: true
      },
      'c31de3ee-ec3a-424c-8ed2-13d5b8f4b5b2': {
        id: 'c31de3ee-ec3a-424c-8ed2-13d5b8f4b5b2',
        name: 'Drip Coffee',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 19,
        products: [
          'ffd4cff7-872c-4839-d8ca-436fcfc815d8',
          '4e15da1b-16b5-4e7b-ad2a-4ff39db628a1',
          'c773db03-49ef-4a2c-aaec-629c00485763'
        ],
        isSubCategory: true
      },
      '060163ad-390a-462b-946d-cdbede99d4b1': {
        id: '060163ad-390a-462b-946d-cdbede99d4b1',
        name: 'Cafe Au Lait',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 20,
        products: [
          '935e3385-de46-4ebf-c410-d62bd55cb9df',
          'f4e470ec-ef8f-4ec9-fb24-72179b041686',
          '0905895f-7e57-484b-8346-4f2bfea8123a'
        ],
        isSubCategory: true
      },
      '6eec7308-ed01-418c-d665-032450573320': {
        id: '6eec7308-ed01-418c-d665-032450573320',
        name: 'Milk',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 21,
        products: [
          '2945e9d6-5791-49ca-f57b-2697c1babe51',
          '2830f572-8052-45ba-d09e-e5ce0a2c6dc4'
        ],
        isSubCategory: true
      },
      '0167c90f-7de9-43c3-e55c-e0d31f38ed6a': {
        id: '0167c90f-7de9-43c3-e55c-e0d31f38ed6a',
        name: 'Hot Chocolate',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 22,
        products: [
          '339ae445-944c-4a8e-87ba-5438e28c89e9',
          'e3e1b493-516e-4265-c038-e24db48fb56f',
          '6a87beb2-ddc1-42dc-f081-05236b1c6f33'
        ],
        isSubCategory: true
      },
      'd6df4e14-6c96-4ea1-fc66-0ccb6c75f074': {
        id: 'd6df4e14-6c96-4ea1-fc66-0ccb6c75f074',
        name: 'Apple Cider',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 23,
        products: [
          '57a9b88a-4c84-4afd-ff40-077cf429a7fb',
          'f65df121-d2c6-400b-9115-70c57ffd459f',
          'bc37099e-54dc-4046-bcf5-8cd694ddf1a7'
        ],
        isSubCategory: true
      },
      '6cdb5759-436b-4676-d92a-ab53dc5ede5d': {
        id: '6cdb5759-436b-4676-d92a-ab53dc5ede5d',
        name: 'Cappuccino',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 24,
        products: [
          'fa1df5a0-7a58-45c3-87b6-9accbed8564c',
          '22f77333-c38e-4fa1-99b6-0f7facd6c56a'
        ],
        isSubCategory: true
      },
      '9098060e-6637-44d1-c448-01a2baa3e837': {
        id: '9098060e-6637-44d1-c448-01a2baa3e837',
        name: 'Steamers',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 106,
        products: [
          '7a3eb7c0-6457-4000-e606-6abd388bdb51',
          '555e90f5-baaf-4212-fc03-7296ce9de0b7',
          '1bd2515f-362a-4fb2-8019-ae8c2a052ee3'
        ],
        isSubCategory: true
      },
      '277beaa3-5360-476b-b1c4-081fc7df3ee7': {
        id: '277beaa3-5360-476b-b1c4-081fc7df3ee7',
        name: 'Chai',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        lineNo: 107,
        products: [
          '292cf434-c722-4681-a76d-6d9c99ba7989',
          '8af8fc55-dd94-4a87-8c85-b9c008972fb8',
          'b02f7686-c85b-4dda-d836-9b788614b2ad'
        ],
        isSubCategory: true
      },
      '61a5dc25-c9d4-417b-a53f-7454ea34420e': {
        id: '61a5dc25-c9d4-417b-a53f-7454ea34420e',
        name: 'Hot Drinks',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 12,
        subCategories: [
          '1e7865dd-3155-4081-ca1e-17ba28ee48bb',
          'ea8a871b-24ed-477a-8ba4-514bd4ea93e2',
          '01b91a56-60c9-4e2f-af06-8a3caf263071',
          'db82b010-8dcb-496d-91a3-47ce37b35c1e',
          'd0360cc5-22d1-4641-961f-a47efbb6f6a2',
          'c31de3ee-ec3a-424c-8ed2-13d5b8f4b5b2',
          '060163ad-390a-462b-946d-cdbede99d4b1',
          '6eec7308-ed01-418c-d665-032450573320',
          '0167c90f-7de9-43c3-e55c-e0d31f38ed6a',
          'd6df4e14-6c96-4ea1-fc66-0ccb6c75f074',
          '6cdb5759-436b-4676-d92a-ab53dc5ede5d',
          '9098060e-6637-44d1-c448-01a2baa3e837',
          '277beaa3-5360-476b-b1c4-081fc7df3ee7'
        ]
      },
      '91c635ef-f240-4d68-bf18-d80a843fe010': {
        id: '91c635ef-f240-4d68-bf18-d80a843fe010',
        name: 'Iced Coffee',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 30,
        products: [
          'e0087de2-fe83-4824-aab2-19bf15a49a80',
          'c96189f4-7d60-4656-ce70-60a2cf77cc12'
        ],
        isSubCategory: true
      },
      'c5d3c88b-4b26-43a7-f86a-e135cc296ff3': {
        id: 'c5d3c88b-4b26-43a7-f86a-e135cc296ff3',
        name: 'Iced Latte',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 31,
        products: [
          '20071916-5ba8-4c25-b470-7ef0574b5b8f',
          'bd42d278-eeba-440a-a7ac-dd1b3fadf0a2'
        ],
        isSubCategory: true
      },
      '1cf35b46-81dc-406b-fbdf-873f77fc49f0': {
        id: '1cf35b46-81dc-406b-fbdf-873f77fc49f0',
        name: 'Iced Americano',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 32,
        products: [
          'f6ee9503-b4f6-4ca9-c4d8-763724ae581a',
          '9fb9cf27-242b-4af9-e88b-623faa13e4df'
        ],
        isSubCategory: true
      },
      '02255adc-e51f-4193-ed1c-f0ebc339c61c': {
        id: '02255adc-e51f-4193-ed1c-f0ebc339c61c',
        name: 'Iced Chai',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 33,
        products: [
          'c88b6062-b8f6-4864-faa1-f69ef513f469',
          'aa953eaf-64cf-4646-a32a-f47cda03f1de'
        ],
        isSubCategory: true
      },
      '2f818ded-6da1-49cf-a939-37fce8c5a827': {
        id: '2f818ded-6da1-49cf-a939-37fce8c5a827',
        name: 'Iced Mocha',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 34,
        products: [
          'fc336f97-314d-430a-ad67-46b35df9136a',
          '03e37434-de63-4577-b0e9-66f42197599d'
        ],
        isSubCategory: true
      },
      '7e96c1cb-4677-4a2e-da31-aefdb84dd013': {
        id: '7e96c1cb-4677-4a2e-da31-aefdb84dd013',
        name: 'Orange Juice',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 35,
        products: [
          '62eb8e16-19c6-4fa9-dc46-8858a8725f68',
          '5d3a7b1b-5b9b-44d6-fbea-4c4fa7f052fc'
        ],
        isSubCategory: true
      },
      'd791b1fd-6773-4b16-d357-9093c9d89342': {
        id: 'd791b1fd-6773-4b16-d357-9093c9d89342',
        name: 'Fresh Juices',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 108,
        products: [
          '440dc154-e045-474b-8ec0-8981abaa2b2a',
          '66d790f7-1819-4f53-850b-6ae54bc6a64e',
          'b0ac3da2-33e7-4fbd-aa82-819855d96261',
          '18c53916-c747-4f0e-a68c-6387a5f4d2b8'
        ],
        isSubCategory: true
      },
      '8faca02e-57ee-46a6-9212-a374279e895b': {
        id: '8faca02e-57ee-46a6-9212-a374279e895b',
        name: 'Iced Espresso',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        lineNo: 109,
        products: [
          'a433bcc7-d088-4d03-fe4a-7bb188609037'
        ],
        isSubCategory: true
      },
      '0daff840-6982-4ada-92c9-cb33fdcf6426': {
        id: '0daff840-6982-4ada-92c9-cb33fdcf6426',
        name: 'Cold Drinks',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 29,
        subCategories: [
          '91c635ef-f240-4d68-bf18-d80a843fe010',
          'c5d3c88b-4b26-43a7-f86a-e135cc296ff3',
          '1cf35b46-81dc-406b-fbdf-873f77fc49f0',
          '02255adc-e51f-4193-ed1c-f0ebc339c61c',
          '2f818ded-6da1-49cf-a939-37fce8c5a827',
          '7e96c1cb-4677-4a2e-da31-aefdb84dd013',
          'd791b1fd-6773-4b16-d357-9093c9d89342',
          '8faca02e-57ee-46a6-9212-a374279e895b'
        ],
        products: [
          '45b14ff5-cd3b-498d-aec1-f2109d82a8a8'
        ]
      },
      '9dcbbbd5-7a64-44b5-cfb0-81d2105d35e5': {
        id: '9dcbbbd5-7a64-44b5-cfb0-81d2105d35e5',
        name: 'Specialty Drinks',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 36,
        subCategories: [],
        products: [
          '90b6bb4c-8c8b-450d-9e46-3cae6ae4eab4',
          '7207ec42-d86a-45a4-e523-f039112cf3a4',
          '9435211f-d1fa-46ed-bef8-0f699bc0df6b',
          'b49a7897-79fa-4875-a226-6e7538b37e2a'
        ]
      },
      'ec285a46-a082-4f08-e559-a97ee7d354ce': {
        id: 'ec285a46-a082-4f08-e559-a97ee7d354ce',
        name: 'Bottled Drinks',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 37,
        subCategories: [],
        products: [
          '6ac7f658-9d22-481f-d376-317a692caf4b',
          '79d72394-4abb-4854-cf0a-f6f6c2749433',
          'ed007e91-fc17-4533-db63-c5bb13e20140',
          '0ffc56a5-377f-49bd-808b-d8b2744d4c63',
          'cf3f896c-82e6-47b9-ea99-e35bbb0bf5d5',
          'aa6621f3-4e86-437f-9f8e-91807bc667b1',
          '4c6f2bb3-e2f4-43ee-e416-154b50cf7661',
          '467bc66f-d7aa-41c1-ad29-c07375c0544f',
          'f09a87aa-d408-40ef-e817-f45ca5abcb99',
          '5b92acf9-5851-4a1a-94bc-847b8c3b002f',
          '56ada0e5-7ceb-4d1b-8d18-a2ffc587680c',
          'ec1cf3ff-569c-4568-8076-85606b61156a',
          '042ca325-22fa-4896-c34f-3bf0ae01b9c2'
        ]
      },
      '8d7f6ec8-35b7-4667-95d0-2b8fdd7df336': {
        id: '8d7f6ec8-35b7-4667-95d0-2b8fdd7df336',
        name: 'Hot Teas',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '8c6b5ffe-2948-4c11-b41f-2f8e30c1dd62',
        lineNo: 41,
        products: [
          '6dc3eac5-2928-4da0-ce4c-fcc39e9f9765',
          '94e7f88e-2022-4856-fc4e-7b079f065b6e',
          '3548a700-8de6-468d-cc46-a3bfb4d6096d',
          '2f6a263d-c6b6-49a7-bcb6-173d818ada64',
          '62a898e3-203e-4e87-9338-5b139602ea37',
          'f8addc80-0e02-4ef9-e934-d14e5b4125f1',
          'bbf07f5b-5572-44c4-9dc4-037974a0f0e7',
          '5be333fe-9769-4eeb-9fa2-9585140a20d3',
          'd310658d-0bee-424d-c6a1-b0f8912404c2',
          'b9e25d31-2595-4a19-c0a2-9d8dff239b59',
          'bb9026f3-9072-480e-8b9c-2c798b08f7b8',
          '78b035b7-b746-4586-dd0a-76f54d99a041',
          '32da140c-d1df-43a3-c465-4119a8e872e3'
        ],
        isSubCategory: true
      },
      'ab20cca2-e4fa-4c1a-9528-1d9d50de029b': {
        id: 'ab20cca2-e4fa-4c1a-9528-1d9d50de029b',
        name: 'Iced Teas',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '8c6b5ffe-2948-4c11-b41f-2f8e30c1dd62',
        lineNo: 42,
        products: [
          '54878f05-25e1-4cde-8ec4-e4112041d604',
          '1bdcbf13-20d4-479a-beb2-d8d55d639b50',
          'ed2c894e-8573-4e3c-a7c6-d77a8f0b935c',
          '2ebcbbf7-6e78-41ae-a05c-360a28dc4c9d',
          '7411d3b9-2a2f-421b-b76b-7d93af8d0202',
          '907f5d36-85aa-4651-bd4a-0b606c1875ce',
          '51180e62-356b-4866-9452-6ec03f74d4a4'
        ],
        isSubCategory: true
      },
      '8c6b5ffe-2948-4c11-b41f-2f8e30c1dd62': {
        id: '8c6b5ffe-2948-4c11-b41f-2f8e30c1dd62',
        name: 'Tea',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 38,
        subCategories: [
          '8d7f6ec8-35b7-4667-95d0-2b8fdd7df336',
          'ab20cca2-e4fa-4c1a-9528-1d9d50de029b'
        ]
      },
      'cbfb9c79-b2dc-4b94-abcc-55308ddd6c98': {
        id: 'cbfb9c79-b2dc-4b94-abcc-55308ddd6c98',
        name: 'DINNER',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 110,
        subCategories: [],
        products: [
          'a1261216-ecfc-47e7-adc0-4eddf1790713',
          'aae95f7d-7749-4c3f-9ea0-83ed2245aaf7',
          '9a9025df-9d29-4085-de05-8531c6726fa9',
          '36af7822-61ab-41ba-e5b8-3df9143e3d07'
        ]
      },
      '65c8c2b6-8e98-41b8-a446-37a418b857ea': {
        id: '65c8c2b6-8e98-41b8-a446-37a418b857ea',
        name: 'KITCHEN ACCESSORIES',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        parentId: '3821ade6-a7ce-4937-80d1-eb298ebef8fd',
        lineNo: 116,
        products: [
          '85e441fd-cbf9-409f-f9a7-e71b91f34a61',
          '6a40e86e-020e-45b2-89ab-0c5022facb8d'
        ],
        isSubCategory: true
      },
      '3821ade6-a7ce-4937-80d1-eb298ebef8fd': {
        id: '3821ade6-a7ce-4937-80d1-eb298ebef8fd',
        name: 'THE PANTRY',
        description: '',
        merchantId: 'c20-cafeiveta95060',
        sessionId: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        lineNo: 114,
        subCategories: [
          '65c8c2b6-8e98-41b8-a446-37a418b857ea'
        ],
        products: [
          '6251c9a1-a355-4369-c648-2c2f5d9ccfc4'
        ]
      }
    },
    currentSession: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
    currentDay: 2,
    notifications: [],
    products: {
      '6e6d4aa1-1187-416f-ecaf-82da8e476280': {
        id: '6e6d4aa1-1187-416f-ecaf-82da8e476280',
        name: 'Home-made Granola',
        description: 'A tasty mix of flax, oats, honey, and nuts together with plain yogurt and strawberries.',
        merchantId: 'c20-cafeiveta95060',
        reference: '1',
        price: 6,
        isAuxiliary: false
      },
      'a1c738ca-0160-4419-e64d-dc196f81db85': {
        id: 'a1c738ca-0160-4419-e64d-dc196f81db85',
        name: 'Lox Toast',
        description: 'Wild smoked Alaskan sockeye salmon, cream cheese, tomato, red onion, and capers on sourdough toast.',
        merchantId: 'c20-cafeiveta95060',
        reference: '2',
        price: 7,
        isAuxiliary: false
      },
      'fea84a42-8bc1-4b32-9088-562f34f7b033': {
        id: 'fea84a42-8bc1-4b32-9088-562f34f7b033',
        name: 'Spicy Egg Biscuit',
        description: 'Poached egg served on a freshly baked chili chive biscuit with a dollop of cream cheese.',
        merchantId: 'c20-cafeiveta95060',
        reference: '3',
        price: 4.5,
        isAuxiliary: false
      },
      '5fd1e7e8-5728-4ecd-8423-221caa297981': {
        id: '5fd1e7e8-5728-4ecd-8423-221caa297981',
        name: 'Eggs and Greens',
        description: 'Steamed scrambled eggs with a side of mixed greens and tossed in a champagne vinaigrette.',
        merchantId: 'c20-cafeiveta95060',
        reference: '4',
        price: 7,
        isAuxiliary: false,
        attributeSet: 'c4890bd1-e4e5-47ed-d9ab-68edfd6f46e3',
        taxCategoryId: '001',
        kitchenName: 'Eggs and Greens',
        receiptName: 'Eggs and Greens'
      },
      '319a75c0-58bd-41ff-abfe-090726dfa4f3': {
        id: '319a75c0-58bd-41ff-abfe-090726dfa4f3',
        name: 'Cruz Bagel',
        description: 'Poached egg, tomato, and a dollop of cream cheese on a fresh toasted bagel.',
        merchantId: 'c20-cafeiveta95060',
        reference: '5',
        price: 6,
        isAuxiliary: false
      },
      '5404182a-5aff-4668-9724-b0764351069a': {
        id: '5404182a-5aff-4668-9724-b0764351069a',
        name: 'Breakfast Burrito',
        description: 'Steamed scrambled eggs, Monterey Jack and sharp cheddar cheese, spinach, and home-made pico de gallo on a soft flour tortilla.',
        merchantId: 'c20-cafeiveta95060',
        reference: '6',
        price: 7,
        isAuxiliary: false
      },
      'd55b1dee-b7e6-4b48-e450-34993ce475e8': {
        id: 'd55b1dee-b7e6-4b48-e450-34993ce475e8',
        name: 'Scrambled Egg Sandwich',
        description: 'Scrambled eggs, havarti cheese and bacon on toasted sprouted wheat bread',
        merchantId: 'c20-cafeiveta95060',
        reference: '7',
        price: 7,
        isAuxiliary: false
      },
      '73ef5a34-a928-4998-b301-7229893a8df9': {
        id: '73ef5a34-a928-4998-b301-7229893a8df9',
        name: 'Oatmeal',
        description: 'Rolled oats with steamed milk, walnuts, banana and agave.',
        merchantId: 'c20-cafeiveta95060',
        reference: '520',
        price: 6,
        isAuxiliary: false
      },
      '80c16a85-b42a-48f8-ef63-7003171efa38': {
        id: '80c16a85-b42a-48f8-ef63-7003171efa38',
        name: 'Bagel',
        merchantId: 'c20-cafeiveta95060',
        reference: '569',
        price: 0,
        isAuxiliary: false
      },
      'a9235b66-7b16-4d8e-8a4f-42d626644807': {
        id: 'a9235b66-7b16-4d8e-8a4f-42d626644807',
        name: 'Cup',
        merchantId: 'c20-cafeiveta95060',
        reference: '19',
        price: 5,
        isAuxiliary: false,
        attributeSet: '3d83b2eb-e386-4490-e50a-1a309eb7c46a',
        taxCategoryId: '001',
        kitchenName: 'Cup of Soup',
        receiptName: 'Cup of Soup'
      },
      '9accdece-b58d-468b-fc96-365f19f2d471': {
        id: '9accdece-b58d-468b-fc96-365f19f2d471',
        name: 'Bowl',
        merchantId: 'c20-cafeiveta95060',
        reference: '20',
        price: 7,
        isAuxiliary: false
      },
      '676e42fe-94fd-4c01-ffbd-3f23fce2d09c': {
        id: '676e42fe-94fd-4c01-ffbd-3f23fce2d09c',
        name: 'Combo Soup/Salad',
        merchantId: 'c20-cafeiveta95060',
        reference: '571',
        price: 9,
        isAuxiliary: false
      },
      'fab4fbb9-29c6-4bcb-9787-1c48629ec19e': {
        id: 'fab4fbb9-29c6-4bcb-9787-1c48629ec19e',
        name: 'Tunisian Tuna Salad',
        description: 'Albacore tuna, diced peppers, onions, hot giardineria, and a touch of olive oil served over mixed greens with our own Dijon dressing.',
        merchantId: 'c20-cafeiveta95060',
        reference: '22',
        price: 8,
        isAuxiliary: false
      },
      'a9274c0e-1793-4275-ec4c-f8f965df5896': {
        id: 'a9274c0e-1793-4275-ec4c-f8f965df5896',
        name: 'Insalata Mista',
        description: 'Mixed greens, cherry tomatoes, red onion, garbanzos, and shaved Parmesan garnished with croutons and our own herbed vinaigrette dressing.',
        merchantId: 'c20-cafeiveta95060',
        reference: '23',
        price: 7,
        isAuxiliary: false
      },
      'e7abcd9a-4fd1-409f-eba3-4ba790ba71cc': {
        id: 'e7abcd9a-4fd1-409f-eba3-4ba790ba71cc',
        name: 'Arugula Salad',
        description: 'Arugula, cherry tomatoes, avocado, and spring onions garnished with goat cheese and Pacific spice dressing.',
        merchantId: 'c20-cafeiveta95060',
        reference: '24',
        price: 8,
        isAuxiliary: false
      },
      'dfba41eb-81c1-4b2d-fef7-f089b7badd8d': {
        id: 'dfba41eb-81c1-4b2d-fef7-f089b7badd8d',
        name: 'Toast 2 Slices',
        merchantId: 'c20-cafeiveta95060',
        reference: '517',
        price: 2,
        isAuxiliary: false
      },
      'a9bc1fd5-b57b-496b-a43b-72d5452103d6': {
        id: 'a9bc1fd5-b57b-496b-a43b-72d5452103d6',
        name: 'Egg On Toast',
        merchantId: 'c20-cafeiveta95060',
        reference: '519',
        price: 5.5,
        isAuxiliary: false
      },
      '286e7c69-38ff-4651-b725-7f9d5350d400': {
        id: '286e7c69-38ff-4651-b725-7f9d5350d400',
        name: 'Scone',
        merchantId: 'c20-cafeiveta95060',
        reference: '8',
        price: 2.25,
        isAuxiliary: false
      },
      '89fbe413-33ef-4e14-9b4f-91318690bbf8': {
        id: '89fbe413-33ef-4e14-9b4f-91318690bbf8',
        name: 'Gluten-free Scone',
        merchantId: 'c20-cafeiveta95060',
        reference: '9',
        price: 2.75,
        isAuxiliary: false
      },
      'eae9fd31-aadc-40c2-8751-f6383b03be66': {
        id: 'eae9fd31-aadc-40c2-8751-f6383b03be66',
        name: 'Bread Pudding',
        merchantId: 'c20-cafeiveta95060',
        reference: '10',
        price: 3.5,
        isAuxiliary: false
      },
      '66153de8-660e-4324-9067-0d1b12d0ad19': {
        id: '66153de8-660e-4324-9067-0d1b12d0ad19',
        name: 'Two Scones with Jam and Clotted Cream',
        merchantId: 'c20-cafeiveta95060',
        reference: '15',
        price: 6,
        isAuxiliary: false
      },
      '7885b4aa-d723-488b-986a-019ccc9a0d33': {
        id: '7885b4aa-d723-488b-986a-019ccc9a0d33',
        name: 'Blini',
        merchantId: 'c20-cafeiveta95060',
        reference: '16',
        price: 2,
        isAuxiliary: false
      },
      '62db0b3e-e1a4-4c1b-b92c-a238798f3eea': {
        id: '62db0b3e-e1a4-4c1b-b92c-a238798f3eea',
        name: 'Home-made Biscotti',
        merchantId: 'c20-cafeiveta95060',
        reference: '17',
        price: 0.5,
        isAuxiliary: false
      },
      'c42a601b-8b06-4adf-9048-170e366f035c': {
        id: 'c42a601b-8b06-4adf-9048-170e366f035c',
        name: 'Croissant',
        merchantId: 'c20-cafeiveta95060',
        reference: '18',
        price: 2.75,
        isAuxiliary: false
      },
      '542430e3-960b-4f05-b15e-986c3fe8221d': {
        id: '542430e3-960b-4f05-b15e-986c3fe8221d',
        name: 'Pumpkin Bar',
        merchantId: 'c20-cafeiveta95060',
        reference: '588',
        price: 3.5,
        isAuxiliary: false
      },
      'e0e59fad-1dfb-411c-df2e-3763e21b90e7': {
        id: 'e0e59fad-1dfb-411c-df2e-3763e21b90e7',
        name: 'Lime Bar',
        merchantId: 'c20-cafeiveta95060',
        reference: '589',
        price: 3.5,
        isAuxiliary: false
      },
      '77116fb8-f433-469d-9d63-36e4bae2aea0': {
        id: '77116fb8-f433-469d-9d63-36e4bae2aea0',
        name: 'choc twist',
        merchantId: 'c20-cafeiveta95060',
        reference: '610',
        price: 2.75,
        isAuxiliary: false
      },
      '9dd6ec3e-bcc9-4e4b-ed66-9132c120dbe8': {
        id: '9dd6ec3e-bcc9-4e4b-ed66-9132c120dbe8',
        name: 'Banana Bread',
        merchantId: 'c20-cafeiveta95060',
        reference: '5555',
        price: 3,
        isAuxiliary: false
      },
      'b0acbe65-eaa6-4f63-bbb3-cb311ede0bd2': {
        id: 'b0acbe65-eaa6-4f63-bbb3-cb311ede0bd2',
        name: 'Muffin Fresh Baked',
        merchantId: 'c20-cafeiveta95060',
        reference: '55566666',
        price: 2.25,
        isAuxiliary: false
      },
      'fb4e9a40-df9d-4dce-8005-0f858afbe9a9': {
        id: 'fb4e9a40-df9d-4dce-8005-0f858afbe9a9',
        name: 'BLT',
        description: 'Bacon, butter lettuce, and Roma tomatoes on sourdough bread with spicy home-made mayonnaise. Add avocado for $1 and poached egg for $2.',
        merchantId: 'c20-cafeiveta95060',
        reference: '25',
        price: 8,
        isAuxiliary: false
      },
      '6e348d32-2e47-4dbd-e180-e40ee779b285': {
        id: '6e348d32-2e47-4dbd-e180-e40ee779b285',
        name: 'Roast Beef',
        description: 'House-roasted top sirloin in our own au jus sauce. Served on a ciabatta roll with garlic butter and provolone. Add hot giardiniera for $1.',
        merchantId: 'c20-cafeiveta95060',
        reference: '26',
        price: 9,
        isAuxiliary: false
      },
      'f787c120-3a05-46cf-ab69-20decc5222d0': {
        id: 'f787c120-3a05-46cf-ab69-20decc5222d0',
        name: 'Ham and Cheese',
        description: 'The classic sandwich Italian style. Shaved Italian prosciutto cotto and provolone served with lettuce, tomato, red onion, and home-made mayonnaise on a ciabatta roll. Add avocado for $1.',
        merchantId: 'c20-cafeiveta95060',
        reference: '27',
        price: 8,
        isAuxiliary: false
      },
      '5ea7f9b9-b7ab-4bc4-fed4-e4a4cc0f683f': {
        id: '5ea7f9b9-b7ab-4bc4-fed4-e4a4cc0f683f',
        name: 'Turkey and Havarti',
        description: 'Free-range Diestel turkey breast and havarti. Served with lettuce, tomato, and home-made mayonnaise on sprouted wheat bread. Add avocado or add slice of bacon for $1.',
        merchantId: 'c20-cafeiveta95060',
        reference: '28',
        price: 8,
        isAuxiliary: false
      },
      'aaf3c8a3-0d2b-4234-98db-5324bb0c2af6': {
        id: 'aaf3c8a3-0d2b-4234-98db-5324bb0c2af6',
        name: 'Spicy Vegan',
        description: 'Home-made hummus, spinach, red onion, tomatoes, sprouts hot giardiniera, and avocado. Served on a sprouted wheat bread.',
        merchantId: 'c20-cafeiveta95060',
        reference: '29',
        price: 8,
        isAuxiliary: false
      },
      'e8dc3919-ef49-484c-e6b6-3f5ff639fdeb': {
        id: 'e8dc3919-ef49-484c-e6b6-3f5ff639fdeb',
        name: 'Caprese',
        description: 'Home-made basil pesto, fresh mozzarella, Roma tomatoes, lettuce, and a dollop of home-made mayonnaise on a ciabatta roll.',
        merchantId: 'c20-cafeiveta95060',
        reference: '30',
        price: 8,
        isAuxiliary: false
      },
      'd26a1035-ce1a-4450-d958-40051b18e54c': {
        id: 'd26a1035-ce1a-4450-d958-40051b18e54c',
        name: 'Grilled Cheese',
        merchantId: 'c20-cafeiveta95060',
        reference: '31',
        price: 6,
        isAuxiliary: false
      },
      '221fa449-f984-48d9-e24b-f2298897b6f9': {
        id: '221fa449-f984-48d9-e24b-f2298897b6f9',
        name: 'Proscuitto and Pear Sandwich',
        merchantId: 'c20-cafeiveta95060',
        reference: '592',
        price: 8,
        isAuxiliary: false
      },
      'e59e4912-b8c6-4c74-cfe2-80bbc8fcb5b8': {
        id: 'e59e4912-b8c6-4c74-cfe2-80bbc8fcb5b8',
        name: 'Tuna Melt',
        description: 'Tunisian Tuna with tomatoes, aioli, cheddar cheese served on ciabatta roll.',
        merchantId: 'c20-cafeiveta95060',
        reference: '6.51761995025777dfsd',
        price: 9,
        isAuxiliary: false
      },
      '137fe328-7b7f-491c-d452-2bb1068c3c3e': {
        id: '137fe328-7b7f-491c-d452-2bb1068c3c3e',
        name: 'BLT Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '522',
        price: 9,
        isAuxiliary: false
      },
      '6cbf66bd-1fb5-4c7c-a1d9-c5b751bc95d7': {
        id: '6cbf66bd-1fb5-4c7c-a1d9-c5b751bc95d7',
        name: 'Roast Beef Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '523',
        price: 9,
        isAuxiliary: false
      },
      'f8efb9fd-97b3-4bc0-ec6c-238bb381c3cb': {
        id: 'f8efb9fd-97b3-4bc0-ec6c-238bb381c3cb',
        name: 'Ham and Cheese Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '524',
        price: 9,
        isAuxiliary: false
      },
      '6c7b9f48-7e4a-461d-ec56-61e454c885c8': {
        id: '6c7b9f48-7e4a-461d-ec56-61e454c885c8',
        name: 'Turkey and Havarti Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '525',
        price: 9,
        isAuxiliary: false
      },
      'fbf13dc3-2613-4258-fede-36333bc6fb11': {
        id: 'fbf13dc3-2613-4258-fede-36333bc6fb11',
        name: 'Spicy Vegan Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '526',
        price: 9,
        isAuxiliary: false
      },
      '3b1b4566-bbe9-416f-be8e-abe0515d2361': {
        id: '3b1b4566-bbe9-416f-be8e-abe0515d2361',
        name: 'Caprese Sandwich and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '527',
        price: 9,
        isAuxiliary: false
      },
      'f0cd23e1-b373-4e74-910c-0de35da66fa9': {
        id: 'f0cd23e1-b373-4e74-910c-0de35da66fa9',
        name: 'Grilled Cheese and Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '528',
        price: 9,
        isAuxiliary: false
      },
      '46a58e55-5ec5-441a-89cb-410b0c6cf5b3': {
        id: '46a58e55-5ec5-441a-89cb-410b0c6cf5b3',
        name: 'Tuna Melt & Soup Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '657657',
        price: 9,
        isAuxiliary: false
      },
      '6e056c34-a372-44d1-9787-565897d3e8ba': {
        id: '6e056c34-a372-44d1-9787-565897d3e8ba',
        name: 'BLT Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '529',
        price: 9,
        isAuxiliary: false
      },
      '696f81f2-9c98-4a68-f92c-323cee702124': {
        id: '696f81f2-9c98-4a68-f92c-323cee702124',
        name: 'Roast Beef Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '530',
        price: 9,
        isAuxiliary: false
      },
      'b3b5ea18-12ed-4572-e176-9797ab705ef4': {
        id: 'b3b5ea18-12ed-4572-e176-9797ab705ef4',
        name: 'Ham and Cheese Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '531',
        price: 9,
        isAuxiliary: false
      },
      '38eb009c-c968-453c-f4e5-eab78915d64b': {
        id: '38eb009c-c968-453c-f4e5-eab78915d64b',
        name: 'Turkey and Havarti Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '532',
        price: 9,
        isAuxiliary: false
      },
      '41bfc758-2d2a-4d20-9df2-a39c2df23f10': {
        id: '41bfc758-2d2a-4d20-9df2-a39c2df23f10',
        name: 'Spicy Vegan Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '533',
        price: 9,
        isAuxiliary: false
      },
      'b2c5731e-015e-4b5d-90ba-5b595930b68b': {
        id: 'b2c5731e-015e-4b5d-90ba-5b595930b68b',
        name: 'Caprese Sandwich and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '534',
        price: 9,
        isAuxiliary: false
      },
      'e102afec-8950-4486-e097-b376e1cc9d26': {
        id: 'e102afec-8950-4486-e097-b376e1cc9d26',
        name: 'Grilled Cheese and Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '535',
        price: 9,
        isAuxiliary: false
      },
      'e78b5d0c-39be-40e5-a186-48d6be621ad1': {
        id: 'e78b5d0c-39be-40e5-a186-48d6be621ad1',
        name: 'Tuna Melt & Salad Combo',
        merchantId: 'c20-cafeiveta95060',
        reference: '34645756',
        price: 9,
        isAuxiliary: false
      },
      'f878ccc8-4f95-4bb9-b498-6ba059e02f5f': {
        id: 'f878ccc8-4f95-4bb9-b498-6ba059e02f5f',
        name: 'Kid\'s Eggs',
        description: 'Comes with no seasoning or cheese.',
        merchantId: 'c20-cafeiveta95060',
        reference: '549',
        price: 5,
        isAuxiliary: false
      },
      '79180475-2897-4d22-c544-50bdcd66b1cd': {
        id: '79180475-2897-4d22-c544-50bdcd66b1cd',
        name: 'Yogurt and Honey',
        merchantId: 'c20-cafeiveta95060',
        reference: '550',
        price: 3.5,
        isAuxiliary: false
      },
      '510f7a3f-0787-43e7-81c2-bd9731c38eb3': {
        id: '510f7a3f-0787-43e7-81c2-bd9731c38eb3',
        name: 'Hummus w/ Veggies 4oz.',
        description: '4oz. of Homemade Hummus with Small Side of Fresh Veggis.',
        merchantId: 'c20-cafeiveta95060',
        reference: '32',
        price: 6,
        isAuxiliary: false
      },
      'dcafabb0-aac7-4651-8cf0-fdae246fe653': {
        id: 'dcafabb0-aac7-4651-8cf0-fdae246fe653',
        name: 'Mixed Greens',
        merchantId: 'c20-cafeiveta95060',
        reference: '34',
        price: 6,
        isAuxiliary: false
      },
      '1eb1da7a-e649-4907-df47-b05143948b6c': {
        id: '1eb1da7a-e649-4907-df47-b05143948b6c',
        name: 'Fresh Veggies',
        merchantId: 'c20-cafeiveta95060',
        reference: '35',
        price: 5,
        isAuxiliary: false
      },
      'e3357205-8799-4c8e-d132-291f06ee288d': {
        id: 'e3357205-8799-4c8e-d132-291f06ee288d',
        name: 'Bacon 2 Slices',
        merchantId: 'c20-cafeiveta95060',
        reference: '36',
        price: 2,
        isAuxiliary: false
      },
      '498d5c9e-354a-4352-8e9d-605a4e1261b3': {
        id: '498d5c9e-354a-4352-8e9d-605a4e1261b3',
        name: 'Fruit Bowl',
        merchantId: 'c20-cafeiveta95060',
        reference: '37',
        price: 6,
        isAuxiliary: false
      },
      '602ba9ff-7da7-49df-e49a-461bbc495c74': {
        id: '602ba9ff-7da7-49df-e49a-461bbc495c74',
        name: 'Tuna',
        merchantId: 'c20-cafeiveta95060',
        reference: '38',
        price: 5,
        isAuxiliary: false
      },
      'a06139dd-b64a-4afd-cbf2-6d5a604bdce6': {
        id: 'a06139dd-b64a-4afd-cbf2-6d5a604bdce6',
        name: 'Chips',
        merchantId: 'c20-cafeiveta95060',
        reference: '39',
        price: 1.65,
        isAuxiliary: false
      },
      '1c019819-7834-4d87-e7bc-c2946add2f56': {
        id: '1c019819-7834-4d87-e7bc-c2946add2f56',
        name: 'Sausage 2 Links',
        merchantId: 'c20-cafeiveta95060',
        reference: '548',
        price: 2,
        isAuxiliary: false
      },
      '1631cdc9-a602-401a-8baa-6ef3ac15f071': {
        id: '1631cdc9-a602-401a-8baa-6ef3ac15f071',
        name: 'Side Egg',
        merchantId: 'c20-cafeiveta95060',
        reference: '591',
        price: 1.5,
        isAuxiliary: false
      },
      'fcedf46d-f640-449c-ec42-311017243d1d': {
        id: 'fcedf46d-f640-449c-ec42-311017243d1d',
        name: 'Ice Cream',
        merchantId: 'c20-cafeiveta95060',
        reference: '593',
        price: 3,
        isAuxiliary: false
      },
      'ba1d338e-db0b-437f-b620-44b0f57c18de': {
        id: 'ba1d338e-db0b-437f-b620-44b0f57c18de',
        name: 'Lox Add-On',
        merchantId: 'c20-cafeiveta95060',
        reference: '597',
        price: 4.5,
        isAuxiliary: false
      },
      '31e7be7e-ab14-4e00-c887-ebdd43799f89': {
        id: '31e7be7e-ab14-4e00-c887-ebdd43799f89',
        name: 'Organic Turkey',
        merchantId: 'c20-cafeiveta95060',
        reference: '600',
        price: 4,
        isAuxiliary: false
      },
      '55921914-629f-4bb0-e99c-622de2178310': {
        id: '55921914-629f-4bb0-e99c-622de2178310',
        name: 'Add Avocado',
        merchantId: 'c20-cafeiveta95060',
        reference: '606',
        price: 1,
        isAuxiliary: false
      },
      '7e117a3e-5fcd-4b77-9c14-83c88b999b29': {
        id: '7e117a3e-5fcd-4b77-9c14-83c88b999b29',
        name: 'Side of Jam 1oz',
        merchantId: 'c20-cafeiveta95060',
        reference: '608',
        price: 0.75,
        isAuxiliary: false
      },
      '9036d28c-3867-4349-ee1c-e483f368e90c': {
        id: '9036d28c-3867-4349-ee1c-e483f368e90c',
        name: 'Side Lemon Curd 1 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '609',
        price: 1,
        isAuxiliary: false
      },
      '2cd82be0-9bd3-4bfd-f1ea-8441f1c1e2e6': {
        id: '2cd82be0-9bd3-4bfd-f1ea-8441f1c1e2e6',
        name: 'Single Espresso',
        merchantId: 'c20-cafeiveta95060',
        reference: '42',
        price: 2.25,
        isAuxiliary: false
      },
      '7a88f5e3-9e17-44ff-bb7e-91f69df3532c': {
        id: '7a88f5e3-9e17-44ff-bb7e-91f69df3532c',
        name: 'Double Espresso',
        merchantId: 'c20-cafeiveta95060',
        reference: '43',
        price: 2.75,
        isAuxiliary: false
      },
      '59ab0144-161d-47f2-b846-c5317065beb7': {
        id: '59ab0144-161d-47f2-b846-c5317065beb7',
        name: 'Iced Single Espresso',
        merchantId: 'c20-cafeiveta95060',
        reference: '572',
        price: 2.25,
        isAuxiliary: false
      },
      '765cb713-597f-495d-beca-1d59f57a6193': {
        id: '765cb713-597f-495d-beca-1d59f57a6193',
        name: 'Single Macchiato',
        merchantId: 'c20-cafeiveta95060',
        reference: '44',
        price: 2.25,
        isAuxiliary: false
      },
      '8dadf491-c087-494f-cca8-77bd025e8648': {
        id: '8dadf491-c087-494f-cca8-77bd025e8648',
        name: 'Double Macchiato',
        merchantId: 'c20-cafeiveta95060',
        reference: '45',
        price: 2.75,
        isAuxiliary: false
      },
      'c8b2b590-c0a0-4903-fa00-a5f2deb58d74': {
        id: 'c8b2b590-c0a0-4903-fa00-a5f2deb58d74',
        name: '8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '46',
        price: 2.25,
        isAuxiliary: false
      },
      'a262e785-57fc-40a5-9a81-986dcc7d1c8a': {
        id: 'a262e785-57fc-40a5-9a81-986dcc7d1c8a',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '47',
        price: 2.75,
        isAuxiliary: false
      },
      'b0cd209d-f59e-4ead-ef51-fbb62c0c8685': {
        id: 'b0cd209d-f59e-4ead-ef51-fbb62c0c8685',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '48',
        price: 3.25,
        isAuxiliary: false
      },
      'fdaea2ae-36e6-4b16-dd24-835466810553': {
        id: 'fdaea2ae-36e6-4b16-dd24-835466810553',
        name: 'Latte 8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '50',
        price: 3,
        isAuxiliary: false
      },
      '59c5d219-ef6c-401e-b3e1-3e2691842ab7': {
        id: '59c5d219-ef6c-401e-b3e1-3e2691842ab7',
        name: 'Latte 12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '51',
        price: 3.5,
        isAuxiliary: false
      },
      'b2a437c9-28aa-413e-e272-f33cd9436350': {
        id: 'b2a437c9-28aa-413e-e272-f33cd9436350',
        name: 'Latte 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '52',
        price: 4,
        isAuxiliary: false
      },
      '2d993f1b-7f6d-4197-9f30-7e9c428710a2': {
        id: '2d993f1b-7f6d-4197-9f30-7e9c428710a2',
        name: '8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '53',
        price: 3.5,
        isAuxiliary: false
      },
      '55264403-dc5d-464d-8577-0ba773d8e8c9': {
        id: '55264403-dc5d-464d-8577-0ba773d8e8c9',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '54',
        price: 4,
        isAuxiliary: false
      },
      '7379e6c2-915b-4fc4-8390-df5a2f11cfdb': {
        id: '7379e6c2-915b-4fc4-8390-df5a2f11cfdb',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '55',
        price: 4.5,
        isAuxiliary: false
      },
      'ffd4cff7-872c-4839-d8ca-436fcfc815d8': {
        id: 'ffd4cff7-872c-4839-d8ca-436fcfc815d8',
        name: '8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '59',
        price: 2.25,
        isAuxiliary: false
      },
      '4e15da1b-16b5-4e7b-ad2a-4ff39db628a1': {
        id: '4e15da1b-16b5-4e7b-ad2a-4ff39db628a1',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '60',
        price: 2.5,
        isAuxiliary: false
      },
      'c773db03-49ef-4a2c-aaec-629c00485763': {
        id: 'c773db03-49ef-4a2c-aaec-629c00485763',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '61',
        price: 2.75,
        isAuxiliary: false
      },
      '935e3385-de46-4ebf-c410-d62bd55cb9df': {
        id: '935e3385-de46-4ebf-c410-d62bd55cb9df',
        name: '8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '72',
        price: 2.25,
        isAuxiliary: false,
        attributeSet: 'd8522630-e52f-4f17-95e0-e6717372004c',
        taxCategoryId: '001',
        kitchenName: 'Au Lait 8oz.',
        receiptName: 'Au Lait 8oz.'
      },
      'f4e470ec-ef8f-4ec9-fb24-72179b041686': {
        id: 'f4e470ec-ef8f-4ec9-fb24-72179b041686',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '73',
        price: 2.75,
        isAuxiliary: false
      },
      '0905895f-7e57-484b-8346-4f2bfea8123a': {
        id: '0905895f-7e57-484b-8346-4f2bfea8123a',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '74',
        price: 3.25,
        isAuxiliary: false
      },
      '2945e9d6-5791-49ca-f57b-2697c1babe51': {
        id: '2945e9d6-5791-49ca-f57b-2697c1babe51',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '75',
        price: 1.5,
        isAuxiliary: false
      },
      '2830f572-8052-45ba-d09e-e5ce0a2c6dc4': {
        id: '2830f572-8052-45ba-d09e-e5ce0a2c6dc4',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '76',
        price: 2,
        isAuxiliary: false
      },
      '339ae445-944c-4a8e-87ba-5438e28c89e9': {
        id: '339ae445-944c-4a8e-87ba-5438e28c89e9',
        name: '8 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '77',
        price: 2.25,
        isAuxiliary: false
      },
      'e3e1b493-516e-4265-c038-e24db48fb56f': {
        id: 'e3e1b493-516e-4265-c038-e24db48fb56f',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '78',
        price: 2.75,
        isAuxiliary: false
      },
      '6a87beb2-ddc1-42dc-f081-05236b1c6f33': {
        id: '6a87beb2-ddc1-42dc-f081-05236b1c6f33',
        name: '16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '79',
        price: 3.25,
        isAuxiliary: false
      },
      '57a9b88a-4c84-4afd-ff40-077cf429a7fb': {
        id: '57a9b88a-4c84-4afd-ff40-077cf429a7fb',
        name: '8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '80',
        price: 3,
        isAuxiliary: false
      },
      'f65df121-d2c6-400b-9115-70c57ffd459f': {
        id: 'f65df121-d2c6-400b-9115-70c57ffd459f',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '81',
        price: 3.5,
        isAuxiliary: false
      },
      'bc37099e-54dc-4046-bcf5-8cd694ddf1a7': {
        id: 'bc37099e-54dc-4046-bcf5-8cd694ddf1a7',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '82',
        price: 4,
        isAuxiliary: false
      },
      'fa1df5a0-7a58-45c3-87b6-9accbed8564c': {
        id: 'fa1df5a0-7a58-45c3-87b6-9accbed8564c',
        name: 'Cappuccino Single',
        merchantId: 'c20-cafeiveta95060',
        reference: '49',
        price: 2.75,
        isAuxiliary: false
      },
      '22f77333-c38e-4fa1-99b6-0f7facd6c56a': {
        id: '22f77333-c38e-4fa1-99b6-0f7facd6c56a',
        name: 'Cappuccino Double',
        merchantId: 'c20-cafeiveta95060',
        reference: '567',
        price: 3.25,
        isAuxiliary: false
      },
      '7a3eb7c0-6457-4000-e606-6abd388bdb51': {
        id: '7a3eb7c0-6457-4000-e606-6abd388bdb51',
        name: '8oz. Steamer',
        merchantId: 'c20-cafeiveta95060',
        reference: '574',
        price: 2,
        isAuxiliary: false
      },
      '555e90f5-baaf-4212-fc03-7296ce9de0b7': {
        id: '555e90f5-baaf-4212-fc03-7296ce9de0b7',
        name: 'Steamer 12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '575',
        price: 2.5,
        isAuxiliary: false
      },
      '1bd2515f-362a-4fb2-8019-ae8c2a052ee3': {
        id: '1bd2515f-362a-4fb2-8019-ae8c2a052ee3',
        name: 'Steamer 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '576',
        price: 3,
        isAuxiliary: false
      },
      '292cf434-c722-4681-a76d-6d9c99ba7989': {
        id: '292cf434-c722-4681-a76d-6d9c99ba7989',
        name: 'Chai 8 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '562',
        price: 3,
        isAuxiliary: false
      },
      '8af8fc55-dd94-4a87-8c85-b9c008972fb8': {
        id: '8af8fc55-dd94-4a87-8c85-b9c008972fb8',
        name: 'Chai 12 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '563',
        price: 3.5,
        isAuxiliary: false
      },
      'b02f7686-c85b-4dda-d836-9b788614b2ad': {
        id: 'b02f7686-c85b-4dda-d836-9b788614b2ad',
        name: 'Chai 16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '564',
        price: 4,
        isAuxiliary: false
      },
      '45b14ff5-cd3b-498d-aec1-f2109d82a8a8': {
        id: '45b14ff5-cd3b-498d-aec1-f2109d82a8a8',
        name: 'Italian Soda',
        merchantId: 'c20-cafeiveta95060',
        reference: '570',
        price: 2.75,
        isAuxiliary: false
      },
      'e0087de2-fe83-4824-aab2-19bf15a49a80': {
        id: 'e0087de2-fe83-4824-aab2-19bf15a49a80',
        name: '12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '62',
        price: 2.5,
        isAuxiliary: false
      },
      'c96189f4-7d60-4656-ce70-60a2cf77cc12': {
        id: 'c96189f4-7d60-4656-ce70-60a2cf77cc12',
        name: '16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '63',
        price: 2.75,
        isAuxiliary: false
      },
      '20071916-5ba8-4c25-b470-7ef0574b5b8f': {
        id: '20071916-5ba8-4c25-b470-7ef0574b5b8f',
        name: 'Iced Latte 12 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '554',
        price: 3.5,
        isAuxiliary: false
      },
      'bd42d278-eeba-440a-a7ac-dd1b3fadf0a2': {
        id: 'bd42d278-eeba-440a-a7ac-dd1b3fadf0a2',
        name: 'Iced Latte 16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '555',
        price: 4,
        isAuxiliary: false
      },
      'f6ee9503-b4f6-4ca9-c4d8-763724ae581a': {
        id: 'f6ee9503-b4f6-4ca9-c4d8-763724ae581a',
        name: 'Iced Americano 12 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '556',
        price: 2.75,
        isAuxiliary: false
      },
      '9fb9cf27-242b-4af9-e88b-623faa13e4df': {
        id: '9fb9cf27-242b-4af9-e88b-623faa13e4df',
        name: 'Iced Americano 16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '557',
        price: 3.25,
        isAuxiliary: false
      },
      'c88b6062-b8f6-4864-faa1-f69ef513f469': {
        id: 'c88b6062-b8f6-4864-faa1-f69ef513f469',
        name: 'Iced Chai 12 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '558',
        price: 3.5,
        isAuxiliary: false
      },
      'aa953eaf-64cf-4646-a32a-f47cda03f1de': {
        id: 'aa953eaf-64cf-4646-a32a-f47cda03f1de',
        name: 'Iced Chai 16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '559',
        price: 4,
        isAuxiliary: false
      },
      'fc336f97-314d-430a-ad67-46b35df9136a': {
        id: 'fc336f97-314d-430a-ad67-46b35df9136a',
        name: 'Iced Mocha 12 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '560',
        price: 4,
        isAuxiliary: false
      },
      '03e37434-de63-4577-b0e9-66f42197599d': {
        id: '03e37434-de63-4577-b0e9-66f42197599d',
        name: 'Iced Mocha 16 oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '561',
        price: 4.5,
        isAuxiliary: false
      },
      '62eb8e16-19c6-4fa9-dc46-8858a8725f68': {
        id: '62eb8e16-19c6-4fa9-dc46-8858a8725f68',
        name: 'ORANGE JUICE 8oz',
        merchantId: 'c20-cafeiveta95060',
        reference: '566',
        price: 3.5,
        isAuxiliary: false
      },
      '5d3a7b1b-5b9b-44d6-fbea-4c4fa7f052fc': {
        id: '5d3a7b1b-5b9b-44d6-fbea-4c4fa7f052fc',
        name: 'Orange Juice 12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '568',
        price: 4,
        isAuxiliary: false
      },
      '440dc154-e045-474b-8ec0-8981abaa2b2a': {
        id: '440dc154-e045-474b-8ec0-8981abaa2b2a',
        name: 'Grapefruit & OJ  8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '580',
        price: 3.5,
        isAuxiliary: false
      },
      '66d790f7-1819-4f53-850b-6ae54bc6a64e': {
        id: '66d790f7-1819-4f53-850b-6ae54bc6a64e',
        name: 'Grapefruit & OJ  12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '579',
        price: 4,
        isAuxiliary: false
      },
      'b0ac3da2-33e7-4fbd-aa82-819855d96261': {
        id: 'b0ac3da2-33e7-4fbd-aa82-819855d96261',
        name: 'Grapefruit Juice 8oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '578',
        price: 3.5,
        isAuxiliary: false
      },
      '18c53916-c747-4f0e-a68c-6387a5f4d2b8': {
        id: '18c53916-c747-4f0e-a68c-6387a5f4d2b8',
        name: 'Grapefruit Juice 12oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '577',
        price: 4,
        isAuxiliary: false
      },
      'a433bcc7-d088-4d03-fe4a-7bb188609037': {
        id: 'a433bcc7-d088-4d03-fe4a-7bb188609037',
        name: 'Iced Double Espresso',
        merchantId: 'c20-cafeiveta95060',
        reference: '573',
        price: 2.75,
        isAuxiliary: false
      },
      '90b6bb4c-8c8b-450d-9e46-3cae6ae4eab4': {
        id: '90b6bb4c-8c8b-450d-9e46-3cae6ae4eab4',
        name: 'Fresh Lemonade 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '83',
        price: 3,
        isAuxiliary: false
      },
      '7207ec42-d86a-45a4-e523-f039112cf3a4': {
        id: '7207ec42-d86a-45a4-e523-f039112cf3a4',
        name: 'Fresh Limeade 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '84',
        price: 3,
        isAuxiliary: false
      },
      '9435211f-d1fa-46ed-bef8-0f699bc0df6b': {
        id: '9435211f-d1fa-46ed-bef8-0f699bc0df6b',
        name: 'Mint and Lime Spritzer 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '85',
        price: 3.75,
        isAuxiliary: false
      },
      'b49a7897-79fa-4875-a226-6e7538b37e2a': {
        id: 'b49a7897-79fa-4875-a226-6e7538b37e2a',
        name: 'Arnold Palmer 16oz.',
        merchantId: 'c20-cafeiveta95060',
        reference: '86',
        price: 3.75,
        isAuxiliary: false
      },
      '6ac7f658-9d22-481f-d376-317a692caf4b': {
        id: '6ac7f658-9d22-481f-d376-317a692caf4b',
        name: 'Apple Juice',
        merchantId: 'c20-cafeiveta95060',
        reference: '87',
        price: 1.75,
        isAuxiliary: false
      },
      '79d72394-4abb-4854-cf0a-f6f6c2749433': {
        id: '79d72394-4abb-4854-cf0a-f6f6c2749433',
        name: 'Orangina',
        merchantId: 'c20-cafeiveta95060',
        reference: '88',
        price: 1.8,
        isAuxiliary: false
      },
      'ed007e91-fc17-4533-db63-c5bb13e20140': {
        id: 'ed007e91-fc17-4533-db63-c5bb13e20140',
        name: 'San Pellegrino',
        merchantId: 'c20-cafeiveta95060',
        reference: '89',
        price: 1.5,
        isAuxiliary: false
      },
      '0ffc56a5-377f-49bd-808b-d8b2744d4c63': {
        id: '0ffc56a5-377f-49bd-808b-d8b2744d4c63',
        name: 'Coke',
        merchantId: 'c20-cafeiveta95060',
        reference: '90',
        price: 2.25,
        isAuxiliary: false
      },
      'cf3f896c-82e6-47b9-ea99-e35bbb0bf5d5': {
        id: 'cf3f896c-82e6-47b9-ea99-e35bbb0bf5d5',
        name: 'Black Cherry',
        merchantId: 'c20-cafeiveta95060',
        reference: '91',
        price: 2.25,
        isAuxiliary: false
      },
      'aa6621f3-4e86-437f-9f8e-91807bc667b1': {
        id: 'aa6621f3-4e86-437f-9f8e-91807bc667b1',
        name: 'Fresh Ginger Ale',
        merchantId: 'c20-cafeiveta95060',
        reference: '92',
        price: 2.6,
        isAuxiliary: false
      },
      '4c6f2bb3-e2f4-43ee-e416-154b50cf7661': {
        id: '4c6f2bb3-e2f4-43ee-e416-154b50cf7661',
        name: 'Gus Soda',
        merchantId: 'c20-cafeiveta95060',
        reference: '93',
        price: 2.15,
        isAuxiliary: false
      },
      '467bc66f-d7aa-41c1-ad29-c07375c0544f': {
        id: '467bc66f-d7aa-41c1-ad29-c07375c0544f',
        name: 'Virgil Root Beer',
        merchantId: 'c20-cafeiveta95060',
        reference: '94',
        price: 2.5,
        isAuxiliary: false
      },
      'f09a87aa-d408-40ef-e817-f45ca5abcb99': {
        id: 'f09a87aa-d408-40ef-e817-f45ca5abcb99',
        name: 'Cc Pollen Up',
        merchantId: 'c20-cafeiveta95060',
        reference: '152',
        price: 5,
        isAuxiliary: false
      },
      '5b92acf9-5851-4a1a-94bc-847b8c3b002f': {
        id: '5b92acf9-5851-4a1a-94bc-847b8c3b002f',
        name: 'Diet Coke',
        merchantId: 'c20-cafeiveta95060',
        reference: '565',
        price: 1.6,
        isAuxiliary: false
      },
      '56ada0e5-7ceb-4d1b-8d18-a2ffc587680c': {
        id: '56ada0e5-7ceb-4d1b-8d18-a2ffc587680c',
        name: 'Blood Orange Pelligrino',
        merchantId: 'c20-cafeiveta95060',
        reference: '594',
        price: 2,
        isAuxiliary: false
      },
      'ec1cf3ff-569c-4568-8076-85606b61156a': {
        id: 'ec1cf3ff-569c-4568-8076-85606b61156a',
        name: 'Coconut Water',
        merchantId: 'c20-cafeiveta95060',
        reference: '596',
        price: 2.95,
        isAuxiliary: false
      },
      '042ca325-22fa-4896-c34f-3bf0ae01b9c2': {
        id: '042ca325-22fa-4896-c34f-3bf0ae01b9c2',
        name: 'Water',
        merchantId: 'c20-cafeiveta95060',
        reference: '599',
        price: 1,
        isAuxiliary: false
      },
      '6dc3eac5-2928-4da0-ce4c-fcc39e9f9765': {
        id: '6dc3eac5-2928-4da0-ce4c-fcc39e9f9765',
        name: 'Earl Grey',
        merchantId: 'c20-cafeiveta95060',
        reference: '495',
        price: 2.25,
        isAuxiliary: false
      },
      '94e7f88e-2022-4856-fc4e-7b079f065b6e': {
        id: '94e7f88e-2022-4856-fc4e-7b079f065b6e',
        name: 'Paris',
        merchantId: 'c20-cafeiveta95060',
        reference: '497',
        price: 2.25,
        isAuxiliary: false
      },
      '3548a700-8de6-468d-cc46-a3bfb4d6096d': {
        id: '3548a700-8de6-468d-cc46-a3bfb4d6096d',
        name: 'Sencha',
        merchantId: 'c20-cafeiveta95060',
        reference: '498',
        price: 2.25,
        isAuxiliary: false
      },
      '2f6a263d-c6b6-49a7-bcb6-173d818ada64': {
        id: '2f6a263d-c6b6-49a7-bcb6-173d818ada64',
        name: 'Jasmine',
        merchantId: 'c20-cafeiveta95060',
        reference: '499',
        price: 2.25,
        isAuxiliary: false
      },
      '62a898e3-203e-4e87-9338-5b139602ea37': {
        id: '62a898e3-203e-4e87-9338-5b139602ea37',
        name: 'Chamomile',
        merchantId: 'c20-cafeiveta95060',
        reference: '500',
        price: 2.25,
        isAuxiliary: false
      },
      'f8addc80-0e02-4ef9-e934-d14e5b4125f1': {
        id: 'f8addc80-0e02-4ef9-e934-d14e5b4125f1',
        name: 'Rooibos',
        merchantId: 'c20-cafeiveta95060',
        reference: '501',
        price: 2.25,
        isAuxiliary: false
      },
      'bbf07f5b-5572-44c4-9dc4-037974a0f0e7': {
        id: 'bbf07f5b-5572-44c4-9dc4-037974a0f0e7',
        name: 'Hot Cinnamon Spice',
        merchantId: 'c20-cafeiveta95060',
        reference: '502',
        price: 2.25,
        isAuxiliary: false
      },
      '5be333fe-9769-4eeb-9fa2-9585140a20d3': {
        id: '5be333fe-9769-4eeb-9fa2-9585140a20d3',
        name: 'Peppermint',
        merchantId: 'c20-cafeiveta95060',
        reference: '503',
        price: 2.25,
        isAuxiliary: false
      },
      'd310658d-0bee-424d-c6a1-b0f8912404c2': {
        id: 'd310658d-0bee-424d-c6a1-b0f8912404c2',
        name: 'Darjeeling',
        merchantId: 'c20-cafeiveta95060',
        reference: '504',
        price: 2.25,
        isAuxiliary: false
      },
      'b9e25d31-2595-4a19-c0a2-9d8dff239b59': {
        id: 'b9e25d31-2595-4a19-c0a2-9d8dff239b59',
        name: 'Holiday',
        merchantId: 'c20-cafeiveta95060',
        reference: '505',
        price: 2.25,
        isAuxiliary: false
      },
      'bb9026f3-9072-480e-8b9c-2c798b08f7b8': {
        id: 'bb9026f3-9072-480e-8b9c-2c798b08f7b8',
        name: 'Vanilla Comoro',
        merchantId: 'c20-cafeiveta95060',
        reference: '506',
        price: 2.25,
        isAuxiliary: false
      },
      '78b035b7-b746-4586-dd0a-76f54d99a041': {
        id: '78b035b7-b746-4586-dd0a-76f54d99a041',
        name: 'Formosa Oolong',
        merchantId: 'c20-cafeiveta95060',
        reference: '507',
        price: 2.25,
        isAuxiliary: false
      },
      '32da140c-d1df-43a3-c465-4119a8e872e3': {
        id: '32da140c-d1df-43a3-c465-4119a8e872e3',
        name: 'Pomegranate Oolong',
        merchantId: 'c20-cafeiveta95060',
        reference: '508',
        price: 2.25,
        isAuxiliary: false
      },
      '54878f05-25e1-4cde-8ec4-e4112041d604': {
        id: '54878f05-25e1-4cde-8ec4-e4112041d604',
        name: 'Raspberry Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '509',
        price: 2.5,
        isAuxiliary: false
      },
      '1bdcbf13-20d4-479a-beb2-d8d55d639b50': {
        id: '1bdcbf13-20d4-479a-beb2-d8d55d639b50',
        name: 'Orange Passion Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '510',
        price: 2.5,
        isAuxiliary: false
      },
      'ed2c894e-8573-4e3c-a7c6-d77a8f0b935c': {
        id: 'ed2c894e-8573-4e3c-a7c6-d77a8f0b935c',
        name: 'Peach Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '511',
        price: 2.5,
        isAuxiliary: false
      },
      '2ebcbbf7-6e78-41ae-a05c-360a28dc4c9d': {
        id: '2ebcbbf7-6e78-41ae-a05c-360a28dc4c9d',
        name: 'Plain Black Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '512',
        price: 2.5,
        isAuxiliary: false
      },
      '7411d3b9-2a2f-421b-b76b-7d93af8d0202': {
        id: '7411d3b9-2a2f-421b-b76b-7d93af8d0202',
        name: 'Black Currant Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '513',
        price: 2.5,
        isAuxiliary: false
      },
      '907f5d36-85aa-4651-bd4a-0b606c1875ce': {
        id: '907f5d36-85aa-4651-bd4a-0b606c1875ce',
        name: 'Green Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '514',
        price: 2.5,
        isAuxiliary: false
      },
      '51180e62-356b-4866-9452-6ec03f74d4a4': {
        id: '51180e62-356b-4866-9452-6ec03f74d4a4',
        name: 'Mango Ice Tea',
        merchantId: 'c20-cafeiveta95060',
        reference: '515',
        price: 2.5,
        isAuxiliary: false
      },
      'a1261216-ecfc-47e7-adc0-4eddf1790713': {
        id: 'a1261216-ecfc-47e7-adc0-4eddf1790713',
        name: 'Soup Cup',
        merchantId: 'c20-cafeiveta95060',
        reference: '624',
        price: 4,
        isAuxiliary: false
      },
      'aae95f7d-7749-4c3f-9ea0-83ed2245aaf7': {
        id: 'aae95f7d-7749-4c3f-9ea0-83ed2245aaf7',
        name: 'soup bowl',
        merchantId: 'c20-cafeiveta95060',
        reference: '625',
        price: 6,
        isAuxiliary: false
      },
      '9a9025df-9d29-4085-de05-8531c6726fa9': {
        id: '9a9025df-9d29-4085-de05-8531c6726fa9',
        name: 'salad',
        merchantId: 'c20-cafeiveta95060',
        reference: '626',
        price: 7,
        isAuxiliary: false
      },
      '36af7822-61ab-41ba-e5b8-3df9143e3d07': {
        id: '36af7822-61ab-41ba-e5b8-3df9143e3d07',
        name: 'Panini',
        merchantId: 'c20-cafeiveta95060',
        reference: '627',
        price: 8,
        isAuxiliary: false
      },
      '6251c9a1-a355-4369-c648-2c2f5d9ccfc4': {
        id: '6251c9a1-a355-4369-c648-2c2f5d9ccfc4',
        name: 'Lavazza Gran Selezione',
        merchantId: 'c20-cafeiveta95060',
        reference: 'R-122',
        price: 14,
        isAuxiliary: false
      },
      '85e441fd-cbf9-409f-f9a7-e71b91f34a61': {
        id: '85e441fd-cbf9-409f-f9a7-e71b91f34a61',
        name: '03337-14 Scone Pan, Mini',
        merchantId: 'c20-cafeiveta95060',
        reference: '0 1117203337 6',
        price: 35,
        isAuxiliary: false
      },
      '6a40e86e-020e-45b2-89ab-0c5022facb8d': {
        id: '6a40e86e-020e-45b2-89ab-0c5022facb8d',
        name: 'Condiment Spoons',
        merchantId: 'c20-cafeiveta95060',
        reference: '53796105411',
        price: 2.5,
        isAuxiliary: false
      }
    },
    restaurantInfo: {
      id: 'c20-cafeiveta95060',
      name: 'Cafe Iveta',
      crmId: '0017000000yJanXAAS',
      baseImgUrl: 'http://leapset-superadmin-s3.leapset.com/',
      bgImageUrl: 'c20-cafeiveta95060.jpg',
      location: {
        lat: '36.955048100000000000000000000',
        lng: '-122.050440400000000000000000000'
      },
      address: {
        address1: '2125 DELAWARE AVE',
        state: 'CA',
        city: 'SANTA CRUZ',
        zipCode: '95060-5757',
        country: 'United States',
        countryCode: 'US'
      },
      phone: {
        countryCode: '1',
        areaCode: '831',
        exchangeCode: '713',
        subscriberNumber: '0320'
      },
      pickup: true,
      deliver: true,
      deliveryFee: 0,
      deliveryTime: 0,
      dineIn: true,
      isOnlineOrderAvailable: true,
      isCakeOloAvailable: true,
      minimumOrderValue: 0,
      orderPrepTime: 15,
      minDeliveryOrderAmount: 0,
      sessions: [
        'b6248184-2f68-438b-9263-6e1f3abfe99f'
      ],
      cuisines: [
        {
          id: '215',
          name: 'Cafes',
          merchantId: 'c20-cafeiveta95060'
        }
      ],
      queueConsumers: 0,
      merchantClusterId: '786f56c1-4444-4182-b5bb-c0ae7129f86e',
      merchantStatus: 'LIVE',
      images: [
        {
          id: '7be30ea2-8a36-4bf4-9d95-9e3e4bcd05b0',
          dimension: '100x100',
          actualDimension: '200x86',
          resourceUri: 'http://leapset-superadmin-s3.leapset.com/c20-cafeiveta95060-100x100.jpg'
        }
      ],
      merchantMinCheckinDistance: 100,
      clientVersion: '6.0',
      merchantType: 'CINCO',
      timeZone: 'America/Los_Angeles',
      emailAddress: 'john@iveta.com',
      languageCode: 'en',
      countryCode: 'US',
      active: true,
      isPaypalAvailable: false
    },
    selectedSession: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
    sessions: {
      'b6248184-2f68-438b-9263-6e1f3abfe99f': {
        id: 'b6248184-2f68-438b-9263-6e1f3abfe99f',
        name: 'Menu',
        slots: [
          {
            day: '2',
            start: '07:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '6',
            start: '07:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '7',
            start: '08:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '3',
            start: '07:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '1',
            start: '08:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '4',
            start: '07:00:00',
            end: '15:45:00',
            isavailable: true
          },
          {
            day: '5',
            start: '07:00:00',
            end: '15:45:00',
            isavailable: true
          }
        ],
        categories: [
          '0aa403d1-bd20-43af-9176-c64ac06bf13c',
          'bd10692b-f653-4b1e-cb53-b0deb949ed52',
          '8c800dd3-673e-4add-d093-2b3f22c2a29e',
          '206a95c3-6cbf-4687-d9aa-83424e02bb5f',
          '359caf2f-a0f0-419f-c69e-a17712c0e915',
          '4be9dfe0-c34f-4088-c4e1-f623c171c6d9',
          'a0681e59-a954-436f-80f8-043aa81a5554',
          '96234515-7d8c-480e-d392-921989a5844b',
          'ad8465bf-c71b-4c2d-bf55-509493bb9eeb',
          '42e1637e-1494-4cec-d761-8904e9d1a4ec',
          '61a5dc25-c9d4-417b-a53f-7454ea34420e',
          '0daff840-6982-4ada-92c9-cb33fdcf6426',
          '9dcbbbd5-7a64-44b5-cfb0-81d2105d35e5',
          'ec285a46-a082-4f08-e559-a97ee7d354ce',
          '8c6b5ffe-2948-4c11-b41f-2f8e30c1dd62',
          'cbfb9c79-b2dc-4b94-abcc-55308ddd6c98',
          '3821ade6-a7ce-4937-80d1-eb298ebef8fd'
        ]
      }
    }
  },
  user: {isDrawerOpen: false}
}
