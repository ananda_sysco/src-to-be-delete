import {selectSession} from "../../src/store/app";

export default {
  currentDay : 6,
  sessions: {
    "5bda0d35-f1b5-438d-a599-436436f6d2ce": {
      "id": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
      "name": "All Day -e",
      "slots": [
        {
          "day": "5",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "6",
          "start": "16:00:00",
          "end": "16:00:00",
          "isavailable": true
        },
        {
          "day": "3",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "4",
          "start": "16:00:00",
          "end": "23:55:00",
          "isavailable": true
        },
        {
          "day": "1",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "2",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        }
      ],
      "isAvailable": false,
      "isPending": true
    },
    "88e528cc-89a1-48bd-e920-509c217e84e2": {
      "id": "88e528cc-89a1-48bd-e920-509c217e84e2",
      "name": "Session B",
      "slots": [
        {
          "day": "6",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "5",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "4",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "3",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        }
      ],
      "isAvailable": true,
      "isPending": false
    },
    "94c44d77-5beb-431e-ecbe-7da640597817": {
      "id": "94c44d77-5beb-431e-ecbe-7da640597817",
      "name": "Session F",
      "slots": [
        {
          "day": "6",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "4",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "1",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "3",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        },
        {
          "day": "2",
          "start": "00:00:00",
          "end": "23:59:59",
          "isavailable": true
        }
      ],
      "isAvailable": true,
      "isPending": false
    }
  },
  selectedSession: "88e528cc-89a1-48bd-e920-509c217e84e2",
  activeSession: null,
  sessionChange: selectSession,
  orderEnabled: true

}
