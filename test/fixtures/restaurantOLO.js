export default  {
    "id": "c0020-10213688",
    "name": "OLO_Auto_Merchant1",
    "crmId": "0011800000WmjVwAAJ",
    "location": {
      "lat": "37.486863100000000000000000000",
      "lng": "-122.226582900000000000000000000"
    },
    "address": {
      "address1": "2010 Broadway Street",
      "state": "CA",
      "city": "Redwood City",
      "zipCode": "94603",
      "country": "United States",
      "countryCode": "US"
    },
    "phone": {
      "countryCode": "1",
      "areaCode": "222",
      "exchangeCode": "213",
      "subscriberNumber": "2199"
    },
    "pickup": true,
    "deliver": true,
    "deliveryFee": 3,
    "deliveryTime": 3,
    "dineIn": true,
    "isOnlineOrderAvailable": true,
    "isCakeOloAvailable": true,
    "activeCCPaymentGateway": "LPG",
    "minimumOrderValue": 5,
    "orderPrepTime": 5,
    "minDeliveryOrderAmount": 10,
    "queueConsumers": 1,
    "merchantStatus": "LIVE",
    "merchantType": "CINCO",
    "timeZone": "America/Los_Angeles",
    "emailAddress": "olo_auto_merchant1@leap.com",
    "languageCode": "en",
    "countryCode": "US",
    "accountId": "10213688",
    "pin": "22492",
    "active": true,
    "isPaypalAvailable": false,
    "sessions": [
      "5bda0d35-f1b5-438d-a599-436436f6d2ce",
      "88e528cc-89a1-48bd-e920-509c217e84e2",
      "db004ad5-50f7-475a-bef4-338a3de1e181",
      "94c44d77-5beb-431e-ecbe-7da640597817"
    ],
    "cuisines": null,
    "images": null,
    "mapInfo": {
      "sideMapURI": "https://maps.googleapis.com/maps/api/staticmap?size=351x164&maptype=roadmap%0A%20%20%20%20&center=37.7871219,-122.4131318&scale=2%0A%20%20%20%20&zoom=15&key=AIzaSyBFKgvsTavyDJRJ2pOX2djyVn89Wr-TQmY%0A%20%20%20%20&markers=color%3A0xF2700F%7C37.7871219%2C-122.4131318&signature=TqJ84L2qRUht2DiWzTl8k3odm60=",
      "headerMapURI": "https://maps.googleapis.com/maps/api/staticmap?size=1232x158&maptype=roadmap%0A%20%20%20%20&center=37.7871219,-122.4131318&scale=2%0A%20%20%20%20&zoom=15&key=AIzaSyBFKgvsTavyDJRJ2pOX2djyVn89Wr-TQmY%0A%20%20%20%20&markers=color%3A0xF2700F%7C37.7871219%2C-122.4131318&signature=yDV-90sH6eMWFz57t0LyFC0Z_-Y=",
      "mobileHeaderMapURI": "https://maps.googleapis.com/maps/api/staticmap?size=772x164&maptype=roadmap%0A%20%20%20%20&center=37.7871219,-122.4131318&scale=2%0A%20%20%20%20&zoom=15&key=AIzaSyBFKgvsTavyDJRJ2pOX2djyVn89Wr-TQmY%0A%20%20%20%20&markers=color%3A0xF2700F%7C37.7871219%2C-122.4131318&signature=pWtWvMl-FR_nhl62IMdeeKyzM3Q="
    }
  }


