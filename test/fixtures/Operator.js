module.exports={
    AccountIdResponse:{
        data:
        {
            "accountId": "10217493",
            "crmId": "0011800000Xg7klAAB",
            "accountName": "OLO_Auto_Test_Merchant",
            "timezone": "America/Los_Angeles",
            "currencyCode": "USD",
            "countryCode": "US",
            "lat": 37.4868575,
            "lng": -122.2265767,
            "accountStatus": "ACTIVE",
            "phones": [
                {
                    "id": "525505332",
                    "phoneType": "BUSINESS",
                    "countryCode": "1",
                    "number": "2222132199"
                }
            ],
            "addresses": [
                {
                    "id": "525175061",
                    "addressType": "BUSINESS",
                    "addressLineOne": "2010",
                    "addressLineTwo": "Broadway Street",
                    "city": "Redwood City",
                    "state": "CA",
                    "zip": "94603",
                    "countryCode": "US"
                }
            ],
            "products": [
                {
                    "productId": "c0010-10217493",
                    "product": "POS",
                    "createdDate": 1520942925742,
                    "updatedDate": 1520942925742,
                    "thirdPartyDataList": [],
                    "active": true
                }
            ]
        },
        statusCode: 200,
        message: undefined },
    accessTokenObject:{ access_token: 'c1b1a977-0510-467e-ab05-23d1eb42e0d2',
        token_type: 'bearer',
        expires_in: 1799,
        scope: 'trust' }
}