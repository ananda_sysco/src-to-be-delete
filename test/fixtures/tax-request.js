export default {
  merchantId: 'c20-cafeiveta95060',
  lineItems: {
    lineItem: [
      {
        productId: '12280ffd-4d0b-46c9-803d-6107157ef015',
        qty: 1,
      },
      {
        productId: '01f766a8-cc30-4068-9b5a-7eb84ee1748e',
        qty: 1,
      }
    ]
  }
};
