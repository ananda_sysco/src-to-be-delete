export default {
  app:{
    "attributes": {
      "1_7332fe60-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe61-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe62-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "1_7332fe62-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe63-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61",
          "name": "Md Grp 4",
          "minSelections": null,
          "maxSelections": 3,
          "values": [
          "185d14d5-a8d7-44eb-8d8f-da0b2955a165",
          "365dc8c7-40a3-4deb-aaf6-bc818e93011c",
          "e03908c1-54e2-449c-b191-7116861fc865"
        ]
      },
      "1_7332fe63-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe64-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe65-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe66-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe67-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "8ba32518-ff09-4198-b5fc-f364b083897f_7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "8ba32518-ff09-4198-b5fc-f364b083897f",
          "name": "Modifier Group 1",
          "minSelections": 1,
          "maxSelections": 3,
          "values": [
          "96f850cb-bcf0-44b2-8196-0476cc0a972c",
          "5d7a3dad-54a3-4bb7-b427-27c29e7dd90e",
          "cdc2e35e-a3ea-4f73-985d-b8ad346671e2",
          "b249b8c5-c254-493c-8785-3f397f0d0d94"
        ]
      },
      "1_7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "1_7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "217956d6-27bc-47df-c496-9b5d3caddfb9_7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "217956d6-27bc-47df-c496-9b5d3caddfb9",
          "name": "modi",
          "minSelections": null,
          "maxSelections": 1,
          "values": [
          "45d61c10-e5b5-4d90-a8fd-0ac6cd024294",
          "3dff1d9c-e193-42e6-9105-a93f3941970f",
          "4081147d-30e3-4ba1-b938-601dc5869384"
        ]
      },
      "1_7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe6e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe68-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "83de2abe-67e5-45f0-ba6f-5ceec54a20ab_7332fe69-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "83de2abe-67e5-45f0-ba6f-5ceec54a20ab",
          "name": "Modifier Group 2",
          "minSelections": null,
          "maxSelections": 3,
          "values": [
          "891dae16-70b3-479d-abd5-ffd2b45eaa25",
          "ac40ce0e-dc34-4c22-9d77-984631a781b1",
          "cba9935e-e663-4939-a3e7-142ab598c3cd"
        ]
      },
      "1_7332fe69-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61",
          "name": "Md Grp 4",
          "minSelections": null,
          "maxSelections": 3,
          "values": [
          "185d14d5-a8d7-44eb-8d8f-da0b2955a165",
          "365dc8c7-40a3-4deb-aaf6-bc818e93011c",
          "e03908c1-54e2-449c-b191-7116861fc865"
        ]
      },
      "0153f244-5584-4af5-d504-17f30128a0e2_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "0153f244-5584-4af5-d504-17f30128a0e2",
          "name": "Mod Grp 5",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "47e26274-3d03-4fda-831e-8e1dde3fc6bc",
          "1bf3989c-c34e-47e2-9dda-f80a3813251c",
          "d24b7651-39b0-48a3-efa2-6428456c90be"
        ]
      },
      "16705dce-496d-45c2-864a-4d05c6979d32_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "16705dce-496d-45c2-864a-4d05c6979d32",
          "name": "ModGrp!@#$%^&*(()-=`~[];',./{}:\"<>?",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "673aba9c-2cf5-463c-b223-93077eb2cf1d",
          "1263a48f-9113-4209-d2e8-6e055f40cfeb",
          "634e478e-7fe3-4933-b361-61410267726d"
        ]
      },
      "b8473e49-e5d1-4e67-a335-acb4ee385333_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "b8473e49-e5d1-4e67-a335-acb4ee385333",
          "name": "Modi Grp 6",
          "minSelections": 2,
          "maxSelections": 4,
          "values": [
          "4808e5d5-8819-4cdf-f075-b20b7c06040d",
          "ca67ea17-732d-4b2d-f932-30916276f404",
          "af2b80a5-a6f1-440a-c9ee-877ba64a1187",
          "9218b808-3ee2-4ad6-d4e7-424f69929611"
        ]
      },
      "8ba32518-ff09-4198-b5fc-f364b083897f_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "8ba32518-ff09-4198-b5fc-f364b083897f",
          "name": "Modifier Group 1",
          "minSelections": 1,
          "maxSelections": 3,
          "values": [
          "96f850cb-bcf0-44b2-8196-0476cc0a972c",
          "5d7a3dad-54a3-4bb7-b427-27c29e7dd90e",
          "cdc2e35e-a3ea-4f73-985d-b8ad346671e2",
          "b249b8c5-c254-493c-8785-3f397f0d0d94"
        ]
      },
      "83de2abe-67e5-45f0-ba6f-5ceec54a20ab_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "83de2abe-67e5-45f0-ba6f-5ceec54a20ab",
          "name": "Modifier Group 2",
          "minSelections": null,
          "maxSelections": 3,
          "values": [
          "891dae16-70b3-479d-abd5-ffd2b45eaa25",
          "ac40ce0e-dc34-4c22-9d77-984631a781b1",
          "cba9935e-e663-4939-a3e7-142ab598c3cd"
        ]
      },
      "1_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe6f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe70-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe71-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe72-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe73-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe74-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe75-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe76-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe77-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe78-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe79-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe7a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe7b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe7c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe7d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "16705dce-496d-45c2-864a-4d05c6979d32_7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "16705dce-496d-45c2-864a-4d05c6979d32",
          "name": "ModGrp!@#$%^&*(()-=`~[];',./{}:\"<>?",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "673aba9c-2cf5-463c-b223-93077eb2cf1d",
          "1263a48f-9113-4209-d2e8-6e055f40cfeb",
          "634e478e-7fe3-4933-b361-61410267726d"
        ]
      },
      "1_7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe7f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe80-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe81-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe82-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe85-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "0153f244-5584-4af5-d504-17f30128a0e2_7332fe83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "0153f244-5584-4af5-d504-17f30128a0e2",
          "name": "Mod Grp 5",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "47e26274-3d03-4fda-831e-8e1dde3fc6bc",
          "1bf3989c-c34e-47e2-9dda-f80a3813251c",
          "d24b7651-39b0-48a3-efa2-6428456c90be"
        ]
      },
      "1_7332fe83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "16705dce-496d-45c2-864a-4d05c6979d32_7332fe84-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "16705dce-496d-45c2-864a-4d05c6979d32",
          "name": "ModGrp!@#$%^&*(()-=`~[];',./{}:\"<>?",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "673aba9c-2cf5-463c-b223-93077eb2cf1d",
          "1263a48f-9113-4209-d2e8-6e055f40cfeb",
          "634e478e-7fe3-4933-b361-61410267726d"
        ]
      },
      "1_7332fe84-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe86-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe87-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe89-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe88-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe8a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "b8473e49-e5d1-4e67-a335-acb4ee385333_7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "b8473e49-e5d1-4e67-a335-acb4ee385333",
          "name": "Modi Grp 6",
          "minSelections": 2,
          "maxSelections": 4,
          "values": [
          "4808e5d5-8819-4cdf-f075-b20b7c06040d",
          "ca67ea17-732d-4b2d-f932-30916276f404",
          "af2b80a5-a6f1-440a-c9ee-877ba64a1187",
          "9218b808-3ee2-4ad6-d4e7-424f69929611"
        ]
      },
      "1_7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe8c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "0153f244-5584-4af5-d504-17f30128a0e2_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "0153f244-5584-4af5-d504-17f30128a0e2",
          "name": "Mod Grp 5",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "47e26274-3d03-4fda-831e-8e1dde3fc6bc",
          "1bf3989c-c34e-47e2-9dda-f80a3813251c",
          "d24b7651-39b0-48a3-efa2-6428456c90be"
        ]
      },
      "1_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61",
          "name": "Md Grp 4",
          "minSelections": null,
          "maxSelections": 2,
          "values": [
          "185d14d5-a8d7-44eb-8d8f-da0b2955a165",
          "365dc8c7-40a3-4deb-aaf6-bc818e93011c",
          "e03908c1-54e2-449c-b191-7116861fc865"
        ]
      },
      "1_7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7332fe8f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "0153f244-5584-4af5-d504-17f30128a0e2_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "0153f244-5584-4af5-d504-17f30128a0e2",
          "name": "Mod Grp 5",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "47e26274-3d03-4fda-831e-8e1dde3fc6bc",
          "1bf3989c-c34e-47e2-9dda-f80a3813251c",
          "d24b7651-39b0-48a3-efa2-6428456c90be"
        ]
      },
      "1_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73334c80-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73334c81-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "c442c02a-4703-4088-983d-de31327d140b_73334c82-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "c442c02a-4703-4088-983d-de31327d140b",
          "name": "Mod Group",
          "minSelections": null,
          "maxSelections": 2,
          "values": [
          "9a9bb1ee-f02a-4661-d38f-c14b4ac48b5d",
          "a88611f2-19d0-431d-d0e6-046f44e92122"
        ]
      },
      "1_73334c82-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "53e0cfc3-49ad-441e-ffed-acebc55a5d43_73334c83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "53e0cfc3-49ad-441e-ffed-acebc55a5d43",
          "name": "Mod Group Copy copy",
          "minSelections": null,
          "maxSelections": 2,
          "values": [
          "7f946c5d-331e-4837-ddd5-005ae3a215b6",
          "eb8ec732-1e08-4d71-86e0-3349e454ac8f"
        ]
      },
      "1_73334c83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73334c87-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73334c84-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "4a86511f-17ce-43e1-d343-332a4680bb54_73334c85-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "4a86511f-17ce-43e1-d343-332a4680bb54",
          "name": "Md Grp 3",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "f7cd8896-f316-42ee-844f-4d80369559b8",
          "20d602eb-be0c-433d-ad69-058f051ff558",
          "77c9c039-6999-4cac-cf24-46947665d634"
        ]
      },
      "1_73334c85-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73334c86-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "8594eab9-487b-4c21-f5a9-07371482c83b_73334c88-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "8594eab9-487b-4c21-f5a9-07371482c83b",
          "name": "Mod change group - edited",
          "minSelections": 1,
          "maxSelections": 1,
          "values": [
          "8b37516b-c60e-4fdc-891c-35413c8d0149",
          "0650e169-cd37-423c-d462-de4cdf1504ae"
        ]
      },
      "1_73334c88-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "060a564a-4e4c-42d6-b1e6-a8535f013577_73334c89-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "060a564a-4e4c-42d6-b1e6-a8535f013577",
          "name": "Mod RM group",
          "minSelections": null,
          "maxSelections": 2,
          "values": [
          "89a815bd-05e7-4ae4-d6fe-f1ecb1225535",
          "b0732373-8c16-491d-8980-89c8b438b342"
        ]
      },
      "1_73334c89-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73337392-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "c6749e6b-b742-4b63-bd39-f07d9429228d_73337393-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "c6749e6b-b742-4b63-bd39-f07d9429228d",
          "name": "Mod AAC",
          "minSelections": 2,
          "maxSelections": 4,
          "values": [
          "91b39955-7827-4ac4-bd17-f4de2951df8c",
          "d216dae9-227d-4020-a314-74808bea724d",
          "b7549d6a-79c7-4a15-8468-fb442fe92eed",
          "c891062e-d66a-48c6-c18e-30db01a4271f",
          "64fabc96-9081-45e9-f8a9-b39f3b6eb487"
        ]
      },
      "1_73337393-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "85014dc1-713d-464f-b0d8-ac8d4772e9cb_73337390-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "85014dc1-713d-464f-b0d8-ac8d4772e9cb",
          "name": "Mod AAA",
          "minSelections": null,
          "maxSelections": 3,
          "values": [
          "91be45b9-a12d-4b3c-f4f8-83b82684916c",
          "4854cf1c-9adc-4535-e367-097e12df9cb0",
          "b2eb63d6-911b-4280-f5a1-fec413b0a88a"
        ]
      },
      "1_73337390-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "afc6ef10-b0dd-4e3c-9257-7fb9d494cff5_73337391-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "afc6ef10-b0dd-4e3c-9257-7fb9d494cff5",
          "name": "Mod AAB",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "141d2e53-fc7c-4246-a934-3bb815cf4954",
          "afea052d-fab1-4048-fcb5-0b5f754874d1",
          "cdc6ecd4-2885-4708-a7d8-28f5a5297c2a"
        ]
      },
      "1_73337391-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "83a9fbca-7071-45f8-b848-aba24c27aea2_73337394-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "83a9fbca-7071-45f8-b848-aba24c27aea2",
          "name": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "minSelections": null,
          "maxSelections": 2,
          "values": [
          "bb517ea5-1f63-48d4-809c-ff6618733f3b",
          "a7893146-c9b0-42c4-c959-9fe0f519d7d2"
        ]
      },
      "1_73337394-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73337398-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "b16b6e41-9d74-4658-dd98-7374351e16ef_73337395-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "b16b6e41-9d74-4658-dd98-7374351e16ef",
          "name": "Modifiers 222",
          "minSelections": 2,
          "maxSelections": 4,
          "values": [
          "a6ed3bba-82a7-42a5-e390-91e18d456aef",
          "59669f0e-bfaf-4a7e-ecb7-75588f161387",
          "c02fdff1-02f8-4910-d02f-1df8e7acb7e2",
          "bbfcdd16-0c85-4072-fdca-b4983b217ea8"
        ]
      },
      "1_73337395-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "b394e696-d863-4dc9-e72e-e8c5f02bd85c_73337396-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "b394e696-d863-4dc9-e72e-e8c5f02bd85c",
          "name": "Modifiers 111",
          "minSelections": 1,
          "maxSelections": 2,
          "values": [
          "8843999e-66ab-4023-dd79-7e1f0bd8596e",
          "9dc252ad-a59e-4ce3-ae78-27ff49645672",
          "e95eaa72-2785-492b-996f-3716fa3cbcd7"
        ]
      },
      "1_73337396-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73337397-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_73337399-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      },
      "1_7333739a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "1",
          "name": "Special Instructions",
          "maxSelections": 0,
          "minSelections": 0,
          "values": []
      }
    },
    "attributeSets": {
      "7332fe60-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe60-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe60-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe61-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe61-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe61-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe62-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe62-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe62-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe62-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe63-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe63-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe63-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe63-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe64-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe64-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe64-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe65-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe65-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe65-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe66-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe66-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe66-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe67-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe67-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe67-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "8ba32518-ff09-4198-b5fc-f364b083897f_7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "217956d6-27bc-47df-c496-9b5d3caddfb9_7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6e-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe6e-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe68-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe68-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe68-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe69-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe69-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "83de2abe-67e5-45f0-ba6f-5ceec54a20ab_7332fe69-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe69-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "0153f244-5584-4af5-d504-17f30128a0e2_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "16705dce-496d-45c2-864a-4d05c6979d32_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "b8473e49-e5d1-4e67-a335-acb4ee385333_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "8ba32518-ff09-4198-b5fc-f364b083897f_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "83de2abe-67e5-45f0-ba6f-5ceec54a20ab_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe6f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe6f-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe6f-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe70-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe70-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe70-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe71-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe71-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe71-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe72-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe72-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe72-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe73-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe73-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe73-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe74-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe74-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe74-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe75-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe75-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe75-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe76-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe76-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe76-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe77-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe77-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe77-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe78-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe78-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe78-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe79-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe79-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe79-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7a-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe7a-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7b-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe7b-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7c-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe7c-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7d-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe7d-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "16705dce-496d-45c2-864a-4d05c6979d32_7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe7f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe7f-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe7f-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe80-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe80-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe80-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe81-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe81-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe81-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe82-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe82-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe82-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe85-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe85-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe85-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe83-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "0153f244-5584-4af5-d504-17f30128a0e2_7332fe83-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe83-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe84-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe84-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "16705dce-496d-45c2-864a-4d05c6979d32_7332fe84-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe84-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe86-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe86-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe86-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe87-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe87-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe87-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe89-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe89-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe89-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe88-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe88-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe88-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8a-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe8a-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "b8473e49-e5d1-4e67-a335-acb4ee385333_7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8c-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8c-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe8c-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d",
          "0153f244-5584-4af5-d504-17f30128a0e2_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "687566ba-8b69-4f7d-bc7a-e3f4d75ffc61_7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe8f-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe8f-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7332fe8f-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7332fe90-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7332fe90-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d",
          "0153f244-5584-4af5-d504-17f30128a0e2_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_7332fe90-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c80-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c80-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73334c80-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c81-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c81-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73334c81-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c82-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c82-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "c442c02a-4703-4088-983d-de31327d140b_73334c82-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73334c82-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c83-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c83-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "53e0cfc3-49ad-441e-ffed-acebc55a5d43_73334c83-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73334c83-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c87-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c87-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73334c87-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c84-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c84-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73334c84-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c85-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c85-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "4a86511f-17ce-43e1-d343-332a4680bb54_73334c85-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73334c85-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c86-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c86-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73334c86-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c88-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c88-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "8594eab9-487b-4c21-f5a9-07371482c83b_73334c88-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73334c88-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73334c89-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73334c89-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "060a564a-4e4c-42d6-b1e6-a8535f013577_73334c89-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73334c89-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337392-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337392-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73337392-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337393-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337393-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "c6749e6b-b742-4b63-bd39-f07d9429228d_73337393-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337393-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337390-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337390-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "85014dc1-713d-464f-b0d8-ac8d4772e9cb_73337390-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337390-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337391-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337391-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "afc6ef10-b0dd-4e3c-9257-7fb9d494cff5_73337391-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337391-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337394-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337394-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "83a9fbca-7071-45f8-b848-aba24c27aea2_73337394-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337394-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337398-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337398-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73337398-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337395-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337395-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "b16b6e41-9d74-4658-dd98-7374351e16ef_73337395-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337395-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337396-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337396-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "b394e696-d863-4dc9-e72e-e8c5f02bd85c_73337396-1ad8-11e8-99c4-a9d1f80ee75d",
          "1_73337396-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337397-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337397-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73337397-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "73337399-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "73337399-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_73337399-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      },
      "7333739a-1ad8-11e8-99c4-a9d1f80ee75d": {
        "id": "7333739a-1ad8-11e8-99c4-a9d1f80ee75d",
          "name": "Default",
          "attributes": [
          "1_7333739a-1ad8-11e8-99c4-a9d1f80ee75d"
        ]
      }
    },
    "attributeValues": {
      "f7cd8896-f316-42ee-844f-4d80369559b8": {
        "attributeValueId": "f7cd8896-f316-42ee-844f-4d80369559b8",
          "value": "Md 1",
          "price": 1
      },
      "20d602eb-be0c-433d-ad69-058f051ff558": {
        "attributeValueId": "20d602eb-be0c-433d-ad69-058f051ff558",
          "value": "Md 2",
          "price": 1.11
      },
      "77c9c039-6999-4cac-cf24-46947665d634": {
        "attributeValueId": "77c9c039-6999-4cac-cf24-46947665d634",
          "value": "Md 3"
      },
      "185d14d5-a8d7-44eb-8d8f-da0b2955a165": {
        "attributeValueId": "185d14d5-a8d7-44eb-8d8f-da0b2955a165",
          "value": "Mid 1",
          "price": 1
      },
      "365dc8c7-40a3-4deb-aaf6-bc818e93011c": {
        "attributeValueId": "365dc8c7-40a3-4deb-aaf6-bc818e93011c",
          "value": "Mid 2"
      },
      "e03908c1-54e2-449c-b191-7116861fc865": {
        "attributeValueId": "e03908c1-54e2-449c-b191-7116861fc865",
          "value": "Mid 3",
          "price": 3
      },
      "96f850cb-bcf0-44b2-8196-0476cc0a972c": {
        "attributeValueId": "96f850cb-bcf0-44b2-8196-0476cc0a972c",
          "value": "m1",
          "price": 1.1
      },
      "5d7a3dad-54a3-4bb7-b427-27c29e7dd90e": {
        "attributeValueId": "5d7a3dad-54a3-4bb7-b427-27c29e7dd90e",
          "value": "m2",
          "price": 2
      },
      "cdc2e35e-a3ea-4f73-985d-b8ad346671e2": {
        "attributeValueId": "cdc2e35e-a3ea-4f73-985d-b8ad346671e2",
          "value": "m3"
      },
      "b249b8c5-c254-493c-8785-3f397f0d0d94": {
        "attributeValueId": "b249b8c5-c254-493c-8785-3f397f0d0d94",
          "value": "m4",
          "price": 4.4
      },
      "45d61c10-e5b5-4d90-a8fd-0ac6cd024294": {
        "attributeValueId": "45d61c10-e5b5-4d90-a8fd-0ac6cd024294",
          "value": "modi1",
          "price": 1
      },
      "3dff1d9c-e193-42e6-9105-a93f3941970f": {
        "attributeValueId": "3dff1d9c-e193-42e6-9105-a93f3941970f",
          "value": "modi2"
      },
      "4081147d-30e3-4ba1-b938-601dc5869384": {
        "attributeValueId": "4081147d-30e3-4ba1-b938-601dc5869384",
          "value": "modi3",
          "price": 3
      },
      "891dae16-70b3-479d-abd5-ffd2b45eaa25": {
        "attributeValueId": "891dae16-70b3-479d-abd5-ffd2b45eaa25",
          "value": "m21",
          "price": 5.4
      },
      "ac40ce0e-dc34-4c22-9d77-984631a781b1": {
        "attributeValueId": "ac40ce0e-dc34-4c22-9d77-984631a781b1",
          "value": "m22",
          "price": 1.21
      },
      "cba9935e-e663-4939-a3e7-142ab598c3cd": {
        "attributeValueId": "cba9935e-e663-4939-a3e7-142ab598c3cd",
          "value": "m33",
          "price": 9.99
      },
      "47e26274-3d03-4fda-831e-8e1dde3fc6bc": {
        "attributeValueId": "47e26274-3d03-4fda-831e-8e1dde3fc6bc",
          "value": "Mod 1",
          "price": 11.11
      },
      "1bf3989c-c34e-47e2-9dda-f80a3813251c": {
        "attributeValueId": "1bf3989c-c34e-47e2-9dda-f80a3813251c",
          "value": "Mod 2",
          "price": 1
      },
      "d24b7651-39b0-48a3-efa2-6428456c90be": {
        "attributeValueId": "d24b7651-39b0-48a3-efa2-6428456c90be",
          "value": "Mod 3"
      },
      "673aba9c-2cf5-463c-b223-93077eb2cf1d": {
        "attributeValueId": "673aba9c-2cf5-463c-b223-93077eb2cf1d",
          "value": "!@",
          "price": 1
      },
      "1263a48f-9113-4209-d2e8-6e055f40cfeb": {
        "attributeValueId": "1263a48f-9113-4209-d2e8-6e055f40cfeb",
          "value": "#$",
          "price": 2
      },
      "634e478e-7fe3-4933-b361-61410267726d": {
        "attributeValueId": "634e478e-7fe3-4933-b361-61410267726d",
          "value": "$%^",
          "price": 3
      },
      "4808e5d5-8819-4cdf-f075-b20b7c06040d": {
        "attributeValueId": "4808e5d5-8819-4cdf-f075-b20b7c06040d",
          "value": "Modi 1",
          "price": 1
      },
      "ca67ea17-732d-4b2d-f932-30916276f404": {
        "attributeValueId": "ca67ea17-732d-4b2d-f932-30916276f404",
          "value": "Modi 2",
          "price": 2
      },
      "af2b80a5-a6f1-440a-c9ee-877ba64a1187": {
        "attributeValueId": "af2b80a5-a6f1-440a-c9ee-877ba64a1187",
          "value": "Modi 3",
          "price": 3
      },
      "9218b808-3ee2-4ad6-d4e7-424f69929611": {
        "attributeValueId": "9218b808-3ee2-4ad6-d4e7-424f69929611",
          "value": "Modi 4",
          "price": 4
      },
      "9a9bb1ee-f02a-4661-d38f-c14b4ac48b5d": {
        "attributeValueId": "9a9bb1ee-f02a-4661-d38f-c14b4ac48b5d",
          "value": "Mod Copy 1",
          "price": 1
      },
      "a88611f2-19d0-431d-d0e6-046f44e92122": {
        "attributeValueId": "a88611f2-19d0-431d-d0e6-046f44e92122",
          "value": "Mod copy 2",
          "price": 2
      },
      "7f946c5d-331e-4837-ddd5-005ae3a215b6": {
        "attributeValueId": "7f946c5d-331e-4837-ddd5-005ae3a215b6",
          "value": "Mod Copy 1",
          "price": 1
      },
      "eb8ec732-1e08-4d71-86e0-3349e454ac8f": {
        "attributeValueId": "eb8ec732-1e08-4d71-86e0-3349e454ac8f",
          "value": "Mod copy 2",
          "price": 2
      },
      "8b37516b-c60e-4fdc-891c-35413c8d0149": {
        "attributeValueId": "8b37516b-c60e-4fdc-891c-35413c8d0149",
          "value": "Mod change-edited",
          "price": 20
      },
      "0650e169-cd37-423c-d462-de4cdf1504ae": {
        "attributeValueId": "0650e169-cd37-423c-d462-de4cdf1504ae",
          "value": "Mod change2",
          "price": 20
      },
      "89a815bd-05e7-4ae4-d6fe-f1ecb1225535": {
        "attributeValueId": "89a815bd-05e7-4ae4-d6fe-f1ecb1225535",
          "value": "Mod RM new change"
      },
      "b0732373-8c16-491d-8980-89c8b438b342": {
        "attributeValueId": "b0732373-8c16-491d-8980-89c8b438b342",
          "value": "Mod RM change2",
          "price": 10
      },
      "91b39955-7827-4ac4-bd17-f4de2951df8c": {
        "attributeValueId": "91b39955-7827-4ac4-bd17-f4de2951df8c",
          "value": "Mod AA - $1.11",
          "price": 1.11
      },
      "d216dae9-227d-4020-a314-74808bea724d": {
        "attributeValueId": "d216dae9-227d-4020-a314-74808bea724d",
          "value": "Mod AB - $0.00"
      },
      "b7549d6a-79c7-4a15-8468-fb442fe92eed": {
        "attributeValueId": "b7549d6a-79c7-4a15-8468-fb442fe92eed",
          "value": "Mod AC - $2.39",
          "price": 2.39
      },
      "c891062e-d66a-48c6-c18e-30db01a4271f": {
        "attributeValueId": "c891062e-d66a-48c6-c18e-30db01a4271f",
          "value": "Mod AD - $3.33",
          "price": 3.33
      },
      "64fabc96-9081-45e9-f8a9-b39f3b6eb487": {
        "attributeValueId": "64fabc96-9081-45e9-f8a9-b39f3b6eb487",
          "value": "Mod AE - $1.11",
          "price": 1.11
      },
      "91be45b9-a12d-4b3c-f4f8-83b82684916c": {
        "attributeValueId": "91be45b9-a12d-4b3c-f4f8-83b82684916c",
          "value": "Mod AA - $0.05",
          "price": 0.05
      },
      "4854cf1c-9adc-4535-e367-097e12df9cb0": {
        "attributeValueId": "4854cf1c-9adc-4535-e367-097e12df9cb0",
          "value": "Mod AB - $1.11",
          "price": 1.11
      },
      "b2eb63d6-911b-4280-f5a1-fec413b0a88a": {
        "attributeValueId": "b2eb63d6-911b-4280-f5a1-fec413b0a88a",
          "value": "Mod AC - $0.00"
      },
      "141d2e53-fc7c-4246-a934-3bb815cf4954": {
        "attributeValueId": "141d2e53-fc7c-4246-a934-3bb815cf4954",
          "value": "Mod AA - $0.00"
      },
      "afea052d-fab1-4048-fcb5-0b5f754874d1": {
        "attributeValueId": "afea052d-fab1-4048-fcb5-0b5f754874d1",
          "value": "Mod AB - $0.00"
      },
      "cdc6ecd4-2885-4708-a7d8-28f5a5297c2a": {
        "attributeValueId": "cdc6ecd4-2885-4708-a7d8-28f5a5297c2a",
          "value": "Mod AC - $0.00"
      },
      "bb517ea5-1f63-48d4-809c-ff6618733f3b": {
        "attributeValueId": "bb517ea5-1f63-48d4-809c-ff6618733f3b",
          "value": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"?????//////////",
          "price": 1
      },
      "a7893146-c9b0-42c4-c959-9fe0f519d7d2": {
        "attributeValueId": "a7893146-c9b0-42c4-c959-9fe0f519d7d2",
          "value": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "price": 2
      },
      "a6ed3bba-82a7-42a5-e390-91e18d456aef": {
        "attributeValueId": "a6ed3bba-82a7-42a5-e390-91e18d456aef",
          "value": "Modi 1",
          "price": 1
      },
      "59669f0e-bfaf-4a7e-ecb7-75588f161387": {
        "attributeValueId": "59669f0e-bfaf-4a7e-ecb7-75588f161387",
          "value": "Modi 2",
          "price": 2
      },
      "c02fdff1-02f8-4910-d02f-1df8e7acb7e2": {
        "attributeValueId": "c02fdff1-02f8-4910-d02f-1df8e7acb7e2",
          "value": "Modi 3",
          "price": 3
      },
      "bbfcdd16-0c85-4072-fdca-b4983b217ea8": {
        "attributeValueId": "bbfcdd16-0c85-4072-fdca-b4983b217ea8",
          "value": "Modi 4",
          "price": 4
      },
      "8843999e-66ab-4023-dd79-7e1f0bd8596e": {
        "attributeValueId": "8843999e-66ab-4023-dd79-7e1f0bd8596e",
          "value": "Mod 1",
          "price": 11.11
      },
      "9dc252ad-a59e-4ce3-ae78-27ff49645672": {
        "attributeValueId": "9dc252ad-a59e-4ce3-ae78-27ff49645672",
          "value": "Mod 2",
          "price": 1
      },
      "e95eaa72-2785-492b-996f-3716fa3cbcd7": {
        "attributeValueId": "e95eaa72-2785-492b-996f-3716fa3cbcd7",
          "value": "Mod 3"
      }
    },
    "categories": {
      "dcdf638c-a5ac-4613-a16c-77e76ed876bd": {
        "id": "dcdf638c-a5ac-4613-a16c-77e76ed876bd",
          "name": "Auto_Category_Main",
          "description": "Automation category",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "444484d1-a892-412f-adca-fe1d2168ba2c",
          "0755e518-23c7-4904-d830-59e4629ffbd2",
          "bf801bea-72b3-48cb-ecb1-a0755d327486",
          "537f0de6-aeae-44f7-99f7-73073f29377a",
          "bf9668ae-cc83-4e85-cc75-5329c69c79c3",
          "1845b14a-2e65-4f47-b42a-6aed8399c9ad",
          "a3167afb-6edf-4878-b7f3-e8813c8b9331",
          "4fe52152-3e52-4752-9822-d4d283d50ccf"
        ],
          "isSubCategory": false
      },
      "d982d949-f7b3-46dc-9e06-255791fcbcf3": {
        "id": "d982d949-f7b3-46dc-9e06-255791fcbcf3",
          "name": "Sub Category 1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "134afadb-ec85-41ef-ef6f-35a904ca6eda",
          "products": [
          "95f5bb35-e9a4-4068-cd18-1fadd56581e6",
          "bb24fc11-03da-407b-90d5-9ee32e409235",
          "ec53e8d7-e1be-4215-8d05-19fedf20a982"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "134afadb-ec85-41ef-ef6f-35a904ca6eda": {
        "id": "134afadb-ec85-41ef-ef6f-35a904ca6eda",
          "name": "Category 1-all day only",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "d982d949-f7b3-46dc-9e06-255791fcbcf3"
        ],
          "products": [
          "63099832-046f-477d-804a-47f2c35b9e9c",
          "f9a07792-d0c2-44dd-871d-4b0a1c5996f8",
          "0978e5cf-d875-464f-f5ff-c0f0a6fa84ae",
          "bd0b3207-f0ba-4100-ab0b-11c8a983e7a0"
        ],
          "isSubCategory": false
      },
      "72914dd8-15ef-40a7-dcfa-1494d81b9861": {
        "id": "72914dd8-15ef-40a7-dcfa-1494d81b9861",
          "name": "Category A",
          "description": "Description: Our most popular bottle, available in a variety of colors to help brighten up anybody’s gear. The large opening on our wide-mouth bottles easily accommodates ice cubes, fits most water purifiers and filters, and makes hand washing a breeze. The attached loop-top never gets lost and screws on and off easily. Printed graduations let keep track of your hydration. Dishwasher safe (Please make sure the top does not touch the heating element, or it will melt).\r\nWhy we love it: It’s clear this is a product that customers love. The description clearly articulates the water bottles special perks and practicality. Nalgene is also very smart to include that its product is dishwasher safe; touching on what could be a pain point for its customers. Finally, the company makes an important disclaimer with the text “Please make sure the top does not touch the heating element” to provide valuable product care information that will help the company proactively reduce returns.",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "8fd14cb0-6e82-4dd5-b940-bdbf3b4c1be9",
          "eceead13-5954-4aeb-89c8-5ea0905dd898",
          "a723c231-06bd-47fb-d7ff-d93fa8541574",
          "97022875-522d-481e-df31-473c91cc04ab"
        ],
          "isSubCategory": false
      },
      "14e0aee2-0c1f-48f5-e887-d14076b90d6e": {
        "id": "14e0aee2-0c1f-48f5-e887-d14076b90d6e",
          "name": "Category B",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "9343af09-9761-4fe7-aca7-9fb7b553b9fa",
          "510d9768-0b8e-4174-d6cd-e9218eb3a88f",
          "832c06cc-8f54-433c-efc2-f1f598b79780"
        ],
          "isSubCategory": false
      },
      "09b1fd4b-c2de-4375-eb48-f6c0c6c044c4": {
        "id": "09b1fd4b-c2de-4375-eb48-f6c0c6c044c4",
          "name": "Category C",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "63348401-631f-4140-e9f5-58445ea9b6b0",
          "7adc434d-7f61-4dd8-b20e-f8996570527b"
        ],
          "isSubCategory": false
      },
      "c43dbf3d-0757-4f7a-97d7-ce3bfa870418": {
        "id": "c43dbf3d-0757-4f7a-97d7-ce3bfa870418",
          "name": "tax sub",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "09281b53-7a6b-4ac9-901e-d0fc64af92bd",
          "products": [],
          "subCategories": [],
          "isSubCategory": true
      },
      "09281b53-7a6b-4ac9-901e-d0fc64af92bd": {
        "id": "09281b53-7a6b-4ac9-901e-d0fc64af92bd",
          "name": "Category D",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "c43dbf3d-0757-4f7a-97d7-ce3bfa870418"
        ],
          "products": [
          "6eb43ab7-6ee1-458b-de76-dcc29c357167",
          "d787a71b-37a9-4660-83bb-2bfbb08df11b"
        ],
          "isSubCategory": false
      },
      "899285b9-b2cf-40c7-e89d-327f62b87bcf": {
        "id": "899285b9-b2cf-40c7-e89d-327f62b87bcf",
          "name": "Category E",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "85c4c79d-9915-42fc-8b0e-0703776a59eb",
          "3e50bd77-ffc9-4e16-ee11-2848c4b8b17f"
        ],
          "isSubCategory": false
      },
      "6e9020fb-7980-46b5-b44e-c15f20721f21": {
        "id": "6e9020fb-7980-46b5-b44e-c15f20721f21",
          "name": "Category Price Check",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      },
      "56230ffd-6271-4135-cbe9-59b65273f2a4": {
        "id": "56230ffd-6271-4135-cbe9-59b65273f2a4",
          "name": "Sub Category IS",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "d90d94ad-63d7-4b59-a1d7-2e46c4f6e590",
          "products": [
          "5cc3a127-ff50-4bfe-bd4b-442ffb13d76a",
          "6be956ed-047e-4c66-faeb-bf7a4a2081a0"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "d90d94ad-63d7-4b59-a1d7-2e46c4f6e590": {
        "id": "d90d94ad-63d7-4b59-a1d7-2e46c4f6e590",
          "name": "Category IS",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "56230ffd-6271-4135-cbe9-59b65273f2a4"
        ],
          "products": [],
          "isSubCategory": false
      },
      "9d14ebce-fbe6-4702-93d7-61b01927b55f": {
        "id": "9d14ebce-fbe6-4702-93d7-61b01927b55f",
          "name": "Cate 1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "20e9a39c-8b1d-4152-ca41-5d3270f98276"
        ],
          "isSubCategory": false
      },
      "46e0a152-bfc7-4492-9625-973974db510f": {
        "id": "46e0a152-bfc7-4492-9625-973974db510f",
          "name": "Cate 2",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "9d25414a-4268-49e3-8589-c131ec18f2be",
          "1d927a95-cda2-44f3-d49e-50e50f6c3f9e"
        ],
          "isSubCategory": false
      },
      "4c1ccdd0-5cd6-4f77-fa64-dbce0a351d96": {
        "id": "4c1ccdd0-5cd6-4f77-fa64-dbce0a351d96",
          "name": "Cate 3",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "37f044e0-96a5-4d79-cff0-c1a613dc07c1"
        ],
          "isSubCategory": false
      },
      "f46f6de4-ab87-416b-c2fe-9cbc9b0f3c74": {
        "id": "f46f6de4-ab87-416b-c2fe-9cbc9b0f3c74",
          "name": "Cate 4",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "4dbc5cd5-e175-4c2c-85a8-2d8eba4d42a2"
        ],
          "isSubCategory": false
      },
      "c46f79f3-a853-41ce-b415-bf08444acd31": {
        "id": "c46f79f3-a853-41ce-b415-bf08444acd31",
          "name": "Sub Cat 1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "741142f9-5b90-4a70-c4af-3e77d5b15e5d",
          "products": [
          "4cd91af4-0e85-4cb4-df0a-c5edc181066a",
          "f23e2bbc-8eaa-442c-b422-074621892722"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "741142f9-5b90-4a70-c4af-3e77d5b15e5d": {
        "id": "741142f9-5b90-4a70-c4af-3e77d5b15e5d",
          "name": "Cat 1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "c46f79f3-a853-41ce-b415-bf08444acd31"
        ],
          "products": [
          "ba1b827a-4ba0-48ee-95fa-6b060c02147c"
        ],
          "isSubCategory": false
      },
      "af8f93c8-4e43-4e38-8fe7-c2eeb9423761": {
        "id": "af8f93c8-4e43-4e38-8fe7-c2eeb9423761",
          "name": "Sub Cat 2",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "14845038-d906-488d-d6bf-6bf97340c4d3",
          "products": [
          "d0acccbe-74af-4d4f-917e-9f8f73775833"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "14845038-d906-488d-d6bf-6bf97340c4d3": {
        "id": "14845038-d906-488d-d6bf-6bf97340c4d3",
          "name": "Cat 2",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "af8f93c8-4e43-4e38-8fe7-c2eeb9423761"
        ],
          "products": [],
          "isSubCategory": false
      },
      "02b2a625-3dcd-4734-d43f-f0ec8300b954": {
        "id": "02b2a625-3dcd-4734-d43f-f0ec8300b954",
          "name": "Cat 3",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "43639e12-3617-4983-c484-66f0587bfca5"
        ],
          "isSubCategory": false
      },
      "c8941083-6bf6-4ed3-922a-93cc33286a69": {
        "id": "c8941083-6bf6-4ed3-922a-93cc33286a69",
          "name": "Cat 4",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      },
      "17cbb8ba-e63a-4040-b104-338cac51e943": {
        "id": "17cbb8ba-e63a-4040-b104-338cac51e943",
          "name": "Sub Cat Sp!~@#$%^&*()~-=[]{};':\",./<>?",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "dfe6f9e4-ff3a-49dd-add2-fb567464c8d3",
          "products": [
          "5dc63442-052d-401a-cc85-d5faaa255eb0"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "dfe6f9e4-ff3a-49dd-add2-fb567464c8d3": {
        "id": "dfe6f9e4-ff3a-49dd-add2-fb567464c8d3",
          "name": "Cat Sp_!@#$%^&*~()[]{};':\",./<>?`-=",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "17cbb8ba-e63a-4040-b104-338cac51e943"
        ],
          "products": [
          "15f1d6ee-3a44-4eaf-ef29-fa0beb75c93a"
        ],
          "isSubCategory": false
      },
      "c21cd284-afb5-485b-9f44-0d5bd3961cee": {
        "id": "c21cd284-afb5-485b-9f44-0d5bd3961cee",
          "name": "Category with items",
          "description": "Category with items",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "fab8eeb2-45c4-46ec-cae9-fa0d96b35db1",
          "bf4fc2ca-9d50-43e0-ed22-b419bac250b4"
        ],
          "isSubCategory": false
      },
      "fdfbb87f-b72f-4879-f964-11759c3630e7": {
        "id": "fdfbb87f-b72f-4879-f964-11759c3630e7",
          "name": "POS ONLY",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "97bbbab5-7c1d-48a3-8a75-080f8ebd74f7"
        ],
          "isSubCategory": false
      },
      "ad51f375-8c5c-407d-ab0a-d7d7a0c9422e": {
        "id": "ad51f375-8c5c-407d-ab0a-d7d7a0c9422e",
          "name": "new",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      },
      "82f4cd52-2b69-4897-c0e3-9c49a6f7d360": {
        "id": "82f4cd52-2b69-4897-c0e3-9c49a6f7d360",
          "name": "C1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      },
      "90527040-798c-4d96-ac06-1b7b34aac3a8": {
        "id": "90527040-798c-4d96-ac06-1b7b34aac3a8",
          "name": "C2",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "dd214ce9-fb33-47ce-ff8d-eb3f2932a3f4",
          "600880c7-f726-48f7-b477-6b9a004098a2",
          "df247fb7-df2e-496e-f943-90ac80b1d89b",
          "d72ab53f-2247-4c21-a1c2-cbcc374b51f3"
        ],
          "isSubCategory": false
      },
      "ad5d60d0-8c0a-4f2c-f892-fa30ef213fb5": {
        "id": "ad5d60d0-8c0a-4f2c-f892-fa30ef213fb5",
          "name": "main cat",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "af1042cc-75d0-4b07-8611-d73d9d501241"
        ],
          "isSubCategory": false
      },
      "79f5ccb1-66c2-4b58-8504-a517dea35d63": {
        "id": "79f5ccb1-66c2-4b58-8504-a517dea35d63",
          "name": "DelCat",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "4bae9667-92a5-4cc3-acd3-e137049ce618"
        ],
          "isSubCategory": false
      },
      "1e0e2be3-8f94-451f-d68b-cb6abb6c5c8c": {
        "id": "1e0e2be3-8f94-451f-d68b-cb6abb6c5c8c",
          "name": "mod copy",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "d39e0142-cfdf-4998-9fa2-0d96cb2c932d",
          "da910ad5-94b0-4624-ad58-7e4cc506c189"
        ],
          "isSubCategory": false
      },
      "a59058fb-3fcf-49a4-9a61-8adc66c41b38": {
        "id": "a59058fb-3fcf-49a4-9a61-8adc66c41b38",
          "name": "sub tax",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "fd6da33b-9655-44b9-f666-4aee1faddc8b",
          "products": [
          "588fe77e-0f90-4bd6-e047-e52516814d9c",
          "d0d498f0-9170-4a91-e1ac-1a7445c71b2b",
          "0cdf576a-0af3-4dc9-cc75-37d7782795b6"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "fd6da33b-9655-44b9-f666-4aee1faddc8b": {
        "id": "fd6da33b-9655-44b9-f666-4aee1faddc8b",
          "name": "Tax Cat",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "a59058fb-3fcf-49a4-9a61-8adc66c41b38"
        ],
          "products": [
          "fde69973-78d3-4b63-aab5-dcca9c77f0fd"
        ],
          "isSubCategory": false
      },
      "f35bccbb-91b1-4202-da6f-4137fd183334": {
        "id": "f35bccbb-91b1-4202-da6f-4137fd183334",
          "name": "Sub Cat change -edited",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "abc89ef7-18b7-4f37-d51c-3241e715d730",
          "products": [
          "23acc0b6-2b23-4b52-86a2-464a80c9c6a3"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "abc89ef7-18b7-4f37-d51c-3241e715d730": {
        "id": "abc89ef7-18b7-4f37-d51c-3241e715d730",
          "name": "Name Change - edited",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "f35bccbb-91b1-4202-da6f-4137fd183334"
        ],
          "products": [],
          "isSubCategory": false
      },
      "42cf59b5-9a02-41cc-8048-e7fcd5150ac2": {
        "id": "42cf59b5-9a02-41cc-8048-e7fcd5150ac2",
          "name": "Sub Cat RM",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "c5ad4d9e-e1e4-4495-cd4d-dfdaa15a6aa5",
          "products": [
          "ff57419c-3896-4422-d317-294036c16881"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "c5ad4d9e-e1e4-4495-cd4d-dfdaa15a6aa5": {
        "id": "c5ad4d9e-e1e4-4495-cd4d-dfdaa15a6aa5",
          "name": "Name RM",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "42cf59b5-9a02-41cc-8048-e7fcd5150ac2"
        ],
          "products": [],
          "isSubCategory": false
      },
      "7d208f47-e9d2-459f-f66b-5109f358ab3e": {
        "id": "7d208f47-e9d2-459f-f66b-5109f358ab3e",
          "name": "Sub Category AAA",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "ec064b76-8ede-4097-e9b6-f851b7041660",
          "products": [
          "c6ee8433-8342-4463-e863-8e05e02e0fd0",
          "643884c7-a560-4c97-c595-b2044388dd10"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "ec064b76-8ede-4097-e9b6-f851b7041660": {
        "id": "ec064b76-8ede-4097-e9b6-f851b7041660",
          "name": "Category AAA",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "7d208f47-e9d2-459f-f66b-5109f358ab3e"
        ],
          "products": [
          "0c77dbd5-1bb6-4189-b370-d7e3641be6f4",
          "211b8aae-d004-4196-c61a-a0057a27a092"
        ],
          "isSubCategory": false
      },
      "6a6f84eb-6ae5-4392-de2a-a94615de9bd1": {
        "id": "6a6f84eb-6ae5-4392-de2a-a94615de9bd1",
          "name": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "35e4408c-bdfa-48b1-bb76-40d2454081b8"
        ],
          "isSubCategory": false
      },
      "420fd028-3f35-4f54-f2ff-13711535caf9": {
        "id": "420fd028-3f35-4f54-f2ff-13711535caf9",
          "name": "Sub Ca1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "5bd46817-b990-4c70-e1fa-70ee27b42448",
          "products": [
          "878c6a96-9c0b-41b2-92c8-057d2be16bb7",
          "87900996-a483-4219-dada-6feca832613d"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "4dfba4c8-2db8-4628-f68f-eab5f1716003": {
        "id": "4dfba4c8-2db8-4628-f68f-eab5f1716003",
          "name": "Sub Ca2",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "parentId": "5bd46817-b990-4c70-e1fa-70ee27b42448",
          "products": [
          "fced21d4-1154-4868-d698-74478a239a08"
        ],
          "subCategories": [],
          "isSubCategory": true
      },
      "5bd46817-b990-4c70-e1fa-70ee27b42448": {
        "id": "5bd46817-b990-4c70-e1fa-70ee27b42448",
          "name": "Ca 1",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [
          "420fd028-3f35-4f54-f2ff-13711535caf9",
          "4dfba4c8-2db8-4628-f68f-eab5f1716003"
        ],
          "products": [
          "50372392-aecc-4652-f3f0-11419233382c"
        ],
          "isSubCategory": false
      },
      "0aea26cc-69a3-4f10-cdcf-919c8841e359": {
        "id": "0aea26cc-69a3-4f10-cdcf-919c8841e359",
          "name": "Session A",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "bd2cb881-a89a-48d8-c7de-006d0d2a5a0f"
        ],
          "isSubCategory": false
      },
      "21936f2e-10ab-4dec-f4c8-70945d037805": {
        "id": "21936f2e-10ab-4dec-f4c8-70945d037805",
          "name": "Session B",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [
          "0075be7a-f001-4570-8381-a01c23cfc1d1"
        ],
          "isSubCategory": false
      },
      "d7f3f04f-8c87-45ee-991c-99417345e822": {
        "id": "d7f3f04f-8c87-45ee-991c-99417345e822",
          "name": "ABCD",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      },
      "06d3513c-937e-40a5-b375-20dbaab8cfac": {
        "id": "06d3513c-937e-40a5-b375-20dbaab8cfac",
          "name": "Dili",
          "description": "",
          "merchantId": "10213688",
          "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "isActive": true,
          "lineNo": null,
          "subCategories": [],
          "products": [],
          "isSubCategory": false
      }
    },
    "currentSession": null,
      "currentDay": 2,
      "notifications": [],
      "products": {
      "444484d1-a892-412f-adca-fe1d2168ba2c": {
        "id": "444484d1-a892-412f-adca-fe1d2168ba2c",
          "name": "Price Check 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "15",
          "price": 10.55,
          "kitchenName": "Price Check 1",
          "receiptName": "Price Check 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe60-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "0755e518-23c7-4904-d830-59e4629ffbd2": {
        "id": "0755e518-23c7-4904-d830-59e4629ffbd2",
          "name": "Price Check 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "16",
          "price": 20.55,
          "kitchenName": "Price Check 2",
          "receiptName": "Price Check 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe61-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bf801bea-72b3-48cb-ecb1-a0755d327486": {
        "id": "bf801bea-72b3-48cb-ecb1-a0755d327486",
          "name": "It 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "19",
          "price": 4,
          "kitchenName": "It 1",
          "receiptName": "It 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe62-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "537f0de6-aeae-44f7-99f7-73073f29377a": {
        "id": "537f0de6-aeae-44f7-99f7-73073f29377a",
          "name": "It 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "20",
          "price": 5,
          "kitchenName": "It 2",
          "receiptName": "It 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe63-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bf9668ae-cc83-4e85-cc75-5329c69c79c3": {
        "id": "bf9668ae-cc83-4e85-cc75-5329c69c79c3",
          "name": "IS 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "35",
          "price": 10.55,
          "kitchenName": "IS 1",
          "receiptName": "IS 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe64-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "1845b14a-2e65-4f47-b42a-6aed8399c9ad": {
        "id": "1845b14a-2e65-4f47-b42a-6aed8399c9ad",
          "name": "IS 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "36",
          "price": 20.55,
          "kitchenName": "IS 2",
          "receiptName": "IS 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe65-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "a3167afb-6edf-4878-b7f3-e8813c8b9331": {
        "id": "a3167afb-6edf-4878-b7f3-e8813c8b9331",
          "name": "Item_auto_min_order",
          "description": "",
          "merchantId": "10213688",
          "reference": "37",
          "price": 4.33,
          "kitchenName": "Item_auto_min_order",
          "receiptName": "Item_auto_min_order",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe66-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "4fe52152-3e52-4752-9822-d4d283d50ccf": {
        "id": "4fe52152-3e52-4752-9822-d4d283d50ccf",
          "name": "Item_auto_min_del_order",
          "description": "",
          "merchantId": "10213688",
          "reference": "38",
          "price": 8.66,
          "kitchenName": "Item_auto_min_del_order",
          "receiptName": "Item_auto_min_del_order",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe67-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "63099832-046f-477d-804a-47f2c35b9e9c": {
        "id": "63099832-046f-477d-804a-47f2c35b9e9c",
          "name": "Custom Item 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "2",
          "price": 5.5,
          "kitchenName": "Custom Item 2",
          "receiptName": "Custom Item 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6b-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "f9a07792-d0c2-44dd-871d-4b0a1c5996f8": {
        "id": "f9a07792-d0c2-44dd-871d-4b0a1c5996f8",
          "name": "temp_itm",
          "description": "",
          "merchantId": "10213688",
          "reference": "41",
          "price": 1.22,
          "kitchenName": "temp_itm",
          "receiptName": "temp_itm",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6c-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "0978e5cf-d875-464f-f5ff-c0f0a6fa84ae": {
        "id": "0978e5cf-d875-464f-f5ff-c0f0a6fa84ae",
          "name": "modi1",
          "description": "",
          "merchantId": "10213688",
          "reference": "61",
          "price": 10,
          "kitchenName": "modi1",
          "receiptName": "modi1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6d-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bd0b3207-f0ba-4100-ab0b-11c8a983e7a0": {
        "id": "bd0b3207-f0ba-4100-ab0b-11c8a983e7a0",
          "name": "Item OLO",
          "description": "OLO description",
          "merchantId": "10213688",
          "reference": "77",
          "price": 12,
          "kitchenName": "Item OLO",
          "receiptName": "Item OLO",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6e-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "95f5bb35-e9a4-4068-cd18-1fadd56581e6": {
        "id": "95f5bb35-e9a4-4068-cd18-1fadd56581e6",
          "name": "Custom Item 3",
          "description": "",
          "merchantId": "10213688",
          "reference": "3",
          "price": 11.99,
          "kitchenName": "Custom Item 3",
          "receiptName": "Custom Item 3",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe68-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bb24fc11-03da-407b-90d5-9ee32e409235": {
        "id": "bb24fc11-03da-407b-90d5-9ee32e409235",
          "name": "Custom Item 4",
          "description": "",
          "merchantId": "10213688",
          "reference": "4",
          "price": 9.99,
          "kitchenName": "Custom Item 4",
          "receiptName": "Custom Item 4",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe69-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "ec53e8d7-e1be-4215-8d05-19fedf20a982": {
        "id": "ec53e8d7-e1be-4215-8d05-19fedf20a982",
          "name": "Item mod grps",
          "description": "",
          "merchantId": "10213688",
          "reference": "34",
          "price": 13.33,
          "kitchenName": "Item mod grps",
          "receiptName": "Item mod grps",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6a-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "8fd14cb0-6e82-4dd5-b940-bdbf3b4c1be9": {
        "id": "8fd14cb0-6e82-4dd5-b940-bdbf3b4c1be9",
          "name": "Custom Item 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "1",
          "price": 12.75,
          "kitchenName": "Custom Item 1",
          "receiptName": "Custom Item 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe6f-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "eceead13-5954-4aeb-89c8-5ea0905dd898": {
        "id": "eceead13-5954-4aeb-89c8-5ea0905dd898",
          "name": "Item",
          "description": "",
          "merchantId": "10213688",
          "reference": "5",
          "price": 12,
          "kitchenName": "Item",
          "receiptName": "Item",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe70-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "a723c231-06bd-47fb-d7ff-d93fa8541574": {
        "id": "a723c231-06bd-47fb-d7ff-d93fa8541574",
          "name": "Item A2",
          "description": "",
          "merchantId": "10213688",
          "reference": "6",
          "price": 14,
          "kitchenName": "Item A2",
          "receiptName": "Item A2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe71-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "97022875-522d-481e-df31-473c91cc04ab": {
        "id": "97022875-522d-481e-df31-473c91cc04ab",
          "name": "Cat1_item1_lennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngthyyyyyyyyyyyyyyyyyyyyy",
          "description": "",
          "merchantId": "10213688",
          "reference": "29",
          "price": 10,
          "kitchenName": "Cat1_item1_lennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngthyyyyyyyyyyyyyyyyyyyyy",
          "receiptName": "Cat1_item1_lennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngthyyyyyyyyyyyyyyyyyyyyy",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe72-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "9343af09-9761-4fe7-aca7-9fb7b553b9fa": {
        "id": "9343af09-9761-4fe7-aca7-9fb7b553b9fa",
          "name": "Item B1",
          "description": "",
          "merchantId": "10213688",
          "reference": "7",
          "price": 10,
          "kitchenName": "Item B1",
          "receiptName": "Item B1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe73-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "510d9768-0b8e-4174-d6cd-e9218eb3a88f": {
        "id": "510d9768-0b8e-4174-d6cd-e9218eb3a88f",
          "name": "Item B2",
          "description": "",
          "merchantId": "10213688",
          "reference": "8",
          "price": 12,
          "kitchenName": "Item B2",
          "receiptName": "Item B2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe74-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "832c06cc-8f54-433c-efc2-f1f598b79780": {
        "id": "832c06cc-8f54-433c-efc2-f1f598b79780",
          "name": "Cat1_item_2_leeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeennnnnnnnnnnnnnnngggggggggggggggtttttttttthhhhhhhhhhhhyyyyyyyyy",
          "description": "",
          "merchantId": "10213688",
          "reference": "30",
          "price": 5,
          "kitchenName": "Cat1_item_2_leeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeennnnnnnnnnnnnnnngggggggggggggggtttttttttthhhhhhhhhhhhyyyyyyyyy",
          "receiptName": "Cat1_item_2_leeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeennnnnnnnnnnnnnnngggggggggggggggtttttttttthhhhhhhhhhhhyyyyyyyyy",
          "taxCategoryId": "cd116173-cf00-40d7-ecce-007497873167",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe75-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "63348401-631f-4140-e9f5-58445ea9b6b0": {
        "id": "63348401-631f-4140-e9f5-58445ea9b6b0",
          "name": "Item C1",
          "description": "",
          "merchantId": "10213688",
          "reference": "9",
          "price": 14,
          "kitchenName": "Item C1",
          "receiptName": "Item C1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe76-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "7adc434d-7f61-4dd8-b20e-f8996570527b": {
        "id": "7adc434d-7f61-4dd8-b20e-f8996570527b",
          "name": "Item C2",
          "description": "",
          "merchantId": "10213688",
          "reference": "10",
          "price": 16,
          "kitchenName": "Item C2",
          "receiptName": "Item C2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe77-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "6eb43ab7-6ee1-458b-de76-dcc29c357167": {
        "id": "6eb43ab7-6ee1-458b-de76-dcc29c357167",
          "name": "Item D1",
          "description": "",
          "merchantId": "10213688",
          "reference": "11",
          "price": 8,
          "kitchenName": "Item D1",
          "receiptName": "Item D1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe78-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "d787a71b-37a9-4660-83bb-2bfbb08df11b": {
        "id": "d787a71b-37a9-4660-83bb-2bfbb08df11b",
          "name": "Item D2",
          "description": "",
          "merchantId": "10213688",
          "reference": "12",
          "price": 10,
          "kitchenName": "Item D2",
          "receiptName": "Item D2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe79-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "85c4c79d-9915-42fc-8b0e-0703776a59eb": {
        "id": "85c4c79d-9915-42fc-8b0e-0703776a59eb",
          "name": "Item E1",
          "description": "",
          "merchantId": "10213688",
          "reference": "13",
          "price": 6,
          "kitchenName": "Item E1",
          "receiptName": "Item E1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7a-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "3e50bd77-ffc9-4e16-ee11-2848c4b8b17f": {
        "id": "3e50bd77-ffc9-4e16-ee11-2848c4b8b17f",
          "name": "Item E2",
          "description": "",
          "merchantId": "10213688",
          "reference": "14",
          "price": 8,
          "kitchenName": "Item E2",
          "receiptName": "Item E2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7b-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "5cc3a127-ff50-4bfe-bd4b-442ffb13d76a": {
        "id": "5cc3a127-ff50-4bfe-bd4b-442ffb13d76a",
          "name": "Price IS1",
          "description": "",
          "merchantId": "10213688",
          "reference": "17",
          "price": 10.55,
          "kitchenName": "Price IS1",
          "receiptName": "Price IS1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7c-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "6be956ed-047e-4c66-faeb-bf7a4a2081a0": {
        "id": "6be956ed-047e-4c66-faeb-bf7a4a2081a0",
          "name": "Price IS2",
          "description": "",
          "merchantId": "10213688",
          "reference": "18",
          "price": 20.55,
          "kitchenName": "Price IS2",
          "receiptName": "Price IS2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7d-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "20e9a39c-8b1d-4152-ca41-5d3270f98276": {
        "id": "20e9a39c-8b1d-4152-ca41-5d3270f98276",
          "name": "~!@#$%^&*()[]{}_+;':\",./<>?",
          "description": "",
          "merchantId": "10213688",
          "reference": "31",
          "price": 12.12,
          "kitchenName": "~!@#$%^&*()[]{}_+;':\",./<>?",
          "receiptName": "~!@#$%^&*()[]{}_+;':\",./<>?",
          "taxCategoryId": "92118e5a-c84c-4b0d-b4cb-b8103d8f8dba",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7e-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "9d25414a-4268-49e3-8589-c131ec18f2be": {
        "id": "9d25414a-4268-49e3-8589-c131ec18f2be",
          "name": "CatSp_item_2",
          "description": "",
          "merchantId": "10213688",
          "reference": "28",
          "price": 5,
          "kitchenName": "CatSp_item_2",
          "receiptName": "CatSp_item_2",
          "taxCategoryId": "cd116173-cf00-40d7-ecce-007497873167",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe7f-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "1d927a95-cda2-44f3-d49e-50e50f6c3f9e": {
        "id": "1d927a95-cda2-44f3-d49e-50e50f6c3f9e",
          "name": "itm2_?></.,;':\"7&HH383@",
          "description": "",
          "merchantId": "10213688",
          "reference": "32",
          "price": 3.33,
          "kitchenName": "itm2_?></.,;':\"7&HH383@",
          "receiptName": "itm2_?></.,;':\"7&HH383@",
          "taxCategoryId": "ab409b88-bfb6-4102-c8e5-b8d68d3814e5",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe80-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "37f044e0-96a5-4d79-cff0-c1a613dc07c1": {
        "id": "37f044e0-96a5-4d79-cff0-c1a613dc07c1",
          "name": "It 3",
          "description": "",
          "merchantId": "10213688",
          "reference": "21",
          "price": 4.99,
          "kitchenName": "It 3",
          "receiptName": "It 3",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe81-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "4dbc5cd5-e175-4c2c-85a8-2d8eba4d42a2": {
        "id": "4dbc5cd5-e175-4c2c-85a8-2d8eba4d42a2",
          "name": "It 4",
          "description": "",
          "merchantId": "10213688",
          "reference": "22",
          "price": 4,
          "kitchenName": "It 4",
          "receiptName": "It 4",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe82-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "ba1b827a-4ba0-48ee-95fa-6b060c02147c": {
        "id": "ba1b827a-4ba0-48ee-95fa-6b060c02147c",
          "name": "CatSp_item1!@#$%",
          "description": "",
          "merchantId": "10213688",
          "reference": "27",
          "price": 10,
          "kitchenName": "CatSp_item1!@#$%",
          "receiptName": "CatSp_item1!@#$%",
          "taxCategoryId": "cd116173-cf00-40d7-ecce-007497873167",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe85-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "4cd91af4-0e85-4cb4-df0a-c5edc181066a": {
        "id": "4cd91af4-0e85-4cb4-df0a-c5edc181066a",
          "name": "Itm",
          "description": "",
          "merchantId": "10213688",
          "reference": "23",
          "price": 5.55,
          "kitchenName": "Itm",
          "receiptName": "Itm",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe83-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "f23e2bbc-8eaa-442c-b422-074621892722": {
        "id": "f23e2bbc-8eaa-442c-b422-074621892722",
          "name": "Itm mid",
          "description": "",
          "merchantId": "10213688",
          "reference": "33",
          "price": 6.77,
          "kitchenName": "Itm mid",
          "receiptName": "Itm mid",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe84-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "d0acccbe-74af-4d4f-917e-9f8f73775833": {
        "id": "d0acccbe-74af-4d4f-917e-9f8f73775833",
          "name": "Itm 3",
          "description": "",
          "merchantId": "10213688",
          "reference": "25",
          "price": 2,
          "kitchenName": "Itm 3",
          "receiptName": "Itm 3",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe86-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "43639e12-3617-4983-c484-66f0587bfca5": {
        "id": "43639e12-3617-4983-c484-66f0587bfca5",
          "name": "Itm 4",
          "description": "",
          "merchantId": "10213688",
          "reference": "26",
          "price": 10,
          "kitchenName": "Itm 4",
          "receiptName": "Itm 4",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe87-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "15f1d6ee-3a44-4eaf-ef29-fa0beb75c93a": {
        "id": "15f1d6ee-3a44-4eaf-ef29-fa0beb75c93a",
          "name": "item_item2",
          "description": "",
          "merchantId": "10213688",
          "reference": "44",
          "price": 22,
          "kitchenName": "item_item2",
          "receiptName": "item_item2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe89-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "5dc63442-052d-401a-cc85-d5faaa255eb0": {
        "id": "5dc63442-052d-401a-cc85-d5faaa255eb0",
          "name": "item_item",
          "description": "",
          "merchantId": "10213688",
          "reference": "43",
          "price": 1.11,
          "kitchenName": "item_item",
          "receiptName": "item_item",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe88-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "fab8eeb2-45c4-46ec-cae9-fa0d96b35db1": {
        "id": "fab8eeb2-45c4-46ec-cae9-fa0d96b35db1",
          "name": "Item 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "39",
          "price": 10,
          "kitchenName": "Item 1",
          "receiptName": "Item 1",
          "taxCategoryId": "1e7f143d-b5dc-4b41-ee2d-331e1d5e78c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8a-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bf4fc2ca-9d50-43e0-ed22-b419bac250b4": {
        "id": "bf4fc2ca-9d50-43e0-ed22-b419bac250b4",
          "name": "Item 23",
          "description": "",
          "merchantId": "10213688",
          "reference": "40",
          "price": 15,
          "kitchenName": "Item 23",
          "receiptName": "Item 23",
          "taxCategoryId": "1e7f143d-b5dc-4b41-ee2d-331e1d5e78c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8b-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "97bbbab5-7c1d-48a3-8a75-080f8ebd74f7": {
        "id": "97bbbab5-7c1d-48a3-8a75-080f8ebd74f7",
          "name": "POS ONLY Item 1",
          "description": "This item is available only in POS",
          "merchantId": "10213688",
          "reference": "45",
          "price": 12,
          "kitchenName": "POS ONLY Item 1",
          "receiptName": "POS ONLY Item 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8c-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "dd214ce9-fb33-47ce-ff8d-eb3f2932a3f4": {
        "id": "dd214ce9-fb33-47ce-ff8d-eb3f2932a3f4",
          "name": "C1-item",
          "description": "",
          "merchantId": "10213688",
          "reference": "46",
          "price": 20,
          "kitchenName": "C1-item",
          "receiptName": "C1-item",
          "taxCategoryId": "f3a99ace-2c16-4674-b1ee-73a9b6e0f2c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8d-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "600880c7-f726-48f7-b477-6b9a004098a2": {
        "id": "600880c7-f726-48f7-b477-6b9a004098a2",
          "name": "C",
          "description": "",
          "merchantId": "10213688",
          "reference": "47",
          "price": 5,
          "kitchenName": "C",
          "receiptName": "C",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8e-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "df247fb7-df2e-496e-f943-90ac80b1d89b": {
        "id": "df247fb7-df2e-496e-f943-90ac80b1d89b",
          "name": "Item C1_copy",
          "description": "",
          "merchantId": "10213688",
          "reference": "48",
          "price": 20,
          "kitchenName": "Item C1_copy",
          "receiptName": "Item C1_copy",
          "taxCategoryId": "f3a99ace-2c16-4674-b1ee-73a9b6e0f2c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe8f-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "d72ab53f-2247-4c21-a1c2-cbcc374b51f3": {
        "id": "d72ab53f-2247-4c21-a1c2-cbcc374b51f3",
          "name": "C1-item_copy_copy",
          "description": "",
          "merchantId": "10213688",
          "reference": "54",
          "price": 20,
          "kitchenName": "C1-item_copy_copy",
          "receiptName": "C1-item_copy_copy",
          "taxCategoryId": "f3a99ace-2c16-4674-b1ee-73a9b6e0f2c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7332fe90-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "af1042cc-75d0-4b07-8611-d73d9d501241": {
        "id": "af1042cc-75d0-4b07-8611-d73d9d501241",
          "name": "main cat item 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "52",
          "price": 6,
          "kitchenName": "main cat item 1",
          "receiptName": "main cat item 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c80-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "4bae9667-92a5-4cc3-acd3-e137049ce618": {
        "id": "4bae9667-92a5-4cc3-acd3-e137049ce618",
          "name": "DelI1",
          "description": "",
          "merchantId": "10213688",
          "reference": "53",
          "price": 11,
          "kitchenName": "DelI1",
          "receiptName": "DelI1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c81-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "d39e0142-cfdf-4998-9fa2-0d96cb2c932d": {
        "id": "d39e0142-cfdf-4998-9fa2-0d96cb2c932d",
          "name": "Mod Item 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "55",
          "price": 10,
          "kitchenName": "Mod Item 1",
          "receiptName": "Mod Item 1",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c82-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "da910ad5-94b0-4624-ad58-7e4cc506c189": {
        "id": "da910ad5-94b0-4624-ad58-7e4cc506c189",
          "name": "Mod Item 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "56",
          "price": 10,
          "kitchenName": "Mod Item 2",
          "receiptName": "Mod Item 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c83-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "fde69973-78d3-4b63-aab5-dcca9c77f0fd": {
        "id": "fde69973-78d3-4b63-aab5-dcca9c77f0fd",
          "name": "tt",
          "description": "",
          "merchantId": "10213688",
          "reference": "60",
          "price": 1,
          "kitchenName": "tt",
          "receiptName": "tt",
          "taxCategoryId": "f3a99ace-2c16-4674-b1ee-73a9b6e0f2c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c87-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "588fe77e-0f90-4bd6-e047-e52516814d9c": {
        "id": "588fe77e-0f90-4bd6-e047-e52516814d9c",
          "name": "Tax Item 1",
          "description": "",
          "merchantId": "10213688",
          "reference": "58",
          "price": 9.01,
          "kitchenName": "Tax Item 1",
          "receiptName": "Tax Item 1",
          "taxCategoryId": "000",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c84-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "d0d498f0-9170-4a91-e1ac-1a7445c71b2b": {
        "id": "d0d498f0-9170-4a91-e1ac-1a7445c71b2b",
          "name": "Tax Item 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "59",
          "price": 9.99,
          "kitchenName": "Tax Item 2",
          "receiptName": "Tax Item 2",
          "taxCategoryId": "000",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c85-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "0cdf576a-0af3-4dc9-cc75-37d7782795b6": {
        "id": "0cdf576a-0af3-4dc9-cc75-37d7782795b6",
          "name": "10",
          "description": "",
          "merchantId": "10213688",
          "reference": "62",
          "price": 10,
          "kitchenName": "10",
          "receiptName": "10",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c86-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "23acc0b6-2b23-4b52-86a2-464a80c9c6a3": {
        "id": "23acc0b6-2b23-4b52-86a2-464a80c9c6a3",
          "name": "Item change -editede",
          "description": "",
          "merchantId": "10213688",
          "reference": "63",
          "price": 100,
          "kitchenName": "Item change -edited",
          "receiptName": "Item change -editede",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c88-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "ff57419c-3896-4422-d317-294036c16881": {
        "id": "ff57419c-3896-4422-d317-294036c16881",
          "name": "Item change RM",
          "description": "",
          "merchantId": "10213688",
          "reference": "64",
          "price": 10,
          "kitchenName": "Item change RM",
          "receiptName": "Item change RM",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73334c89-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "0c77dbd5-1bb6-4189-b370-d7e3641be6f4": {
        "id": "0c77dbd5-1bb6-4189-b370-d7e3641be6f4",
          "name": "Item AAC- $10.00",
          "description": "",
          "merchantId": "10213688",
          "reference": "67",
          "price": 10,
          "kitchenName": "Item AAC- $10.00",
          "receiptName": "Item AAC- $10.00",
          "taxCategoryId": "c62f26f4-98c3-4501-e33a-de5585ebf3eb",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337392-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "211b8aae-d004-4196-c61a-a0057a27a092": {
        "id": "211b8aae-d004-4196-c61a-a0057a27a092",
          "name": "Item AAD- $0.09",
          "description": "",
          "merchantId": "10213688",
          "reference": "68",
          "price": 0.09,
          "kitchenName": "Item AAD- $0.09",
          "receiptName": "Item AAD- $0.09",
          "taxCategoryId": "f3a99ace-2c16-4674-b1ee-73a9b6e0f2c4",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337393-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "c6ee8433-8342-4463-e863-8e05e02e0fd0": {
        "id": "c6ee8433-8342-4463-e863-8e05e02e0fd0",
          "name": "Item AAA- $9.99",
          "description": "",
          "merchantId": "10213688",
          "reference": "65",
          "price": 9.99,
          "kitchenName": "Item AAA- $9.99",
          "receiptName": "Item AAA- $9.99",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337390-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "643884c7-a560-4c97-c595-b2044388dd10": {
        "id": "643884c7-a560-4c97-c595-b2044388dd10",
          "name": "Item AAB- $11.98",
          "description": "",
          "merchantId": "10213688",
          "reference": "66",
          "price": 11.98,
          "kitchenName": "Item AAB- $11.98",
          "receiptName": "Item AAB- $11.98",
          "taxCategoryId": "c62f26f4-98c3-4501-e33a-de5585ebf3eb",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337391-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "35e4408c-bdfa-48b1-bb76-40d2454081b8": {
        "id": "35e4408c-bdfa-48b1-bb76-40d2454081b8",
          "name": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "description": "",
          "merchantId": "10213688",
          "reference": "69",
          "price": 10,
          "kitchenName": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "receiptName": "\"~`!@#$%^&*()_+=-}]{[\"':;?/\"",
          "taxCategoryId": "49f8f585-98e4-4818-a4f8-da82960de212",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337394-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "50372392-aecc-4652-f3f0-11419233382c": {
        "id": "50372392-aecc-4652-f3f0-11419233382c",
          "name": "Itm 4 - $ 4.00",
          "description": "",
          "merchantId": "10213688",
          "reference": "72",
          "price": 4,
          "kitchenName": "Itm 4 - $ 4.00",
          "receiptName": "Itm 4 - $ 4.00",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337398-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "878c6a96-9c0b-41b2-92c8-057d2be16bb7": {
        "id": "878c6a96-9c0b-41b2-92c8-057d2be16bb7",
          "name": "Itm 2",
          "description": "",
          "merchantId": "10213688",
          "reference": "24",
          "price": 9.99,
          "kitchenName": "Itm 2",
          "receiptName": "Itm 2",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337395-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "87900996-a483-4219-dada-6feca832613d": {
        "id": "87900996-a483-4219-dada-6feca832613d",
          "name": "Itm 1 - $ 5.55",
          "description": "",
          "merchantId": "10213688",
          "reference": "70",
          "price": 5.55,
          "kitchenName": "Itm 1 - $ 5.55",
          "receiptName": "Itm 1 - $ 5.55",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337396-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "fced21d4-1154-4868-d698-74478a239a08": {
        "id": "fced21d4-1154-4868-d698-74478a239a08",
          "name": "Itm 3 - $ 2.00",
          "description": "",
          "merchantId": "10213688",
          "reference": "71",
          "price": 2,
          "kitchenName": "Itm 3 - $ 2.00",
          "receiptName": "Itm 3 - $ 2.00",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337397-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "bd2cb881-a89a-48d8-c7de-006d0d2a5a0f": {
        "id": "bd2cb881-a89a-48d8-c7de-006d0d2a5a0f",
          "name": "Session A",
          "description": "",
          "merchantId": "10213688",
          "reference": "73",
          "price": 5,
          "kitchenName": "Session A",
          "receiptName": "Session A",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "73337399-1ad8-11e8-99c4-a9d1f80ee75d"
      },
      "0075be7a-f001-4570-8381-a01c23cfc1d1": {
        "id": "0075be7a-f001-4570-8381-a01c23cfc1d1",
          "name": "Session B",
          "description": "",
          "merchantId": "10213688",
          "reference": "74",
          "price": 5,
          "kitchenName": "Session B",
          "receiptName": "Session B",
          "taxCategoryId": "001",
          "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
          "isAuxiliary": false,
          "attributeSet": "7333739a-1ad8-11e8-99c4-a9d1f80ee75d"
      }
    },
    "restaurantInfo": {
      "id": "c0020-10213688",
        "name": "OLO_Auto_Merchant1",
        "crmId": "0011800000WmjVwAAJ",
        "location": {
        "lat": "37.486863100000000000000000000",
          "lng": "-122.226582900000000000000000000"
      },
      "address": {
        "address1": "2010 Broadway Street",
          "state": "CA",
          "city": "Redwood City",
          "zipCode": "94603",
          "country": "United States",
          "countryCode": "US"
      },
      "phone": {
        "countryCode": "1",
          "areaCode": "222",
          "exchangeCode": "213",
          "subscriberNumber": "2199"
      },
      "pickup": true,
        "deliver": true,
        "deliveryFee": 3,
        "deliveryTime": 3,
        "dineIn": true,
        "isOnlineOrderAvailable": true,
        "isCakeOloAvailable": true,
        "minimumOrderValue": 5,
        "orderPrepTime": 5,
        "minDeliveryOrderAmount": 10,
        "queueConsumers": 1,
        "merchantStatus": "LIVE",
        "merchantType": "CINCO",
        "timeZone": "America/Los_Angeles",
        "emailAddress": "olo_auto_merchant1@leap.com",
        "languageCode": "en",
        "countryCode": "US",
        "accountId": "10213688",
        "pin": "22492",
        "active": true,
        "isPaypalAvailable": false,
        "sessions": [
        "5bda0d35-f1b5-438d-a599-436436f6d2ce",
        "88e528cc-89a1-48bd-e920-509c217e84e2",
        "db004ad5-50f7-475a-bef4-338a3de1e181",
        "94c44d77-5beb-431e-ecbe-7da640597817"
      ],
        "cuisines": null,
        "images": null
    },
    "restaurantError": null,
      "selectedSession": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
      "sessions": {
      "5bda0d35-f1b5-438d-a599-436436f6d2ce": {
        "id": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
          "name": "All Day -e",
          "slots": [
          {
            "day": "3",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "5",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "2",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "4",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "7",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "1",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "6",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          }
        ],
          "isAvailable": true,
          "isPending": false
      },
      "88e528cc-89a1-48bd-e920-509c217e84e2": {
        "id": "88e528cc-89a1-48bd-e920-509c217e84e2",
          "name": "Session B",
          "slots": [
          {
            "day": "2",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "1",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "6",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "5",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "4",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "3",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          },
          {
            "day": "7",
            "start": "00:00:00",
            "end": "23:59:59",
            "isavailable": true
          }
        ],
          "isAvailable": true,
          "isPending": false
      }
    },
    "sessionCategories": [
      "dcdf638c-a5ac-4613-a16c-77e76ed876bd",
      "134afadb-ec85-41ef-ef6f-35a904ca6eda",
      "72914dd8-15ef-40a7-dcfa-1494d81b9861",
      "14e0aee2-0c1f-48f5-e887-d14076b90d6e",
      "09b1fd4b-c2de-4375-eb48-f6c0c6c044c4",
      "09281b53-7a6b-4ac9-901e-d0fc64af92bd",
      "899285b9-b2cf-40c7-e89d-327f62b87bcf",
      "6e9020fb-7980-46b5-b44e-c15f20721f21",
      "d90d94ad-63d7-4b59-a1d7-2e46c4f6e590",
      "9d14ebce-fbe6-4702-93d7-61b01927b55f",
      "46e0a152-bfc7-4492-9625-973974db510f",
      "4c1ccdd0-5cd6-4f77-fa64-dbce0a351d96",
      "f46f6de4-ab87-416b-c2fe-9cbc9b0f3c74",
      "741142f9-5b90-4a70-c4af-3e77d5b15e5d",
      "14845038-d906-488d-d6bf-6bf97340c4d3",
      "02b2a625-3dcd-4734-d43f-f0ec8300b954",
      "c8941083-6bf6-4ed3-922a-93cc33286a69",
      "dfe6f9e4-ff3a-49dd-add2-fb567464c8d3",
      "c21cd284-afb5-485b-9f44-0d5bd3961cee",
      "fdfbb87f-b72f-4879-f964-11759c3630e7",
      "ad51f375-8c5c-407d-ab0a-d7d7a0c9422e",
      "82f4cd52-2b69-4897-c0e3-9c49a6f7d360",
      "90527040-798c-4d96-ac06-1b7b34aac3a8",
      "ad5d60d0-8c0a-4f2c-f892-fa30ef213fb5",
      "79f5ccb1-66c2-4b58-8504-a517dea35d63",
      "1e0e2be3-8f94-451f-d68b-cb6abb6c5c8c",
      "fd6da33b-9655-44b9-f666-4aee1faddc8b",
      "abc89ef7-18b7-4f37-d51c-3241e715d730",
      "c5ad4d9e-e1e4-4495-cd4d-dfdaa15a6aa5",
      "ec064b76-8ede-4097-e9b6-f851b7041660",
      "6a6f84eb-6ae5-4392-de2a-a94615de9bd1",
      "5bd46817-b990-4c70-e1fa-70ee27b42448",
      "0aea26cc-69a3-4f10-cdcf-919c8841e359",
      "21936f2e-10ab-4dec-f4c8-70945d037805",
      "d7f3f04f-8c87-45ee-991c-99417345e822",
      "06d3513c-937e-40a5-b375-20dbaab8cfac"
    ],
      "catalogIsLoading": false,
      "disableCheckout": false
  },
  cart:{
    "items": [
      {
        "sessionId": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
        "lineItemId": "f8f02e90-1ad0-11e8-8bf1-89db8456112c",
        "menuId": "0755e518-23c7-4904-d830-59e4629ffbd2",
        "productId": "0755e518-23c7-4904-d830-59e4629ffbd2",
        "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
        "quantity": 1,
        "name": "Price Check 2",
        "isSubCategory": false,
        "totalCost": 20.55,
        "attributeGroups": {
          "1": ""
        },
        "instructions": [],
        "specialInstructions": "",
        "invalidCartItems": []
      }
    ],
      "empty": false,
      "totalCost": 20.55,
      "totalItems": 1,
      "orderType": "PICKUP",
      "tax": 10.28,
      "tip": null,
      "tipPercentage": null,
      "discountCode": null,
      "discountType": null,
      "discount": 0,
      "grandTotal": 30.83,
      "orderTime": "2018-02-26T00:50:00+05:30",
      "deliveryAddress": null,
      "deliveryFee": 3,
      "totalIsLoading": false,
      "orderIsSending": true,
      "readyByOptions": [
      "2018-02-26T00:50:00+05:30",
      "2018-02-26T00:55:00+05:30",
      "2018-02-26T01:00:00+05:30",
      "2018-02-26T01:05:00+05:30",
      "2018-02-26T01:10:00+05:30",
      "2018-02-26T01:15:00+05:30",
      "2018-02-26T01:20:00+05:30",
      "2018-02-26T01:25:00+05:30",
      "2018-02-26T01:30:00+05:30",
      "2018-02-26T01:35:00+05:30",
      "2018-02-26T01:40:00+05:30",
      "2018-02-26T01:45:00+05:30",
      "2018-02-26T01:50:00+05:30",
      "2018-02-26T01:55:00+05:30",
      "2018-02-26T02:00:00+05:30",
      "2018-02-26T02:05:00+05:30",
      "2018-02-26T02:10:00+05:30",
      "2018-02-26T02:15:00+05:30",
      "2018-02-26T02:20:00+05:30",
      "2018-02-26T02:25:00+05:30",
      "2018-02-26T02:30:00+05:30",
      "2018-02-26T02:35:00+05:30",
      "2018-02-26T02:40:00+05:30",
      "2018-02-26T02:45:00+05:30",
      "2018-02-26T02:50:00+05:30",
      "2018-02-26T02:55:00+05:30",
      "2018-02-26T03:00:00+05:30",
      "2018-02-26T03:05:00+05:30",
      "2018-02-26T03:10:00+05:30",
      "2018-02-26T03:15:00+05:30",
      "2018-02-26T03:20:00+05:30",
      "2018-02-26T03:25:00+05:30",
      "2018-02-26T03:30:00+05:30",
      "2018-02-26T03:35:00+05:30",
      "2018-02-26T03:40:00+05:30",
      "2018-02-26T03:45:00+05:30",
      "2018-02-26T03:50:00+05:30",
      "2018-02-26T03:55:00+05:30",
      "2018-02-26T04:00:00+05:30",
      "2018-02-26T04:05:00+05:30",
      "2018-02-26T04:10:00+05:30",
      "2018-02-26T04:15:00+05:30",
      "2018-02-26T04:20:00+05:30",
      "2018-02-26T04:25:00+05:30",
      "2018-02-26T04:30:00+05:30",
      "2018-02-26T04:35:00+05:30",
      "2018-02-26T04:40:00+05:30",
      "2018-02-26T04:45:00+05:30",
      "2018-02-26T04:50:00+05:30",
      "2018-02-26T04:55:00+05:30",
      "2018-02-26T05:00:00+05:30",
      "2018-02-26T05:05:00+05:30",
      "2018-02-26T05:10:00+05:30",
      "2018-02-26T05:15:00+05:30",
      "2018-02-26T05:20:00+05:30",
      "2018-02-26T05:25:00+05:30",
      "2018-02-26T05:30:00+05:30",
      "2018-02-26T05:35:00+05:30",
      "2018-02-26T05:40:00+05:30",
      "2018-02-26T05:45:00+05:30",
      "2018-02-26T05:50:00+05:30",
      "2018-02-26T05:55:00+05:30",
      "2018-02-26T06:00:00+05:30",
      "2018-02-26T06:05:00+05:30",
      "2018-02-26T06:10:00+05:30",
      "2018-02-26T06:15:00+05:30",
      "2018-02-26T06:20:00+05:30",
      "2018-02-26T06:25:00+05:30",
      "2018-02-26T06:30:00+05:30",
      "2018-02-26T06:35:00+05:30",
      "2018-02-26T06:40:00+05:30",
      "2018-02-26T06:45:00+05:30",
      "2018-02-26T06:50:00+05:30",
      "2018-02-26T06:55:00+05:30",
      "2018-02-26T07:00:00+05:30",
      "2018-02-26T07:05:00+05:30",
      "2018-02-26T07:10:00+05:30",
      "2018-02-26T07:15:00+05:30",
      "2018-02-26T07:20:00+05:30",
      "2018-02-26T07:25:00+05:30",
      "2018-02-26T07:30:00+05:30",
      "2018-02-26T07:35:00+05:30",
      "2018-02-26T07:40:00+05:30",
      "2018-02-26T07:45:00+05:30",
      "2018-02-26T07:50:00+05:30",
      "2018-02-26T07:55:00+05:30",
      "2018-02-26T08:00:00+05:30",
      "2018-02-26T08:05:00+05:30",
      "2018-02-26T08:10:00+05:30",
      "2018-02-26T08:15:00+05:30",
      "2018-02-26T08:20:00+05:30",
      "2018-02-26T08:25:00+05:30",
      "2018-02-26T08:30:00+05:30",
      "2018-02-26T08:35:00+05:30",
      "2018-02-26T08:40:00+05:30",
      "2018-02-26T08:45:00+05:30",
      "2018-02-26T08:50:00+05:30",
      "2018-02-26T08:55:00+05:30",
      "2018-02-26T09:00:00+05:30",
      "2018-02-26T09:05:00+05:30",
      "2018-02-26T09:10:00+05:30",
      "2018-02-26T09:15:00+05:30",
      "2018-02-26T09:20:00+05:30",
      "2018-02-26T09:25:00+05:30",
      "2018-02-26T09:30:00+05:30",
      "2018-02-26T09:35:00+05:30",
      "2018-02-26T09:40:00+05:30",
      "2018-02-26T09:45:00+05:30",
      "2018-02-26T09:50:00+05:30",
      "2018-02-26T09:55:00+05:30",
      "2018-02-26T10:00:00+05:30",
      "2018-02-26T10:05:00+05:30",
      "2018-02-26T10:10:00+05:30",
      "2018-02-26T10:15:00+05:30",
      "2018-02-26T10:20:00+05:30",
      "2018-02-26T10:25:00+05:30",
      "2018-02-26T10:30:00+05:30",
      "2018-02-26T10:35:00+05:30",
      "2018-02-26T10:40:00+05:30",
      "2018-02-26T10:45:00+05:30",
      "2018-02-26T10:50:00+05:30",
      "2018-02-26T10:55:00+05:30",
      "2018-02-26T11:00:00+05:30",
      "2018-02-26T11:05:00+05:30",
      "2018-02-26T11:10:00+05:30",
      "2018-02-26T11:15:00+05:30",
      "2018-02-26T11:20:00+05:30",
      "2018-02-26T11:25:00+05:30",
      "2018-02-26T11:30:00+05:30",
      "2018-02-26T11:35:00+05:30",
      "2018-02-26T11:40:00+05:30",
      "2018-02-26T11:45:00+05:30",
      "2018-02-26T11:50:00+05:30",
      "2018-02-26T11:55:00+05:30",
      "2018-02-26T12:00:00+05:30",
      "2018-02-26T12:05:00+05:30",
      "2018-02-26T12:10:00+05:30",
      "2018-02-26T12:15:00+05:30",
      "2018-02-26T12:20:00+05:30",
      "2018-02-26T12:25:00+05:30",
      "2018-02-26T12:30:00+05:30",
      "2018-02-26T12:35:00+05:30",
      "2018-02-26T12:40:00+05:30",
      "2018-02-26T12:45:00+05:30",
      "2018-02-26T12:50:00+05:30",
      "2018-02-26T12:55:00+05:30",
      "2018-02-26T13:00:00+05:30",
      "2018-02-26T13:05:00+05:30",
      "2018-02-26T13:10:00+05:30",
      "2018-02-26T13:15:00+05:30",
      "2018-02-26T13:20:00+05:30",
      "2018-02-26T13:25:00+05:30",
      "2018-02-26T13:30:00+05:30",
      "2018-02-26T13:35:00+05:30",
      "2018-02-26T13:40:00+05:30",
      "2018-02-26T13:45:00+05:30",
      "2018-02-26T13:50:00+05:30",
      "2018-02-26T13:55:00+05:30",
      "2018-02-26T14:00:00+05:30",
      "2018-02-26T14:05:00+05:30",
      "2018-02-26T14:10:00+05:30",
      "2018-02-26T14:15:00+05:30",
      "2018-02-26T14:20:00+05:30",
      "2018-02-26T14:25:00+05:30",
      "2018-02-26T14:30:00+05:30",
      "2018-02-26T14:35:00+05:30",
      "2018-02-26T14:40:00+05:30",
      "2018-02-26T14:45:00+05:30",
      "2018-02-26T14:50:00+05:30",
      "2018-02-26T14:55:00+05:30",
      "2018-02-26T15:00:00+05:30",
      "2018-02-26T15:05:00+05:30",
      "2018-02-26T15:10:00+05:30",
      "2018-02-26T15:15:00+05:30",
      "2018-02-26T15:20:00+05:30",
      "2018-02-26T15:25:00+05:30",
      "2018-02-26T15:30:00+05:30",
      "2018-02-26T15:35:00+05:30",
      "2018-02-26T15:40:00+05:30",
      "2018-02-26T15:45:00+05:30",
      "2018-02-26T15:50:00+05:30",
      "2018-02-26T15:55:00+05:30",
      "2018-02-26T16:00:00+05:30",
      "2018-02-26T16:05:00+05:30",
      "2018-02-26T16:10:00+05:30",
      "2018-02-26T16:15:00+05:30",
      "2018-02-26T16:20:00+05:30",
      "2018-02-26T16:25:00+05:30",
      "2018-02-26T16:30:00+05:30",
      "2018-02-26T16:35:00+05:30",
      "2018-02-26T16:40:00+05:30",
      "2018-02-26T16:45:00+05:30",
      "2018-02-26T16:50:00+05:30",
      "2018-02-26T16:55:00+05:30",
      "2018-02-26T17:00:00+05:30",
      "2018-02-26T17:05:00+05:30",
      "2018-02-26T17:10:00+05:30",
      "2018-02-26T17:15:00+05:30",
      "2018-02-26T17:20:00+05:30",
      "2018-02-26T17:25:00+05:30",
      "2018-02-26T17:30:00+05:30",
      "2018-02-26T17:35:00+05:30",
      "2018-02-26T17:40:00+05:30",
      "2018-02-26T17:45:00+05:30",
      "2018-02-26T17:50:00+05:30",
      "2018-02-26T17:55:00+05:30",
      "2018-02-26T18:00:00+05:30",
      "2018-02-26T18:05:00+05:30",
      "2018-02-26T18:10:00+05:30",
      "2018-02-26T18:15:00+05:30",
      "2018-02-26T18:20:00+05:30",
      "2018-02-26T18:25:00+05:30",
      "2018-02-26T18:30:00+05:30",
      "2018-02-26T18:35:00+05:30",
      "2018-02-26T18:40:00+05:30",
      "2018-02-26T18:45:00+05:30",
      "2018-02-26T18:50:00+05:30",
      "2018-02-26T18:55:00+05:30",
      "2018-02-26T19:00:00+05:30",
      "2018-02-26T19:05:00+05:30",
      "2018-02-26T19:10:00+05:30",
      "2018-02-26T19:15:00+05:30",
      "2018-02-26T19:20:00+05:30",
      "2018-02-26T19:25:00+05:30",
      "2018-02-26T19:30:00+05:30",
      "2018-02-26T19:35:00+05:30",
      "2018-02-26T19:40:00+05:30",
      "2018-02-26T19:45:00+05:30",
      "2018-02-26T19:50:00+05:30",
      "2018-02-26T19:55:00+05:30",
      "2018-02-26T20:00:00+05:30",
      "2018-02-26T20:05:00+05:30",
      "2018-02-26T20:10:00+05:30",
      "2018-02-26T20:15:00+05:30",
      "2018-02-26T20:20:00+05:30",
      "2018-02-26T20:25:00+05:30",
      "2018-02-26T20:30:00+05:30",
      "2018-02-26T20:35:00+05:30",
      "2018-02-26T20:40:00+05:30",
      "2018-02-26T20:45:00+05:30",
      "2018-02-26T20:50:00+05:30",
      "2018-02-26T20:55:00+05:30",
      "2018-02-26T21:00:00+05:30",
      "2018-02-26T21:05:00+05:30",
      "2018-02-26T21:10:00+05:30",
      "2018-02-26T21:15:00+05:30",
      "2018-02-26T21:20:00+05:30",
      "2018-02-26T21:25:00+05:30",
      "2018-02-26T21:30:00+05:30",
      "2018-02-26T21:35:00+05:30",
      "2018-02-26T21:40:00+05:30",
      "2018-02-26T21:45:00+05:30",
      "2018-02-26T21:50:00+05:30",
      "2018-02-26T21:55:00+05:30",
      "2018-02-26T22:00:00+05:30",
      "2018-02-26T22:05:00+05:30",
      "2018-02-26T22:10:00+05:30",
      "2018-02-26T22:15:00+05:30",
      "2018-02-26T22:20:00+05:30",
      "2018-02-26T22:25:00+05:30",
      "2018-02-26T22:30:00+05:30",
      "2018-02-26T22:35:00+05:30",
      "2018-02-26T22:40:00+05:30",
      "2018-02-26T22:45:00+05:30",
      "2018-02-26T22:50:00+05:30",
      "2018-02-26T22:55:00+05:30",
      "2018-02-26T23:00:00+05:30",
      "2018-02-26T23:05:00+05:30",
      "2018-02-26T23:10:00+05:30",
      "2018-02-26T23:15:00+05:30",
      "2018-02-26T23:20:00+05:30",
      "2018-02-26T23:25:00+05:30",
      "2018-02-26T23:30:00+05:30",
      "2018-02-26T23:35:00+05:30",
      "2018-02-26T23:40:00+05:30",
      "2018-02-26T23:45:00+05:30",
      "2018-02-26T23:50:00+05:30",
      "2018-02-26T23:55:00+05:30"
    ],
      "invalidCartItems": [],
      "isInvalidItemsModalShown": false,
      "erroneousSession": null,
      "sessionBasedInvalidItemsInCart": false,
      "lastAddedItemSession": "5bda0d35-f1b5-438d-a599-436436f6d2ce",
      "persistExpiresAt": "2018-02-26T09:42:26.682Z"
  },
  user:{
    "currentUser": null,
      "loggedInUser": null,
      "isLoginModalShown": false,
      "isForgotPasswordModalShown": false,
      "isResetPasswordModalShown": false,
      "isLogoutModalShown": false,
      "isFlowWaiting": false,
      "isPasswordResetSuccess": false,
      "isPasswordResetEmailSuccess": false,
      "passwordResetUser": null,
      "hasAccount": false,
      "isGuest": false,
      "loginModalErrorCode": null,
      "loginSectionErrorCode": null,
      "resetPasswordErrorCode": null,
      "resetPasswordEmailErrorCode": null,
      "isSignUpCompleted": false,
      "isSignUpRequested": false,
      "signUpErrorCode": null,
      "cardRemoveConfirmationModalShown": false,
      "paymentInfoEnteringFormShown": false,
      "hasCards": false,
      "selectedCardIdentifier": null,
      "isPwdResetTokenError": false,
      "pwdResetTokenErrorMessage": null,
      "selectedState": null
  }
}
