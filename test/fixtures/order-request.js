export default {
  lineItems: {
    lineItem: [
      {
        productId: 'cd310301-5db0-4d2e-b8e1-1934900de912',
        qty: 2,
      }
    ]
  },
  orderType: 'DELIVERY',
  tax: 0.05,
  discount: 0,
  tipAmount: 0,
  subTotal: 0.1,
  discountCode: 'MYDISC',
  deliveryFee: 0,
  orderReadyTime: 1332737705353,
  customer: {
    id: '80f1cea8-6456-4dc2-9c11-f4f866a10782',
    firstName: 'ABC',
    lastName: 'Def',
    email: 'customer@leapset.com',
    contacts: {
      contact: [
        {
          address: {
            id: null,
            state: 'CA',
            address1: '1110-F',
            address2: 'Franklin Street Apartments',
            city: 'Redwood City',
            zipCode: '94063'
          },
          phone: {
            id: null,
            areaCode: '650',
            exchangeCode: '450',
            subscriberNumber: '7211',
            extension: '123'
          },
        }
      ]
    },
    creditCards: {
      creditCard: [
        {
          id: null,
          type: 'MASTER',
          number: '411111111111111111',
          nameOnCard: 'Homer Simpson',
          secCode: '447',
          expire: '0613'
        }
      ]
    },
    firstOrder: false,
    eReceiptNotification: false,
  },
};
