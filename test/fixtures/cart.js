export default {
  orderType: 'PICKUP',
  items: [
    {
      id: '6e6d4aa1-1187-416f-ecaf-82da8e476280',
      quantity: 50,
      name: 'Lots of Breakfast Platters with a Really Long Name',
      price: 9.95,
    },
    {
      id: 'a1c738ca-0160-4419-e64d-dc196f81db85',
      quantity: 2,
      name: 'Breakfast Taco',
      price: 4.95,
    },
  ],
  totalCost: 14.9,
  tax: 0.63,
  discountCode: 'AVBCDF',
  discountType: 'PERCENT',
  discountAmount: 5,
  discount: 1.41,
  grandTotal: 15.53,
};
