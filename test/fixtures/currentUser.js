export default {
  "id": "19b5a9b8-873d-4ebb-a57f-9dc3bf66a962",
  "idmUserId": "735611b4-9412-4c3b-8999-69165caeb493",
  "firstName": "MyFirstName",
  "lastName": "MyLastName",
  "email": "testuser@gmail.com",
  "sendMail": false,
  "contacts": [
    {
      "contactId": "ffbfaad8-23f5-4456-bef8-b80de108298e",
      "phoneId": "aa11c4ed-6621-4cff-8ad1-8044aba4ed9f",
      "phone": "3786231587"
    }
  ],
  "paymentTypes": {
    "credit_card": [
      {
        "paymentMethod": "credit_card",
        "id": "004935d0-95b7-41b1-a0b7-1dc8832c9ce8",
        "number": "XXXXXXXXXXXX1111"
      }
    ]
  },
  "eReceiptNotification": true,
  "rewardsNotification": true,
  "dateOfBirthFormat": "yyyy-MM-dd",
  "cardDropDownOption": {
    "value": "004935d0-95b7-41b1-a0b7-1dc8832c9ce8",
    "label": "XXXXXXXXXXXX1111",
    "card": {
      "paymentMethod": "credit_card",
      "id": "004935d0-95b7-41b1-a0b7-1dc8832c9ce8",
      "number": "XXXXXXXXXXXX1111"
    },
    "isSelected": true
  },
  "textMessageConfirmation": false
}
