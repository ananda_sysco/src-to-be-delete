/**
 * Created by Salinda on 2/19/18.
 * Can pass the account ID as a run time parameter like below
 * npm run server_test "10213688"
 */
const argv = require('minimist')(process.argv.slice(2));
const authRestUtil = require('../../../src/server/api/util/restUtil/authRestUtil');
const consumerApiHandler = require('../../../src/server/api/handler/consumerApiHandler');
const config = require('../../../src/server/config').getConfigs();
const moment = require('moment');
const httpMocks = require('node-mocks-http');


const getAccountIdForTest = () => {
  /*
  npm run server_test "10213688"
   */
  const merchantId = argv[0] || '10217493';
  return merchantId;
};

const getMerchantIdForTest = () => {
  /*
  npm run server_test "10213688"
   */
  const merchantId = 'c0010-10217493';
  return merchantId;
};

const getBearerToken = async () => {
  const proxyUserName = config.proxyUser;
  const proxyUserPassword = config.proxyUserPassword;
  const loginResponse = await authRestUtil.login(proxyUserName, proxyUserPassword);
  const jwtData = loginResponse.getData();
  const bearerTokenResponse = await authRestUtil.getUserSpecificBearerToken(jwtData.id_token);
  return bearerTokenResponse.data.access_token;
};

const getSessionIdForTest = async (browserSession, merchantId) => {
  const req = await httpMocks.createRequest({
    url: `http://localhost:3003/api-v3/getRestaurantInfo?account-id=${merchantId}`,
    session: browserSession
  });
  const restaurantInfo = await consumerApiHandler.handleGetRestaurantInfo(req);
  return restaurantInfo.data.sessions.session[0].id;
};

const getBrowserSessionForTest = async () => {
  const session = {};
  const bearerToken = await getBearerToken();
  session['token'] = bearerToken;
  session['tokenExpiration'] = moment().add(bearerToken.expires_in, 'seconds');
  return session;
};


module.exports = {
  getSessionIdForTest,
  getAccountIdForTest,
  getBrowserSessionForTest,
  getMerchantIdForTest
};
