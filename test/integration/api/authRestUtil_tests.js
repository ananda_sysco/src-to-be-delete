const chai = require('chai');
const authRestUtil = require('../../../src/server/api/util/restUtil/authRestUtil');
const expect = chai.expect;
const moment = require('moment');
const API_CONSTANTS = require('../../../src/server/api/constant/httpConstants');
const intergrationTestHelper = require('../helpers/intergrationTestHelper');

let jwtToken = null;
let browserSession = null;

describe('Test auth rest util', function () {
  before(async function () {
    this.timeout(10000);
    //Use environment specific user for test
    browserSession = await intergrationTestHelper.getBrowserSessionForTest();
    authRestUtil.login('salinda@leap.com', '12345678').then((response) => {
      jwtToken = response.data.id_token;

    }).catch(err => {
    });
  });
  describe('Test getUserSpecificBearerToken', function () {
    it('should return 401 for getUserSpecificBearerToken', function (done) {
      this.timeout(10000);
      authRestUtil.getUserSpecificBearerToken('9009').then((response) => {
        done();
      }).catch(err => {
        expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_UNAUTHORIZED);
        done();
      });
    });

    it('should return Ok getUserSpecificBearerToken', function (done) {
      this.timeout(10000);
      authRestUtil.getUserSpecificBearerToken(jwtToken).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        done();
      }).catch(err => {
        done();
      });
    });
  });

  describe('Test login', function () {
    it('should return OK for login', function (done) {
      this.timeout(10000);
      authRestUtil.login('salinda@leap.com', '12345678').then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        done();
      }).catch(err => {
        done();
      });
    });

    it('should return 401 for login', function (done) {
      this.timeout(10000);
      authRestUtil.login('salinda@leap.com', '123456789').then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_UNAUTHORIZED);
        done();
      }).catch(err => {
        done();
      });
    });
  });

  describe('Test getUserByToken', function () {
    it('should return 401 for getUserByToken', function (done) {
      this.timeout(10000);
      authRestUtil.getUserByToken('invalid token', '12345678').then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_UNAUTHORIZED);
        done();
      }).catch(err => {
        done();
      });
    });
  });

});





