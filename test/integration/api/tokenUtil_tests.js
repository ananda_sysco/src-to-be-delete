const chai = require('chai');
const tokenUtil = require('../../../src/server/api/util/tokenUtil');
const expect = chai.expect;
const moment = require('moment');
const config = require('../../../src/server/config').getConfigs();

describe('Test isTokenExpired', function () {
  it('should return true for null token', function (done) {
    expect(tokenUtil.isTokenExpired(null, moment())).to.eql(true);
    done();
  });

  it('should return true for previous expiration', function (done) {
    const token = "345345435";
    const result = tokenUtil.isTokenExpired(token, moment().subtract(1, 'day'));
    expect(result).to.eql(true);
    done();
  });

  it('should return false for future expiration', function (done) {
    const token = "345345435";
    const result = tokenUtil.isTokenExpired(token, moment().add(1, 'day'));
    expect(result).to.eql(false);
    done();
  });
});

describe('Test setBearerTokenToSession', function () {
  it('should return updated session', function (done) {
    const token = "345345435";
    const time = moment().add(1, 'day');
    let tokenData = {};
    tokenData.access_token = "1234";
    tokenData.expires_in = 1000;

    let session = {};
    tokenUtil.setBearerTokenToSession(session, tokenData);
    expect(session.token).to.eql("1234");
    expect(session.tokenExpiration).not.to.eql(null);
    done();
  });

});

describe('Test setJwtTokenToSession', function () {
  it('should return updated session', function (done) {
    const token = "345345435";
    const time = moment().add(1, 'day');
    let tokenData = {};
    tokenData.id_token = "1234";
    tokenData.expires_in = 1000;

    let session = {};
    tokenUtil.setJwtTokenToSession(session, tokenData);
    expect(session.jwtToken).to.eql("1234");
    expect(session.jwtTokenExpiration).not.to.eql(null);
    done();
  });

});

describe('Test requestAndSetJwtAndBearerTokensToSession', function () {
  it('should return updated session', function (done) {
    this.timeout(10000);
    let session = {};

    tokenUtil.requestAndSetJwtAndBearerTokensToSession(session, config.proxyUser, config.proxyUserPassword).then((response) => {
      expect(session.jwtToken).not.to.eql(null);
      expect(session.jwtTokenExpiration).not.to.eql(null);
      done();
    }).catch(err => {
      done();
    });
  });

});

describe('Test getBearerTokenFromSession', function () {
  it('should return updated session', function (done) {
    let session = {};
    session.token = "345345435";
    tokenUtil.getBearerTokenFromSession(session);
    expect(session.token).to.eql("345345435");
    done();
  });

});




