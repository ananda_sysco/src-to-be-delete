const chai = require('chai');
const httpMocks = require('node-mocks-http');
const intergrationTestHelper = require('../helpers/intergrationTestHelper');
const menuHandler = require('../../../src/server/api/handler/menuHandler');
const API_CONSTANTS = require('../../../src/server/api/constant/httpConstants');

const expect = chai.expect;

let accountId = null;
let sessionId = null;
let browserSession = null;

describe('Menu Api Tests', function () {
  before(async function () {
    this.timeout(10000);
    accountId = intergrationTestHelper.getAccountIdForTest();
    browserSession = await intergrationTestHelper.getBrowserSessionForTest();
    sessionId = await intergrationTestHelper.getSessionIdForTest(browserSession, accountId);
  });

  describe('GET /getCatalog', function () {

    it('should get catalog', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/menu/getCatalog?account-id=${accountId}&session-id=${sessionId}`,
        session: browserSession
      });

      menuHandler.handleGetCatalog(req).then((response) => {
        const resultObject = response.getData();
        expect(response.getStatusCode()).to.eql(200);
        expect(resultObject.categories.category[0]).to.include.all.keys('id', 'name', 'description', 'sessionId', 'subCategories');

        done();
      }).catch(err => {
        console.log(err);
        done('TEST FAILED');
      });
    });


    it('should return HTTP_NOT_FOUND for the input data that is not valid', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/menu/getCatalog?account-id=123&session-id=123`,
        session: browserSession
      });

      menuHandler.handleGetCatalog(req).then((response) => {
        console.log(response);
        done('TEST FAILED');
      }).catch(err => {
        expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR);
        done();
      });
    });
  });

  // Session endpoint
  describe('GET /getSessions', function () {
    it('should get sessions', function (done) {
      this.timeout(10000);
      menuHandler.getSessions(browserSession, accountId).then((response) => {
        //expect(response).to.have.lengthOf(4);
        expect(response[0]).to.include.all.keys('id', 'name', 'sessionhours');

        done();
      }).catch(err => {
        console.log(err);
        done('TEST FAILED');
      });
    });
  });


});
