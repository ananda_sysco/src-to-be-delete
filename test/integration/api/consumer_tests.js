const chai = require('chai');
const httpMocks = require('node-mocks-http');
const moment = require('moment');
const API_CONSTANTS = require('../../../src/server/api/constant/httpConstants');

const consumerApiHandler = require('../../../src/server/api/handler/consumerApiHandler');
const menuApiHandler = require('../../../src/server/api/handler/menuHandler');
const intergrationTestHelper = require('../helpers/intergrationTestHelper');

// const args = require('../../argument_heper');
/*TODO Most of these tests are running with hard coded inputs. This is wrong and test can be failed in different scenarios
* such as environment switches or merchant changes, due to the time limitation and test to be up and run for the moment
 * this approach was introduced. The ultimate solution should be
* a combination of real integration tests with parameterized inputs and unit tests with mock API responses*/

global.expect = chai.expect;


let accountId = null;
let merchantId = null;
let browserSession = null;
const errorMerchantId = 'invalid';
const errorMerchantId2 = 'invalid';
let catalog = null;
let sessionId = null;
let product = null;

describe('Consumer Api Tests', function () {
  before(async function () {
    this.timeout(10000);
    accountId = intergrationTestHelper.getAccountIdForTest();
    merchantId = intergrationTestHelper.getMerchantIdForTest();
    browserSession = await intergrationTestHelper.getBrowserSessionForTest();
    sessionId = await intergrationTestHelper.getSessionIdForTest(browserSession, accountId);

    const catalogRequest = httpMocks.createRequest({
      url: `http://localhost:3003/api-v3/getCatalog?account-id=${accountId}&session-id=${sessionId}`,
      session: browserSession
    });
    catalog = await menuApiHandler.handleGetCatalog(catalogRequest);
    product = catalog.data.categories.category[0].products.product[5];
  });

  describe('GET /getRestaurantInfo', function () {

    it('should get restaurant info', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/getRestaurantInfo?account-id=${accountId}`,
        session: browserSession
      });

      consumerApiHandler.handleGetRestaurantInfo(req).then((response) => {
        const resultObject = response.getData();
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        expect(resultObject).to.include.all.keys('id', 'isCakeOloAvailable', 'sessions', 'merchantType', 'accountId');
        done();
      }).catch(err => {
        console.log(err);
        done('TEST FAILED');
      });
    });

    it('should fail with operator api merchant not found', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/getRestaurantInfo?account-id=${errorMerchantId}`,
        session: browserSession
      });

      consumerApiHandler.handleGetRestaurantInfo(req).then((response) => {
        console.log(response);
        done('TEST FAILED');
      }).catch(err => {
        expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR);
        done();
      });
    });

    it('should fail with account info', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/getRestaurantInfo?account-id=${accountId}`,
        session: {
          token: 'invalidToken',
          tokenExpiration: moment().add(2, 'minutes')
        }
      });

      consumerApiHandler.handleGetRestaurantInfo(req).then((response) => {
        console.log(response);
        done('TEST FAILED');
      }).catch(err => {
        expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_UNAUTHORIZED);
        done();
      });
    });
  });


  describe('POST /calculateTax', function () {
//TODO this test need to be modified due to(hard coded test data, api parameters also changed)
    it('should calculate tax', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/calculateTax`,
        method: 'POST',
        session: browserSession,
        body: {
          "accountId": accountId,
          "merchantId": merchantId,
          "lineItems": {
            "lineItem": [
              {
                "productId": product.id,
                "variationId": product.variationId,
                "qty": 1,
                "attributeValueIds": []
              }
            ]
          }
        }
      });

      consumerApiHandler.handleCalculateTax(req).then((response) => {
        const resultObject = response.getData();
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        expect(resultObject).to.include.all.keys('subTotal', 'totalTax');
        done();
      }).catch(err => {
        console.log(err);
        done('TEST FAILED');
      });
    });

  });

  it('should not calculate tax', function (done) {
    this.timeout(10000);
    const req = httpMocks.createRequest({
      url: `http://localhost:3003/api-v3/calculateTax`,
      method: 'POST',
      session: browserSession,
      body: {
        "accountId": accountId,
        "merchantId": merchantId,
        "lineItems": {
          "lineItem": [
            {
              "productId": null,
              "variationId": null,
              "qty": 1,
              "attributeValueIds": []
            }
          ]
        }
      }
    });

    try {
      consumerApiHandler.handleCalculateTax(req).then((response) => {
        const resultObject = response.getData();

        done();
      }).catch(err => {
        expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_BAD_REQUEST);
        done();
      });
    } catch (e) {
      done();
    }
  });



  describe('POST /checkUser', function () {

    it('should return isAvailable false', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/checkUser?mail-param=test@l.com`,
        method: 'GET',
        session: browserSession
      });

      consumerApiHandler.handleCheckUser(req).then((response) => {
        const resultObject = response.getData();
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        expect(resultObject.isAvailable).to.eql(false);
        done();
      }).catch(err => {
        console.log(err);
        done('TEST FAILED');
      });
    });

  });


  describe('POST /createOrder', function () {

    it('should not create an order', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/createOrder?account-id=${accountId}`,
        method: 'POST',
        session: browserSession,
        body: {
          "merchantId": merchantId,
          "lineItems": {
            "lineItem": [{
              "productId": "87acaf44-0533-4dcd-ecd1-66823ba19119",
              "qty": 1,
              "attributeValueIds": ["26ffb5b7-21d4-49a6-cf38-a9b29621dab7", "ed44f828-f073-4a06-aef5-a9b9c8c8cfc1"]
            }]
          }
        }
      });
      try {
        consumerApiHandler.handleCreateOrder(req).then((response) => {
          const resultObject = response.getData();
          done();
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR);
          done();
        });
      } catch (e) {
        done();
      }
    });


    it('should  not create a successful order', function (done) {
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/createOrder?account-id=${accountId}`,
        method: 'POST',
        session: browserSession,
        body: {
          "accountId": "10530519",
          "lineItems": {
            "lineItem": [
              {
                "productId": "dce70f33-f9c5-4d12-a5ff-ade17714d2c5",
                "qty": 1,
                "price": 410,
                "variationId": "2a16ba67-8fb8-4b27-b669-ec9450a39c14",
                "attributeSetInstance": {
                  "atttributeSetId": "5374f3c4-21b9-11e8-b9b8-b77ac9944873",
                  "attributes": {
                    "attribute": [
                      {
                        "value": 10,
                        "attributeId": "9d494ee5-6eb1-4eb4-f9e5-50926408734a",
                        "attributeValueId": "019513d1-52b5-4dba-83e9-8f0e3f1dc24e"
                      }
                    ]
                  }
                }
              }
            ]
          },
          "orderType": "PICKUP",
          "subTotal": 410,
          "tax": 82,
          "discount": 0,
          "discountCode": null,
          "deliveryFee": null,
          "tipAmount": null,
          "total": 492,
          "orderReadyTime": 1520395200000,
          "customer": {
            "id": null,
            "firstName": "sa",
            "lastName": "ra",
            "email": "sal@l.com",
            "sendMail": true,
            "contacts": {
              "contact": [
                {
                  "phone": {
                    "areaCode": "354",
                    "exchangeCode": "456",
                    "subscriberNumber": "3646"
                  }
                }
              ]
            },
            "paymentTypes": {
              "credit_card": [
                {
                  "paymentMethod": "credit_card",
                  "type": "VISA",
                  "number": "4111111111111111",
                  "nameOnCard": "sa",
                  "expire": "1219",
                  "secCode": "123"
                }
              ]
            }
          },
          "textMessageConfirmation": false
        }
      });
      try {
        consumerApiHandler.handleCreateOrder(req).then((response) => {

          done();
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR);
          done();
        });
      } catch (e) {
        done();
      }
    });

  });

  //new tests
  describe('POST /send reset password', function () {

    it('should send email for password reset', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/sendResetPasswordEmail`,
        method: 'POST',
        session: browserSession,
        body: {
          "email": "salinda@leap.com",
          "redirectUrl": "https://tst3-orders.cake.net/10530519?resetPassword=true"
        }
      });
      try {
        consumerApiHandler.handleSendingResetPasswordEmail(req).then((response) => {
          expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
          done();
        }).catch(err => {

          done();
        });
      } catch (e) {
        done();
      }
    });


    it('should return 404  for password reset', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/sendResetPasswordEmail`,
        method: 'POST',
        session: browserSession,
        body: {
          "email": "salinda@leap1.com",
          "redirectUrl": "https://tst3-orders.cake.net/10530519?resetPassword=true"
        }
      });
      try {
        consumerApiHandler.handleSendingResetPasswordEmail(req).then((response) => {
          done();
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_NOT_FOUND);
          done();
        });
      } catch (e) {
        done();
      }
    });
  });

  describe('POST /update user', function () {

    it('should update the user', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/updateUser`,
        method: 'PUT',
        session: browserSession,
        body: {
          "id": "5cd9d4c6-98af-4bab-9fdf-7279ead37b6c",
          "idmUserId": "2c202cc0-7a41-4acf-8d73-44e297323110",
          "firstName": "salin",
          "lastName": "ra",
          "email": "salinda@leap.com",
          "sendMail": false,
          "contacts": {
            "contact": [
              {
                "phone": {
                  "areaCode": "354",
                  "exchangeCode": "456",
                  "subscriberNumber": "3646",
                  "id": "0a201cc6-810c-46da-8ba4-b20a410f3771"
                },
                "id": "81683097-1fdf-4669-9aaf-52d9b7b636a9"
              }
            ]
          },
          "paymentTypes": {},
          "eReceiptNotification": true,
          "rewardsNotification": true,
          "dateOfBirthFormat": "yyyy-MM-dd"
        }
      });

      consumerApiHandler.handleUpdateUser(req).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        done();
      }).catch(err => {
        done();
      });

    });

    it('should not update the user', function (done) {
      this.timeout(10000);
      done();
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/updateUser`,
        method: 'PUT',
        session: browserSession,
        body: {
          "id": "invalid_id",
          "idmUserId": "invalid_id",
          "firstName": "salin",
          "lastName": "ra",
          "email": "salinda@leap.com",
          "sendMail": false,
          "contacts": {
            "contact": [
              {
                "phone": {
                  "areaCode": "354",
                  "exchangeCode": "456",
                  "subscriberNumber": "3646",
                  "id": "0a201cc6-810c-46da-8ba4-b20a410f3771"
                },
                "id": "81683097-1fdf-4669-9aaf-52d9b7b636a9"
              }
            ]
          },
          "paymentTypes": {},
          "eReceiptNotification": true,
          "rewardsNotification": true,
          "dateOfBirthFormat": "yyyy-MM-dd"
        }
      });
      try {
        consumerApiHandler.handleUpdateUser(req).then((response) => {

          done();
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_NOT_FOUND);
          done();
        });
      } catch (e) {
        done();
      }
    });


  });


  describe('GET /checkPhone ', function () {

    it('should update the user', function (done) {
      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/checkPhone`,
        method: 'GET',
        session: browserSession
      });
      try {
        consumerApiHandler.handleCheckPhone(req).then((response) => {
          done();
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_BAD_REQUEST);
          done();
        });
      } catch (e) {
        done();
      }
    });

  });

  describe('POST /getWalletDetails ', function () {

    it('should fetch wallet details', function (done) {
      const cardNo = "4610211093859001"; // plat1 cluster specif card
      const pin = "1234";

      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/wallet/getWalletDetails`,
        method: 'POST',
        session: browserSession,
        body: {
          "CARD_NO": cardNo,
          "PIN": pin,
        }
      });
      try {
        consumerApiHandler.handleGetWalletData(cardNo, pin, req).then((response) => {
          expect(response.card);
          done();
        }).catch(err => {
          done('TEST FAILED');
        });
      } catch (e) {
        done();
      }
    });

    it('should not fetch data for invalid card', function (done) {
      const cardNo = "4610211093859001"; //plat1 cluster specif card
      const pin = "8834";

      this.timeout(10000);
      const req = httpMocks.createRequest({
        url: `http://localhost:3003/api-v3/wallet/getWalletDetails`,
        method: 'POST',
        session: browserSession,
        body: {
          "CARD_NO": cardNo,
          "PIN": pin,
        }
      });
      try {
        consumerApiHandler.handleGetWalletData(cardNo, pin, req).then((response) => {
          done('TEST FAILED');
        }).catch(err => {
          expect(err.getStatusCode()).to.eql(API_CONSTANTS.HTTP_INTERNAL_SERVER_ERROR);
          done();
        });
      } catch (e) {
        done();
      }
    });

  });

});




