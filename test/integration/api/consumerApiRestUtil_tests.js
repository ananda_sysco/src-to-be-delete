const chai = require('chai');
const authRestUtil = require('../../../src/server/api/util/restUtil/authRestUtil');
const consumerApiRestUtil = require('../../../src/server/api/util/restUtil/consumerApiRestUtil');
const expect = chai.expect;
const API_CONSTANTS = require('../../../src/server/api/constant/httpConstants');
const intergrationTestHelper = require('../helpers/intergrationTestHelper');

let jwtToken = null;
let browserSession = null;

describe('Test auth rest util', function () {
  before(async function () {
    this.timeout(10000);
    //Use environment specific user for test
    browserSession = await intergrationTestHelper.getBrowserSessionForTest();
    authRestUtil.login('salinda@leap.com', '12345678').then((response) => {
      jwtToken = response.data.id_token;

    }).catch(err => {
    });
  });

  describe('Test resetPassword', function () {
    it('should return OK for resetPassword', function (done) {
      this.timeout(10000);
      consumerApiRestUtil.resetPassword('salinda@leap.com', '12345678', browserSession.token).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_OK);
        done();
      }).catch(err => {
        done();
      });
    });

    it('should return 404 for resetPassword', function (done) {
      this.timeout(10000);
      consumerApiRestUtil.resetPassword('salinda@leap.com1', '12345678', browserSession.token).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_NOT_FOUND);
        done();
      }).catch(err => {
        done();
      });
    });
  });

  describe('Test getPhone', function () {
    it('should return return 404 for getPhone', function (done) {
      this.timeout(10000);
      consumerApiRestUtil.getPhone('2332553456', browserSession.token).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_NOT_FOUND);
        done();
      }).catch(err => {
        done();
      });
    });
  });


  describe('Test getPhone', function () {
    it('should return return 404 for getPhone', function (done) {
      this.timeout(10000);
      let user = {
        "firstName": "salin",
        "lastName": "ra",
        "email": "salinda1@leap.com",
        "sendMail": false,
        "contacts": {
          "contact": [
            {
              "phone": {
                "areaCode": "354",
                "exchangeCode": "456",
                "subscriberNumber": "3646"
              }
            }
          ]
        },
        "paymentTypes": {},
        "eReceiptNotification": true,
        "rewardsNotification": true,
        "dateOfBirthFormat": "yyyy-MM-dd"
      };
      consumerApiRestUtil.createUser(user, browserSession.token).then((response) => {
        expect(response.getStatusCode()).to.eql(API_CONSTANTS.HTTP_NOT_FOUND);
        done();
      }).catch(err => {
        done();
      });
    });
  });

});





