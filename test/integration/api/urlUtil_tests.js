/**
 * Created by Salinda
 */

const {updateEndpointPathVariables, updateEndpointQueryParams} = require("../../../src/server/api/util/urlUtils");
const {RESET_PWD_TOKEN} = require("../../../src/server/api/util/restUtil/endpoint/authEndpoints.js");
const {expect} = require('chai');

describe(' urlUtil unit test', () => {
  it('should return updated endpoint with given parameters.', (done) => {
    const inputmap = new Map();
    inputmap.set('token-param', '1234563568');
    expect(updateEndpointPathVariables(RESET_PWD_TOKEN, inputmap)).to.be.equal('/idm/admin/realms/consumers/users/token/1234563568');
    done()
  });

  it('should return exact endpoint without updating', (done) => {
    const inputmap = new Map();
    expect(updateEndpointPathVariables(RESET_PWD_TOKEN, inputmap)).to.be.equal(RESET_PWD_TOKEN);
    done()
  });

  it('should return exact endpoint without updating', (done) => {
    expect(updateEndpointPathVariables(RESET_PWD_TOKEN)).to.be.equal(RESET_PWD_TOKEN);
    done()
  });

  it('should return updated endpoint with given query params with &.', (done) => {
    const inputmap = new Map();
    inputmap.set('from', '100');
    inputmap.set('to', '300');
    expect(updateEndpointQueryParams(RESET_PWD_TOKEN, inputmap)).to.be.equal(RESET_PWD_TOKEN + '?from=100&to=300');
    done()
  });

  it('should return updated endpoint with given query params without &.', (done) => {
    const inputmap = new Map();
    inputmap.set('from', '100');
    expect(updateEndpointQueryParams(RESET_PWD_TOKEN, inputmap)).to.be.equal(RESET_PWD_TOKEN + '?from=100');
    done()
  });

  it('should return exact endpoint without updating', (done) => {
    const inputmap = new Map();
    expect(updateEndpointQueryParams(RESET_PWD_TOKEN, inputmap)).to.be.equal(RESET_PWD_TOKEN);
    done()
  });

  it('should return exact endpoint without updating', (done) => {
    expect(updateEndpointQueryParams(RESET_PWD_TOKEN, null)).to.be.equal(RESET_PWD_TOKEN);
    done()
  });


});
