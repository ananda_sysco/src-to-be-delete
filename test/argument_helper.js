const httpMocks = require('node-mocks-http');
const moment = require('moment');
const prepare = require('mocha-prepare');
const argv = require('minimist')(process.argv.slice(2));
const authRestUtil = require('../src/server/api/util/restUtil/authRestUtil');

const consumerApiHandler = require('../src/server/api/handler/consumerApiHandler');
const intergrationTestHelper = require('../test/integration/helpers/intergrationTestHelper');
const merchantId = intergrationTestHelper.getAccountIdForTest();
//FEtches account id from the arguments provided.ie:npm run server_test "10213688"


// prepare(function (done) {
//   // get a bearer token and append it to the session
//   authRestUtil.getGuestUserBearerToken().then(
//     (response) => {
//       const session = {};
//       const tokenData = response.data;
//       session['token'] = tokenData.access_token;
//       session['tokenExpiration'] = moment().add(tokenData.expires_in, 'seconds');
//       global.session = session;
//       // get a session related to the merchant
//       const req = httpMocks.createRequest({
//         url: `http://localhost:3003/api-v3/getRestaurantInfo?account-id=${merchantId}`,
//         session: session
//       });
//       consumerApiHandler.handleGetRestaurantInfo(req).then((response) => {
//         const resultObject = response.getData();
//         global.merchantId = merchantId;
//         global.sessionId = resultObject.sessions.session[0].id;
//         done();
//       }).catch(err => {
//         console.log('ERROR OCCURED', err);
//         console.log("\n\n!!!!!!!!!!!!!!!!!!TESTS FAILED TO RUN!!!!!!!!!!!!!!!!\n\n");
//         process.exit(0);
//       });
//
//     },
//     (error) => {
//
//     }
//   );
// }, function (done) {
//   // called after all test completes (regardless of errors)
//   done();
// });
//
// // module.exports = argv;
