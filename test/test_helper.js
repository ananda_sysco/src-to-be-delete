const {JSDOM} = require('jsdom');

const jsdom = new JSDOM('<!doctype html><html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head><body></body></html>', {
  url: "http://localhoost:3003",
  referrer: "http://localhoost:3003",
  userAgent: "Mozilla /5.0 (Compatible MSIE 9.0;Windows NT 6.1;WOW64; Trident/5.0)",
  includeNodeLocations: true
});

const {window} = jsdom;

global.document = window.document;
global.window = window;

// from mocha-jsdom https://github.com/rstacruz/mocha-jsdom/blob/master/index.js#L80
Object.keys(window).forEach((key) => {
  if (!(key in global)) {
    global[key] = window[key];
  }
});
